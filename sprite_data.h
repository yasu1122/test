#ifndef INCLUDED_SPRITE_DATA
#define	INCLUDED_SPRITE_DATA
//******************************************************************************
//
//
//		SPRITE_DATA
//
//
//******************************************************************************

#include "./GameLib/texture.h"

// ラベル定義
enum TEXNO
{	// ゲーム
    PLAYER,
    ENEMY,
    ITEM,
    CLEAK,
    TEXT,
    TEXT2,
    TEXT3,
    BLACK,
    MAP_BACK,
    MAP_TERRAIN,
    BLOCK,
    PORTAL,
    MAP_MINIMAP,
    MESSAGE_WINDOW,
    UI_FONT,
    EFFECT_CLICK,
    VIRUS,
    TITLE,
    TITLE_UI,
    URL_BAR,
    FAVO_BUTTON,
    URL,
    MOUSE_CURSOR,
    REVERSE_BAR,
    DOOR,
    URL_TEXT,
    ROOM_NAME,
    TOUROKUHUKA,
    SAKUJOHUKA,
    SAKUJOMENU,
    SELECT_SPRITE,
    SELECT_ICON,
    SHUTTER_SWITCH,
    BULE_RED_SWITCH,
	MINIMAP_ICON,
	PASS_WINDOW,
    LIFE,
    GAME_OVER,
    GAME_CLEAR,
    MIRROR,
    MONSTER_ROOM_BACK,
};

extern GameLib::LoadTexture loadTexture[];

extern GameLib::SpriteData color_black;






extern GameLib::SpriteData spr_gimmick_switch00;
extern GameLib::SpriteData spr_gimmick_switch01;
extern GameLib::SpriteData spr_gimmick_wall00;

extern GameLib::SpriteData spr_portal;
extern GameLib::SpriteData spr_ui00;
extern GameLib::SpriteData spr_ui01;
extern GameLib::SpriteData spr_ui02;
extern GameLib::SpriteData spr_player00;
extern GameLib::SpriteData spr_star00;

//-------< マウスカーソル >------------------------------------------------------
extern GameLib::SpriteData spr_arrow;
extern GameLib::SpriteData spr_hand00;

extern GameLib::SpriteData spr_wave_effect00;
extern GameLib::SpriteData spr_wave_effect01;
extern GameLib::SpriteData spr_wave_effect02;
extern GameLib::SpriteData spr_wave_effect03;
extern GameLib::SpriteData spr_wave_effect04;

//-------< url >-----------------------------------------------------------------
extern GameLib::SpriteData spr_url00_00;
extern GameLib::SpriteData spr_url00_01;
extern GameLib::SpriteData spr_url00_02;
extern GameLib::SpriteData spr_url00_03;
extern GameLib::SpriteData spr_url00_04;
extern GameLib::SpriteData spr_url00_05;
extern GameLib::SpriteData spr_url00_06;
extern GameLib::SpriteData spr_url00_07;
extern GameLib::SpriteData spr_url00_08;
extern GameLib::SpriteData spr_url00_09;
extern GameLib::SpriteData spr_url00_10;
extern GameLib::SpriteData spr_url00_11;

extern GameLib::SpriteData spr_url01_00;
extern GameLib::SpriteData spr_url01_01;
extern GameLib::SpriteData spr_url01_02;
extern GameLib::SpriteData spr_url01_03;
extern GameLib::SpriteData spr_url01_04;
extern GameLib::SpriteData spr_url01_05;
extern GameLib::SpriteData spr_url01_06;
extern GameLib::SpriteData spr_url01_07;
extern GameLib::SpriteData spr_url01_08;
extern GameLib::SpriteData spr_url01_09;
extern GameLib::SpriteData spr_url01_10;
extern GameLib::SpriteData spr_url01_11;

extern GameLib::SpriteData spr_url02_00;
extern GameLib::SpriteData spr_url02_01;
extern GameLib::SpriteData spr_url02_02;
extern GameLib::SpriteData spr_url02_03;
extern GameLib::SpriteData spr_url02_04;
extern GameLib::SpriteData spr_url02_05;
extern GameLib::SpriteData spr_url02_06;
extern GameLib::SpriteData spr_url02_07;
extern GameLib::SpriteData spr_url02_08;
extern GameLib::SpriteData spr_url02_09;
extern GameLib::SpriteData spr_url02_10;
extern GameLib::SpriteData spr_url02_11;

//--------------------------------------------------------------------------------

extern GameLib::SpriteData spr_hp0;
extern GameLib::SpriteData spr_hp1;
extern GameLib::SpriteData spr_hp2;
extern GameLib::SpriteData spr_hp3;
extern GameLib::SpriteData spr_hp4;
extern GameLib::SpriteData spr_maru;
extern GameLib::SpriteData spr_batsu;
extern GameLib::SpriteData spr_messageWindow;
extern GameLib::SpriteData spr_key00;
extern GameLib::SpriteData spr_key01;
extern GameLib::SpriteData spr_start;
extern GameLib::SpriteData spr_exit;
extern GameLib::SpriteData spr_restart;
extern GameLib::SpriteData spr_title;
extern GameLib::SpriteData spr_effect_move00;
extern GameLib::SpriteData spr_effect_move01;
extern GameLib::SpriteData spr_effect_move02;
extern GameLib::SpriteData spr_effect_move03;
extern GameLib::SpriteData spr_effect_move04;
extern GameLib::SpriteData spr_effect_move05;
extern GameLib::SpriteData spr_effect_move06;
extern GameLib::SpriteData spr_effect_move07;
extern GameLib::SpriteData spr_effect_move08;
extern GameLib::SpriteData spr_effect_move09;
extern GameLib::SpriteData spr_effect_move10;
extern GameLib::SpriteData spr_effect_move11;
extern GameLib::SpriteData spr_effect_move12;
extern GameLib::SpriteData spr_effect_move13;
extern GameLib::SpriteData spr_effect_move14;

//------< プレイヤー >-----------------------------------------------------------
extern GameLib::SpriteData spr_player00;
extern GameLib::SpriteData spr_player01;
extern GameLib::SpriteData spr_player02;
extern GameLib::SpriteData spr_player03;

//-------< 波紋エフェクト >------------------------------------------------------
extern GameLib::SpriteData spr_effect_click00;
extern GameLib::SpriteData spr_effect_click01;
extern GameLib::SpriteData spr_effect_click02;
extern GameLib::SpriteData spr_effect_click03;
extern GameLib::SpriteData spr_effect_click04;
extern GameLib::SpriteData spr_effect_click05;
extern GameLib::SpriteData spr_effect_click06;

//ギミック地形
extern GameLib::SpriteData spr_gimmickterr00;
extern GameLib::SpriteData spr_gimmickterr01;
extern GameLib::SpriteData spr_gimmickterr02;
extern GameLib::SpriteData spr_gimmickterr03;
extern GameLib::SpriteData spr_gimmickterr04;
extern GameLib::SpriteData spr_gimmickterr05;
extern GameLib::SpriteData spr_gimmickterr06;


//----------------------------< ウィルス >------------------------------------------
extern GameLib::SpriteData spr_virus00;
extern GameLib::SpriteData spr_virus01;

//----------------------------< エネミー >------------------------------------------
extern GameLib::SpriteData spr_adsence00;
extern GameLib::SpriteData spr_adsence2929;
extern GameLib::SpriteData spr_adsence9317;
extern GameLib::SpriteData spr_adsence8526;

// タイトル画面
extern GameLib::SpriteData spr_title_sprite;

//----------------------------< ギミック >------------------------------------------------------
// 反転
extern GameLib::SpriteData spr_reverse_bar;
extern GameLib::SpriteData spr_reverse_bar2;

//----------------------------< タイトルUI >----------------------------------------------------
extern GameLib::SpriteData spr_title_ui00;
extern GameLib::SpriteData spr_title_ui01;
extern GameLib::SpriteData spr_game_over_ui00;
extern GameLib::SpriteData spr_game_over_ui01;

// ゲーム画面上部のバー
extern GameLib::SpriteData spr_url_bar;

// お気に入りボタン
extern GameLib::SpriteData spr_favo_button00;
extern GameLib::SpriteData spr_favo_button01;

//----------------------------< 扉 >------------------------------------------------------------

extern GameLib::SpriteData spr_door00;
extern GameLib::SpriteData spr_door01;
extern GameLib::SpriteData spr_goal;

//----------------------------< URLテキスト画像 >-----------------------------------------------

extern GameLib::SpriteData spr_url_text_home;
extern GameLib::SpriteData spr_url_text_01;
extern GameLib::SpriteData spr_url_text_02;
extern GameLib::SpriteData spr_url_text_03;
extern GameLib::SpriteData spr_url_text_04;
extern GameLib::SpriteData spr_url_text_05;
extern GameLib::SpriteData spr_url_text_06;
extern GameLib::SpriteData spr_url_text_07;
extern GameLib::SpriteData spr_url_text_08;
extern GameLib::SpriteData spr_url_text_09;
extern GameLib::SpriteData spr_url_text_10;
extern GameLib::SpriteData spr_url_text_11;

//----------------------------< 部屋名 >--------------------------------------------------------
extern GameLib::SpriteData spr_room_name_home;
extern GameLib::SpriteData spr_room_name_01;
extern GameLib::SpriteData spr_room_name_02;
extern GameLib::SpriteData spr_room_name_03;
extern GameLib::SpriteData spr_room_name_04;
extern GameLib::SpriteData spr_room_name_05;
extern GameLib::SpriteData spr_room_name_06;
extern GameLib::SpriteData spr_room_name_07;
extern GameLib::SpriteData spr_room_name_08;
extern GameLib::SpriteData spr_room_name_09;
extern GameLib::SpriteData spr_room_name_10;
extern GameLib::SpriteData spr_room_name_11;
extern GameLib::SpriteData spr_favo_buttom;

//----------------------------< メニュー >------------------------------------------------------
extern GameLib::SpriteData spr_tourokuhuka00;

//----------------------------< 削除メニュー >--------------------------------------------------
extern GameLib::SpriteData spr_delete_menu;

//----------------------------< 削除メニュー >--------------------------------------------------
extern GameLib::SpriteData spr_sakujohuka;

//----------------------------< セレクト画面 >--------------------------------------------------
extern GameLib::SpriteData spr_select;

//----------------------------< セレクトアイコン >--------------------------------------------------
extern GameLib::SpriteData spr_select_icon_stage01;
extern GameLib::SpriteData spr_select_icon_stage02;
extern GameLib::SpriteData spr_select_icon_stage03;
extern GameLib::SpriteData spr_select_icon_stage04;
extern GameLib::SpriteData spr_select_icon_stage05;
extern GameLib::SpriteData spr_select_icon_tutorial;
extern GameLib::SpriteData spr_select_icon_title;

//----------------------------< 青赤スイッチ >----------------------------------------------------
extern GameLib::SpriteData spr_bule_red_switch00;
extern GameLib::SpriteData spr_bule_red_switch01;

//----------------------------< シャッタースイッチ >--------------------------------------------
extern GameLib::SpriteData spr_shutter_switch00;
extern GameLib::SpriteData spr_shutter_switch01;

//----------------------------< ミニマップ用アイコン >--------------------------------------------
extern GameLib::SpriteData spr_miniMapIcon00;
extern GameLib::SpriteData spr_miniMapIcon01;

//----------------------------< 暗号用ウインドウ >------------------------------------------------
extern GameLib::SpriteData spr_pass_window00;
extern GameLib::SpriteData spr_pass_window01;

//----------------------------< life >------------------------------------------------------------
extern GameLib::SpriteData spr_life;

//----------------------------< ゲームオーバー >--------------------------------------------------
extern GameLib::SpriteData spr_game_over;

//----------------------------< ゲームクリア >----------------------------------------------------
extern GameLib::SpriteData spr_game_clear;
extern GameLib::SpriteData spr_pass00;
extern GameLib::SpriteData spr_pass01;
extern GameLib::SpriteData spr_pass02;
extern GameLib::SpriteData spr_pass03;
extern GameLib::SpriteData spr_pass04;

//----------------------------< 分身壁 >----------------------------------------------------------
extern GameLib::SpriteData spr_mirror_wall;

//----------------------------< モンスタールーム背景 >--------------------------------------------
extern GameLib::SpriteData spr_monster_room;

//******************************************************************************
#endif // !INCLUDED_SPRITE_DATA

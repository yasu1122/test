#ifndef _SCENE_START
#define	_SCENE_START

//******************************************************************************
//
//
//      ゲーム開始演出
//
//
//******************************************************************************

//==============================================================================
//
//      Scene_Startクラス
//
//==============================================================================

class SceneStart : public Scene, public Singleton<SceneStart>
{
private:
    bool isPaused;

public:
    void init();
    void update();
    void draw();
    void uninit();

};

static SceneStart* const SCENE_START = SceneStart::getInstance();

//******************************************************************************

#endif // !_SCENE_START

#ifndef     _BOOK_MARK_H
#define     _BOOK_MARK_H
//******************************************************************************
//
//
//      ブックマーククラス
//
//
//******************************************************************************

//-----------------<定数>-------------------------------------------------------
namespace bookmark{
    // フラグラベル
    enum bookMarkLabel {
        naturalflg  =(0<<0),
        viewFlg     =(1<<0),
        storageFlg  =(1<<1),
        okiniiriFlg =(1<<2),
    };

    // iWorkのラベル
    enum iWork {
        actFlg,
        storage,
        doorLink,
        cnt,
        roomNum,
    };

    // fWorkのラベル
    

    // ipWorkのラベル
    enum ipWork {
        actFlgPointa,
        bookMarkPointa,

    };

    // fpWorkのラベル
    enum fpWork {
        posPointaX,
        posPointaY,
    };

    

}

//==============================================================================
//
//      更新アルゴリズム
//
//==============================================================================

// ブックマーク更新クラス
class BookMark : public LISTAlg
{
public:
    void update(LIST* list);
};

// 更新アルゴリズムの実体
EXTERN BookMark bookMarkAlg;

//==============================================================================
//
//      消去アルゴリズム
//
//==============================================================================

// 消去アルゴリズム
class EraseBookMark :public LISTEraseAlg
{
public:
    void erase(LIST* list);
};

//==============================================================================
//
//      BookMarkManagerクラス
//
//==============================================================================
class BookMarkManager : public LISTManager, public Singleton<BookMarkManager>
{
public:

    BookMarkManager() :hitFlg(false), existingInitFavoFlg(false) {};
    ~BookMarkManager() {};

    void init();            // 初期化
    void update();          // 更新
    void draw();            // 描画

    LIST* add(LISTAlg* bookAlg, JumpData::posandscroll url, VECTOR2 pos , wchar_t* urlText=L" ");            // 生成

    void setHitFlg(bool flg) { hitFlg = flg; }
    bool getHitFlg() { return hitFlg; }
    void setExistingInitFavoFlg(bool flg) { existingInitFavoFlg = flg; }
    bool getExistingInitFavoFlg() { return existingInitFavoFlg; }

private:
    bool hitFlg;
    bool existingInitFavoFlg;

};

//------< インスタンス取得 >-----------------------------------------------------

static BookMarkManager* const pBookMarkManager = BookMarkManager::getInstance();

//******************************************************************************

#endif // _BOOK_MARK_H

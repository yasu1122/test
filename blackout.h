#ifndef     _BLACK_OUT_H
#define     _BLACK_OUT_H
//******************************************************************************
//
//
//      ブラックアウトクラス
//
//
//******************************************************************************

//------------------------------< 定　数 >------------------------------------
#define BLACK_OUT_INIT_POS      (VECTOR2(0.0f,0.0f))    // 暗転登録時の初期座標
#define BLACK_OUT_MAX           (1)                     // ブラックアウトの最大数

//------------------------------< クラス >------------------------------------

class BlackoutManager :public OBJ2DManager, public Singleton<BlackoutManager>
{

public:
	void init();    // 初期化
	void update();  // 更 新
	void draw();    // 描 画

private:

	int getSize() { return BLACK_OUT_MAX; }

};

//------< 関数プロトタイプ宣言 >-------------------------------------------------
void blackout_move00(OBJ2D*);   // 
void blackout_move01(OBJ2D*);   // 
void blackout_move02(OBJ2D*);   // 
void blackout_move03(OBJ2D*);   // 
void blackout_move04(OBJ2D*);   // 
void blackout_move05(OBJ2D*);   // 

//------< インスタンス取得 >-----------------------------------------------------

#define pBlackoutManager	(BlackoutManager::getInstance())

//******************************************************************************

#endif //_BLACK_OUT_H
//******************************************************************************
//
//
//      ゲーム
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;

//--------------------------------
//  初期化処理
//--------------------------------
void Tuto::init()
{
	Scene::init();	    // 基底クラスのinitを呼ぶ
	isPaused = false;   // ポーズフラグの初期化
	STAGE->stageNo = 0;
}

//--------------------------------
//  更新処理
//--------------------------------
void Tuto::update()
{
	using namespace input;

    // ソフトリセット
    if ((STATE(0) & PAD_START) &&  // 0コンのセレクトボタンが押されている状態で
        STAGE->getReturnTitleFlg())       // 0コンのスタートボタンが押された瞬間
    {
        changeScene(SCENE_TITLE);   // タイトルシーンに切り替える
        return;
    }

	switch (state)
	{
	case 0:
		//////// 初期設定 ////////

		timer = 0;
		GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
		texture::load(loadTexture);
		pMessageWinManager->init();

        pBlackoutManager->init();       // ブラックアウト

		STAGE->init();
		music::load(BGM_GAME, L"./Data/Musics/main.wav", 0.3f);
		music::play(BGM_GAME);

		state++;
		//	break;// 意図的なコメントアウト

	case 1:
		//////// 通常時の処理 ////////
		if (TRG(0) & PAD_START)//クリック音
			sound::play(SE_CLICK);


		timer++;

		pMessageWinManager->update();

        pBlackoutManager->update();         // ブラックアウト

		if (isPaused)
			break;
		STAGE->update();

		if (flagFunc::Flag_Check(pGoalManager->getOBJ2D(0)->iWork[goal::iWork::goal], goal::Flg::isGoal))
		{
			//STAGE->stageNo++;
			SCENE_TUTO->changeScene(SCENE_RESULT);
		}

        if (!pPlayerManager->getOBJ2D(0)->mover)
            SCENE_TUTO->changeScene(SCENE_GAMEOVER);


		break;

	
	}
}

//--------------------------------
//  描画処理
//--------------------------------
void Tuto::draw()
{
	// 画面クリア
	GameLib::clear(VECTOR4(0, 0, 0, 1));

	font::textOut(4, "SELECT", VECTOR2(60, 60), VECTOR2(2.0f, 2.0f), VECTOR4(1, 1, 0, 1));

	STAGE->draw();

	pMessageWinManager->draw_not_scroll();


    pBlackoutManager->draw();       // ブラックアウト

}

//--------------------------------
//  終了処理
//--------------------------------
void Tuto::uninit()
{
	// テクスチャの解放
	texture::releaseAll();

	music::stop(BGM_GAME);
	music::unload(BGM_GAME);


}

//******************************************************************************

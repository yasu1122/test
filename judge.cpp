//******************************************************************************
//
//
//      当たり判定
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//-------< 定数 >---------------------------------------------------------------

//-------< 変数 >---------------------------------------------------------------

//==============================================================================
//
//      Judgeクラス
//
//==============================================================================

//------------------------------
//      ヒットチェック
//------------------------------
bool Judge::hitcheck(const Judge* other)
{
    if (left > other->right) return false;
    if (right < other->left) return false;
    if (top > other->bottom) return false;
    if (bottom < other->top) return false;

    return true;
}
//------------------------------
//      レクト生成
//------------------------------
void Judge::set(const VECTOR2& pos, const VECTOR2& size)
{
    left = pos.x - size.x;
    right = pos.x + size.x;
    top = pos.y - size.y;
    bottom = pos.y + size.y;
}

void Judge::setThis(const fRECT& in)
{

	left = in.left;
	right = in.right;
	top = in.top;
	bottom =in.bottom;

}
void Judge::set(const VECTOR2& pos, const fRECT& in)
{

	left = pos.x - in.left;
	right = pos.x + in.right;
	top = pos.y - in.top;
	bottom = pos.y + in.bottom;

}

void Judge::setCenter(const VECTOR2& pos, const fRECT& in)
{

	left = pos.x - in.left;
	right = pos.x + in.right;
	top = pos.y - in.top*2;
	bottom = pos.y;

}
void Judge::setBottom(const VECTOR2& pos, const VECTOR2& size)
{
    left = pos.x - size.x;
    right = pos.x + size.x;
    top = pos.y - size.y;
    bottom = pos.y;
}

//----------------------------
//  JudgeStageManagerクラス
//----------------------------


//------------------------------------------
//      当たり判定処理
//------------------------------------------
void JudgeStageManager::update()
{
    using namespace flagFunc;
    using namespace vecFunc;

    //---------------------------
    //      プレイヤー
    //---------------------------
    OBJ2D* pPl = pPlayerManager->getOBJ2D(0);

    // プレイヤーと扉
    for (auto & pDoor : *pDoorManager)
    {
        if (!pDoor.mover)continue;

        if (isHitPl(&pDoor)) {

            Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::pldoorChange);
            pPl->iWork[player::iWork::linkDoor] = pDoor.iWork[door::iWork::doorLink];
            pPl->fWork[player::fWork::hitDoorPosX] = pDoor.position.x;
            pPl->fWork[player::fWork::hitDoorPosY] = pDoor.position.y;

            break;
        }
		 
    }

	// プレイヤーと扉(ゴール)
	for (auto & pGoal : *pGoalManager)
	{
		if (!pGoal.mover)continue;

        if (isHitPl(&pGoal)) {

            Flg_On(pGoal.iWork[goal::iWork::goal], goal::Flg::isGoal);

            break;
        }
	}

    // プレイヤーと特定の場所へジャンプするドア
    for (auto & pOnePlaceDoor : *pOnePlaceDoorManager)
    {
        if (!pOnePlaceDoor.mover)continue;
        if (isHitPl(&pOnePlaceDoor)) {
            Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::plOnePlaceJump);
            pPl->iWork[player::iWork::linkDoor] = pOnePlaceDoor.iWork[one_place_door::iWork::doorLink];
            pOnePlaceDoor.data = &spr_door01;
            break;
        }
    }

    // プレイヤーと部屋のテキストデータ
    for (auto & pRmText : *pRoomText) {
        if (!pRmText.mover)continue;
        if (isHitPl(&pRmText)) {
            pPl->text = pRmText.text;
            pPl->iWork[player::iWork::roomNum] = pRmText.iWork[roomNum];
			pMessageWinManager->begin()->iWork[messageWindow::iWork::messageNum] = pRmText.iWork[roomNum];
            pUiManager->setRoomNum(pRmText.iWork[roomNum]);
        }
    }

    // プレイヤーとモンスター部屋
    for (auto & pMonster : *pMonsterRoom) {
        if (!pMonster.mover)continue;
        if (isHitPl(&pMonster)) {
            Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::plNotJump);

        }
        else {
            Flg_Off(pPl->iWork[player::iWork::actflg], player::ActFlg::plNotJump);

        }
    }

    // プレイヤーとエネミー
    for (auto & pEnemy : *pEnemyManager)
    {
        if (!pEnemy.mover)continue;
        if (isHitPl(&pEnemy)) {
            Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::plEnmJump);
            pPl->iWork[player::iWork::linkDoor] = pEnemy.iWork[enemy::iWork::EnemyURL];
            break;
        }
    }

	// プレイヤーとエネミー2(弱点判定）
	for (auto & pEnemy : *pEnemyManager)
	{
		if (!pEnemy.mover)continue;

		if (isHitPl2(&pEnemy)) {
			pEnemy.clear();
			sound::play(SE::SE_CLICK);

			break;
		}
	}

	// プレイヤーとウイルス
	for (auto & pVirus : *pVirusManager)
	{
		if (pVirus.size.y <= 0)continue;
		if (!pVirus.mover)continue;
        if (pPlayerManager->getOBJ2D(0)->hp <= 0)return;

		if (isHitPlVirus(&pVirus)) {

			Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::plIsDamage);
            Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::plNowNockBack);
            VECTOR2 vec = UnitVec(pVirus.position, pPl->position);
            pPl->fWork[player::fWork::nockBackVecX] = vec.x;
            pPl->fWork[player::fWork::nockBackVecY] = vec.y;

			break;
		}
		else
		{
			Flg_Off(pPl->iWork[player::iWork::actflg], player::ActFlg::plIsDamage);
		}
	}

	// プレイヤーとギミック(スイッチ）
	for (auto & pGimmick : *pGimmickManager)
	{
		if (pGimmick.mover!=gimmick_move_rb_switch)continue;

		if (!(Flag_Check(pGimmick.iWork[gimmick::iWork::actFlg], gimmick::ActFlg::isHitFlg))&&isHitPl(&pGimmick))
		{

			//切り替え
			Flg_On(pGimmick.iWork[gimmick::iWork::actFlg], gimmick::ActFlg::isHitFlg);

			sound::play(SE::SE_CURSOL);
			if (!pGimmickTerrManager->change_flg)
			{
				pGimmickTerrManager->change_flg = true;
			}
			else 
			{
				pGimmickTerrManager->change_flg = false;
			}
		}

		else if (!isHitPl(&pGimmick))
		{
			Flg_Off(pGimmick.iWork[gimmick::iWork::actFlg], gimmick::ActFlg::isHitFlg);
		}
	}

	// プレイヤーとギミック(押してる間だけスイッチ）
	for (auto & pGimmick : *pGimmickManager)
	{
		if (pGimmick.mover != gimmick_move_down_switch)continue;

		if (isHitPl(&pGimmick))
		{

			//切り替え
			pGimmickManager->shutter_move_flg=true;
			break;
		}

		else
		{
			pGimmickManager->shutter_move_flg = false;
		}
	}

	// プレイヤーとギミック(壁）
	for (auto & pGimmick : *pGimmickManager)
	{
		if (pGimmick.type != gimmick::Type::SHUTTER)continue;

		OBJ2D *pl = pPlayerManager->getOBJ2D(0);

		if (isHitPl4(&pGimmick)&&pGimmick.judgeFlag)
		{

			if (pl->position.y - pl->size.y<pGimmick.position.y - pGimmick.size.y)
			{
				Flg_On(pl->iWork[player::iWork::actflg], player::ActFlg::plisGimmickCell);
				pl->target = &pGimmick;
				break;
			}
			
			Flg_On(pl->iWork[player::iWork::actflg], player::ActFlg::plisGimmickWall);
			pl->target = &pGimmick;
			break;
		}
		else 
		{
			Flg_Off(pl->iWork[player::iWork::actflg], player::ActFlg::plisGimmickCell);
			Flg_Off(pl->iWork[player::iWork::actflg], player::ActFlg::plisGimmickWall);
		}

	}




	//通過ギミックとプレイヤー
	for (auto & pGimmick : *pGimmickManager)

	{

		if (pGimmick.type != gimmick::Type::SHUTTER)continue;


		if (isHitPl5(&pGimmick)) {
			pGimmick.flg = true;
		}
	
	}


	//反転ギミックとプレイヤー
	for (auto & pGimmick : *pGimmickManager)

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (pGimmick.mover != gimmick_move_inverce)continue;


		if (!isHitPl3(&pGimmick)) {
			pGimmick.invert_flg = false;
		}
		else if (isHitPl3(&pGimmick)) {

			sound::play(SE::SE_WOAP);

			pGimmick.invert_flg = true;

			if (pGimmick.position.y - pl->position.y > 0)
			{
				pl->position.y = pGimmick.position.y + 100;
				player::PlMoveCancel2(pl);
			}
			else
			{
				pl->position.y = pGimmick.position.y - 100;
				player::PlMoveCancel2(pl);
			}

		}

	

	
		Judge rect1;
		rect1.set(pGimmick.position, pGimmick.size);

		for (auto& enemy : *pEnemyManager)
		{
			Judge rect2;
			rect2.set(enemy.position, enemy.size);

			if (rect1.hitcheck(&rect2)) {

				enemy.invert_pos = pGimmick.position.y - enemy.position.y;
				enemy.invert_flg = true;
			}

		}
	
	//反転ギミックとアイテム

		for (auto& item : *pItemManager)
		{

			Judge rect2;
			rect2.set(item.position, item.size);


			if (rect1.hitcheck(&rect2)) {

				item.invert_pos = pGimmick.position.y - item.position.y;

				item.invert_flg = true;
			}

		}
	

		for (auto& virus : *pVirusManager)
		{

			Judge rect2;
			rect2.set(virus.position, virus.size);

			if (rect1.hitcheck(&rect2)) {

				virus.invert_pos = pGimmick.position.y - virus.position.y;

				virus.invert_flg = true;
			}


		}

	}


	//反転ギミックとプレイヤー
	for (auto & pGimmick : *pGimmickManager)

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (pGimmick.mover != gimmick_move_inverce2)continue;


		if (!isHitPl3(&pGimmick)) {
			pGimmick.invert_flg2 = false;
		}
		else if (isHitPl3(&pGimmick)) {

			sound::play(SE::SE_WOAP);

			pGimmick.invert_flg2 = true;

			if (pGimmick.position.x - pl->position.x > 0)
			{
				pl->position.x = pGimmick.position.x + 100;
				player::PlMoveCancel2(pl);
			}
			else
			{
				pl->position.x = pGimmick.position.x - 100;
				player::PlMoveCancel2(pl);
			}

		}

		Judge rect1;
		rect1.set(pGimmick.position, pGimmick.size);

		for (auto& enemy : *pEnemyManager)
		{
			Judge rect2;
			rect2.set(enemy.position, enemy.size);

			if (rect1.hitcheck(&rect2)) {

				enemy.invert_pos2 = pGimmick.position.x - enemy.position.x;
				enemy.invert_flg2 = true;
			}

		}
	
	
		for (auto& item : *pItemManager)
		{

			Judge rect2;
			rect2.set(item.position, item.size);


			if (rect1.hitcheck(&rect2)) {

				item.invert_pos2 = pGimmick.position.x - item.position.x;

				item.invert_flg2= true;
			}

		}
	

	

		for (auto& virus : *pVirusManager)
		{

			Judge rect2;
			rect2.set(virus.position, virus.size);

			if (rect1.hitcheck(&rect2)) {

				virus.invert_pos2 = pGimmick.position.x - virus.position.x;

				virus.invert_flg2 = true;
			}


		}

	}




	// プレイヤーとアイテム
	for (auto & item : *pItemManager)
	{
        if (!item.mover)continue;
		if (item.judgeFlag)continue;

		if (isHitPl(&item))
		{
			sound::play(SE::SE_GET);

			OBJ2D *pl = pPlayerManager->getOBJ2D(0);
			OBJ2D *portal = pPortalManager->getOBJ2D(0);

			if (item.param == 0) {
				pl->getURLcnt++;
                pl->hp += item.iWork[item::PlayerOneUp];
                if (pl->hp > PLAYER_HP_MAX)pl->hp = PLAYER_HP_MAX;
			}
			if (pl->getURLcnt > 12)
				pl->getURLcnt = 12;
			
			item.flg = true;

			if(pl->getURLcnt==12)
				pGoalManager->searchSet(goalMove, VECTOR2(portal->position.x, portal->position.y+150));

			break;
		}
		


	}


	// ギミック(分身）
	for (auto & pGimmick : *pGimmickManager)
	{
		if (pGimmick.mover != gimmick_move_clone)continue;

		if (isHitPl(&pGimmick)) {

			OBJ2D* pl = pPlayerManager->getOBJ2D(0);
			pGimmick.clone_flg = true;
			pGimmick.clone_pos = pGimmick.position.x - pl->position.x;

		}
		
		else 
		pGimmick.clone_flg = false;



	}
    //--------------------------
    //      マウスポインタ
    //--------------------------
    // マウスポインタと扉
    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);    

    // マウスポインタと敵
    for (auto & pEnm : *pEnemyManager)
    {
        if (!pEnm.mover)continue;
        if (isHitPoint(&pEnm)) {
            if (TRG(0)&PAD_SELECT && (STATE(0) & PAD_SELECT) != 0) {
                Flg_On(pMouse->iWork[mouse_pointa::iWork::storage], mouse_pointa::clickLabel::storageFlg);
                pMouse->iWork[mouse_pointa::iWork::enmBlockName] = pEnm.iWork[enemy::iWork::EnemyBlockName];
                break;
            }
        }
    }

    // 
    pMousePointa->setHitFlg(false);
    pBookMarkManager->setHitFlg(false);

    // マウスポインタとメニュー
    for (auto & pMenu : *pMenuManager)
    {
        if (!pMenu.mover)continue;

        if (isHitPoint2(&pMenu)) {
                Flg_On(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::pushOn);
                pMousePointa->setHitFlg(true);
        }
        else {
            Flg_Off(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::pushOn);
        }
    }

    // マウスポインタとお気に入り実体
    for (auto & pBookMark : *pBookMarkManager->getList())
    {
        
        if (pDeleteMenuManager->getDeleteMenuViewFlg()) break;
        
        if (!pBookMark.listAlg)continue;

        if (isHitList(&pBookMark)) {
               Flg_On(pBookMark.iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::okiniiriFlg);
               pMousePointa->setHitFlg(true);
        } else {
            if (Flag_Check(pBookMark.iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::okiniiriFlg)) {
               Flg_Off(pBookMark.iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::okiniiriFlg);
            }
        }

    }

    // マウスポインタと削除メニュー
    for (auto & pDeleteMenu : *pDeleteMenuManager)
    {
        if (!pDeleteMenu.mover)continue;

        Flg_Off(pDeleteMenu.iWork[delete_menu::iWork::actFlg], delete_menu::pushOn);

        if (!pDeleteMenuManager->getDeleteMenuViewFlg())break;

        if (isHitPoint2(&pDeleteMenu)) {
          
            Flg_On(pDeleteMenu.iWork[delete_menu::iWork::actFlg], delete_menu::pushOn);

            pMousePointa->setHitFlg(true);

        }
        
    }
}

//----------------------------
//  JudgeTitleManagerクラス
//----------------------------
void JudgeTitleManager::update()
{
    using namespace flagFunc;

    //--------------------------
    //      マウスポインタ
    //--------------------------
    for (auto & pTMenu : *pTitleMenu)
    {
        if (!pTMenu.mover)continue;

        if (isHitPoint2(&pTMenu)) {
            Flg_On(pTMenu.iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn);
        } else {
            Flg_Off(pTMenu.iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn);
        }

    }
	for (auto & pRMenu : *pResultMenu)
	{
		if (!pRMenu.mover)continue;

		if (isHitPoint2(&pRMenu)) {
			Flg_On(pRMenu.iWork[result_menu::iWork::actFlg], result_menu::menuLabel::pushOn);

		} else {
			Flg_Off(pRMenu.iWork[result_menu::iWork::actFlg], title_menu::menuLabel::pushOn);
		}

	}

}

//----------------------------
//  JudgeSceneStartManagerクラス
//----------------------------

void JudgeSceneStartManager::update()
{
    using namespace flagFunc;

    for (auto & pMouse : *pMousePointa)
    {
        if (isHitPl(&pMouse)) {
            Flg_On(pMouse.iWork[mouse_pointa::iWork::actFlg], mouse_pointa::clickLabel::plHitFlg);
        
        } else {
            Flg_Off(pMouse.iWork[mouse_pointa::iWork::actFlg], mouse_pointa::clickLabel::plHitFlg);
        }

    }

}

//----------------------------
//  JudgeSelectManagerクラス
//----------------------------
void JudgeSceneSelectManager::update()
{
    using namespace flagFunc;

    //--------------------------
    //      マウスポインタ
    //--------------------------
    for (auto & pSelect : *pSelectMenu)
    {
        if (!pSelect.mover)continue;

        if (isHitPoint2(&pSelect)) {
            Flg_On(pSelect.iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn);

        }
        else {
            Flg_Off(pSelect.iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn);
        }

    }
    

}

//----------------------------
//  JudgeGameOverManagerクラス
//----------------------------
void JudgeGameOverManager::update()
{
    using namespace flagFunc;

    //--------------------------
    //      マウスポインタ
    //--------------------------
    for (auto & pGameOver : *pGameOverMenu)
    {
        if (!pGameOver.mover)continue;

        if (isHitPoint2(&pGameOver)) {
            Flg_On(pGameOver.iWork[game_over_menu::iWork::actFlg], game_over_menu::menuLabel::pushOn);

        }
        else {
            Flg_Off(pGameOver.iWork[game_over_menu::iWork::actFlg], game_over_menu::menuLabel::pushOn);
        }

    }
}

//------------------------------------------
//      当たり判定処理
//------------------------------------------


//=============================================================================
//
//      JudgeManagerクラス
//
//=============================================================================
//------------------------------------------
//      当たり判定
//------------------------------------------

//---------------<OBJ2D　当たり判定>------------------

// プレイヤー
bool JudgeManager::isHitPl(OBJ2D* obj)
{
    Judge rect1;
    rect1.set(obj->position, obj->size);
    for (auto& pl : *pPlayerManager)
    {
        if (!pl.mover)continue;

        Judge rect2;
        rect2.set(pl.position, pl.size);
      
		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}

	for (auto& cl : *pGimmickManager)
	{
		if (cl.mover!=gimmick_move_clone_child)continue;

		Judge rect2;
		rect2.set(cl.position, cl.size);

		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}
    return false;
}

// プレイヤー
bool JudgeManager::isHitPlVirus(OBJ2D* obj)
{
	Judge rect1;
	rect1.set(obj->position, obj->size);
	for (auto& pl : *pPlayerManager)
	{
		if (!pl.mover)continue;

		Judge rect2;
		rect2.set(pl.position, pl.size);

		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}

	for (auto& cl : *pGimmickManager)
	{
		if (cl.mover != gimmick_move_clone_child)continue;

		Judge rect2;
		rect2.set(cl.position, cl.size);

		if (rect1.hitcheck(&rect2)) {

			cl.clear();
			return false;

		}
	}
	return false;
}

//プレイヤー（敵の弱点(hitRect)と）
bool JudgeManager::isHitPl2(OBJ2D* obj)
{
	Judge rect1;
	rect1.set(VECTOR2(obj->fWork[enemy::weekPositionX], obj->fWork[enemy::weekPositionY]),VECTOR2(obj->fWork[enemy::weekSizeX], obj->fWork[enemy::weekSizeY]));
	
	for (auto& pl : *pPlayerManager)
	{
		if (!pl.mover)continue;

		Judge rect2;
		rect2.set(pl.position, pl.size);
		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}

	for (auto& cl : *pGimmickManager)
	{
		if (cl.mover != gimmick_move_clone_child)continue;

		Judge rect2;
		rect2.set(cl.position, cl.size);

		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}
	return false;
}

//プレイヤー（敵の弱点(hitRect)と）
bool JudgeManager::isHitPl3(OBJ2D* obj)
{
	Judge rect1;
	rect1.set(obj->position, obj->hitRect);
	for (auto& pl : *pPlayerManager)
	{
		if (!pl.mover)continue;

		Judge rect2;
		rect2.set(pl.position, pl.size);
		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}

	for (auto& cl : *pGimmickManager)
	{
		if (cl.mover != gimmick_move_clone_child)continue;

		Judge rect2;
		rect2.set(cl.position, cl.size);

		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}
	return false;
}
bool JudgeManager::isHitPl4(OBJ2D* obj)
{
	Judge rect1;
	rect1.setBottom(obj->position,obj->size);
	for (auto& pl : *pPlayerManager)
	{
		if (!pl.mover)continue;

		Judge rect2;
		rect2.set(pl.position, pl.size);
		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}

	for (auto& cl : *pGimmickManager)
	{
		if (cl.mover != gimmick_move_clone_child)continue;

		Judge rect2;
		rect2.set(cl.position, cl.size);

		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}
	return false;
}
bool JudgeManager::isHitPl5(OBJ2D* obj)
{
	Judge rect1;
	rect1.setCenter(obj->position, obj->hitRect);
	for (auto& pl : *pPlayerManager)
	{
		if (!pl.mover)continue;

		Judge rect2;
		rect2.set(pl.position, pl.size);
		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}

	for (auto& cl : *pGimmickManager)
	{
		if (cl.mover != gimmick_move_clone_child)continue;

		Judge rect2;
		rect2.set(cl.position, cl.size);

		if (rect1.hitcheck(&rect2)) {

			return true;
		}
	}
	return false;
}


// マウスポインタ

// ゲーム座標とスクリーン座標
bool JudgeManager::isHitPoint(OBJ2D* obj)
{
    Judge rect1;
    rect1.set(obj->position, obj->size);
    for (auto& p : *pMousePointa)
    {
        if (!p.mover)continue;

        Judge rect2;
        rect2.set(p.position + pBGManager->getScrollPos(), p.size);
        if (rect1.hitcheck(&rect2)) {

            return true;
        }
    }
    return false;
}

// スクリーン座標とスクリーン座標
bool JudgeManager::isHitPoint2(OBJ2D* obj)
{
    Judge rect1;
    rect1.set(obj->position, obj->size);
    for (auto& p : *pMousePointa)
    {
        if (!p.mover)continue;

        Judge rect2;
        rect2.set(p.position, VECTOR2(0.0f,0.0f));
        if (rect1.hitcheck(&rect2)) {

            return true;
        }
    }
    return false;
}

//---------------------------------------------------


//---------------<LIST　当たり判定>------------------
// マウスポインタ
// スクリーン座標とスクリーン座標
bool JudgeManager::isHitList(LIST* list)
{
    Judge rect1;
    rect1.set(list->position, list->size);
    for (auto& p : *pMousePointa)
    {
        if (!p.mover)continue;

        Judge rect2;
        rect2.set(p.position, VECTOR2(0.0f, 0.0f));
        if (rect1.hitcheck(&rect2)) {

            return true;
        }
    }
    return false;
}

//-----------------------------------------------------
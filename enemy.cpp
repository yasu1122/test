#include"all.h"

#include <string>
#include <bitset>

using namespace GameLib;

VECTOR2 getVec2Player(VECTOR2 src);

//--------------------------------------------
//
//  消去アルゴリズム
//
//--------------------------------------------

bool enemy_erase(OBJ2D* obj)
{
    if (!obj->mover) {
        obj->clear();
        return true;
    }
    return false;
}
//--------------------------------------------


#define ENEMY_SIZEX			122.f
#define ENEMY_SIZEY			106.f
#define ENEMY_WEEK_ADJUSTX	105.f
#define ENEMY_WEEK_ADJUSTY	125.f
#define ENEMY_WEEK_SIZEX    30.f
#define ENEMY_WEEK_SIZEY    30.f
#define ENEMY_MOVE_CNT		80.f

//下からにゅっと
void enemy_adsence_move00(OBJ2D*obj)
{
	float DistanceToPlayer = fabs(obj->position.x - pPlayerManager->getOBJ2D(0)->position.x);
	float &weekPositionX = obj->fWork[enemy::weekPositionX];
	float &weekPositionY = obj->fWork[enemy::weekPositionY];
	float &weekSizeX = obj->fWork[enemy::weekSizeX];
	float &weekSizeY = obj->fWork[enemy::weekSizeY];

	weekPositionX = obj->position.x + ENEMY_WEEK_ADJUSTX * obj->scale.x;
	weekPositionY = obj->position.y - ENEMY_WEEK_ADJUSTY * obj->scale.y;

	switch (obj->state) {



	case 0:

		obj->param = enemy::EnemyType::AD;
		obj->data = &spr_adsence00;
		obj->size = VECTOR2(ENEMY_SIZEX, ENEMY_SIZEY);
		obj->eraser = enemy_erase;
		obj->state++;
		weekSizeX = ENEMY_WEEK_SIZEX;
		weekSizeY = ENEMY_WEEK_SIZEY;
		//break;

	case 1:
		


		if (DistanceToPlayer < ENMEMY00_MOVE_DISTANCE)
		{
			obj->speed = getVec2Player(obj->position)*ENMEMY00_MOVE_SPEED;

			obj->state++;
		}

		break;

	case 2:



		obj->position.y += obj->speed.y / 2;

		if (++obj->timer > ENEMY_MOVE_CNT)
			obj->state++;

		break;

	case 3:

		break;




	}

	//反転
	obj->OBJ2D::PosInverce(obj);

}


//静止
void enemy_adsence_move01(OBJ2D*obj)
{

	float &weekPositionX = obj->fWork[enemy::weekPositionX];
	float &weekPositionY = obj->fWork[enemy::weekPositionY];
	float &weekSizeX = obj->fWork[enemy::weekSizeX];
	float &weekSizeY = obj->fWork[enemy::weekSizeY];

	weekPositionX = obj->position.x + ENEMY_WEEK_ADJUSTX * obj->scale.x;
	weekPositionY = obj->position.y - ENEMY_WEEK_ADJUSTY * obj->scale.y;

	switch (obj->state) {



	case 0:

		obj->param = enemy::EnemyType::AD;
		obj->data = &spr_adsence00;
		obj->size = VECTOR2(ENEMY_SIZEX, ENEMY_SIZEY);
		obj->eraser = enemy_erase;
		obj->state++;
		weekSizeX = ENEMY_WEEK_SIZEX;
		weekSizeY = ENEMY_WEEK_SIZEY;
		//break;

	case 1:



		break;





	}

	//反転
	obj->OBJ2D::PosInverce(obj);

}

//横移動
void enemy_adsence_move02(OBJ2D*obj)
{
	float DistanceToPlayer = fabs(obj->position.x - pPlayerManager->getOBJ2D(0)->position.x);

	float &weekPositionX = obj->fWork[enemy::weekPositionX];
	float &weekPositionY = obj->fWork[enemy::weekPositionY];
	float &weekSizeX = obj->fWork[enemy::weekSizeX];
	float &weekSizeY = obj->fWork[enemy::weekSizeY];

	weekPositionX = obj->position.x + ENEMY_WEEK_ADJUSTX * obj->scale.x;
	weekPositionY = obj->position.y - ENEMY_WEEK_ADJUSTY * obj->scale.y;

	switch (obj->state) {



	case 0:

		obj->param = enemy::EnemyType::AD;
		obj->data = &spr_adsence00;
		obj->size = VECTOR2(ENEMY_SIZEX, ENEMY_SIZEY);
		obj->eraser = enemy_erase;
		obj->state++;
		weekSizeX = ENEMY_WEEK_SIZEX;
		weekSizeY = ENEMY_WEEK_SIZEY;
		//break;

	case 1:



		if (DistanceToPlayer < ENMEMY00_MOVE_DISTANCE)
		{
			obj->speed = getVec2Player(obj->position)*ENMEMY00_MOVE_SPEED;

			obj->state++;
		}

		break;

	case 2:



		obj->position.x += obj->speed.x / 2;

		if (++obj->timer > ENEMY_MOVE_CNT)
			obj->state++;

		break;

	case 3:

		break;




	}

	//反転
	obj->OBJ2D::PosInverce(obj);

}


// リンク先ラベル
#define LINK_ADSENCE00      (0)


//ムーブ関数とデータ
EnemyData enemyData00[] = 
{
	{ enemy_adsence_move01 ,&spr_adsence00 , LINK_ADSENCE00 },
	{ enemy_adsence_move00 ,&spr_adsence00 , LINK_ADSENCE00 },
	{ enemy_adsence_move02 ,&spr_adsence00 , LINK_ADSENCE00 },
};

EnemyData* enemyDataArray[] =
{
    enemyData00,
	enemyData00,
	enemyData00,
	enemyData00,
    enemyData00,
    enemyData00,
    nullptr,
};

void EnemyManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化
    doorURL.resize(EnemyManager::getEnmJumpSize());

    for (auto & pEnmJumpPoint : doorURL)
    {
        linkClear(&pEnmJumpPoint);
    }

	STAGE_SCRIPT*pScript = STAGE->getStageScript();
	//URL_DATA*pURL = STAGE->getURL();

	// 敵データの読み込み
	char** mapEnemy = new char*[pBGManager->CHIP_NUM_Y];
    char** mapEnmJump = new char*[pBGManager->CHIP_NUM_Y];

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		
        mapEnemy[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapEnemy[i], sizeof(char)*pBGManager->CHIP_NUM_X);

        mapEnmJump[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapEnmJump[i], sizeof(char)*pBGManager->CHIP_NUM_X);

	}

	if (!pBGManager->loadMapData(pScript->fileNameEnemy, mapEnemy))
	{
		assert(!"敵データ読み込み失敗");
	}

    if (!pBGManager->loadMapData(pScript->fileNameEnmJump, mapEnmJump))
    {
        assert(!"敵ジャンプ先読み込み失敗");
    }

	// 敵配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int enemyIndex = mapEnemy[y][x];
            if (enemyIndex >= 0) {
                EnemyData enmData = enemyDataArray[STAGE->getStageNo()][enemyIndex];

                OBJ2D* pEnemy = searchSet(enmData.mover, VECTOR2(
                    static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
                    static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
                );

                pEnemy->iWork[enemy::iWork::EnemyURL] = enmData.linkNo;//触れたらどのページに飛ぶか
                pEnemy->data = enmData.data;
            }
            const int enmJumpIndex = mapEnmJump[y][x];

            if (enmJumpIndex < 0)continue;
            pEnemyManager->linkSearchSet(enmJumpIndex,VECTOR2(
                static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
                static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
            );

		}
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapEnemy[i];
        delete[] mapEnmJump[i];
	}
	delete[] mapEnemy;
    delete[] mapEnmJump;


}

void EnemyManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの更新
}

void EnemyManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの描画
}


VECTOR2 getVec2Player(VECTOR2 src)
{
	using namespace GameLib::system;

	OBJ2D* player = pPlayerManager->getOBJ2D(0);

	VECTOR2	dst = (player->mover) ? player->position : VECTOR2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	dst -= src;
	dst /= sqrtf(dst.x*dst.x + dst.y*dst.y);
	return dst;
}

// ドアのリンクセット
void EnemyManager::linkSearchSet(int link, VECTOR2 linkPos)
{

    if (doorURL[link].linkCnt >= DOOR_LINK_ENEMY_MAX)return;

    int linkCnt = doorURL[link].linkCnt;

    doorURL[link].linkPos[linkCnt] = linkPos;
    doorURL[link].scrollPos[linkCnt] = setScrollPos(linkPos);
    doorURL[link].respawnPos[linkCnt] = setRespawnPos(linkPos);

    doorURL[link].linkCnt++;

};

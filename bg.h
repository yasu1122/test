#ifndef INCLUDED_BG
#define INCLUDED_BG

//******************************************************************************
//
//
//      bg.h
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "./GameLib/template.h"

//==============================================================================
//
//      BGクラス
//
//==============================================================================


class BG : public Singleton<BG>
{
public:
	//------< 定数 >------------------------------------------------------------
	static const int CHIP_SIZE = 32;                          // %演算子を使用するためint型を使用する
	static const int CHIP_LINE_NUM = 4;                       // マップチップが４行
	static const int CHIP_NUM_PER_LINE = 8;                   // マップチップの１列が８個

	static const int MINI_MAP_POS_X = 150;					// ミニマップを0, 0の位置からどれだけずらした位置に表示するか
	static const int MINI_MAP_POS_Y = 240;

	static const int CHIP_MINI_SCALING = 8;
	static const int CHIP_SIZE_MINI = 4;

	static constexpr float SCROLL_MERGIN_X = 0.f;         // この数値より画面端に近づいたらスクロールする（横）
	static constexpr float SCROLL_MERGIN_Y = 0.f;         // この数値より画面端に近づいたらスクロールする（縦）
	static constexpr float ADJUST_Y = 0.125f;               // あたり判定での位置調整用（縦）
	static constexpr float ADJUST_X = 0.0125f;              // あたり判定での位置調整用（横）
	static constexpr float AREA_LIMIT = 256.0f;             // これ以上エリアの外に出たらOBJ2Dが消滅する

public:
    //------<マップ描画用>------------------------------------------------------
    int CHIP_NUM_X;                                         // マップの横方向のチップ数
    int CHIP_NUM_Y;                                         // マップの縦方向のチップ数

    float WIDTH;                                            // マップの幅（ドット）
    float HEIGHT;                                           // マップの高さ（ドット）

    // 地形(Terrain)の属性
	enum TR_ATTR
	{
        TR_NONE = -1,   // -1:何もなし
        ALL_BLOCK,          //  0:四方ブロック
        ALL_BLOCK_RED,      //  1:四方ブロック(赤)
        ALL_BLOCK_BLUE,     //  2:四方ブロック(青)
        ALL_BLOCK_KEY,      //  3: カギ
        ALL_BLOCK_KEY2,     //  4: カギ２
        ALL_BLOCK_ELASE,    //  5: // TODO:01よくわからんので作成者書き直すこと。
        ALL_BLOCK_PASS,     //  6: // TODO:02よくわからんので作成者書き直すこと。
        QUARTER_BLOCK_LEFT = 8, //  7: 4分の1で判定するブロック(左)
        QUARTER_BLOCK_RIGHT,//  8: 4分の1で判定するブロック(右)
        HALF_BLOCK_LOWER,   //  9: 4分の1で判定するブロック(下)

	};

	// 背景（Back）の属性
	enum BG_ATTR
	{
		BG_NONE = -1,   // -1:何もなし
		NORMAL,         //  0:普通（特に何もなし）
		WATER,          //  1:水中（抵抗を受ける）
	};

	// 探索用ラベル定義
	enum VISIT_SCAN
	{
		VISIT_NONE,			// 未探索
		VISITED,		// 探索済
	};

	enum MINI_MAP_ATTR
	{
		MAP_NONE,
		WALL,
		DOOR,
		BLUE_WALL,
		RED_WALL,
		SHUTTER,
		ROCK,				// 条件付き解放ブロック類
		INVERSE,
	};

	enum GIMICK
	{
		IS_SHUTTER = 2,
		IS_REVERSE_WALL = 3,
	};
	enum GIMICKOT
	{
		IS_RED_WALL = 1,
		IS_BLUE_WALL = 2,
		IS_KEY0 = 3,
		IS_KEY1 = 4,
		IS_KEY2 = 5,
		IS_KEY3 = 6,
	};
	enum DOOR
	{
		IS_DOOR = -1,
	};
	enum WALL
	{
		IS_WALL = -1,
	};
	

private:

	// 地形チップテクスチャの各部分の属性を定義する
	const TR_ATTR terrainAttr[CHIP_LINE_NUM][CHIP_NUM_PER_LINE] = {
		{ TR_ATTR::ALL_BLOCK,   TR_ATTR::ALL_BLOCK_RED,   TR_ATTR::ALL_BLOCK_BLUE,   TR_ATTR::ALL_BLOCK_KEY,   TR_ATTR::ALL_BLOCK_KEY2, TR_ATTR::ALL_BLOCK_ELASE, TR_ATTR::ALL_BLOCK_PASS, TR_ATTR::ALL_BLOCK, },
		{ TR_ATTR::QUARTER_BLOCK_LEFT,		TR_ATTR::QUARTER_BLOCK_RIGHT,	  TR_ATTR::HALF_BLOCK_LOWER,		TR_ATTR::QUARTER_BLOCK_LEFT,	  TR_ATTR::QUARTER_BLOCK_RIGHT,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE, },
		{ TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE, },
		{ TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,     TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE,   TR_ATTR::TR_NONE, },
	};

	// 背景チップテクスチャの各部分の属性を定義する
	const BG_ATTR bgAttr[CHIP_LINE_NUM][CHIP_NUM_PER_LINE] = {
		{ BG_ATTR::NORMAL,      BG_ATTR::NORMAL,      BG_ATTR::NORMAL,      BG_ATTR::NORMAL,      BG_ATTR::NORMAL,    BG_ATTR::NORMAL,    BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE, },
		{ BG_ATTR::WATER,       BG_ATTR::WATER,       BG_ATTR::WATER,       BG_ATTR::WATER,       BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE, },
		{ BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE, },
		{ BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,     BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE,   BG_ATTR::BG_NONE, },
	};

	//------< 変数 >------------------------------------------------------------
	VECTOR2 scroll;                                         // 現在表示されている左上の地点の座標

    char** back;                                            // 背景データ
    char** terr;                                            // 地形データ
	char** terr2;                                           // 地形データ2
	char** miniMap;											// ミニマップデータ
	char** plVisit;											// 探索判別用データ
	char** doorMap;											// 
	char** oneDoorMap;				
	char** gimickMap;
	char** gimickMap1;										// 
	char** gimickMap2;										// 
	bool erase_flg;											//消滅
	bool erase_flg_key;											//消滅
	bool erase_flg_key2;											//消滅

public:
	BG();
	~BG();

	// 初期化
	void init();

	// 更新
	void update();

	void DataConvertMinimap();								// 地形データをミニマップ用データに変換

	void ScanPlVisit();										// プレイヤーの現在位置から上下左右に再帰的に走査する関数
	bool ScanPlNow(int, int);

	// 描画
	void drawBack();                                        // 背景描画
	void drawTerrain();                                     // 地形描画
	void drawMiniMap();										// マップ描画

															// スクロール位置取得
	float getScrollX() { return scroll.x; }
	float getScrollY() { return scroll.y; }
	const VECTOR2& getScrollPos() { return scroll; }

    // マップ全体のサイズを取得
    bool allMapCount(const char* file_name);

	// マップデータのロード
	bool BG::loadMapData(const char* file_name, char** map);

    // scrollセット
    void setScrollPos(float x, float y);


	// あたり判定

	// 下方向
	bool isFloor(float, float, float);      // 床にめり込んでいるか
	void mapHoseiDown(OBJ2D*);              // 下方向補正処理
    bool isFloor2(float, float, float);      // 床にめり込んでいるか
    void mapHoseiDown2(OBJ2D*);              // 下方向補正処理
											// 上方向
	bool isCeiling(float, float, float);    // 天井にあたっているか
	void mapHoseiUp(OBJ2D*);                // 上方向補正処理

	// 横方向
	bool isWall(float, float, float);       // 横方向に壁にめり込んでいるか
	void mapHoseiRight(OBJ2D*);             // 右方向補正処理
	void mapHoseiLeft(OBJ2D*);              // 左方向補正処理

    bool isHitWall(float, float);
    bool isWall2(float, float, float);       // 横方向に壁にめり込んでいるか
    void mapHoseiRight2(OBJ2D*);             // 右方向補正処理
    void mapHoseiLeft2(OBJ2D*);              // 左方向補正処理

											// 抵抗
	
	TR_ATTR getTerrainAttr(float, float);
	TR_ATTR getTerrainAttr2(float, float);

	// char(*getTerrain())[BG::CHIP_NUM_X]{ return terrain; }

private:
	// クリア
	void clear();

    // ミニマップ用に地形チェック
    bool terrCheck(int);

    // 地形データ、背景データ削除
    void mapDelete();									//	newしたものはdelete

	// BG、Terrain共通の描画関数
	void draw(int, char**);  // 描画処理（ソースコード整理したバージョン）

	// ミニマップ用の描画関数
	void drawMini(int, char**);

	// マップスクロール用
	void scrollMap();// マップスクロールの値更新

	bool isHitDown(float, float);
    bool isHitDown2(float, float);

	bool isHitAll(float, float);

    bool isLowerHalf(float); 
    bool isRightQuarter(float);
    bool isLeftQuarter(float);
    
    
    int getData(char**, float, float);

};

//------< インスタンス取得 >-----------------------------------------------------

static BG* const pBGManager = BG::getInstance();

//******************************************************************************

#endif // ! INCLUDED_BG

#ifndef INCLUDED_ALL
#define INCLUDED_ALL

//******************************************************************************
//
//
//      all.h
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "./GameLib/game_lib.h"
#include "./GameLib/template.h"
#include "./GameLib/input_manager.h"
#include "./GameLib/obj2d_data.h"

#include "work.h"
#include "my_util.h"
#include "text.h"
#include "sprite_data.h"
#include "stage_data.h"
#include "url_data.h"
#include "common.h"
#include "actor.h"
#include "obj2d.h"
#include "messageWindow.h"
#include "message_tbl.h"
#include "list_class.h"
#include "room_text.h"
#include "monster_room.h"
#include "undoRedo.h"
#include "bookmark.h"
#include "bookMarkObj.h"
#include "link_set.h"
#include "item.h"
#include "judge.h"
#include "bg.h"
#include "player.h"
#include "mouse_pointa.h"
#include "door.h"
#include "go_to_one_place_door.h"
#include "goal.h"
#include "menu.h"
#include "titleMenu.h"
#include "selectmenu.h"
#include "deleteMenu.h"
#include "resultMenu.h"
#include "gameoverMenu.h"
#include "enemy.h"
#include "enemy_virus.h"
#include "portal.h"
#include "ui.h"
#include "gimmick.h"
#include "gimmick_terr.h"
#include "stage.h"
#include "effect.h"
#include "blackout.h"
#include "scene.h"
#include "scene_start.h"
#include "scene_tutoreal.h"
#include "scene_game.h"
#include "scene_title.h"
#include "scene_select.h"
#include "scene_result.h"
#include "scene_gameover.h"



//******************************************************************************

#endif // !INCLUDED_ALL
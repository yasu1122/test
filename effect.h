#pragma once

// ラベル定義
#define EFFECT_MAX		10

// EffectManagerクラス
class EffectManager : public OBJ2DManager, public Singleton<EffectManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return EFFECT_MAX; }
};

#define pEffectManager	(EffectManager::getInstance())

// プロトタイプ宣言
void effect_move_wave(OBJ2D*);
void effect_move_wave01(OBJ2D*);
void effect_move_favo_button(OBJ2D*);
void effect_move_player_spawn(OBJ2D*);
void effect_move_mark(OBJ2D*obj);

#pragma once



void ItemMoveURL00(OBJ2D*obj);
void ItemMoveURL01(OBJ2D*obj);
void ItemMoveURL02(OBJ2D*obj);
void ItemMoveURL03(OBJ2D*obj);
void ItemMoveURL04(OBJ2D*obj);
void ItemMoveURL05(OBJ2D*obj);
void ItemMoveURL06(OBJ2D*obj);
void ItemMoveURL07(OBJ2D*obj);
void ItemMoveURL08(OBJ2D*obj);
void ItemMoveURL09(OBJ2D*obj);
void ItemMoveURL10(OBJ2D*obj);
void ItemMoveURL11(OBJ2D*obj);
void ItemMoveKey2(OBJ2D*obj);
void ItemMoveKey(OBJ2D* obj);
void ItemMoveKey2_stop(OBJ2D* obj);

namespace item
{

	enum ActFlg {
		EraseFlg = (1 << 0),
	};

	// iWorkのラベル
	enum iWork {
		PlayerOneUp,        // プレイヤーシールド設定用
		isURL,
	};

	// fWorkのラベル
	enum fWork {
		MoveAngle
	};

	enum BlockName
	{
		http_xvideos_com,
		http_pornhub_com,
	};

}

class ItemManager : public OBJ2DManager, public Singleton<ItemManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return 100; }

};

#define pItemManager	(ItemManager::getInstance())




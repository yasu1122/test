//******************************************************************************
//
//
//      メニュー表示
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//------<変　数>----------------------------------------------------------------
//MOVER titleMenuFuncArray[] =
//{
//    gameStart,
//    exitGame,
//    nullptr,
//};

//--------------------------------
//  menu関数
//--------------------------------

// ゲーム開始
void gameStart(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(128.0f, 70.0f);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
		//obj->position = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2);
		obj->scale = VECTOR2(0.5f, 0.5f);
		obj->data =&spr_title_ui00;
        obj->state++;
        // break;

    case 1:
        pTitleMenu->setStartFlg(Flag_Check(obj->iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn));
        

        if (Flag_Check(obj->iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(0.5f, 0.5f);
        }
        else
        {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(0.45f, 0.45f);
        }

        break;
    }
}

// ゲーム終了
void exitGame(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
		obj->size = VECTOR2(128.0f, 70.0f);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        //obj->position = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT/1.3);
        obj->scale = VECTOR2(0.5f, 0.5f);
        obj->data = &spr_title_ui01;
        obj->state++;
        // break;

    case 1:
       pTitleMenu->setExitFlg(Flag_Check(obj->iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn));
	   
       if (Flag_Check(obj->iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn))
       {
           obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
           obj->scale = VECTOR2(0.5f, 0.5f);
       }
       else {
           obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
           obj->scale = VECTOR2(0.45f, 0.45f);
       }

        break;
    }
}

// 初期化
void TitleMenu::init()
{
    OBJ2DManager::init();       // OBJ2DManagerの初期化

  
        searchSet(gameStart, VECTOR2(700.0f, 900.0f));

		searchSet(exitGame, VECTOR2(1200.0f, 900.0f));


}

// 更新処理
void TitleMenu::update() 
{


    OBJ2DManager::update();   // OBJ2DManagerの更新
	  

}

// 描画処理
void TitleMenu::draw()
{

    OBJ2DManager::draw_not_scroll();   // OBJ2DManagerの描画

}


//******************************************************************************


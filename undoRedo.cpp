//******************************************************************************
//
//
//      UndoRedoManagerクラス
//
//
//******************************************************************************

//------< インクルード >--------------------------------------------------------

#include "all.h"

using namespace GameLib;
using namespace input;

//==============================================================================
//
//      UndoRedoクラス
//
//==============================================================================
// static変数実態宣言
std::list<std::list<LIST>> UndoRedo::undoList;
std::list<std::list<LIST>> UndoRedo::redoList;
std::list<JumpData::posandscroll> UndoRedo::undoPlPos;
std::list<JumpData::posandscroll> UndoRedo::redoPlPos;
std::list<std::vector<OBJ2D>> UndoRedo::undoEnemyList;
std::list<std::vector<OBJ2D>> UndoRedo::redoEnemyList;
std::list<std::vector<OBJ2D>> UndoRedo::undoVirusList;
std::list<std::vector<OBJ2D>> UndoRedo::redoVirusList;
std::list<std::vector<OBJ2D>> UndoRedo::undoGimmickList;
std::list<std::vector<OBJ2D>> UndoRedo::redoGimmickList;
std::list<std::vector<OBJ2D>> UndoRedo::undoPortalList;
std::list<std::vector<OBJ2D>> UndoRedo::redoPortalList;
std::list<std::vector<OBJ2D>> UndoRedo::undoDoorList;
std::list<std::vector<OBJ2D>> UndoRedo::redoDoorList;
std::list<std::vector<OBJ2D>> UndoRedo::undoItemList;
std::list<std::vector<OBJ2D>> UndoRedo::redoItemList;
std::list<std::vector<OBJ2D>> UndoRedo::undoBkObjList;
std::list<std::vector<OBJ2D>> UndoRedo::redoBkObjList;

//--------------------------------
//  初期化
//--------------------------------
//void UndoRedo::init()
//{
//
//    undoList.clear();
//    redoList.clear();
//
//    undoPlPos.clear();
//    redoPlPos.clear();
//    
//    undoEnemyList.clear();
//    redoEnemyList.clear();
//
//    undoGimmickList.clear();
//    redoGimmickList.clear();
//
//}


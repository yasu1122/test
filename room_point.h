#ifndef     _ROOM_POINT_H
#define     _ROOM_POINT_H
//******************************************************************************
//
//
//      RoomPointManagerクラス
//
//
//******************************************************************************

//----------------------< 定数 >-------------------------
#define ROOM_POINT_MAX      (20)


class RoomPointManager : public OBJ2DManager, public Singleton<RoomPointManager>
{
public:
    void init();
    void update();

private:
    int getSize() { return ROOM_POINT_MAX; }

};

static RoomPointManager* const pRoomPointManager = RoomPointManager::getInstance();

//******************************************************************************

#endif //!_ROOM_POINT_H

#ifndef INCLUDED_DELETEMENU
#define INCLUDED_DELETEMENU

//******************************************************************************
//
//
//      deleteMenu.h
//
//
//******************************************************************************

using namespace GameLib;

//-------------------<定数>-----------------------------------------------------
namespace delete_menu
{
    enum delMenuLabel {
        naturalflg      = (0 << 0),
        viewFlg         = (1 << 0),
        storageFlg      = (1 << 1),
        okiniiriFlg     = (1 << 2),
        deleteFlg       = (1 << 3),
        delDecisionFlg  = (1 << 4),
        notDelFlg       = (1 << 5),
        pushOn          = (1 << 6),
    };

    // iWorkのラベル
    enum iWork {
        actFlg,
        storage,
        doorLink,
        cnt,
    };

    // fWorkのラベル
    enum fWork {
        Pos,
        Angle,
    };

    // ipWorkのラベル
    enum ipWork {
        actFlgPointa,
    };

}


#define DELETEMENU_MAX     (3)

//==============================================================================
//
//      deleteMenu管理関数
//
//==============================================================================
extern void DeleteWindow(OBJ2D* obj);
extern void DelDecision(OBJ2D* obj);
extern void NotDel(OBJ2D* obj);
extern void DisEnableDel(OBJ2D *obj);

//==============================================================================
//
//      Menuクラス
//
//==============================================================================

class deleteMenu : public OBJ2DManager, public Singleton<deleteMenu>
{
public:
    static Text*  deleteText;        // 部屋名テキスト

public:
    ~deleteMenu();

    void init();                     // 初期化
    void update();                   // 更新
    void draw();                     // 描画
    
public:
    //////////各種ヘルパー関数////////////

    // 削除対象のリスト設定
    void setEraseList(LIST* list) { eraseList = list; }
    LIST* getEraseList() { return eraseList; }

    // 削除不可能ウィンドウ表示フラグ
    void setDisEnableDel(bool flg) { disEnableDel = flg; }
    bool getDisEnableDel() { return disEnableDel; }

    // デリート選択ウィンドウ表示フラグ
    void setDeleteMenuViewFlg(bool flg) { deleteMenuViewFlg = flg; }
    bool getDeleteMenuViewFlg() { return deleteMenuViewFlg; }

    // 削除実行可能フラグ
    void setDecisionFlg(bool flg) { delDecisionFlg = flg; }
    bool getDecisionFlg() { return delDecisionFlg; }

    // デリート選択ウィンドウ終了フラグ
    void setNotDeleteFlg(bool flg) { notDeleteFlg = flg; }
    bool getNotDeleteFlg() { return notDeleteFlg; }

private:
    int getSize() { return MENU_MAX; }
    LIST*   eraseList;

private:
    bool disEnableDel;              // 削除不可能ウィンドウ表示フラグ
    bool deleteMenuViewFlg;         // デリート選択ウィンドウ表示フラグ
    bool delDecisionFlg;            // 削除実行可能フラグ
    bool notDeleteFlg;              // デリート選択ウィンドウ終了フラグ

};

//------< インスタンス取得 >-----------------------------------------------------
#define pDeleteMenuManager  (deleteMenu::getInstance())

//******************************************************************************

#endif //!INCLUDE_DELETEMENU
#pragma once


struct ActorParam
{
	GameLib::AnimeData* animeUp;    // 上方向アニメ
	GameLib::AnimeData* animeRight; // 右方向アニメ
	GameLib::AnimeData* animeDown;  // 下方向アニメ
	GameLib::AnimeData* animeLeft;  // 左方向アニメ

	float accelX;                   // 横方向の加速度
	float accelY;                   // 縦方向の加速度

	float speedMaxX;                // 横方向の最大速度
	float speedMaxY;                // 縦方向の最大速度

	float rotateSpeed;				// 回転速度
	float speedJumpY;               // ジャンプ時の上昇スピード
};
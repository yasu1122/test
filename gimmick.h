#pragma once

// ラベル定義
#define GIMMICK_MAX		100
#define GIMMICK_ADJUST_Y 0.125f
#define GIMMICK_ADJUST_X 0.0125f

namespace gimmick
{
	//行動分岐
	enum ActFlg {
		ChildMoveFlg = (1 << 0),
		PushFlg = (1 << 1),
		isHitFlg = (1 << 2),
		NotHitFlg = (1 << 3),

	};

	//タイプ分け
	enum Type {
		SWITCH,
		WALL,
		SHUTTER
	};

	// iWorkのラベル
	enum iWork {
		actFlg,
		typeFlg
	};

	// fWorkのラベル
	enum fWork {

	};


}

void gimmick_move_rb_switch(OBJ2D*obj);
void gimmick_move_inverce(OBJ2D*obj);
void gimmick_move_inverce2(OBJ2D*obj);
void gimmick_move_password(OBJ2D*obj);
void gimmick_move_clone(OBJ2D*obj);
void gimmick_move_clone_child(OBJ2D*obj);
void gimmick_move_down_child(OBJ2D*obj);
void gimmick_move_down_switch(OBJ2D*obj);
void gimmick_move_clone_child(OBJ2D*obj);
bool isAllShutterSucces();

class GimmickManager : public OBJ2DManager, public Singleton<GimmickManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
	bool shutter_move_flg;
	
	int  succes_cnt;
	std::ostringstream ostr;

private:
	int getSize() { return GIMMICK_MAX; }

};

#define pGimmickManager	(GimmickManager::getInstance())


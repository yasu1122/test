#pragma once


namespace pl_shot
{
	// iWorkのラベル
	enum iWork {

	};

	// fWorkのラベル
	enum fWork
	{
		fromPlAngle,
		fromPlDistance,
	};
}


class PlShotManager : public OBJ2DManager, public Singleton<PlShotManager>
{
public:
	void init();
	void update();
	void draw();
private:
	int getSize() { return 1; }
};


#define pPlShotManager	(PlShotManager::getInstance())

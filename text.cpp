#include	"all.h"
#include	<stdio.h>

#define TEXT_SPEED 3



Text::Text(int texNo, const char* textFilename) : texNo(texNo), str(nullptr)
{
	FILE* fp;
	if (fopen_s(&fp, textFilename, "rt"))
		assert(0 && "Textクラス_ファイルが存在しない");

	//	ローカルを日本語に
	setlocale(LC_ALL, "japanese");

	int	n;
	fscanf_s(fp, "%d", &size);			//	フォントサイズ
	fscanf_s(fp, "%d", &n);				//	文字数
	str = new wchar_t[n + 1];				//	領域確保
	fwscanf_s(fp, L"%s", str, n + 1);	//	テキスト読み込み
	fclose(fp);

	//pStr = &str;
	pStr = 0;

	numOf1Line = TEX_W / size;			//	画像の1行の文字数

	strEnd = false;
}


void Text::_render(wchar_t c, VECTOR2 pos, VECTOR2 scale, VECTOR4 color)
{
	const VECTOR2 texSize = VECTOR2((float)size, (float)size);
	VECTOR2 texPos;
	int n;
	for (n = 0; str[n]; n++)
	{
		if (c != str[n])	continue;
		texPos.x = (float)(n % numOf1Line)*(float)size;
		texPos.y = (float)(n / numOf1Line)*(float)size;
		break;
	}
	if (!str[n])	return;

	GameLib::texture::draw(texNo, pos, scale, texPos, texSize, VECTOR2(0, 0), 0, color);
}



void Text::Render(wchar_t* text, VECTOR2 pos, VECTOR2 scale, VECTOR4 color, int timer, float maxWidth, float maxHeight)
{

	VECTOR2 startPos = pos;

	GameLib::texture::begin(texNo);

	valuePerLine = (int)(maxWidth / size / scale.x);		// 最大列数
	valuePerRow = (int)(maxHeight / size / scale.y);		// 最大行数

	// nは文字列の何文字目かを表す
	int n = pStr;
	strEnd = false;

	// timerが送られてくるときは文字列の先頭から一文字ずつ、timer % TEXT_SPEEDフレームごとに描画を進める
	if (timer)
	{
		for (; n < roop_cnt; n++)
		{

			auto c = text[n];

			// null文字だったら終了フラグをオンにする
			if (!c)
			{
				//pStr = 0;
				strEnd = true;
				break;
			}

			// 改行文字だったらx座標を開始位置に、y座標を一文字分下にずらす
			if (c == L'\n')
			{
				pos.x = startPos.x;
				pos.y += size * scale.y;
				continue;
			}

			// 次の文字数が最大列数を超えたら改行処理、ただし改行文字と違ってこちらは描画しないといけないのでcontinueしない
			if (maxWidth < pos.x + size - startPos.x)
			{
				pos.x = startPos.x;
				pos.y += size * scale.y;
			}

			// 次の文字数が最大行数を超えるなら、
			if (maxHeight < pos.y + size - startPos.y)
				pStr += valuePerLine;

			// 描画
			_render(c, pos, scale, color);

			// 描画位置を一文字分進める
			pos.x += size * scale.x;
		}
	}
	else
	{
		for (; text[n]; n++)
		{
			auto c = text[n];
			// 改行処理
			if (c == L'\n')
			{
				pos.x = startPos.x;
				pos.y += size * scale.y;
				continue;
			}
			_render(c, pos, scale, color);
			pos.x += size * scale.x;
		}
		strEnd = true;

	}
	GameLib::texture::end(texNo);

	if (timer && (timer % TEXT_SPEED == 0))roop_cnt++;



	//return false;
}





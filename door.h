#ifndef  _INCLUDE_DOOR
#define  _INCLUDE_DOOR
//******************************************************************************
//
//
//      door.h
//
//
//******************************************************************************

//-------------------<定数>-----------------------------------------------------
#define DOOR_MAX            (20)
#define DOOR_DATA_MAX       (20)

#define STAGE_MAX_NUM_X     (5)
#define STAGE_MAX_NUM_Y     (5)

// スクロール値
#define SCRL_X_MAX            (5760.0f)
#define SCRL_X_MIDDLE1        (1920.0f)
#define SCRL_X_MIDDLE2        (3840.0f)
#define SCRL_X_MIN            (0.0f)
#define SCRL_X_MIDDLE_HALF    (SCRL_X_MIDDLE1/2.0f) 
#define SCRL_Y_MAX            (2200.0f)
#define SCRL_Y_MIDDLE1        (1105.0f)
#define SCRL_Y_MIN            (8.0f)

namespace door
{
    enum changeFlg {
        doorChange = (1 << 0),
    };

    enum iWork {
        doorCnt,
        doorChangeflg,
        doorLink,
        doorURL,
    };

    
    
};



//==============================================================================
//
//		DoorManagerクラス
//
//==============================================================================
class DoorManager : public OBJ2DManager, public LinkSet, public Singleton<DoorManager>
{
public:
    void init();    // 初期化
    void update();  // 更新
    void draw();    // 描画

    int doorStageNo;                                    // ステージナンバー

private:

    //void doorURLClear(DoorURL* doorURL);

    int getSize() { return DOOR_MAX; }
    int getDoorURLSize() { return DOOR_DATA_MAX; }


};
void doorMove(OBJ2D* obj);


//------< インスタンス取得 >-----------------------------------------------------
#define pDoorManager  (DoorManager::getInstance())

//==============================================================================

#endif //!_INCLUDE_DOOR
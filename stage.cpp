#include"all.h"

using namespace GameLib::input;

#define INIT_START_CNT (20)

//先に進むのに必用なURL数
int getURLnum[] =
{
    3,
	3,
	3,
	3,
	3,
    6,
    6,
};

void StageManager::init()
{
	state = 0;
	timer = 0;
	pScript = stage_script[stageNo];
	pURL = url_data[stageNo];
	map_init_flg = false;
	init_start_cnt = INIT_START_CNT;

	URLmax = getURLnum[stageNo];



}

void StageManager::update()
{
	using namespace flagFunc;

	str_start = nullptr;


	// ステージクリア対応にする
	switch (state) {

	case 0:			// ステージ初期化

		pBGManager->init();

		pPlayerManager->init();

		pMousePointa->init(0);

		pDoorManager->init();

		pMenuManager->init();

		pBookMarkObj->init();

		pEnemyManager->init();

		pDeleteMenuManager->init();

		pItemManager->init();

		pVirusManager->init();

		pGimmickManager->init();

		pGimmickTerrManager->init();

		pPortalManager->init();

		pEffectManager->init();

		pRoomText->init();

		pMonsterRoom->init();

		pUiManager->init();

		pOnePlaceDoorManager->init();

		pGoalManager->init();

		pBookMarkManager->init();
		sound::play(SE::SE_WOAP);
		state++;

		//break;

	case 1:

		pauseFlgOn();

		pMousePointa->update();

		if (!Flag_Check(iWork[stage::iWork::generalflg], stage::InitFlg::scene_pause)) {

			pItemManager->update();

			pPlayerManager->update();

			pDoorManager->update();

			pOnePlaceDoorManager->update();

		}



		pMenuManager->update();
		pBookMarkManager->update();
		pDeleteMenuManager->update();

		pEffectManager->update();

		pBookMarkObj->update();

		if (!Flag_Check(iWork[stage::iWork::generalflg], stage::InitFlg::scene_pause)) {


			pBGManager->update();

			pEnemyManager->update();

			pVirusManager->update();

			pGimmickManager->update();

			pGimmickTerrManager->update();

			pPortalManager->update();

			pRoomText->update();

			pMonsterRoom->update();

			pUiManager->update();

		}



		if (Flag_Check(STAGE->iWork[stage::iWork::initflg], stage::InitFlg::map_init_pl_respawn))
		{
			state = 3; return;
		}

		pGoalManager->update();

		pJudgeStageManager->update();


		//カウントがゼロになったらmap切り替え時の初期化を実行
		if (init_start_cnt < 0)
		{
			state = 2; return;
		}
		if (STAGE->map_init_flg)
		{
			--init_start_cnt;//カウント開始
		}

		break;

	case 2:
		//マップ切り替え初期化処理
		for (auto & pItem : *pItemManager)
		{
			if (pItem.mover != ItemMoveKey2)continue;
			pItem.clear();
		}
		// ギミック(分身）
		for (auto & pGimmick : *pGimmickManager)
		{
			if (pGimmick.mover != gimmick_move_clone_child)continue;
			pGimmick.clear();

		}
		// ギミック(シャッター）
		for (auto & pGimmick : *pGimmickManager)
		{
			if (pGimmick.type !=  gimmick::Type::SHUTTER)continue;
			pGimmick.state = 0;

		}

		pEnemyManager->init();
		pVirusManager->init();

		STAGE->map_init_flg = false;
		init_start_cnt = INIT_START_CNT;
		

		state = 1;

		break;

	case 3:

		// リスポーン時の初期化
		respawnInit();
		sound::play(SE::SE_WOAP);

		break;

	}

}



void StageManager::draw()
{
	pBGManager->drawBack();

    pMonsterRoom->draw();

	pBGManager->drawTerrain();

	pGimmickManager->draw();

	pGimmickTerrManager->draw();


	pPortalManager->draw();

	pBookMarkObj->draw();

	pPlayerManager->draw();

	pDoorManager->draw();

	pOnePlaceDoorManager->draw();

	pItemManager->draw();


	pVirusManager->draw();

	pEnemyManager->draw();


	
	using namespace flagFunc;
	if (Flag_Check(iWork[stage::iWork::generalflg], stage::InitFlg::scene_pause))
		pBGManager->drawMiniMap();

	pUiManager->draw();
    pBookMarkManager->draw();
    pDeleteMenuManager->draw();

	pMenuManager->draw();

	pMousePointa->draw();

	pEffectManager->draw();

	pGoalManager->draw();


	if (str_start) GameLib::font::textOut(4, str_start, GameLib::system::SCREEN_WIDTH / 2 - 250.0f, GameLib::system::SCREEN_HEIGHT * 0.5f, 2, 2, 1, 0, 0, 1);



}

// ポーズフラグオン
void StageManager::pauseFlgOn()
{
	using namespace flagFunc;

	OBJ2D* pMenu = pMenuManager->getOBJ2D(0);

	if (Flag_Check(pMenu->iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg)) {
		Flg_On(iWork[stage::iWork::generalflg], stage::InitFlg::scene_pause);
		return;
	}

	Flg_Off(iWork[stage::iWork::generalflg], stage::InitFlg::scene_pause);
}

// リスポーン時の再初期化
void StageManager::respawnInit()
{
	using namespace flagFunc;

	pEnemyManager->init();

	for (auto & pItem : *pItemManager)
	{
		if (pItem.mover != ItemMoveKey2)continue;
		pItem.clear();
	}
	// ギミック(シャッター）
	for (auto & pGimmick : *pGimmickManager)
	{
		if (pGimmick.type != gimmick::Type::SHUTTER)continue;
		pGimmick.state = 0;

	}
	pVirusManager->init();

	pUiManager->init();

	state = 1;

	Flg_Off(STAGE->iWork[stage::iWork::initflg], stage::InitFlg::map_init_pl_respawn);

}

#include "all.h"


#define ESCAPE_DISTANCE (500.f)
#define URL_SIZEX 43.f
#define URL_SIZEY 32.f
#define URL_ALPHA 0.6f
#define KEY_SIZEX 32.f
#define KEY_SIZEY 32.f
#define KEY_ROTATION_ANGLE 1.2f
//URLアイテム
void ItemMoveURL00(OBJ2D*obj)
{ 

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_00;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
        obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;
		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg00);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


        break;
	}
}
void ItemMoveURL01(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_01;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:






		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg01);
			obj->clear();

		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURL02(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_02;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color = VECTOR4(0.0f, 1.0f, 0.0f, URL_ALPHA);

		obj->state++;
        obj->iWork[item::PlayerOneUp] = 1;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg02);
			obj->clear();

		}

		obj->OBJ2D::PosInverce(obj);

		break;
	}
}
void ItemMoveURL03(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_03;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;


		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg03);
			obj->clear();
		}


		obj->OBJ2D::PosInverce(obj);

		break;
	}
}

float distanceToPlayer(OBJ2D*obj)
{
	OBJ2D* pl = pPlayerManager->getOBJ2D(0);

	return vec2Length(pl->position - obj->position);
}


void ItemMoveURL04(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_04;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg04);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURL05(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_05;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 1;
        obj->color = VECTOR4(0.0f, 1.0f, 0.0f, URL_ALPHA);
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg05);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURL06(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_06;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg06);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURL07(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_07;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg07);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURL08(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_08;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 1;
        obj->color = VECTOR4(0.0f, 1.0f, 0.0f, 0.6f);
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg08);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURL09(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_09;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg09);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURL10(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_10;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:
		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg10);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);



		break;
	}
}
void ItemMoveURL11(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_11;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
        obj->iWork[item::PlayerOneUp] = 0;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:
		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg11);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURLex(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_url02_11;
		obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
		obj->color.w = URL_ALPHA;
		obj->state++;
		obj->init_position = obj->position;
		obj->iWork[item::isURL] = 1;

		//break;

	case 1:

		OBJ2D* player = pPlayerManager->getOBJ2D(0);


		//プレイヤーから常に一定距離離れるように動く
		if (fabs(obj->position.x - player->position.x) < 60&& obj->position.x - player->position.x>0)
			obj->position.x = obj->init_position.x- ESCAPE_DISTANCE;

		if (fabs(obj->position.x - player->position.x) < 60 && obj->position.x - player->position.x<0)
			obj->position.x = obj->init_position.x + ESCAPE_DISTANCE;

		if (fabs(obj->position.y - player->position.y) < 60 && obj->position.y - player->position.y>0)
			obj->position.y = obj->init_position.y - ESCAPE_DISTANCE/1.3f;

		if (fabs(obj->position.y - player->position.y) < 60 && obj->position.y - player->position.y<0)
			obj->position.y = obj->init_position.y + ESCAPE_DISTANCE/1.3f;


		if (obj->flg) {

			OBJ2D* portal = pPortalManager->getOBJ2D(0);
			flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg11);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);


		break;
	}
}
void ItemMoveURLex2(OBJ2D*obj)
{
	
		switch (obj->state)
		{

		case 0:
			obj->data = &spr_url02_10;
			obj->size = VECTOR2(URL_SIZEX, URL_SIZEY);
			obj->color.w = URL_ALPHA;
			obj->state++;
			obj->judgeFlag = true;
			obj->iWork[item::isURL] = 1;

			//break;

		case 1:
			if (obj->flg) {

				OBJ2D* portal = pPortalManager->getOBJ2D(0);
				flagFunc::Flg_On(portal->iWork[portal::iWork::GET_URL_FLG], portal::getURLflg10);
				obj->clear();
			}

			obj->OBJ2D::PosInverce(obj);


			break;
		}

		//シャッターを順番に押さなければ当たらない
		if (!isAllShutterSucces()) {
			obj->judgeFlag = true;
			obj->color.w = 0;
		}
		else {
			obj->judgeFlag = false;
			obj->color.w = URL_ALPHA;
		}

	
}
void ItemMoveKey(OBJ2D*obj)
{

	switch (obj->state)
	{

	case 0:
		obj->data = &spr_key00;
		obj->size = VECTOR2(KEY_SIZEX, KEY_SIZEY);
		obj->state++;
		obj->param = 1;

		//break;

	case 1:
		
		if (obj->flg) {
			OBJ2D* player = pPlayerManager->getOBJ2D(0);
			flagFunc::Flg_On(player->iWork[player::iWork::actflg], player::ActFlg::plIsKey);
			obj->clear();
		}

		obj->OBJ2D::PosInverce(obj);

		break;
	}
}

#define KEY_ROTATION_RADIUS 250.0f

void ItemMoveKey2(OBJ2D*obj)
{

	float & MoveAngle = obj->fWork[item::fWork::MoveAngle];


	switch (obj->state)
	{

	case 0:
		obj->data = &spr_key01;
		obj->size = VECTOR2(KEY_SIZEX, KEY_SIZEY);
		obj->state++;
        obj->param = 1;

		//break;

	case 1:



		obj->position.x = obj->parent->position.x + sin(ToRadian(75 * obj->index + MoveAngle)) * KEY_ROTATION_RADIUS;
		obj->position.y = obj->parent->position.y + cos(ToRadian(75 * obj->index + MoveAngle)) * KEY_ROTATION_RADIUS;

		MoveAngle += KEY_ROTATION_ANGLE;


		if (obj->flg) {
			OBJ2D* player = pPlayerManager->getOBJ2D(0);
			flagFunc::Flg_On(player->iWork[player::iWork::actflg], player::ActFlg::plIsKey2);
			obj->clear();
		}

		break;
	}
	obj->OBJ2D::PosInverce(obj);

}
void ItemMoveKey2_stop(OBJ2D*obj)
{


	switch (obj->state)
	{

	case 0:
		obj->data = &spr_key01;
		obj->size = VECTOR2(KEY_SIZEX, KEY_SIZEY);
		obj->state++;
		obj->param = 1;

		//break;

	case 1:



		if (obj->flg) {
			OBJ2D* player = pPlayerManager->getOBJ2D(0);
			flagFunc::Flg_On(player->iWork[player::iWork::actflg], player::ActFlg::plIsKey2);
			obj->clear();
		}

		break;
	}
	obj->OBJ2D::PosInverce(obj);

}
MOVER ItemMoveArray[] =
{
	ItemMoveURL00, 
	ItemMoveURL01,
	ItemMoveURL02,
	ItemMoveURL03,
	ItemMoveURL04,
	ItemMoveURL05,
	ItemMoveURL06,
	ItemMoveURL07,
	ItemMoveURL08,
	ItemMoveURL09,
	ItemMoveURL10,
	ItemMoveURL11,
	ItemMoveKey,
	ItemMoveURLex,
	ItemMoveKey2_stop,
	ItemMoveURLex2,
};

//=======================================================
//
//      
//
//=======================================================

void ItemManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = STAGE->getStageScript();

    // アイテムの読み込み
    char** mapItem = new char*[pBGManager->CHIP_NUM_Y];

    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapItem[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapItem[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

	if (!pBGManager->loadMapData(pScript->fileNameItem, mapItem))
	{
		assert(!"アイテムデータ読み込み失敗");
	}

	// アイテム配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int itemIndex = mapItem[y][x];
			if (itemIndex < 0) continue;
			searchSet(ItemMoveArray[itemIndex], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);

		}
	}

    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapItem[i];
    }
    delete[] mapItem;
}

void ItemManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

void ItemManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの初期化

}


//******************************************************************************
//
//
//		SPRITE_DATA
//
//
//******************************************************************************

//------< インクルード >--------------------------------------------------------

#include "all.h"

using namespace GameLib;

//------< データ >--------------------------------------------------------------

// 2D画像ロードデータ
LoadTexture loadTexture[] = {
    { TEXNO::PLAYER, L"./Data/Images/player00.png", 16U },                  //　プレイヤー
    { TEXNO::ENEMY, L"./Data/Images/enemy.png", 32U },                      //　敵
    { TEXNO::ITEM, L"./Data/Images/keys.png", 32U },                        //　鍵
    { TEXNO::BLACK, L"./Data/Images/color_black.png", 1U },                 //　暗転用画像データ
    { TEXNO::TEXT, L"./Data/Text/font.png", 256U },                         //　テキスト
    { TEXNO::TEXT2,L"./Data/Text/font_2.png", 512U },                       // テキスト2 
    { TEXNO::TEXT3, L"./Data/Text/Tutorial.png", 512U },	                // チュートリアル
    { TEXNO::MAP_BACK,      L"./Data/Maps/back_chip.png",    7192U },       // マップ背景
    { TEXNO::MAP_TERRAIN,   L"./Data/Maps/terrain_chip.png", 7192U },       // マップ地形
    { TEXNO::BLOCK,   L"./Data/Maps/gimmick_chip.png", 1024U },             // マップ地形
    { TEXNO::PORTAL,   L"./Data/Images/portal.png", 1024U },                // マップ地形
    { TEXNO::MAP_MINIMAP,   L"./Data/Maps/miniMap_spr.png", 4096U },        // ミニマップ
    { TEXNO::MESSAGE_WINDOW,  L"./Data/Images/frame.png", 1U },		        // 吹き出し
    { TEXNO::UI_FONT,  L"./Data/Images/UIfont.png", 1U },		            // 吹き出し
    { TEXNO::EFFECT_CLICK, L"./Data/Images/click_effect.png", 32U },        // click時エフェクト
    { TEXNO::VIRUS, L"./Data/Images/virus.png",320U },                      // ウィルス
    { TEXNO::TITLE, L"./Data/Images/bookMarkerTitle.png",1U},               // タイトル画面
    { TEXNO::TITLE_UI, L"./Data/Images/title_ui.png",2U},                   // タイトルUI
    { TEXNO::URL_BAR, L"./Data/Images/url_bar.png" ,1U},                    // ゲーム画面上のバー
    { TEXNO::FAVO_BUTTON, L"./Data/Images/favo_button.png", 2U},            // favoボタン
    { TEXNO::URL, L"./Data/Images/url00.png", 20U },                        // URL画像データ
    { TEXNO::MOUSE_CURSOR, L"./Data/Images/mouse_cursor.png",4U},           // マウスカーソル
    { TEXNO::REVERSE_BAR, L"./Data/Images/reverse_sprite.png",1U },         // 反転壁用画像データ
    { TEXNO::DOOR, L"./Data/Images/door_sprite.png", 2U},                   // 扉画像データ
    { TEXNO::URL_TEXT, L"./Data/Images/room_name_url.png", 20U},            // 部屋上部のURL
    { TEXNO::ROOM_NAME, L"./Data/Images/menu_url.png", 20U },               // お気に入り登録表示画像
    { TEXNO::TOUROKUHUKA, L"./Data/Images/tourokuhuka.png", 1U },           // 登録不可能表示
    { TEXNO::SAKUJOHUKA, L"./Data/Images/sakujohuka.png",1U },              // 削除不可能表示
    { TEXNO::SAKUJOMENU, L"./Data/Images/delete_menu.png",1U },             // デリートメニュー
    { TEXNO::SELECT_SPRITE, L"./Data/Images/select_sprite.png", 1U},        // セレクト画面
    { TEXNO::SELECT_ICON, L"./Data/Images/select_icon.png", 4U },           // セレクトアイコン
    { TEXNO::SHUTTER_SWITCH, L"./Data/Images/shutter_Button.png" , 2U },    // シャッタースイッチ
    { TEXNO::BULE_RED_SWITCH, L"./Data/Images/BlueorRedSwitch.png" , 2U},   // 青赤切り替えスイッチ
	{ TEXNO::MINIMAP_ICON, L"./Data/Images/MinimapIcon.png" , 32U },        // ミニマップ各種アイコン
	{ TEXNO::PASS_WINDOW, L"./Data/Images/password_window.png" , 1U },      // パスワード入力ウィンドウ
    { TEXNO::LIFE, L"./Data/Images/Life.png", 1U },                         // Life
    { TEXNO::GAME_OVER, L"./Data/Images/game_over.png", 1U},                // ゲームオーバー画面
    { TEXNO::GAME_CLEAR, L"./Data/Images/game_clear.png", 1U},              // ゲームクリア画面
    { TEXNO::MIRROR, L"./Data/Images/mirror_sprite.png", 1U },              // 分身壁
    { TEXNO::MONSTER_ROOM_BACK, L"./Data/Images/monster_room_back.png",1U },// モンスタールーム背景


	///////////////////////////////////////////////////仮画像//////////////////////////////////////////////////////
    { 97,    L"./Data/images/effect00.png", 30U },
    { 98,    L"./Data/images/asset00.png", 30U },
    { 99,    L"./Data/images/ui00.png", 1U },
    { 100,   L"./Data/images/ui01.png", 1U },
    { 101,   L"./Data/images/ui02.png", 1U },
    { 103,   L"./Data/images/hp.png", 1U },
    { 104,   L"./Data/images/marubatsu.png", 1U },
	{ 105,   L"./Data/Images/effect_move00.png", 32U },
	{ 106,   L"./Data/Images/pass.png", 32U },
	///////////////////////////////////////////////////仮画像//////////////////////////////////////////////////////

    { -1, nullptr }	// 終了フラグ
};

//------< マクロ >--------------------------------------------------------------
#define SPRITE_CENTER(texno,left,top,width,height)	{ (texno),(left),(top),(width),(height),(width)/2,(height)/2 }  // 画像の真ん中が中心
#define SPRITE_BOTTOM(texno,left,top,width,height)	{ (texno),(left),(top),(width),(height),(width)/2,(height)   }  // 画像の足元が中心
#define SPRITE_ZERO(texno,left,top,width,height)	{ (texno),(left),(top),(width),(height),(0),(0) }  // 画像の真ん中が中心

//------< 待機 >--------------------------------------------------------------
SpriteData spr_player00 = SPRITE_CENTER(TEXNO::PLAYER, 256 * 0, 256 * 0, 256, 256);
SpriteData spr_player01 = SPRITE_CENTER(TEXNO::PLAYER, 256 * 1, 256 * 0, 256, 256);
SpriteData spr_player02 = SPRITE_CENTER(TEXNO::PLAYER, 256 * 0, 256 * 1, 256, 256);
SpriteData spr_player03 = SPRITE_CENTER(TEXNO::PLAYER, 256 * 1, 256 * 1, 256, 256);

SpriteData spr_key00 = SPRITE_CENTER(TEXNO::ITEM, 0, 0, 32, 32);
SpriteData spr_key01 = SPRITE_CENTER(TEXNO::ITEM, 32, 0, 32, 32);

SpriteData color_black = SPRITE_CENTER(TEXNO::BLACK, 0, 0, 1, 1);

SpriteData spr_gimmick_switch00 = SpriteData{ TEXNO::BLOCK,32 * 0,0,32,32, (32 >> 1), (32 >> 1) };
SpriteData spr_gimmick_switch01 = SpriteData{ TEXNO::BLOCK,32 * 1,0,32,32, (32 >> 1), (32 >> 1) };
SpriteData spr_gimmick_wall00 = SPRITE_BOTTOM(TEXNO::BLOCK, 32 * 2, 0, 32, 32);

SpriteData spr_portal = SPRITE_CENTER(TEXNO::PORTAL, 0, 0, 960 , 400);
SpriteData spr_ui00   = SPRITE_CENTER(99, 0, 0, 512, 256);
SpriteData spr_ui01   = SPRITE_CENTER(100, 0, 0, 512, 256);
SpriteData spr_ui02   = SPRITE_CENTER(101, 0, 0, 512, 256);


SpriteData spr_star00 = SPRITE_CENTER(98, 0, 128, 128, 128);

SpriteData spr_arrow  = SPRITE_CENTER(TEXNO::MOUSE_CURSOR, 256 * 0, 256 * 0, 256, 256);
SpriteData spr_hand00 = SPRITE_CENTER(TEXNO::MOUSE_CURSOR, 256 * 0, 256 * 1, 256, 256);

SpriteData spr_wave_effect00 = SPRITE_CENTER(97, 240 * 0, 0, 240, 240);
SpriteData spr_wave_effect01 = SPRITE_CENTER(97, 240 * 1, 0, 240, 240);
SpriteData spr_wave_effect02 = SPRITE_CENTER(97, 240 * 2, 0, 240, 240);
SpriteData spr_wave_effect03 = SPRITE_CENTER(97, 240 * 3, 0, 240, 240);
SpriteData spr_wave_effect04 = SPRITE_CENTER(97, 240 * 4, 0, 240, 240);


//----------------------< 検索枠用のURL >--------------------------------------------
// 暗い版
SpriteData spr_url00_00 = SPRITE_CENTER(TEXNO::URL, 66 * 0 , 81 * 1, 66, 80);
SpriteData spr_url00_01 = SPRITE_CENTER(TEXNO::URL, 66 * 1 , 81 * 1, 66, 80);
SpriteData spr_url00_02 = SPRITE_CENTER(TEXNO::URL, 66 * 2 , 81 * 1, 66, 80);
SpriteData spr_url00_03 = SPRITE_CENTER(TEXNO::URL, 66 * 3 , 81 * 1, 66, 80);
SpriteData spr_url00_04 = SPRITE_CENTER(TEXNO::URL, 66 * 4 , 81 * 1, 66, 80);
SpriteData spr_url00_05 = SPRITE_CENTER(TEXNO::URL, 66 * 5 , 81 * 1, 66, 80);
SpriteData spr_url00_06 = SPRITE_CENTER(TEXNO::URL, 66 * 6 , 81 * 1, 66, 80);
SpriteData spr_url00_07 = SPRITE_CENTER(TEXNO::URL, 66 * 7 , 81 * 1, 66, 80);
SpriteData spr_url00_08 = SPRITE_CENTER(TEXNO::URL, 66 * 8 , 81 * 1, 66, 80);
SpriteData spr_url00_09 = SPRITE_CENTER(TEXNO::URL, 66 * 9 , 81 * 1, 66, 80);
SpriteData spr_url00_10 = SPRITE_CENTER(TEXNO::URL, 66 * 10, 81 * 1, 66, 80);
SpriteData spr_url00_11 = SPRITE_CENTER(TEXNO::URL, 66 * 11, 81 * 1, 66, 80);

// 明るい版
SpriteData spr_url01_00 = SPRITE_CENTER(TEXNO::URL, 66 * 0 , 81 * 2, 66, 80);
SpriteData spr_url01_01 = SPRITE_CENTER(TEXNO::URL, 66 * 1 , 81 * 2, 66, 80);
SpriteData spr_url01_02 = SPRITE_CENTER(TEXNO::URL, 66 * 2 , 81 * 2, 66, 80);
SpriteData spr_url01_03 = SPRITE_CENTER(TEXNO::URL, 66 * 3 , 81 * 2, 66, 80);
SpriteData spr_url01_04 = SPRITE_CENTER(TEXNO::URL, 66 * 4 , 81 * 2, 66, 80);
SpriteData spr_url01_05 = SPRITE_CENTER(TEXNO::URL, 66 * 5 , 81 * 2, 66, 80);
SpriteData spr_url01_06 = SPRITE_CENTER(TEXNO::URL, 66 * 6 , 81 * 2, 66, 80);
SpriteData spr_url01_07 = SPRITE_CENTER(TEXNO::URL, 66 * 7 , 81 * 2, 66, 80);
SpriteData spr_url01_08 = SPRITE_CENTER(TEXNO::URL, 66 * 8 , 81 * 2, 66, 80);
SpriteData spr_url01_09 = SPRITE_CENTER(TEXNO::URL, 66 * 9 , 81 * 2, 66, 80);
SpriteData spr_url01_10 = SPRITE_CENTER(TEXNO::URL, 66 * 10, 81 * 2, 66, 80);
SpriteData spr_url01_11 = SPRITE_CENTER(TEXNO::URL, 66 * 11, 81 * 2, 66, 80);

//---------------------------< 収集用のURL >-----------------------------------

SpriteData spr_url02_00 = SPRITE_CENTER(TEXNO::URL, 66 * 0 , 81 * 0, 66, 80);
SpriteData spr_url02_01 = SPRITE_CENTER(TEXNO::URL, 66 * 1 , 81 * 0, 66, 80);
SpriteData spr_url02_02 = SPRITE_CENTER(TEXNO::URL, 66 * 2 , 81 * 0, 66, 80);
SpriteData spr_url02_03 = SPRITE_CENTER(TEXNO::URL, 66 * 3 , 81 * 0, 66, 80);
SpriteData spr_url02_04 = SPRITE_CENTER(TEXNO::URL, 66 * 4 , 81 * 0, 66, 80);
SpriteData spr_url02_05 = SPRITE_CENTER(TEXNO::URL, 66 * 5 , 81 * 0, 66, 80);
SpriteData spr_url02_06 = SPRITE_CENTER(TEXNO::URL, 66 * 6 , 81 * 0, 66, 80);
SpriteData spr_url02_07 = SPRITE_CENTER(TEXNO::URL, 66 * 7 , 81 * 0, 66, 80);
SpriteData spr_url02_08 = SPRITE_CENTER(TEXNO::URL, 66 * 8 , 81 * 0, 66, 80);
SpriteData spr_url02_09 = SPRITE_CENTER(TEXNO::URL, 66 * 9 , 81 * 0, 66, 80);
SpriteData spr_url02_10 = SPRITE_CENTER(TEXNO::URL, 66 * 10, 81 * 0, 66, 80);
SpriteData spr_url02_11 = SPRITE_CENTER(TEXNO::URL, 66 * 11, 81 * 0, 79, 80);



SpriteData spr_hp0 = SPRITE_CENTER(103, 0, 0, 300, 300);
SpriteData spr_hp1 = SPRITE_CENTER(103, 0, 0, 240, 300);
SpriteData spr_hp2 = SPRITE_CENTER(103, 0, 0, 180, 300);
SpriteData spr_hp3 = SPRITE_CENTER(103, 0, 0, 120, 300);
SpriteData spr_hp4 = SPRITE_CENTER(103, 0, 0, 60, 300);

SpriteData spr_messageWindow = SPRITE_CENTER(TEXNO::MESSAGE_WINDOW, 0, 0, 960, 200);

SpriteData spr_maru = SPRITE_CENTER(104, 0, 0, 256, 256);
SpriteData spr_batsu = SPRITE_CENTER(104, 256, 0, 256, 256);

SpriteData spr_start = SPRITE_CENTER(TEXNO::UI_FONT, 0, 0, 512, 256);
SpriteData spr_exit = SPRITE_CENTER(TEXNO::UI_FONT, 512, 0, 512, 256);
SpriteData spr_restart = SPRITE_CENTER(TEXNO::UI_FONT, 0, 256, 512, 256);
SpriteData spr_title = SPRITE_CENTER(TEXNO::UI_FONT, 512, 256, 512, 256);

//-------------------------<エフェクト>---------------------------------------------
SpriteData spr_effect_move00 = SPRITE_CENTER(105, 1024 * 0, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move01 = SPRITE_CENTER(105, 1024 * 1, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move02 = SPRITE_CENTER(105, 1024 * 2, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move03 = SPRITE_CENTER(105, 1024 * 3, 1024 * 0, 1024, 1024);
SpriteData spr_effect_move04 = SPRITE_CENTER(105, 1024 * 0, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move05 = SPRITE_CENTER(105, 1024 * 1, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move06 = SPRITE_CENTER(105, 1024 * 2, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move07 = SPRITE_CENTER(105, 1024 * 3, 1024 * 1, 1024, 1024);
SpriteData spr_effect_move08 = SPRITE_CENTER(105, 1024 * 0, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move09 = SPRITE_CENTER(105, 1024 * 1, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move10 = SPRITE_CENTER(105, 1024 * 2, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move11 = SPRITE_CENTER(105, 1024 * 3, 1024 * 3, 1024, 1024);
SpriteData spr_effect_move12 = SPRITE_CENTER(105, 1024 * 0, 1024 * 4, 1024, 1024);
SpriteData spr_effect_move13 = SPRITE_CENTER(105, 1024 * 1, 1024 * 4, 1024, 1024);
SpriteData spr_effect_move14 = SPRITE_CENTER(105, 1024 * 2, 1024 * 4, 1024, 1024);

SpriteData spr_effect_click00 = SPRITE_CENTER(TEXNO::EFFECT_CLICK, 512 * 0, 512 * 0, 512, 512);
SpriteData spr_effect_click01 = SPRITE_CENTER(TEXNO::EFFECT_CLICK, 512 * 1, 512 * 0, 512, 512);
SpriteData spr_effect_click02 = SPRITE_CENTER(TEXNO::EFFECT_CLICK, 512 * 2, 512 * 0, 512, 512);
SpriteData spr_effect_click03 = SPRITE_CENTER(TEXNO::EFFECT_CLICK, 512 * 3, 512 * 0, 512, 512);
SpriteData spr_effect_click04 = SPRITE_CENTER(TEXNO::EFFECT_CLICK, 512 * 4, 512 * 0, 512, 512);
SpriteData spr_effect_click06 = SPRITE_CENTER(TEXNO::EFFECT_CLICK, 512 * 1, 512 * 1, 512, 512);
SpriteData spr_effect_click05 = SPRITE_CENTER(TEXNO::EFFECT_CLICK, 512 * 0, 512 * 1, 512, 512);



SpriteData spr_gimmickterr00 = SPRITE_CENTER(TEXNO::MAP_TERRAIN, 32 * 0, 32 * 0, 32, 32);
SpriteData spr_gimmickterr01 = SPRITE_CENTER(TEXNO::MAP_TERRAIN, 32 * 1, 32 * 0, 32, 32);
SpriteData spr_gimmickterr02 = SPRITE_CENTER(TEXNO::MAP_TERRAIN, 32 * 2, 32 * 0, 32, 32);
SpriteData spr_gimmickterr03 = SPRITE_CENTER(TEXNO::MAP_TERRAIN, 32 * 3, 32 * 0, 32, 32);
SpriteData spr_gimmickterr04 = SPRITE_CENTER(TEXNO::MAP_TERRAIN, 32 * 4, 32 * 0, 32, 32);
SpriteData spr_gimmickterr05 = SPRITE_CENTER(TEXNO::MAP_TERRAIN, 32 * 5, 32 * 0, 32, 32);
SpriteData spr_gimmickterr06 = SPRITE_CENTER(TEXNO::MAP_TERRAIN, 32 * 6, 32 * 0, 32, 32);

//----------------------------< ウィルス >------------------------------------------
SpriteData spr_virus00 = SPRITE_CENTER(TEXNO::VIRUS, 256 * 0, 256 * 0, 256, 256);
SpriteData spr_virus01 = SPRITE_CENTER(TEXNO::VIRUS, 256 * 1, 256 * 0, 256, 256);

//----------------------------< エネミー >------------------------------------------
SpriteData spr_adsence00 = SpriteData{ TEXNO::ENEMY,256 * 0,256 * 0,256,256, (256 >> 1), (296 >> 1) };
SpriteData spr_adsence2929 = SpriteData{ TEXNO::ENEMY,256 * 1,256 * 0,256,256, (256 >> 1), (296 >> 1) };
SpriteData spr_adsence9317 = SpriteData{ TEXNO::ENEMY,256 * 0,256 * 1,256,256, (256 >> 1), (296 >> 1) };
SpriteData spr_adsence8526 = SpriteData{ TEXNO::ENEMY,256 * 1,256 * 1,256,256, (256 >> 1), (296 >> 1) };

//----------------------------< タイトル画面>---------------------------------------
SpriteData spr_title_sprite = SPRITE_CENTER(TEXNO::TITLE, 0, 0, 1920, 1080);

//----------------------------< U   I >------------------------------------------
SpriteData spr_title_ui00       = SPRITE_CENTER(TEXNO::TITLE_UI, 512 * 0, 200 * 0, 512, 200);
SpriteData spr_title_ui01       = SPRITE_CENTER(TEXNO::TITLE_UI, 512 * 0, 200 * 1, 512, 200);
SpriteData spr_game_over_ui00   = SPRITE_CENTER(TEXNO::TITLE_UI, 512 * 0, 200 * 2, 512, 200);
SpriteData spr_game_over_ui01   = SPRITE_CENTER(TEXNO::TITLE_UI, 512 * 0, 200 * 3, 512, 200);
SpriteData spr_url_bar          = SPRITE_CENTER(TEXNO::URL_BAR, 0, 0, 1920, 48);
SpriteData spr_favo_button00    = SPRITE_CENTER(TEXNO::FAVO_BUTTON, 64 * 0, 64 * 0, 64, 64);
SpriteData spr_favo_button01    = SPRITE_CENTER(TEXNO::FAVO_BUTTON, 64 * 1, 64 * 0, 64, 64);

//----------------------------< ギミック >------------------------------------------------------

// 反転
SpriteData spr_reverse_bar = SPRITE_CENTER(TEXNO::REVERSE_BAR, 0, 0, 1920, 128);
SpriteData spr_reverse_bar2 = SPRITE_CENTER(TEXNO::REVERSE_BAR, 0, 0, 1080, 128);
//----------------------------< 扉 >------------------------------------------------------------

SpriteData spr_door00 = SPRITE_CENTER(TEXNO::DOOR, 128 * 0, 128 * 0, 128, 128);
SpriteData spr_door01 = SPRITE_CENTER(TEXNO::DOOR, 128 * 1, 128 * 0, 128, 128);
SpriteData spr_goal   = SPRITE_CENTER(TEXNO::DOOR, 128 * 0, 128 * 1, 128, 128);

//----------------------------< URLテキスト画像 >-----------------------------------------------

SpriteData spr_url_text_home = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 0 , 1664, 64);
SpriteData spr_url_text_01   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 1 , 1664, 64);
SpriteData spr_url_text_02   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 2 , 1664, 64);
SpriteData spr_url_text_03   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 3 , 1664, 64);
SpriteData spr_url_text_04   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 4 , 1664, 64);
SpriteData spr_url_text_05   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 5 , 1664, 64);
SpriteData spr_url_text_06   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 6 , 1664, 64);
SpriteData spr_url_text_07   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 7 , 1664, 64);
SpriteData spr_url_text_08   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 8 , 1664, 64);
SpriteData spr_url_text_09   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 9 , 1664, 64);
SpriteData spr_url_text_10   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 10, 1664, 64);
SpriteData spr_url_text_11   = SPRITE_CENTER(TEXNO::URL_TEXT, 0, 64 * 11, 1664, 64);

//----------------------------< 部屋名 >--------------------------------------------------------

SpriteData spr_room_name_home = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 0, 256, 64);
SpriteData spr_room_name_01   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 1, 256, 64);
SpriteData spr_room_name_02   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 2, 256, 64);
SpriteData spr_room_name_03   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 3, 256, 64);
SpriteData spr_room_name_04   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 4, 256, 64);
SpriteData spr_room_name_05   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 5, 256, 64);
SpriteData spr_room_name_06   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 6, 256, 64);
SpriteData spr_room_name_07   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 0, 64 * 7, 256, 64);
SpriteData spr_room_name_08   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 1, 64 * 0, 256, 64);
SpriteData spr_room_name_09   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 1, 64 * 1, 256, 64);
SpriteData spr_room_name_10   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 1, 64 * 2, 256, 64);
SpriteData spr_room_name_11   = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 1, 64 * 3, 256, 64);
SpriteData spr_favo_buttom    = SPRITE_CENTER(TEXNO::ROOM_NAME, 256 * 1, 64 * 5, 256, 64);

//----------------------------< メニュー >------------------------------------------------------
SpriteData spr_tourokuhuka00  = SPRITE_CENTER(TEXNO::TOUROKUHUKA, 0, 0, 256, 128);

//----------------------------< 削除メニュー >--------------------------------------------------
SpriteData spr_delete_menu    = SPRITE_CENTER(TEXNO::SAKUJOMENU, 0, 0, 256, 128);
SpriteData spr_sakujohuka     = SPRITE_CENTER(TEXNO::SAKUJOHUKA, 0, 0, 256, 128);

//----------------------------< セレクト画面 >--------------------------------------------------
SpriteData spr_select         = SPRITE_CENTER(TEXNO::SELECT_SPRITE, 0, 0, 1920, 1080);

//----------------------------< セレクトアイコン >--------------------------------------------------
SpriteData spr_select_icon_stage01  = SPRITE_CENTER(TEXNO::SELECT_ICON, 512 * 0, 512 * 0, 512, 512);
SpriteData spr_select_icon_stage02  = SPRITE_CENTER(TEXNO::SELECT_ICON, 512 * 1, 512 * 0, 512, 512);
SpriteData spr_select_icon_stage03  = SPRITE_CENTER(TEXNO::SELECT_ICON, 512 * 2, 512 * 0, 512, 512);
SpriteData spr_select_icon_stage04  = SPRITE_CENTER(TEXNO::SELECT_ICON, 512 * 3, 512 * 0, 512, 512);
SpriteData spr_select_icon_stage05  = SPRITE_CENTER(TEXNO::SELECT_ICON, 512 * 0, 512 * 1, 512, 512);
SpriteData spr_select_icon_tutorial = SPRITE_CENTER(TEXNO::SELECT_ICON, 512 * 2, 512 * 1, 512, 512);
SpriteData spr_select_icon_title    = SPRITE_CENTER(TEXNO::SELECT_ICON, 512 * 1, 512 * 1, 512, 256);

//----------------------------< 青赤スイッチ >----------------------------------------------------
SpriteData spr_bule_red_switch00 = SPRITE_CENTER(TEXNO::BULE_RED_SWITCH, 0, 0, 512, 128);
SpriteData spr_bule_red_switch01 = SPRITE_CENTER(TEXNO::BULE_RED_SWITCH, 0, 128, 512, 128);

//----------------------------< シャッタースイッチ >--------------------------------------------
SpriteData spr_shutter_switch00 = SPRITE_CENTER(TEXNO::SHUTTER_SWITCH, 0, 0, 128, 128);
SpriteData spr_shutter_switch01 = SPRITE_CENTER(TEXNO::SHUTTER_SWITCH, 128, 0, 128, 128);

//----------------------------< ミニマップ用アセット >--------------------------------------------
SpriteData spr_miniMapIcon00 = SPRITE_CENTER(TEXNO::MINIMAP_ICON, 0, 0, 32, 32);
SpriteData spr_miniMapIcon01 = SPRITE_CENTER(TEXNO::MINIMAP_ICON, 32, 0, 32, 32);

//----------------------------< 暗号用ウインドウ >------------------------------------------------
SpriteData spr_pass_window00 = SPRITE_CENTER(TEXNO::PASS_WINDOW, 256 * 0, 128 * 0, 256, 128);
SpriteData spr_pass_window01 = SPRITE_CENTER(TEXNO::PASS_WINDOW, 256 * 0, 128 * 1, 256, 128);

//----------------------------< life >------------------------------------------------------------
SpriteData spr_life = SPRITE_CENTER(TEXNO::LIFE, 0, 0, 256, 128);

//----------------------------< ゲームオーバー >--------------------------------------------------
SpriteData spr_game_over = SPRITE_CENTER(TEXNO::GAME_OVER, 0, 0, 1920, 1080);

//----------------------------< ゲームクリア >----------------------------------------------------
SpriteData spr_game_clear = SPRITE_CENTER(TEXNO::GAME_CLEAR, 0, 0,1920, 1080);

//----------------------------< パスワードテキスト >----------------------------------------------
SpriteData spr_pass00 = SPRITE_CENTER(106, 256 * 0, 64 * 0, 193, 64);
SpriteData spr_pass01 = SPRITE_CENTER(106, 256 * 0, 64 * 1, 193, 64);
SpriteData spr_pass02 = SPRITE_CENTER(106, 256 * 0, 64 * 2, 193, 64);
SpriteData spr_pass03 = SPRITE_CENTER(106, 256 * 0, 64 * 3, 193, 64);
SpriteData spr_pass04 = SPRITE_CENTER(106, 256 * 0, 64 * 4, 193, 64);

//----------------------------< 分身壁 >----------------------------------------------------------
SpriteData spr_mirror_wall = SPRITE_CENTER(TEXNO::MIRROR, 0, 0, 128, 1920);

//----------------------------< モンスタールーム背景 >--------------------------------------------
SpriteData spr_monster_room = SPRITE_CENTER(TEXNO::MONSTER_ROOM_BACK, 0, 0, 1920, 1080);

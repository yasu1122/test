//******************************************************************************
//
//
//      入力マネージャ
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------

#include "input_manager.h"
#include "./DirectXTK-master/Inc/Keyboard.h"
#include "./DirectXTK-master/Inc/GamePad.h"
#include "game_lib.h"

namespace GameLib
{
    using namespace input;
    using DirectX::Keyboard;

    //------< データ >-----------------------------------------------------------

    // キー割り当てデータ
    PadAssign keyAssign00[] = {
        { PAD_UP       , Keyboard::Up },            // VK_UP
        { PAD_DOWN     , Keyboard::Down },          // VK_DOWN
        { PAD_LEFT     , Keyboard::Left },          // VK_LEFT
        { PAD_RIGHT    , Keyboard::Right },         // VK_RIGHT

        { PAD_UP       , Keyboard::W },             // 'W'
        { PAD_DOWN     , Keyboard::S },             // 'S'
        { PAD_LEFT     , Keyboard::A },             // 'A'
        { PAD_RIGHT    , Keyboard::D },             // 'D'

        { PAD_TRG4     , Keyboard::Space },         // VK_SPACE
        { PAD_TRG5     , Keyboard::Y},              // 'Y'

        { PAD_START    , Keyboard::Z },         // VK_RETURN
        { PAD_TRG3     , Keyboard::Enter },             // 'Z'
        { PAD_TRG2     , Keyboard::X },             // 'X'
        { PAD_TRG1     , Keyboard::C },             // 'C'
        { PAD_L1       , Keyboard::LeftControl },   // VK_LCONTROL
        { PAD_R1       , Keyboard::RightControl },  // VK_RCONTROL

        { PAD_SELECT   , Keyboard::Back },          // VK_BACK

        { PAD_START  , VK_LBUTTON },                // マウス左ボタン
        { PAD_SELECT , VK_RBUTTON },                // マウス右ボタン
        { PAD_TRG1   , VK_MBUTTON },                // マウス中ボタン


		{ PAD_NUM0  , Keyboard::NumPad0 },                // 
		{ PAD_NUM1  , Keyboard::NumPad1 },                // 
		{ PAD_NUM2  , Keyboard::NumPad2 },                // 
		{ PAD_NUM3  , Keyboard::NumPad3 },                // 
		{ PAD_NUM4  , Keyboard::NumPad4 },                // 
		{ PAD_NUM5  , Keyboard::NumPad5 },                // 
		{ PAD_NUM6  , Keyboard::NumPad6 },                // 
		{ PAD_NUM7  , Keyboard::NumPad7 },                // 
		{ PAD_NUM8  , Keyboard::NumPad8 },                // 
		{ PAD_NUM9  , Keyboard::NumPad9 },   
		
		// 
		{ PAD_A  , Keyboard::A },						  // 
		{ PAD_B  , Keyboard::B },						  // 
		{ PAD_C  , Keyboard::C },						  // 
		{ PAD_D  , Keyboard::D },						  // 
		{ PAD_E  , Keyboard::E },						  // 
		{ PAD_F  , Keyboard::F },						  // 
		{ PAD_G  , Keyboard::G },						  // 
		{ PAD_H  , Keyboard::H },						  // 
		{ PAD_I  , Keyboard::I },						  // 
		{ PAD_J  , Keyboard::J },						  // { PAD_NUM0  , Keyboard::NumPad0 },                // 
		{ PAD_K  , Keyboard::K },						  // 
		{ PAD_L  , Keyboard::L },						  // 
		{ PAD_N  , Keyboard::N },						  // 
		{ PAD_M  , Keyboard::M },						  // 
		{ PAD_O  , Keyboard::O },						  // 
		{ PAD_P  , Keyboard::P },						  // 
		{ PAD_Q  , Keyboard::Q },						  // 
		{ PAD_R  , Keyboard::R },						  // 
		{ PAD_S  , Keyboard::S },						  // { PAD_NUM0  , Keyboard::NumPad0 },                // 
		{ PAD_T  , Keyboard::T },						  // 
		{ PAD_U  , Keyboard::U },						  // 
		{ PAD_V  , Keyboard::V },						  // 
		{ PAD_W  , Keyboard::W },						  // 
		{ PAD_X  , Keyboard::X },						  // 
		{ PAD_Y  , Keyboard::Y },						  // 
		{ PAD_Z  , Keyboard::Z },						  // 
		

		{ PAD_NUM0  , Keyboard::D0 },                // 
		{ PAD_NUM1  , Keyboard::D1 },                // 
		{ PAD_NUM2  , Keyboard::D2 },                // 
		{ PAD_NUM3  , Keyboard::D3 },                // 
		{ PAD_NUM4  , Keyboard::D4 },                // 
		{ PAD_NUM5  , Keyboard::D5 },                // 
		{ PAD_NUM6  , Keyboard::D6 },                // 
		{ PAD_NUM7  , Keyboard::D7 },                // 
		{ PAD_NUM8  , Keyboard::D8 },                // 
		{ PAD_NUM9  , Keyboard::D9 },                // 


        { 0x00, 0x00 }                              // 終了コード
    };

    // ジョイスティック割り当てデータ
    PadAssign joyAssign00[] = {
        { PAD_UP        , GamePad::UP },            // 上キー
        { PAD_DOWN      , GamePad::DOWN },          // 下キー
        { PAD_LEFT    	, GamePad::LEFT },          // 左キー
        { PAD_RIGHT     , GamePad::RIGHT },         // 右キー

        { PAD_START     , GamePad::START },         // スタートボタン
        { PAD_TRG1      , GamePad::A },             // Aボタン
        { PAD_TRG2      , GamePad::B },             // Bボタン
        { PAD_TRG3      , GamePad::X },             // Xボタン
        { PAD_TRG4      , GamePad::Y },             // Yボタン
        { PAD_L1        , GamePad::LSHOULDER },     // Lボタン
        { PAD_R1        , GamePad::RSHOULDER },     // Rボタン

        { PAD_SELECT    , GamePad::BACK },          // バック（セレクト）ボタン

        { 0x00, 0x00 }                              // 終了コード
    };

    // キー割り当てデータ
    PadAssign keyAssign01[] = {
        { PAD_UP        , Keyboard::W },            // VK_W
        { PAD_DOWN      , Keyboard::S },            // VK_S
        { PAD_LEFT      , Keyboard::A },            // VK_A
        { PAD_RIGHT     , Keyboard::D },            // VK_D

        { PAD_TRG1      , Keyboard::J },            // 'J'
        { PAD_TRG2      , Keyboard::K },            // 'K'
        { PAD_TRG3      , Keyboard::L },            // 'L'

        { PAD_SELECT    , Keyboard::F1 },           // VK_F1

//      { PAD_START     , VK_LBUTTON },             // マウス左ボタン
//      { PAD_SELECT    , VK_RBUTTON },             // マウス右ボタン
//      { PAD_TRG1      , VK_MBUTTON },             // マウス中ボタン

        { 0x00, 0x00 }                              // 終了コード
    };

    //--------------------------------
    //  キー割り当ての設定
    //--------------------------------
    void InputManager::setKeyAssign(int no, PadAssign *data)
    {
        pad[no].keyAssign = data;
    }

    //--------------------------------
    //  ジョイスティック割り当ての設定
    //--------------------------------
    void InputManager::setJoyAssign(int no, PadAssign *data)
    {
        pad[no].joyAssign = data;
    }

    //--------------------------------
    //  デストラクタ
    //--------------------------------
    InputManager::~InputManager()
    {
        safe_delete(keyboard);
        safe_delete(gamePad);
    }

    //--------------------------------
    //  初期化
    //--------------------------------
    void InputManager::init()
    {
        keyboard = new DirectX::Keyboard;
        gamePad = new DirectX::GamePad;

        // コントローラ0の設定
        setKeyAssign(0, keyAssign00);
        setJoyAssign(0, joyAssign00);

        // コントローラ1の設定
        setKeyAssign(1, keyAssign01);
        setJoyAssign(1, joyAssign00);

        // コントローラ2の設定
        setJoyAssign(2, joyAssign00);

        // コントローラ3の設定
        setJoyAssign(3, joyAssign00);
    }

    //--------------------------------
    //  入力情報の更新
    //--------------------------------
    void InputManager::update(HWND hwnd)
    {
        // キーボードの状態取得
        DirectX::Keyboard::State kb = keyboard->GetState();
        DirectX::GamePad::State gpad[GAMEPAD_NUM] = {};

        // ゲームパッドの状態取得
        for (int i = 0; i < GAMEPAD_NUM; i++)
            gpad[i] = gamePad->GetState(i);

        // 
        for (int i = 0; i < GAMEPAD_NUM; i++)
        {
            PadState* p = &pad[i];

            int old_state = p->state;

            // pad_stateの更新
            p->state = 0;

            // キーボード・マウス
            PadAssign *assign = p->keyAssign;
            if (assign)
            {
                while (assign->bit)
                {
                    if ((assign->code == VK_LBUTTON) ||
                        (assign->code == VK_RBUTTON) ||
                        (assign->code == VK_MBUTTON))
                    {	// Mouse
                        if (GetAsyncKeyState(assign->code) < 0) p->state |= assign->bit;
                    }
                    else
                    {	// Keyboard
                        if (kb.IsKeyDown((Keyboard::Keys)assign->code)) p->state |= assign->bit;
                    }

                    assign++;
                }
            }

            // ジョイスティック
            assign = p->joyAssign;
            if (assign && gpad[i].IsConnected())
            {
                while (assign->bit)
                {
                    struct hoge_t { bool b[10]; } hoge;
                    if (assign->code >= GamePad::A)
                    {	// buttons
                        hoge = *(hoge_t*)&gpad[i].buttons;
                        if (hoge.b[assign->code - GamePad::A]) p->state |= assign->bit;
                    }
                    else
                    {	// dpad
                        hoge = *(hoge_t*)&gpad[i].dpad;
                        if (hoge.b[assign->code]) p->state |= assign->bit;
                    }

                    assign++;
                }

                // 左スティック
                p->leftX = gpad[i].thumbSticks.leftX;
                p->leftY = gpad[i].thumbSticks.leftY;

                // 右スティック
                p->rightX = gpad[i].thumbSticks.rightX;
                p->rightY = gpad[i].thumbSticks.rightY;

                // 左右トリガー
                p->left = gpad[i].triggers.left;
                p->right = gpad[i].triggers.right;

                // 遊びの設定
                // ※必要があれば設定する
            }

            // p->triggerの更新
            p->trigger = (~old_state) & p->state;

            // p->repeatの計算
            p->repeat = p->state;
            for (int j = 8 - 1; j > 0; --j) {
                p->old[j] = p->old[j - 1];				// データをひとつずらす
                p->repeat &= p->old[j];					// repeatを計算する
            }
            p->old[0] = p->state & (~p->repeat);		// 最新のデータを保存
            p->repeat |= p->trigger;					// 本来のトリガー入力も設定する
        }

        // マウス座標の取得
        GetCursorPos(&mousePos);
        ScreenToClient(hwnd, &mousePos);
    }

    //--------------------------------
    //  マウスカーソルのx座標を取得
    //--------------------------------
    int InputManager::getCursorPosX()
    {
        return mousePos.x;
    }

    //--------------------------------
    //  マウスカーソルのy座標を取得
    //--------------------------------
    int InputManager::getCursorPosY()
    {
        return mousePos.y;
    }

}

//******************************************************************************

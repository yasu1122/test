#pragma once

// ラベル定義
#define BLOCK_MAX		100

// BlockManagerクラス
class BlockManager : public OBJ2DManager, public Singleton<BlockManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return BLOCK_MAX; }
};

#define pBlockManager	(BlockManager::getInstance())

// プロトタイプ宣言
void block_move00(OBJ2D* obj);

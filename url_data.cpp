#include "all.h"


//******************************************************************************
//
//      ステージ１
//
//******************************************************************************

//扉のURLデータ
DoorURLData doorURLData01[] = {
	//{ 扉の座標,　リンク先の要素数,　URL用の要素数 ,スクロール座標 , リスポーン座標の設定用　} 

	{ VECTOR2(NULL , NULL), 0 , 0,VECTOR2(1280.0f,   745.0f) , VECTOR2(200.f,0.0f) },
	{ VECTOR2(NULL , NULL), 5 , 1,VECTOR2(1280.0f,   745.0f) , VECTOR2(200.f,0.0f) },
    { VECTOR2(NULL , NULL), 6 , 2,VECTOR2(1280.0f,   745.0f) , VECTOR2(-200.f,0.0f) },
    { VECTOR2(NULL , NULL), 7 , 3,VECTOR2(1280.0f,   745.0f) , VECTOR2(200.f,0.0f) },
    { VECTOR2(NULL , NULL), 8 , 4,VECTOR2(1280.0f,   745.0f) , VECTOR2(-200.f,0.0f) },
    { VECTOR2(NULL , NULL), 1 , 5,VECTOR2(0.0f   ,   8.0f)   , VECTOR2(200.f,0.0f) },
    { VECTOR2(NULL , NULL), 2 , 6,VECTOR2(2560.0f,   8.0f)   , VECTOR2(200.f,0.0f) },
    { VECTOR2(NULL , NULL), 3 , 7,VECTOR2(0.0f   ,   1480.0f), VECTOR2(200.f,0.0f) },
    { VECTOR2(NULL , NULL), 4 , 8,VECTOR2(2560.0f,   1480.0f), VECTOR2(200.f,0.0f) },
};



//敵のURLデータ				扉の座標,URL名
EnmURLData enmURLData01[] = { 1,1,2 };

//URLデータ配列
URL_DATA stage1_urldata[]={doorURLData01,enmURLData01};

//******************************************************************************
//
//      ステージ２
//
//******************************************************************************



// URLデータ配列のアドレス
URL_DATA* url_data[] = {

	stage1_urldata,
	//stage2_urldata,
	nullptr,
};


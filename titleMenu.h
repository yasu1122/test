#ifndef  _INCLUDE_TITLE_MENU_H
#define  _INCLUDE_TITLE_MENU_H
//******************************************************************************
//
//
//      titleMenu.h
//
//
//******************************************************************************

using namespace GameLib;

//-------------------<定数>-----------------------------------------------------

namespace title_menu
{
    // flgnumber
    enum menuLabel {
        naturalflg  =(0<<0),
        pushOn      =(1<<0),
    };

    // iWorkのラベル
    enum iWork {
        actFlg ,
    };
};

#define TITLE_MENU_MAX     (5)

//==============================================================================
//
//      TitleMenuクラス
//
//==============================================================================

class TitleMenu : public OBJ2DManager, public Singleton<TitleMenu>
{
public:

    void init();    // 初期化
    void update();  // 更　新
    void draw();    // 描　画

    void setStartFlg(bool Flg) { StartFlg = Flg; }
    bool getStartFlg() { return StartFlg; }
    void setExitFlg(bool Flg) { ExitFlg = Flg; }
    bool getExitFlg() { return ExitFlg; }
	
private:

    bool StartFlg;
    bool ExitFlg;
    int getSize() { return TITLE_MENU_MAX; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pTitleMenu  (TitleMenu::getInstance())

// プロトタイプ宣言
void gameStart(OBJ2D* obj);
void exitGame(OBJ2D* obj);

//******************************************************************************
#endif //! _INCLUDE_TITLE_MENU_H


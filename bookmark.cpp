//******************************************************************************
//
//
//      ブックマーククラス
//
//
//******************************************************************************

//------< インクルード >--------------------------------------------------------
#include "all.h"

//------< 定数 >----------------------------------------------------------------
#define FAVORITE_MAX            (5)                                 // お気に入り登録最大数
#define ROOM_NUM_MIN            (0)                                 // 部屋名判別の数値の最低値
#define BOOKMARK_INIT_POS       (VECTOR2(0.0f,0.0f))                // URL初期座標
#define BOOKMARK_SIZE           (VECTOR2(130.0f,25.8f))             // 当たり判定の範囲指定用
#define BOOKMARK_POSY_ADJUST    (64.0f)                             // y座標調整
#define BOOKMARK_COLOR1         (VECTOR4(1.0f, 1.0f, 1.0f, 1.0f))   
#define BOOKMARK_COLOR2         (VECTOR4(0.5f, 0.5f, 0.5f, 1.0f))

//------<変数>------------------------------------------------------------------

SpriteData* roomTextSpriteData[] = {
    &spr_room_name_home,
    &spr_room_name_01,
    &spr_room_name_02,
    &spr_room_name_03,
    &spr_room_name_04,
    &spr_room_name_05,
    &spr_room_name_06,
    &spr_room_name_07,
    &spr_room_name_08,
    &spr_room_name_09,
    &spr_room_name_10,
    &spr_room_name_11,
};

//==============================================================================
//
//      更新アルゴリズム
//
//==============================================================================

// 表示フラグをオン
void viewFlgOn(LIST* list)
{
    using namespace flagFunc;
    
    if (Flag_Check(*list->ipWork[bookmark::ipWork::bookMarkPointa], menu::menuLabel::viewFlg))
    {
        Flg_On(list->iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::viewFlg);
    }
}

// お気に入り登録
void setFavo()
{
    using namespace flagFunc;

    OBJ2D* pPlayer = pPlayerManager->getOBJ2D(0);

    if (pMenuManager->getSetFavoMenu()||pMenuManager->getSetFavoFlg()) {

        // お気に入り登録の数が5以下なら追加する
        if (pBookMarkManager->getList()->size() <= FAVORITE_MAX) {

            //*********お気に入りに必要な情報を登録************

            LIST* list = pBookMarkManager->add(&bookMarkAlg, pMousePointa->getScrollPos(), VECTOR2(0.0f, 0.0f));    // お気に入り登録実体を生成
            list->data = roomTextSpriteData[pPlayer->iWork[player::roomNum]];                                       // 画像
            list->iWork[bookmark::iWork::roomNum] = pPlayer->iWork[player::iWork::roomNum];                         // 部屋番号

            //*************************************************

            pEffectManager->searchSet(effect_move_mark, pMousePointa->getScrollPos().objPos);
            
            // ☆マーク生成
            pBookMarkObj->searchSet(&BkMarkObjMove, pMousePointa->getScrollPos().objPos);
		    
            sound::play(SE::SE_BOOK);
            
            // Favoボタンのお気に入り登録可能フラグをオフ
            pMenuManager->setSetFavoFlg(false);
            pMenuManager->setSetFavoMenu(false);

		    for (auto & pGimmick_i : *pGimmickManager)
		    {
		    	if (pGimmick_i.mover != gimmick_move_clone)continue;

		    	if (pGimmick_i.clone_flg) {

		    		for (auto & pGimmick_j : *pGimmickManager)
		    		{
		    			if (pGimmick_j.mover == gimmick_move_clone_child)return;
		    		}
		    		OBJ2D* child = pGimmickManager->searchSet(gimmick_move_clone_child,pPlayer->position);
		    		child->parent = &pGimmick_i;
		    	}
		    }
		    
            return;
          }
         // お気に入り登録の数が5以上なら追加しない
         else if (pBookMarkManager->getList()->size() > FAVORITE_MAX) {
		    sound::play(SE::SE_FAIL);

            for (auto & pMenu : *pMenuManager)
            {
                if (!pMenu.mover)continue;

                // お気に入り登録不可の表示フラグをオン
                if (pMenu.mover == disenable&&!Flag_Check(pMenu.iWork[menu::iWork::actFlg],menu::menuLabel::disEnableBk))
                    Flg_On(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::disEnableBk);
                pMenuManager->setPause(true);

                // Favoボタンのお気に入り登録可能フラグをオフ
                pMenuManager->setSetFavoFlg(false);
                pMenuManager->setSetFavoMenu(false);

            }
        }
    }
    
}

// ゲーム開始時にプレイヤーの初期座標をお気に入りへ登録
void initFavo()
{
    OBJ2D* pPlayer = pPlayerManager->getOBJ2D(0);

    // プレイヤーの位置情報とスクロール値をマウスポインタに取得
    pMousePointa->setScrollPos(pPlayer->position, pBGManager->getScrollPos());

    LIST* list = pBookMarkManager->add(&bookMarkAlg, pMousePointa->getScrollPos(), BOOKMARK_INIT_POS);
    list->data = roomTextSpriteData[0];
    list->iWork[bookmark::iWork::roomNum] = home;
    pEffectManager->searchSet(effect_move_mark, pMousePointa->getScrollPos().objPos);
    pBookMarkObj->searchSet(&BkMarkObjMove, pMousePointa->getScrollPos().objPos);

    return;

}

// お気に入り登録orブロックリスト登録
void Favo()
{
    using namespace flagFunc;
    using namespace input;

        if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0) {
                setFavo();        // お気に入り登録関数
        }
}


// お気に入りの登録先へジャンプ
void urlJump(LIST* list)
{
    using namespace flagFunc;
    using namespace input;

    OBJ2D* pPl = pPlayerManager->getOBJ2D(0);
    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);

    if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0) {

        if (Flag_Check(pPl->iWork[player::iWork::actflg], player::ActFlg::plNotJump))
        {
            sound::play(SE::SE_FAIL);
            return;
        }

        if (list->iWork[bookmark::iWork::doorLink] >= ROOM_NUM_MIN)
        {

            if (pDeleteMenuManager->getDeleteMenuViewFlg())return;

            sound::play(SE::SE_WOAP);

            // ジャンプ先が現在プレイヤーがいる部屋かどうか判定
            bool same_room_flg = (pPl->iWork[player::iWork::roomNum] == list->iWork[bookmark::iWork::roomNum]) ? true : false;

            // ジャンプ時のプレイヤー硬直の時間をセット
            // 同じ部屋内の移動なら硬直時間はなし
            int pl_jump_state = (same_room_flg) ? 0 : 1;

            // プレイヤーの座標移動関数をセット
            pPlayerManager->plJumpMoveFlgSet(true, list->plScrollPos, PL_JUMP_COUNT_MAX, pl_jump_state);

            //　移動先が同じ部屋でないなら暗転実行
            if (!same_room_flg) pBlackoutManager->searchSet(blackout_move04, BLACK_OUT_INIT_POS);

            for (auto & pMenu : *pMenuManager) {
                All_Flag_Off(pMenu.iWork[menu::iWork::actFlg]);
                Flg_Off(pMouse->iWork[mouse_pointa::iWork::storage], mouse_pointa::clickLabel::storageFlg);
                pMouse->iWork[mouse_pointa::iWork::doorLink] = -1;
            }

        }
    }
}


// リスト削除メニュー表示
void listDeleteOn(LIST* list)
{
    using namespace flagFunc;
    using namespace input;    

    if (list->iWork[bookmark::iWork::doorLink] >= 0 &&
        TRG(0) & PAD_SELECT && (STATE(0) & PAD_SELECT) != 0)
    {

        pDeleteMenuManager->setDeleteMenuViewFlg(true);

    }
}

// ブックマーク更新
void BookMark::update(LIST* list)
{
    using namespace flagFunc;
    using namespace input;

    switch (list->state)
    {
    case 0:
        list->size = BOOKMARK_SIZE;
        for (auto & p : *pMenuManager)
        {
            if (p.mover == okiniiriListTab) {
                list->position.x = p.position.x;
                list->position.y = p.position.y - BOOKMARK_POSY_ADJUST;

                list->fpWork[bookmark::fpWork::posPointaX] = &p.position.x;
                list->fpWork[bookmark::fpWork::posPointaY] = &p.position.y;
                list->ipWork[bookmark::ipWork::bookMarkPointa] = &p.iWork[menu::iWork::actFlg];

            }

        }

        list->state++;

        // break;

    case 1:
        list->color = BOOKMARK_COLOR1;

        if (Flag_Check(*list->ipWork[bookmark::ipWork::bookMarkPointa], menu::menuLabel::viewFlg)) {
            Flg_On(list->iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::viewFlg);
        }
        else {
            Flg_Off(list->iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::viewFlg);
        }

        if (Flag_Check(list->iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::okiniiriFlg)) {
            list->color = BOOKMARK_COLOR2;

            urlJump(list);
            listDeleteOn(list);

        }

        viewFlgOn(list);        // メニューの表示フラグがオンであればリストの表示フラグをオン

        break;
    }
}

//==============================================================================
//
//      消去アルゴリズム
//
//==============================================================================

void EraseBookMark::erase(LIST *list)
{
    if (!list->listAlg)               // 消去用のフラグを入れる
        list->clear();
}

//==============================================================================
//
//       BookMarkManagerクラス
//
//==============================================================================

// 初期化
void BookMarkManager::init()
{

    LISTManager::init();            // LISTManagerの初期化

    setHitFlg(false);

    initFavo();                     // プレイヤーの初期座標をお気に入りへ登録

}

// 更新処理
void BookMarkManager::update() {
    
    int cnt = 0;

    Favo();    // お気に入り登録

    LISTManager::update();      // LISTManagerの更新処理

    // 座標更新 ：リストの登録順に上から並ぶように座標を更新する
    for (auto & p : bookMark) {
            
            p.position.x = *p.fpWork[bookmark::fpWork::posPointaX];
            p.position.y = *p.fpWork[bookmark::fpWork::posPointaY];
            p.position.y += BOOKMARK_POSY_ADJUST;
            p.position.y = p.position.y + BOOKMARK_POSY_ADJUST * cnt;
            cnt++;

    }

    
}

// 描画処理
void BookMarkManager::draw() {

    using namespace flagFunc;

    for (auto & p : bookMark) {
        if (!p.ipWork[bookmark::ipWork::bookMarkPointa])continue;
        if (Flag_Check(*p.ipWork[bookmark::ipWork::bookMarkPointa], menu::menuLabel::viewFlg)) {
            if (p.listAlg) {
                p.data->draw(p.position, p.scale, p.angle, p.color);

            }
        }
    }
}

// 実体生成
LIST* BookMarkManager::add(LISTAlg* bookAlg, JumpData::posandscroll url, VECTOR2 pos, wchar_t* urlText)
{

    LIST* booklist = LISTManager::add(bookAlg, url, pos, urlText);

    return booklist;

}

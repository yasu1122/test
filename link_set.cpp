//******************************************************************************
//
//
//      linkSetクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>


using namespace GameLib;
//-------------------------------------
//      linkPos初期化
//-------------------------------------
void LinkSet::linkClear(DoorURL* url)
{
    url->link = 0;
    url->linkCnt = 0;
    SecureZeroMemory(url->linkPos, sizeof(url->linkPos));
    SecureZeroMemory(url->respawnPos, sizeof(url->respawnPos));
    SecureZeroMemory(url->scrollPos, sizeof(url->scrollPos));
}

//-------------------------------------
//      linkPosを設定
//-------------------------------------
void LinkSet::linkSet(int link, VECTOR2 linkPos)
{
    if (doorURL[link].linkCnt >= DOOR_LINK_MAX)return;

    int linkCnt = doorURL[link].linkCnt;

    //doorURL[link].link = link;
    doorURL[link].linkPos[linkCnt] = linkPos;
    doorURL[link].scrollPos[linkCnt] = setScrollPos(linkPos);
    doorURL[link].respawnPos[linkCnt] = setRespawnPos(linkPos);

    doorURL[link].linkCnt++;

};

//-------------------------------------
//      スクロール用の数値セット
//-------------------------------------
VECTOR2 LinkSet::setScrollPos(VECTOR2 pos)
{
    if (pos.x > SCRL_X_MAX) {
        if (pos.y > SCRL_Y_MAX) {
            return VECTOR2(SCRL_X_MAX, SCRL_Y_MAX);
        }
        if (pos.y > SCRL_Y_MIDDLE1) {
            return VECTOR2(SCRL_X_MAX, SCRL_Y_MIDDLE1);
        }

        if (pos.y > SCRL_Y_MIN) {
            return VECTOR2(SCRL_X_MAX, SCRL_Y_MIN);
        }
    }

    if (pos.x > SCRL_X_MIDDLE2) {
        if (pos.y > SCRL_Y_MAX) {
            return VECTOR2(SCRL_X_MIDDLE2, SCRL_Y_MAX);
        }
        if (pos.y > SCRL_Y_MIDDLE1) {
            return VECTOR2(SCRL_X_MIDDLE2, SCRL_Y_MIDDLE1);
        }
        if (pos.y > SCRL_Y_MIN) {
            return VECTOR2(SCRL_X_MIDDLE2, SCRL_Y_MIN);
        }
    }

    if (pos.x > SCRL_X_MIDDLE1) {
        if (pos.y > SCRL_Y_MAX) {
            return VECTOR2(SCRL_X_MIDDLE1, SCRL_Y_MAX);
        }
        if (pos.y > SCRL_Y_MIDDLE1) {
            return VECTOR2(SCRL_X_MIDDLE1, SCRL_Y_MIDDLE1);
        }
        if (pos.y > SCRL_Y_MIN) {
            return VECTOR2(SCRL_X_MIDDLE1, SCRL_Y_MIN);
        }
    }

    if (pos.x > SCRL_X_MIN) {
        if (pos.y > SCRL_Y_MAX) {
            return VECTOR2(SCRL_X_MIN, SCRL_Y_MAX);
        }

        if (pos.y > SCRL_Y_MIDDLE1) {
            return VECTOR2(SCRL_X_MIN, SCRL_Y_MIDDLE1);
        }

        if (pos.y > SCRL_Y_MIN) {
            return VECTOR2(SCRL_X_MIN, SCRL_Y_MIN);
        }
    }

    return VECTOR2(0.0f, 0.0f);
}

//-------------------------------------
//      リスポーン用の数値をセット
//-------------------------------------

VECTOR2 LinkSet::setRespawnPos(VECTOR2 pos)
{
    for (float x = SCRL_X_MAX; x >= 0.0f; x -= SCRL_X_MIDDLE1) {
        if (pos.x > x)
        {
            VECTOR2 respawnPos = (pos.x < (x + SCRL_X_MIDDLE_HALF)) ? VECTOR2(100.0f, 0.0f) : VECTOR2(-100.0f, 0.0f);
            return respawnPos;
        }
    }

    return VECTOR2(0.0f, 0.0f);
}
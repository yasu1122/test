#ifndef  _INCLUDE_GAME_OVER_MENU_H
#define  _INCLUDE_GAME_OVER_MENU_H
//******************************************************************************
//
//
//      gameoverMenu.h
//
//
//******************************************************************************

using namespace GameLib;

//-------------------<定数>-----------------------------------------------------

namespace game_over_menu
{
    // flgnumber
    enum menuLabel {
        naturalflg = (0 << 0),
        pushOn = (1 << 0),
    };

    // iWorkのラベル
    enum iWork {
        actFlg,
    };
};

#define GAMEOVER_MENU_MAX     (5)

//==============================================================================
//
//      GameOverMenuクラス
//
//==============================================================================

class GameOverMenu : public OBJ2DManager, public Singleton<GameOverMenu>
{
public:

    void init();    // 初期化
    void update();  // 更　新
    void draw();    // 描　画

    void setTitleFlg(bool Flg) { titleFlg = Flg; }
    bool getTitleFlg() { return titleFlg; }
    void setSelectFlg(bool Flg) { selectFlg = Flg; }
    bool getSelectFlg() { return selectFlg; }

private:

    bool titleFlg;
    bool selectFlg;
    int getSize() { return GAMEOVER_MENU_MAX; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pGameOverMenu  (GameOverMenu::getInstance())

// プロトタイプ宣言


//******************************************************************************
#endif //! _INCLUDE_GAME_OVER_MENU_H


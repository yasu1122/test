#ifndef INCLUDED_PLAYER
#define INCLUDED_PLAYER

//******************************************************************************
//
//
//      Player.h
//
//
//******************************************************************************

//-------< 定数 >---------------------------------------------------------------
#define PL_NOCK_BACK_CNT        (10)                            // ノックバック時間
#define PL_NOCK_BACK            (3.0f)                          // ノックバック距離
#define PLAYER_VELOCITY_MIN     (3.0f)                          // 最低移動速度
#define PLAYER_VELOCITY_MAX     (8.0f)                          // 最大移動速度
#define PLAYER_ACCEL            (0.5f)                          // 加速度
#define PLAYER_HP_FIRST         (1)                             // 初期体力
#define PLAYER_HP_MAX	        (2)                             // 体力最大値
#define PLAYER_HP_MIN           (0)                             // 体力最低値
#define PLAYER_STOCK_MAX        (2)                             // 残機最大値
#define PL_JUMP_COUNT_MAX       (30)                            // ドア移動orお気に入りによる移動時の硬直時間設定用
#define PL_JUMP_TIME            (20)                            // 暗転あり時の移動タイミング設定用
#define PL_LIMIT_LEFT           (64.0f)                         // 移動制限左側
#define PL_LIMIT_RIGHT          (64.0f)                         // 移動制限右側
#define PL_LIMIT_UP             (96.0f)                         // 移動制限上側
#define PL_LIMIT_DOWN           (32.0f)                         // 移動制限下側
#define PLAYER_COLOR_NORMAL     (VECTOR4(1.0f,1.0f,1.0f,1.0f))  // 通常時カラーコード
#define PLAYER_COLOR_BARRIER    (VECTOR4(0.0f,1.0f,0.0f,1.0f))  // バリア展開時カラーコード
#define PLAYER_COLOR_DAMAGE     (VECTOR4(1.0f,0.5f,0.5f,0.9f))  // ダメージ時カラーコード
#define PLAEYR_DAMAGE_COLOR_CNT (10)                            // ダメージ時点滅間隔
#define PLAYER_SIZE_START       (VECTOR2(16.0f, 30.0f))         // ゲーム開始演出用当たり判定設定
#define PLAYER_SIZE_NORMAL      (VECTOR2(13.0f, 20.0f))         // 当たり判定設定用
#define PLAYER_SCALE            (VECTOR2(0.25f,0.25f))          // スケール初期値
#define PLAYER_RADIUS           (16.0f)                         // 当たり判定設定用(半径)
#define PLAYER_PASSIVEITEM      (-1)                            // アイテム所持枠初期値
#define PLAYER_MOVE_VEC_INIT    (0.0f)                          // ムーブベクトル初期値
#define PLAYER_ACTSTATE_INIT    (0)                             // アクションステート初期値
#define PLAYER_CLICK_CNT_INIT   (0)                             // clickカウント初期値
#define PLAYER_ANGLE_INIT       (0)                             // アングル初期値
#define PLAYER_COLOR_INIT       (VECTOR4(1.0f,1.0f,1.0f,1.0f))  // 初期カラーコード
#define PLAYER_COLORW_SUB_VALUE (0.02f)                         // 消滅時透明度減算値
#define PLAYER_COLORW_ADD_VALUE (0.03f)                         // 登場時透明度加算値
#define PLAYER_COLORW_MAX       (1.0f)                          // 透明度最大値
#define WALL_PUSH_SPEED         (32.f*0.3f)						// 押され時の速さ

namespace player
{

    // プレイヤー行動分岐フラグ
    enum ActFlg {
        plNatural       = (1<<0),
        plClickMove     = (1<<1),
        plNowMove       = (1<<2),
        pldoorChange    = (1<<3),
        plEnmJump       = (1<<4),
        plEnmErase      = (1<<5),
		plisGimmickWall = (1<<6),
		plisGimmickCell = (1 << 19),
        plNotUndo       = (1<<7),
        plDrag          = (1<<8),
        plDragMove      = (1<<9),
		plIsDamage		= (1<<10),
		plIsKey			= (1<<11),
		plIsKey2		= (1<<15),
        plDead          = (1<<12),
        plNowNockBack   = (1<<13),
        plOnePlaceJump  = (1<<14),
		plIsSuccesPass	= (1<<15),
        plVirusTerrPress= (1<<16),
        plSetFavo       = (1<<17),
        plNotJump       = (1<<18),
    };

	// iWorkのラベル
	enum iWork {
        actflg,
        Cnt,
        clickCnt,
        linkDoor,
        dCnt,
        plStock,
        roomNum,
	};

	// fWorkのラベル
    enum fWork {
        fromPlShotAngle,
        fromPlShotDistance,
        mousePointa,
        plMoveVec_x,
        plMoveVec_y,
        moveEndPos_x,
        moveEndPos_y,
        oldSpeed_x,
        oldSpeed_y,
        hitDoorPosX,
        hitDoorPosY,
        nockBackVecX,
        nockBackVecY,
        plFirstPosX,
        plFirstPosY,
        plFirstScrollPosX,
        plFirstScrollPosY,
	};

    // 暗転セレクトラベル
    enum JumpMoveSelect {
        BLACK_OUT_OFF,
        BLACK_OUT_ON,
    };

    // 関数プロトタイプ宣言
    void PlMoveCancel(OBJ2D* obj);
	void PlMoveCancel2(OBJ2D* obj);
    void PlayerDoorMove(OBJ2D* obj);

}


//==============================================================================
//
//      移動アルゴリズム
//
//==============================================================================


//==============================================================================
//
//      PlayerManagerクラス
//
//==============================================================================
class PlayerManager : public OBJ2DManager, public LinkSet, public Singleton<PlayerManager>
{
public:

	void init();            // 初期化
    void scene_start_init();// 開始演出時初期化

	void update();          // 更新
	
    void draw();            // 描画
    void scene_start_draw();// ゲーム開始演出用描画処理

public:

    void plJumpMove();
    void plJumpMove2();
    void plJumpMove3();

    void plJumMoveSelect();

    void plJumpMoveFlgSet(bool Flg, JumpData::posandscroll posAndScroll, int jumpMoveCnt, int jumpMoveState);
    bool plJumpMoveFlgGet() { return plJumpMoveFlg; }
    int getPlJumpMoveSelect() { return plJumpMoveSelectNum; }

private:

    bool plJumpMoveFlg;
    int plJumpMoveSelectNum;
    int plJumpMoveCnt;
    JumpData::posandscroll movePosAndScroll;

private:

	int getSize() { return 1; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pPlayerManager  (PlayerManager::getInstance())

//------< ワーク用 >-------------------------------------------------------------

#endif // !INCLUDED_PLAYER

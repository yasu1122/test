#ifndef     _LIST_H
#define     _LIST_H
//******************************************************************************
//
//
//      LISTクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <list>
#include "./GameLib/vector.h"
#include "./GameLib/obj2d_data.h"

//==============================================================================
//
//      LISTクラス
//
//==============================================================================

// 前方宣言
class LIST;

namespace JumpData
{
    struct posandscroll
    {
        VECTOR2 objPos;
        VECTOR2 scrollPos;
        int     iWork[16];
        int     getURLCnt;
        int     hp;
    };
};


//==============================================================================

// ブックマーク更新アルゴリズム　(抽象クラス)
class LISTAlg
{
public:
    virtual void update(LIST* list) = 0;
};

class LISTEraseAlg
{
public:
    virtual void erase(LIST* list) = 0;
};

class LIST
{
public:

    GameLib::SpriteData*    data;           // スプライトデータ
    GameLib::Anime          anime;          // アニメーションクラス

    JumpData::posandscroll  plScrollPos;    // プレイヤーポス、スクロールポス

    int                     state;

    VECTOR2                 position;       // 座標

    VECTOR2                 size;           // 当たり用サイズ(縦横)
    VECTOR4                 color;          // カラー情報
    VECTOR2                 scale;

    float                   angle;

    wchar_t*                urlText;        // お気に入りリスト表示時のテキスト

    LISTAlg*                listAlg;        // ブックマーク更新アルゴリズム
    LISTEraseAlg*           listEraseAlg;   // 消去アルゴリズム

    int                     iWork[16];      // 汎用int型配列
    float                   fWork[16];      // 汎用float型配列
    int*                    ipWork[16];     // 汎用int型ポインタ配列
    float*                  fpWork[16];     // 汎用float型ポインタ配列
    

    int                     actFlg;         // アクションフラグ用
    //Text*                   textSprite;           // テキスト

public:
    LIST();                                 // コンストラクタ
    void                    clear();        // メンバのクリア関数
    void                    update();       // 更新
    void                    draw();         // 描画
    void not_scroll_draw();                 // スクロール無し描画

};

//==============================================================================
//
//      LISTManagerクラス
//
//==============================================================================

class LISTManager
{
public:
    std::list<LIST>  bookMark;       // ブックマークのリスト

public:
    LISTManager() {};
    ~LISTManager() {};

    void init();                    // 初期化
    void update();                  // 更新
    void draw();                    // 描画
    void not_scroll_draw();         // スクロール無し描画

    // bookMarkに新しく追加
    LIST* add(LISTAlg* bookAlg, JumpData::posandscroll url, VECTOR2  pos, wchar_t* urlText);
    
    // bookMarkを取得する
    std::list<LIST>* getList() { return &bookMark; }

};

#endif      //!_LIST_H

#include "all.h"

using namespace GameLib;
	
#define MSG_WINDOW_FRAME_MERGIN			(25.0f)			
#define MSG_WINDOW_SPEAKER_MERGIN		(50.0f)
#define MSG_WINDOW_SCALE				(0.9f)

// メッセージウィンドウのフレーム画像データ
WINDOW_SPR_DATA winSprData_tbl[] =
{
	{ &spr_messageWindow, VECTOR2(MSG_WINDOW_FRAME_MERGIN, MSG_WINDOW_FRAME_MERGIN), MSG_WINDOW_SPEAKER_MERGIN },

	{ nullptr,{}, -1 },
};


// 
void MsgWindowMove(OBJ2D* obj)
{
	int & messageCnt = obj->iWork[messageWindow::iWork::messageCnt];
	int & messageNum = obj->iWork[messageWindow::iWork::messageNum];
	int & messageOldNum = obj->iWork[messageWindow::iWork::messageOldNum];

	float & txtMerginX = obj->fWork[messageWindow::fWork::txtMerginX];
	float & txtMerginY = obj->fWork[messageWindow::fWork::txtMerginY];
	float & txtStartPosX = obj->fWork[messageWindow::fWork::txtStartPosX];
	float & txtStartPosY = obj->fWork[messageWindow::fWork::txtStartPosY];
	
	using namespace input;

	switch (obj->state)
	{
	case 0:
		// スプライトデータ設定
		obj->data = winSprData_tbl[0].winSpr;				
		
		obj->cnt = 0;
		
		messageCnt = 0;
		
		messageOldNum = messageNum;


		// 位置設定
		obj->position = msgStage_tbl[STAGE->getStageNo()][messageNum].pos;

		// 中心から端までの幅
		obj->size = VECTOR2(obj->data->width / 2, obj->data->height / 2);
		
		obj->scale = VECTOR2(MSG_WINDOW_SCALE, MSG_WINDOW_SCALE);

		// テキストの描画可能幅
		txtMerginX = (obj->size.x * 2 - winSprData_tbl->texMergin.x * 2) * obj->scale.x;
		txtMerginY = obj->size.y * 2 - winSprData_tbl->texMergin.y * 2 - winSprData_tbl->speakerMergin;

		// テキストの描画開始位置
		txtStartPosX = obj->position.x - (obj->size.x - winSprData_tbl->texMergin.x) * obj->scale.x;
		txtStartPosY = obj->position.y - (obj->size.y - winSprData_tbl->texMergin.y - winSprData_tbl->speakerMergin)* obj->scale.y;
	

		obj->state++;
		//break;
	case 1:
		// timer代わり
		obj->cnt++;
	
		//=== <左クリックでメッセージを進める> ==========================================
		// 

		using namespace flagFunc;


		// 今の難易度に応じた部屋番号の[messageCnt / 2番目]のメッセージが存在するなら 
		if (msgStage_tbl[STAGE->getStageNo()][messageNum].pMessage[messageCnt / 2]) 
		{
			// 左クリックしたとき
			if (TRG(0) & PAD_START) {
				// メッセージが最後まで描画されたら
				if (pMessageWinManager->m_msgText.strEnd)
				{
					// メッセージの描画位置を最初まで戻す
					pMessageWinManager->m_msgText.CntReset();
					
					if (messageCnt % 2 == 0) 
					{
						messageCnt++;
					}
				}
				messageCnt++;
			}
		}

		// 部屋移動したときの処理
		if (messageOldNum != messageNum) {

			msgStage_tbl[STAGE->getStageNo()][messageOldNum].read = true;

			messageOldNum = messageNum;

			messageCnt = 0;
		}

		break;
	}
}



void MessageWinManager::init()
{
	OBJ2DManager::init();

	for (int i = 0; ; ++i)
	{
		if (msgStage_tbl[STAGE->getStageNo()][i].messageNum == MSG_NONE) break;

		msgStage_tbl[STAGE->getStageNo()][i].read = false;
	}

	begin()->mover = MsgWindowMove;
}

void MessageWinManager::update()
{
	OBJ2DManager::update();
}

void MessageWinManager::draw_not_scroll()
{

	float & txtMerginX = begin()->fWork[messageWindow::fWork::txtMerginX];
	float & txtMerginY = begin()->fWork[messageWindow::fWork::txtMerginY];
	float & txtStartPosX = begin()->fWork[messageWindow::fWork::txtStartPosX];
	float & txtStartPosY = begin()->fWork[messageWindow::fWork::txtStartPosY];
	int & messageCnt = begin()->iWork[messageWindow::iWork::messageCnt];
	int & messageNum = begin()->iWork[messageWindow::iWork::messageNum];

	// 既読なら描画しない
	if (msgStage_tbl[STAGE->getStageNo()][messageNum].read) return;

	// メッセージが終了したら描画しない
	if (msgStage_tbl[STAGE->getStageNo()][messageNum].pMessage[messageCnt / 2] == NULL) return;

	// ウィンドウ描画
	OBJ2DManager::draw_not_scroll();

	// 
	m_nameText.Render(L"チュートリアル", VECTOR2(txtStartPosX, txtStartPosY - winSprData_tbl[0].speakerMergin * begin()->scale.y), begin()->scale, VECTOR4(1, 1, 1, 1));

	//
	if (!(messageCnt % 2))
		m_msgText.Render(msgStage_tbl[STAGE->getStageNo()][messageNum].pMessage[messageCnt / 2], VECTOR2(txtStartPosX, txtStartPosY), begin()->scale, VECTOR4(1, 1, 1, 1), begin()->cnt,txtMerginX, txtMerginY);
	else
		m_msgText.Render(msgStage_tbl[STAGE->getStageNo()][messageNum].pMessage[messageCnt / 2], VECTOR2(txtStartPosX, txtStartPosY), begin()->scale, VECTOR4(1, 1, 1, 1), 0, txtMerginX, txtMerginY);
}

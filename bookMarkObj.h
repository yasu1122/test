#ifndef     _BOOK_MARK_OBJ_H
#define     _BOOK_MARK_OBJ_H
//******************************************************************************
//
//
//      ☆クラス
//
//
//******************************************************************************


#define BK_OBJ_MAX      (10)

// 更新アルゴリズムの実体
void BkMarkObjMove(OBJ2D* obj);

class BookMarkObj : public OBJ2DManager, public Singleton<BookMarkObj>
{
public:

    void init();
    void update();
    void draw();

    void searchErase(const VECTOR2 pos);

private:
    int getSize() { return BK_OBJ_MAX; }

};

//------< インスタンス取得 >-----------------------------------------------------

static BookMarkObj* const pBookMarkObj = BookMarkObj::getInstance();


//******************************************************************************
#endif //!_BOOK_MARK_OBJ_H

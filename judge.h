#ifndef  _INCLUDE_JUDGE
#define  _INCLUDE_JUDGE
//******************************************************************************
//
//
//      judge.h
//
//
//******************************************************************************

using namespace GameLib;

//-------------------<定数>-----------------------------------------------------

//=============================================================================
//
//      レクト当たり判定クラス
//
//=============================================================================

//------------------------
//  Judgeクラス
//------------------------
class Judge
{
protected:
    float left, top, right, bottom;

public:
    Judge() : left(0), top(0), right(0), bottom(0) {}

    void set(const VECTOR2& pos, const VECTOR2& size);
	void setThis(const fRECT& in);
    void set(const VECTOR2& pos, const fRECT& in);
	void setCenter(const VECTOR2& pos, const fRECT& in);
    void setBottom(const VECTOR2& pos, const VECTOR2& size);

    bool hitcheck(const Judge* other);
};

//------------------------
//  JudgeManagerクラス
//------------------------
class JudgeManager
{
public:
    virtual void update() = 0;
	bool isHitPl(OBJ2D* obj);
	bool isHitPlVirus(OBJ2D* obj);

protected:
    // OBJ2D 当たり判定
	bool isHitPl2(OBJ2D* obj);
	bool isHitPl3(OBJ2D* obj);
	bool isHitPl4(OBJ2D* obj);
	bool isHitPl5(OBJ2D* obj);

    bool isHitPoint(OBJ2D* obj);
    bool isHitPoint2(OBJ2D* obj);

    // LIST 当たり判定
    bool isHitList(LIST* list);
};

//----------------------------
//  JudgeStageManagerクラス
//----------------------------
class JudgeStageManager :public JudgeManager ,public Singleton<JudgeStageManager>
{
public:
    void update();
};

//------< インスタンス取得 >-----------------------------------------------------
#define pJudgeStageManager  (JudgeStageManager::getInstance())

//----------------------------
//  JudgeTitleManagerクラス
//----------------------------
class JudgeTitleManager : public JudgeManager, public Singleton<JudgeTitleManager>
{
public:
    void update();
};

//------< インスタンス取得 >-----------------------------------------------------
#define pJudgeTitleManager  (JudgeTitleManager::getInstance())

//----------------------------
//  JudgeSceneStartManagerクラス
//----------------------------
class JudgeSceneStartManager : public JudgeManager, public Singleton<JudgeSceneStartManager>
{
public:
    void update();
};

//------< インスタンス取得 >-----------------------------------------------------
#define pJudgeSceneStartManager  (JudgeSceneStartManager::getInstance())

//----------------------------
//  JudgeSceneSelectManagerクラス
//----------------------------
class JudgeSceneSelectManager : public JudgeManager, public Singleton<JudgeSceneSelectManager>
{
public:
    void update();
};

//------< インスタンス取得 >-----------------------------------------------------
#define pJudgeSceneSelectManager  (JudgeSceneSelectManager::getInstance())


//----------------------------
//  JudgeGameOverManagerクラス
//----------------------------
class JudgeGameOverManager : public JudgeManager, public Singleton<JudgeGameOverManager>
{
public:
    void update();
};

//------< インスタンス取得 >-----------------------------------------------------
#define pJudgeGameOverManager  (JudgeGameOverManager::getInstance())


//******************************************************************************

#endif //!_INCLUDE_JUDGE
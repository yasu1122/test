//******************************************************************************
//
//
//      LISTクラス
//
//
//******************************************************************************

//------< インクルード >--------------------------------------------------------

#include "all.h"

using namespace GameLib;
using namespace input;

//==============================================================================
//
//      LISTクラス
//
//==============================================================================

//--------------------------------
//  コンストラクタ
//--------------------------------
LIST::LIST()
{
    clear();

    color = VECTOR4(1, 1, 1, 1);    // 初期化忘れが無いように（不透明度ゼロなら何も表示されない）
    scale = VECTOR2(1, 1);
}

//--------------------------------
//  メンバ変数のクリア
//--------------------------------
void LIST::clear()
{
    data = nullptr;
    SecureZeroMemory(&anime, sizeof(anime));

    state = 0;

    plScrollPos = {};
    position = {};

    size = {};
    color = {};
    scale = {};

    angle = 0.0f;

    urlText = nullptr;

    listAlg = nullptr;
    listEraseAlg = nullptr;

    actFlg = 0;
    //textSprite = nullptr;

    SecureZeroMemory(iWork, sizeof(iWork));
    SecureZeroMemory(fWork, sizeof(fWork));
    SecureZeroMemory(ipWork, sizeof(ipWork));
    SecureZeroMemory(fpWork, sizeof(fpWork));

}

//--------------------------------
//  更新処理
//--------------------------------
void LIST::update()
{
    if (listAlg) listAlg->update(this);             // ブックマーク更新処理

    if (listEraseAlg) listEraseAlg->erase(this);    // 消去アルゴリズム
}

//--------------------------------
//  描画
//--------------------------------
void LIST::draw()
{
    if (data)
    {
        data->draw(position - pBGManager->getScrollPos(), scale, angle, color);
    }
}

void LIST::not_scroll_draw()
{
    if (data)
    {
        data->draw(position, scale, angle, color);
    }
}

//==============================================================================
//
//      LISTManagerクラス
//
//==============================================================================

//--------------------------------
//  初期化
//--------------------------------
void LISTManager::init()
{

    // リストのBOOKMARKをすべてクリアする
    bookMark.clear();


}

//--------------------------------
//  リストへ追加
//--------------------------------
LIST* LISTManager::add(LISTAlg* listAlg, JumpData::posandscroll url, VECTOR2 pos, wchar_t* urlText = L" ")
{
    LIST urlLink;
    urlLink.listAlg = listAlg;
    urlLink.plScrollPos = url;
    urlLink.urlText = urlText;
    urlLink.position = pos;

    bookMark.push_back(urlLink);     // リストにurlを追加する

    return &bookMark.back();
}

bool predFunc_(const LIST & list)
{
    if (list.listAlg == nullptr) return true;
    return false;
}

//--------------------------------
//  更新処理
//--------------------------------
void LISTManager::update()
{
    // 更新
    for (auto & it : bookMark)
    {
        it.update();
    }

    // 空ノードの削除(bookAlgがnullptrの要素を削除している)
    bookMark.erase(
        remove_if(bookMark.begin(), bookMark.end(),
            [](const LIST& list) {
                if (list.listAlg == nullptr)return true;
                return false;
            }),
        bookMark.end()
    );

}

//--------------------------------
//  描画
//--------------------------------
void LISTManager::draw()
{
    for (auto& it : bookMark)            // objListの全ての要素をループし、itという名前で各要素にアクセス 
    {
        if (!it.listAlg)continue;
        it.draw();                       // itのdrawメソッドを呼ぶ
    }
}

void LISTManager::not_scroll_draw()
{
    for (auto & it : bookMark)
    {
        if (!it.listAlg)continue;
        it.not_scroll_draw();
    }
}




//******************************************************************************

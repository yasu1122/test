#include"all.h"

#include <string>
#include <bitset>












using namespace GameLib;
void gimmick_move_num00(OBJ2D*obj);
void gimmick_move_num01(OBJ2D*obj);
void gimmick_move_num02(OBJ2D*obj);
void gimmick_move_num03(OBJ2D*obj);
void gimmick_move_num04(OBJ2D*obj);
void gimmick_move_shutter00(OBJ2D*obj);
void gimmick_move_shutter01(OBJ2D*obj);
void gimmick_move_shutter02(OBJ2D*obj);

MOVER GimmickMoveArray[] = { gimmick_move_rb_switch ,gimmick_move_down_switch ,gimmick_move_down_child,gimmick_move_inverce ,gimmick_move_password,gimmick_move_clone,gimmick_move_inverce2,gimmick_move_clone,gimmick_move_num00,gimmick_move_num01,gimmick_move_num02,gimmick_move_num03,gimmick_move_num04,gimmick_move_shutter00 ,gimmick_move_shutter01 ,gimmick_move_shutter02 };

namespace
{
	//------< プレイヤーのアニメデータ >------------------------------------------
	//上方向
	AnimeData animeClone[] = {
		{ &spr_player03, 15 },
		{ &spr_player02, 15 },
		{ &spr_player01, 15 },
		{ &spr_player00, 15 },
		{ &spr_player01, 15 },
		{ &spr_player02, 15 },
		{ &spr_player03, 15 },
		{ &spr_player00, 15 },

		{ nullptr, -1 },// 終了フラグ
	};

}



#define GIMMICK_RB_SWITCH_SIZEX			40.0f
#define GIMMICK_RB_SWITCH_SIZEY			16.0f
#define GIMMICK_RB_SWITCH_SCLAE			0.2f
#define GIMMICK_DOWN_SWITCH_SIZEX		20.0f
#define GIMMICK_DOWN_SWITCH_SIZEY		20.0f
#define GIMMICK_DOWN_SWITCH_SCLAE		0.5f
#define GIMMICK_SHUTTER_NUM				3 //通り抜けシャッターの数
#define GIMMICK_SHUTTER_SCALE_MAX		32
#define GIMMICK_SHUTTER_SPEED			0.2f//シャッタースピード
#define GIMMICK_SHUTTER_SPEED2			0.16f//通り抜けシャッタースピード
#define GIMMICK_INVERCE_SIZEY			64.0f
#define GIMMICK_ADJUST					16



bool isAllShutterSucces()
{

	if (pGimmickManager->succes_cnt < 0)return true;
	return false;

}

//スイッチその1
void gimmick_move_rb_switch(OBJ2D*obj)
{
	using namespace flagFunc;

	switch (obj->state)
	{
	case 0:

		//スイッチフラグON
		obj->type = gimmick::Type::SWITCH;
		obj->data = &spr_bule_red_switch01;
		obj->scale *= GIMMICK_RB_SWITCH_SCLAE;
		obj->size = VECTOR2(GIMMICK_RB_SWITCH_SIZEX, GIMMICK_RB_SWITCH_SIZEY);
		obj->state++;
		//break;
	case 1:

		//切り替えフラグが立っていれば画像を切り替え
		if (pGimmickTerrManager->change_flg)
			obj->data = &spr_bule_red_switch00;
		else
			obj->data = &spr_bule_red_switch01;

		break;
	}
	//反転
	obj->OBJ2D::PosInverce(obj);

}


//シャッター
void gimmick_move_down_switch(OBJ2D*obj)
{
	using namespace flagFunc;

	switch (obj->state)
	{
	case 0:

		//スイッチフラグON
		obj->type = gimmick::Type::SWITCH;
		obj->data = &spr_shutter_switch00;
		obj->scale *= GIMMICK_DOWN_SWITCH_SCLAE;
		obj->size = VECTOR2(GIMMICK_DOWN_SWITCH_SIZEX, GIMMICK_DOWN_SWITCH_SIZEY);
		obj->state++;
		//break;
	case 1:

		//切り替えフラグが立っていれば画像を切り替え
		if (pGimmickManager->shutter_move_flg)
			obj->data = &spr_shutter_switch01;
		else
			obj->data = &spr_shutter_switch00;

		break;
	}
	//反転
	obj->OBJ2D::PosInverce(obj);

}


//通り抜けシャッターその1
void gimmick_move_shutter00(OBJ2D*obj)
{


	switch (obj->state)
	{
	case 0:
		obj->data = &spr_gimmick_wall00;
		obj->scale.x = 1.0f;
		obj->scale.y = 1.0f;
		obj->color = VECTOR4(1, 1, 1, 1);
		obj->judgeFlag = true;
		obj->type = gimmick::Type::SHUTTER;
		obj->hitRect = { GIMMICK_SHUTTER_SCALE_MAX/2,GIMMICK_SHUTTER_SCALE_MAX*(GIMMICK_SHUTTER_SCALE_MAX/2),GIMMICK_SHUTTER_SCALE_MAX/2,0};
		obj->flg = false;

		pGimmickManager->succes_cnt = GIMMICK_SHUTTER_NUM;
		obj->state++;
		//break;
	case 1:
		obj->size = VECTOR2(16 * obj->scale.x, GIMMICK_SHUTTER_SCALE_MAX * obj->scale.y);

		if (obj->flg)
			obj->scale.y += GIMMICK_SHUTTER_SPEED2;

		if (obj->scale.y > GIMMICK_SHUTTER_SCALE_MAX) {
			obj->scale.y = GIMMICK_SHUTTER_SCALE_MAX;
			pGimmickManager->succes_cnt--;


			//順番の正誤判定
			if (pGimmickManager->succes_cnt == GIMMICK_SHUTTER_NUM-1)
			{
				pGimmickManager->succes_cnt--;
				sound::play(SE::SE_SUCCES);
				obj->state++;
			}
			else
			{
				obj->state += 2;
				sound::play(SE::SE_FAIL);

			}
		}

	case 2://正解ステイト
		obj->color = VECTOR4(1, 1, 1, 1);

		break;



	case 3://不正解ステイト
		obj->color = VECTOR4(0, 0, 0, 1);

		break;
	}
}
//通り抜けシャッターその2
void gimmick_move_shutter01(OBJ2D*obj)
{


	switch (obj->state)
	{
	case 0:
		obj->data = &spr_gimmick_wall00;
		obj->scale.x = 1.0f;
		obj->judgeFlag = true;
		obj->type = gimmick::Type::SHUTTER;
		obj->scale.y = 1.0f;
		obj->color = VECTOR4(1, 1, 1, 1);
		obj->hitRect = { GIMMICK_SHUTTER_SCALE_MAX / 2,GIMMICK_SHUTTER_SCALE_MAX * 16,GIMMICK_SHUTTER_SCALE_MAX / 2,0};
		obj->flg = false;

		obj->state++;
		//break;
	case 1:
		obj->size = VECTOR2(GIMMICK_SHUTTER_SCALE_MAX / 2 * obj->scale.x, GIMMICK_SHUTTER_SCALE_MAX * obj->scale.y);

		if (obj->flg)
			obj->scale.y += GIMMICK_SHUTTER_SPEED2;

		if (obj->scale.y > GIMMICK_SHUTTER_SCALE_MAX) {
			obj->scale.y = GIMMICK_SHUTTER_SCALE_MAX;


			//順番の正誤判定
			if (pGimmickManager->succes_cnt == GIMMICK_SHUTTER_NUM-2)
			{
				pGimmickManager->succes_cnt--;
				sound::play(SE::SE_SUCCES);
				obj->state++;
			}
			else
			{
				obj->state += 2;
				sound::play(SE::SE_FAIL);

			}
		}

	case 2://正解ステイト
		obj->color = VECTOR4(1, 1, 1, 1);

		break;



	case 3://不正解ステイト
		obj->color = VECTOR4(0, 0, 0, 1);

		break;
	}

}
//通り抜けシャッターその3
void gimmick_move_shutter02(OBJ2D*obj)
{


	switch (obj->state)
	{
	case 0:
		obj->data = &spr_gimmick_wall00;
		obj->scale.x = 1.0f;
		obj->flg = true;
		obj->judgeFlag = true;
		obj->type = gimmick::Type::SHUTTER;
		obj->scale.y = 1.0f;
		obj->color = VECTOR4(1, 1, 1, 1);
		obj->hitRect = { GIMMICK_SHUTTER_SCALE_MAX / 2,GIMMICK_SHUTTER_SCALE_MAX * 16,GIMMICK_SHUTTER_SCALE_MAX / 2,0};
		obj->flg = false;

		obj->state++;
		//break;
	case 1:
		obj->size = VECTOR2(GIMMICK_SHUTTER_SCALE_MAX / 2 * obj->scale.x, GIMMICK_SHUTTER_SCALE_MAX * obj->scale.y);


		if (obj->flg)
			obj->scale.y += GIMMICK_SHUTTER_SPEED2;

		if (obj->scale.y > GIMMICK_SHUTTER_SCALE_MAX) {
			obj->scale.y = GIMMICK_SHUTTER_SCALE_MAX;

			//順番の正誤判定
			if (pGimmickManager->succes_cnt == GIMMICK_SHUTTER_NUM-3)
			{
				pGimmickManager->succes_cnt--;
				sound::play(SE::SE_SUCCES);
				obj->state++;
			}
			else
			{
				obj->state += 2;
				sound::play(SE::SE_FAIL);

			}
		}

	case 2://正解ステイト
		obj->color = VECTOR4(1, 1, 1, 1);

		break;



	case 3://不正解ステイト
		obj->color = VECTOR4(0, 0, 0, 1);

		break;
	}


}

void gimmick_move_down_child(OBJ2D*obj)
{
	using namespace flagFunc;
	switch (obj->state)
	{
	case 0:
		obj->data = &spr_gimmick_wall00;
		obj->scale.x = 1.0f;
		obj->judgeFlag = true;
		obj->type = gimmick::Type::SHUTTER;

		obj->state++;
		//break;
	case 1:
		obj->size = VECTOR2(16 * obj->scale.x, GIMMICK_SHUTTER_SCALE_MAX * obj->scale.y);



		if (pGimmickManager->shutter_move_flg)
			obj->scale.y -= GIMMICK_SHUTTER_SPEED;
		else
			obj->scale.y += GIMMICK_SHUTTER_SPEED;

		if (obj->scale.y > GIMMICK_SHUTTER_SCALE_MAX)
			obj->scale.y = GIMMICK_SHUTTER_SCALE_MAX;

		if (obj->scale.y < 0) {
			obj->scale.y = 0;
			obj->scale.x = 0;
		}
		else
		{
			obj->scale.x = 1;
		}


	
		break;
	}

}

//反転
void gimmick_move_inverce(OBJ2D*obj)
{
	VECTOR2 size = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2);

	using namespace flagFunc;

	switch (obj->state)
	{
	case 0:

		
		obj->data = &spr_reverse_bar;
		obj->size = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2);
		obj->position.x += GIMMICK_ADJUST;
		obj->hitRect = { size.x, GIMMICK_INVERCE_SIZEY, size.x, GIMMICK_INVERCE_SIZEY };
		obj->state++;
		//break;
	case 1:






		break;
	}


}

void gimmick_move_inverce2(OBJ2D*obj)
{
	VECTOR2 size = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2);

	using namespace flagFunc;

	switch (obj->state)
	{
	case 0:
		//スイッチフラグON
		obj->data = &spr_reverse_bar2;
		obj->angle += ToRadian(90);
		obj->size = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2);
		obj->hitRect = { GIMMICK_INVERCE_SIZEY,size.y, GIMMICK_INVERCE_SIZEY ,size.y };
		obj->state++;
		//break;
	case 1:






		break;
	}


}

int num_bit_data[] = {
	input::PAD_NUM0,
	input::PAD_NUM1,
	input::PAD_NUM2,
	input::PAD_NUM3,
	input::PAD_NUM4,
	input::PAD_NUM5,
	input::PAD_NUM6,
	input::PAD_NUM7,
	input::PAD_NUM8,
	input::PAD_NUM9,
};

namespace
{

	//------< プレイヤーのアニメデータ >------------------------------------------
	//上方向
	AnimeData animeWindow[] = {

		{ &spr_pass_window00, 30 },
		{ &spr_pass_window01, 30 },
		{ nullptr, -1 },// 終了フラグ
	};

}

#define PASS_MAX 4
#define PASS_NUM_ALPHA 0.7f

void gimmick_move_password(OBJ2D*obj)
{

	using namespace flagFunc;
	using namespace input;
	OBJ2D* pl = pPlayerManager->getOBJ2D(0);

	if (!pGimmickTerrManager->spawn_window_flg)return;

	AnimeData* animeData = nullptr;
	animeData = animeWindow;

	switch (obj->state)
	{
	case 0:
		obj->data = &spr_pass_window00;
		obj->cnt = 0;
		pGimmickManager->GimmickManager::ostr.str("");//文字列初期化
		pGimmickManager->GimmickManager::ostr.clear(std::stringstream::goodbit);
		pGimmickTerrManager->success_flg=false;//正解フラグをオフ
		obj->state++;


		//break;
	case 1:
		pGimmickTerrManager->success_flg = false;//正解フラグをオフ
		//0~9まで
		for (int i = 0; i < 10; i++)
		{
			if (TRG(0)&num_bit_data[i] && obj->cnt < PASS_MAX)
			{
				pGimmickManager->GimmickManager::ostr << i;//文字列入力
				obj->cnt++;
			}
		}
		if (TRG(0)&PAD_TRG3)
		{
			obj->cnt = 0;
			obj->state++;
		}

		if (obj->cnt >= PASS_MAX)obj->animeUpdate(animeData);

		break;

		//正誤判定
	case 2:

		//正解してたら
		if (pGimmickManager->GimmickManager::ostr.str() == "0523")
		{
			obj->data = &spr_maru;
			obj->state++;
			sound::play(SE::SE_SUCCES);
			pGimmickTerrManager->success_flg = true;//正解フラグを立てる
		}
		else
		{

			sound::play(SE::SE_FAIL);
			obj->data = &spr_batsu;
			//文字列初期化
			pGimmickManager->GimmickManager::ostr.str("");
			pGimmickManager->GimmickManager::ostr.clear(std::stringstream::goodbit);
			//ウインドウを消す
			pGimmickTerrManager->spawn_window_flg = false;
			pGimmickTerrManager->success_flg = false;//正解フラグをオフ
			obj->state = 0;
		}

		break;

	
	}


}

#define CLONE_WALL_SCALEX  0.3f 
#define CLONE_WALL_SCALEY  0.3f


//分身部屋判定（横に分身生成)
void gimmick_move_clone(OBJ2D*obj)
{

	using namespace flagFunc;

	switch (obj->state)
	{
	case 0:

		//部屋全体と当たり判定
		obj->size = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2);
		obj->scale = VECTOR2(CLONE_WALL_SCALEX, CLONE_WALL_SCALEY);
		obj->data = &spr_mirror_wall;
		obj->state++;
		//break;
	case 1:






		break;
	}


}


//パスワード配置用
void gimmick_move_num00(OBJ2D*obj)
{

	obj->data = &spr_pass00;
	obj->color.w += PASS_NUM_ALPHA;
}
void gimmick_move_num01(OBJ2D*obj)
{

	obj->data = &spr_pass01;
	obj->color.w = PASS_NUM_ALPHA;

}
void gimmick_move_num02(OBJ2D*obj)
{

	obj->data = &spr_pass02;
	obj->color.w = PASS_NUM_ALPHA;

}
void gimmick_move_num03(OBJ2D*obj)
{

	obj->data = &spr_pass03;
	obj->color.w = PASS_NUM_ALPHA;

}
void gimmick_move_num04(OBJ2D*obj)
{

	obj->data = &spr_pass04;
	obj->color.w = PASS_NUM_ALPHA;

}


//クローン移動関数（プレイヤーとほぼ同じ）
void gimmick_move_clone_child(OBJ2D*obj)
{
	AnimeData* animeData = nullptr;
	OBJ2D*pl = pPlayerManager->getOBJ2D(0);
	switch (obj->state)
	{
	case 0:
		obj->size = PLAYER_SIZE_NORMAL;
		obj->scale = PLAYER_SCALE;
		obj->position.x += obj->parent->clone_pos * 2;
		obj->state++;
		//break;
	case 1:
		animeData = animeClone;

		obj->animeUpdate(animeData);


		obj->angle = -pl->angle;
		if (!obj->parent->clone_flg)
			obj->clear();


		float oldX = obj->position.x;
		float oldY = obj->position.y;



		obj->position.x += -pl->speed.x;
		float deltaX = obj->position.x - oldX;

		// 地形あたり判定
		if (deltaX > 0)
		{
			if (pBGManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
			{
				pBGManager->mapHoseiRight(obj); // 右方向の補正処理
				obj->kabeFlag = true;           // 壁フラグをtrueにしておく
			}
			if (pGimmickTerrManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
			{
				pGimmickTerrManager->mapHoseiRight(obj); // 右方向の補正処理
				obj->kabeFlag = true;           // 壁フラグをtrueにしておく
			}
			if (pBGManager->isWall2(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
			{
				pBGManager->mapHoseiRight2(obj); // 右方向の補正処理
				obj->kabeFlag = true;           // 壁フラグをtrueにしておく
			}
			if (flagFunc::Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plisGimmickWall))
			{
				obj->position.x = obj->target->position.x - obj->target->size.x - obj->size.x - GIMMICK_ADJUST_X;
				obj->kabeFlag = true;
			}


		}
		if (deltaX < 0)
		{
			if (pBGManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
			{

				pBGManager->mapHoseiLeft(obj);
				obj->kabeFlag = true;
			}
			if (pGimmickTerrManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
			{

				pGimmickTerrManager->mapHoseiLeft(obj);
				obj->kabeFlag = true;
			}
			if (pBGManager->isWall2(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
			{
				pBGManager->mapHoseiLeft2(obj);
				obj->kabeFlag = true;
			}
			if (flagFunc::Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plisGimmickWall))
			{

				obj->position.x = obj->target->position.x + obj->target->size.x + obj->size.x + GIMMICK_ADJUST_X;
				obj->kabeFlag = true;
			}
		}

		obj->position.y += pl->speed.y;// 位置更新
		float deltaY = obj->position.y - oldY;  // 移動後の位置から移動前の位置を引く

		if (deltaY > 0)
		{   // 下方向チェック
			if (pBGManager->isFloor(obj->position.x, obj->position.y, obj->size.x))
			{
				// 床にあたっていたら
				pBGManager->mapHoseiDown(obj);  // 下方向の補正処理

			}
			if (pGimmickTerrManager->isFloor(obj->position.x, obj->position.y, obj->size.x))
			{
				// 床にあたっていたら
				pGimmickTerrManager->mapHoseiDown(obj);  // 下方向の補正処理

			}

			if (pBGManager->isFloor2(obj->position.x, obj->position.y, obj->size.x))
			{
				// 床にあたっていたら
				pBGManager->mapHoseiDown2(obj);  // 下方向の補正処理
				obj->onGround = true;           // 地面フラグ
			}


		}


		if (deltaY < 0)
		{   // 上方向チェック
			if (pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
			{
				pBGManager->mapHoseiUp(obj);    // 上方向の補正処理
				obj->jumpTimer = 0;             // 天井にぶつかったため、それ以上ジャンプできない
			}

			if (pGimmickTerrManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
			{
				pGimmickTerrManager->mapHoseiUp(obj);    // 上方向の補正処理
			}

		}

		if (flagFunc::Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plisGimmickWall))
		{
			obj->position.y -= WALL_PUSH_SPEED;//押される

			if (deltaY < 0 && pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
			{
				sound::play(SE::SE_DMG);
			}

		}




		break;
	}


}


void GimmickManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = STAGE->getStageScript();

	shutter_move_flg = false;

	succes_cnt = 3;

	//データの読み込み
	char** mapGimmick = new char*[pBGManager->CHIP_NUM_Y];

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapGimmick[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapGimmick[i], sizeof(char)*pBGManager->CHIP_NUM_X);
	}

	if (!pBGManager->loadMapData(pScript->fileNameGimmick, mapGimmick))
	{
		assert(!"ギミックデータ読み込み失敗");
	}

	// 敵配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int gimmickIndex = mapGimmick[y][x];
			if (gimmickIndex < 0) continue;
			searchSet(GimmickMoveArray[gimmickIndex], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);

		}
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapGimmick[i];
	}
	delete[] mapGimmick;

}

void GimmickManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

void GimmickManager::draw()
{

	OBJ2DManager::draw();   // OBJ2DManagerの初期化


	for (auto & pass : *pGimmickManager)
	{
		//パスワード入力時の文字表示
		if (pass.mover == gimmick_move_password)
			font::textOut(4, pGimmickManager->GimmickManager::ostr.str(), VECTOR2(pass.position.x - pBGManager->getScrollX() - 30, pass.position.y - pBGManager->getScrollY()), VECTOR2(1, 1), VECTOR4(0, 0, 0, 1));
	}

}


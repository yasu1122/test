#pragma once

// 敵キャラ発生データ（いずれステージスクリプトになる）
class STAGE_SCRIPT
{
public:
    const char* fileNamePlayer;
    const char* fileNameDoor;
    const char* fileNameBack;
    const char* fileNameTerrain;
    const char* fileNameGimmick;
	const char* fileNameGTerr;
	const char* fileNameGTerr2;
    const char* fileNameEnemy;
	const char* fileNameVirus;
    const char* fileNameItem;
	const char* fileNamePortal;
    const char* fileNameEnmJump;
    const char* fileNameRoomName;
    const char* fileNameOnePlaceDoor;
    const char* fileNameOnePlaceDoorLink;
    const char* fileNameMonsterRoom;
};

//URLデータ
struct DoorURLData {
    VECTOR2 doorAddress;
    int     linkDoor;
    int     url;
    VECTOR2 scrollPos;
    VECTOR2 respawn;
};

struct EnmURLData {
    int enmURL;
};

class URL_DATA
{
public:

    DoorURLData*	doorURLData;
    EnmURLData*		enmURLData;

};

extern STAGE_SCRIPT stage1_script[];
extern STAGE_SCRIPT stage2_script[];
extern STAGE_SCRIPT* stage_script[];
extern char* stage_char[];
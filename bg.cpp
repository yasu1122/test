//******************************************************************************
//
//
//      BGクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>

using namespace GameLib;

//--------------------------------
//  コンストラクタ
//--------------------------------
BG::BG() : scroll(VECTOR2(0, 0)), back(), terr()
{
    mapDelete();
}
//--------------------------------
//  デストラクタ
//--------------------------------
BG::~BG()
{
    mapDelete();
}

//--------------------------------
//  初期設定
//--------------------------------
void BG::init()
{
    // 念のため初期化
   mapDelete();

	// BG用データのクリア
	clear();

    STAGE_SCRIPT*pScript = STAGE->getStageScript();

    // マップ全体のサイズを取得
    if (!allMapCount(pScript->fileNameBack))
    {
        assert(!"マップ全体のサイズのロードに失敗");
    }

    back = new char*[CHIP_NUM_Y];
    terr = new char*[CHIP_NUM_Y];
	miniMap = new char*[CHIP_NUM_Y];
	plVisit = new char*[CHIP_NUM_Y];
	doorMap = new char*[CHIP_NUM_Y];
	gimickMap = new char*[CHIP_NUM_Y];
	gimickMap1 = new char*[CHIP_NUM_Y];
	gimickMap2 = new char*[CHIP_NUM_Y];
	oneDoorMap = new char*[CHIP_NUM_Y];

    for (int i = 0; i < CHIP_NUM_Y; ++i)
    {

        back[i] = new char[CHIP_NUM_X];
        terr[i] = new char[CHIP_NUM_X];
		miniMap[i] = new char[CHIP_NUM_X];
		plVisit[i] = new char[CHIP_NUM_X];
		doorMap[i] = new char[CHIP_NUM_X];
		gimickMap[i] = new char[CHIP_NUM_X];
		gimickMap1[i] = new char[CHIP_NUM_X];
		gimickMap2[i] = new char[CHIP_NUM_X];
		oneDoorMap[i] = new char[CHIP_NUM_X];

        SecureZeroMemory(back[i], sizeof(char)*pBGManager->CHIP_NUM_X);
        SecureZeroMemory(terr[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(miniMap[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(plVisit[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(doorMap[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(gimickMap[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(gimickMap1[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(gimickMap2[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(oneDoorMap[i], sizeof(char)*pBGManager->CHIP_NUM_X);

    }

    // マップデータのロード
    if (!loadMapData(pScript->fileNameBack, back))
    {
        assert(!"背景データのロードに失敗");
    }

    if (!loadMapData(pScript->fileNameTerrain, terr))
    {
        assert(!"地形データのロードに失敗");
    }

	if (!loadMapData(pScript->fileNameDoor, doorMap))
	{
		assert(!"ドアデータのロードに失敗");
	}

	if (!loadMapData(pScript->fileNameGimmick, gimickMap))
	{
		assert(!"ギミックデータのロードに失敗");
	}

	if (!loadMapData(pScript->fileNameGTerr, gimickMap1))
	{
		assert(!"ギミックデータのロードに失敗");
	}

	if (!loadMapData(pScript->fileNameGTerr2, gimickMap2))
	{
		assert(!"ギミックデータのロードに失敗");
	}

	if (!loadMapData(pScript->fileNameOnePlaceDoor, oneDoorMap))
	{
		assert(!"特定扉データのロードに失敗");
	}

	if(!loadMapData(pScript->fileNameTerrain, miniMap))
	{
		assert(!"ミニマップのロードに失敗");
	}

	DataConvertMinimap();

    //scroll.x = 1280.0f;
    //scroll.y = 745.0f;
}

//--------------------------------
//  メンバ変数のクリア
//--------------------------------
void BG::clear()
{
	scroll = {};    // スクロール座標（画面左上の位置のワールド座標）
}

//--------------------------------
//  地形データ、背景データ削除
//--------------------------------
void BG::mapDelete()
{
    if (terr) {
        for (int i = 0; i < CHIP_NUM_Y; ++i)
            delete[] terr[i];
        delete[] terr;
    }
	
    if (back) {
        for (int i = 0; i < CHIP_NUM_Y; ++i)
            delete[] back[i];
        delete[] back;
    }
	if (miniMap) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] miniMap[i];
		delete[] miniMap;
	}
	if (plVisit) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] plVisit[i];
		delete[] plVisit;
	}
    if (doorMap) {
        for (int i = 0; i < CHIP_NUM_Y; ++i)
            delete[] doorMap[i];
        delete[] doorMap;
    }
	if (gimickMap) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] gimickMap[i];
		delete[] gimickMap;
	}
	if (gimickMap1) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] gimickMap1[i];
		delete[] gimickMap1;
	}
	if (gimickMap2) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] gimickMap2[i];
		delete[] gimickMap2;
	}
	if (oneDoorMap) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] oneDoorMap[i];
		delete[] oneDoorMap;
	}
    terr = nullptr;
    back = nullptr;
	miniMap = nullptr;
	plVisit = nullptr;
    doorMap = nullptr;
	gimickMap1 = nullptr;
	gimickMap2 = nullptr;
	oneDoorMap = nullptr;
}

//-----------------------------------
//      マップサイズ取得
//-----------------------------------
bool BG::allMapCount(const char* file_name)
{
    // マップデータ読み込み
    std::ifstream inputFile_terr(file_name);
    if (inputFile_terr.fail()) return false;


    // ラムダ式関数定義
    auto split = [](const char* src, const char delim, int *x)
    {
        std::istringstream stream{ src };
        std::string output;
        int cnt = 0;
        while (std::getline(stream, output, delim))
        {
            cnt++;
        }
        *x = cnt;
    };


    const int bufSize = 1024;
    char buf[bufSize];
    int y = 0;
    while (inputFile_terr.getline(buf, bufSize - 1))
    {
        split(buf, ',', &CHIP_NUM_X);
        y++;
    }

    CHIP_NUM_Y = y;

    WIDTH  = (float)((CHIP_SIZE)*(CHIP_NUM_X));
    HEIGHT = (float)((CHIP_SIZE)*(CHIP_NUM_Y));

    return true;

}

//--------------------------------
//  マップデータのロード
//--------------------------------
bool BG::loadMapData(const char* fileName, char** map)
{
    std::ifstream inputFile(fileName);
    if (inputFile.fail()) return false;

    auto split = [](const char* src, const char delim, const int Y, char** out)
    {
        std::istringstream stream{ src };
        std::string output;
        int cnt = 0;
        while (std::getline(stream, output, delim))
        {
            out[Y][cnt] = static_cast<char>(std::stoi(output));
            cnt++;
        }
    };

    const int bufSize = 4096;

    char* buf = new char[bufSize];

    int y = 0;
    while (inputFile.getline(&buf[0], bufSize - 1))
    {
        split(buf, ',', y, map);//&map[y++][0]とmap[y++]は同じ意味
        y++;
    }

    delete[] buf;

    return true;
}

//--------------------------------
//  更新処理
//--------------------------------
void BG::update()
{	
	ScanPlVisit();
}

//--------------------------------
//  他のデータをミニマップ用データに変換する
//--------------------------------
void BG::DataConvertMinimap()
{
	static const int LOOP_Y = CHIP_NUM_Y;  // Y方向ループ回数
	static const int LOOP_X = CHIP_NUM_X;  // X方向ループ回数

	for (int y = 0; y < LOOP_Y; ++y) {
		for (int x = 0; x < LOOP_X; ++x) {

			// 赤壁
			if (IS_RED_WALL == gimickMap1[y][x] ||
				IS_RED_WALL == gimickMap2[y][x])
			{
				miniMap[y][x] = 
					MINI_MAP_ATTR::RED_WALL;
			}

			// 青壁
			if (IS_BLUE_WALL == gimickMap1[y][x] ||
				IS_BLUE_WALL == gimickMap2[y][x])
			{
				miniMap[y][x] =
					MINI_MAP_ATTR::BLUE_WALL;
			}	

			// 鍵壁
			if ((IS_KEY0 == gimickMap1[y][x]) || (IS_KEY0 == gimickMap2[y][x]) ||
				(IS_KEY1 == gimickMap1[y][x]) || (IS_KEY1 == gimickMap2[y][x]) ||
				(IS_KEY2 == gimickMap1[y][x]) || (IS_KEY2 == gimickMap2[y][x]) ||
				(IS_KEY3 == gimickMap1[y][x]) || (IS_KEY3 == gimickMap2[y][x])
				)
			{
				miniMap[y][x] =
					MINI_MAP_ATTR::ROCK;
			}

			// 反転壁の中心から左端までのブロック数
			const static int REVWALL_LEFT_MERGIN = -28; 
			// 反転壁の中心から右端までのブロック数
			static const int REVWALL_RIGHT_MERGIN = 31;

			// 反転壁
			if (IS_REVERSE_WALL == gimickMap[y][x])
			{
				for (int i = REVWALL_LEFT_MERGIN; i < REVWALL_RIGHT_MERGIN; ++i)
				{
					miniMap[y][x + i] =
						MINI_MAP_ATTR::INVERSE;
				}
			}

			// シャッターの縦幅ブロック数
			static const int SHUTTER_MERGIN = 31;
			// シャッター
			if (IS_SHUTTER == gimickMap[y][x])
			{
				for (int i = 0; i < SHUTTER_MERGIN; ++i)
				{
					miniMap[y - i][x] = MINI_MAP_ATTR::SHUTTER;
				}
			}

			// 扉
			if (IS_DOOR != doorMap[y][x] ||
				IS_DOOR != oneDoorMap[y][x]) {
				miniMap[y][x] =
				miniMap[y][x + 1] =
				miniMap[y - 1][x] =
				miniMap[y - 1][x + 1] =
					MINI_MAP_ATTR::DOOR;
			}

			// 壁
			if (IS_WALL != terr[y][x]) {
				miniMap[y][x] = MINI_MAP_ATTR::WALL;
			}
		}
	}
}


//--------------------------------
//　マップに訪れたことがあるかを判別する
//--------------------------------
void BG::ScanPlVisit()
{
    using namespace flagFunc;

	// プレイヤーの現在位置取得、チップ単位に変換
	auto pl = pPlayerManager->getOBJ2D(0);
    if (!pl->mover)return;
    if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plVirusTerrPress))return;
    if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plNowNockBack))return;


	int y = (int)((pl->position.y) / CHIP_SIZE);
	int x = (int)((pl->position.x) / CHIP_SIZE);

	if (0 > y || 0 > x)
		return;

    if (y > CHIP_NUM_Y || x > CHIP_NUM_X)return;

	// プレイヤーの現在位置が探索済みなら走査しない、return
	if (VISIT_SCAN::VISITED == plVisit[y][x]) return;

	// 現在マスの上下左右を探索
	ScanPlNow(y - 1, x);
	ScanPlNow(y + 1, x);
	ScanPlNow(y, x - 1);
	ScanPlNow(y, x + 1);
}

bool BG::terrCheck(int terrCheck)
{
    if (terrCheck == ALL_BLOCK) return true;
    if (terrCheck == QUARTER_BLOCK_LEFT)return true;
    if (terrCheck == QUARTER_BLOCK_RIGHT)return true;
    if (terrCheck == HALF_BLOCK_LOWER)return true;

    return false;
}

//--------------------------------
//　現在マスが探索済みか調べる
//--------------------------------
bool BG::ScanPlNow(int y, int x)
{
	// 壁ならfalse
	if (terrCheck(terr[y][x])) {
		plVisit[y][x] = VISIT_SCAN::VISITED;
		return false;
	}

	// 探索済みならfalse
	if (VISIT_SCAN::VISITED == plVisit[y][x])
		return false;

	// 探索済みにして更に上下左右探索
	plVisit[y][x] = VISIT_SCAN::VISITED;

	ScanPlNow(y - 1, x);
	ScanPlNow(y + 1, x);
	ScanPlNow(y, x - 1);
	ScanPlNow(y, x + 1);

	return true;
}


//--------------------------------
//  スクロール座標の移動（clampを使用）
//--------------------------------
void BG::scrollMap()
{
	// 定数
	static const float SCROLL_X_MIN = 0.0f;
	static const float SCROLL_X_MAX = WIDTH - system::SCREEN_WIDTH;
	static const float SCROLL_Y_MIN = 0.0f;
	static const float SCROLL_Y_MAX = HEIGHT - system::SCREEN_HEIGHT;
    
	// 変数
	auto pl = *pPlayerManager->begin();
	const float scrollLimitR = pl.position.x + pl.size.x - system::SCREEN_WIDTH + SCROLL_MERGIN_X;
	const float scrollLimitL = pl.position.x - pl.size.x - SCROLL_MERGIN_X;
	const float scrollLimitD = pl.position.y - system::SCREEN_HEIGHT + SCROLL_MERGIN_Y;
	const float scrollLimitU = pl.position.y - pl.size.y - SCROLL_MERGIN_Y;
    
	// clamp
	scroll.x = clamp(scroll.x, scrollLimitR, scrollLimitL);
	scroll.y = clamp(scroll.y, scrollLimitD, scrollLimitU);
	scroll.x = clamp(scroll.x, SCROLL_X_MIN, SCROLL_X_MAX);
	scroll.y = clamp(scroll.y, SCROLL_Y_MIN, SCROLL_Y_MAX);
   
}

//--------------------------------
//  スクロール座標を任意に変更
//--------------------------------
void BG::setScrollPos(float x, float y)
{
    scroll.x = x;
    scroll.y = y;
}

//--------------------------------
//  背景描画
//--------------------------------
void BG::drawBack()
{
	draw(TEXNO::MAP_BACK, back);
}

//--------------------------------
//  地形描画
//--------------------------------
void BG::drawTerrain()
{
	draw(TEXNO::MAP_TERRAIN, terr);

}

//--------------------------------
//  マップ描画
//--------------------------------
void BG::drawMiniMap()
{
	drawMini(TEXNO::MAP_MINIMAP, miniMap);
}

//--------------------------------
//  BGデータ描画（ソースコード整理したバージョン）
//--------------------------------
void BG::draw(int texNo, char** map)
{
	// 定数の定義（2度と内容が変更されない）
	static const int LOOP_Y = static_cast<int>(system::SCREEN_HEIGHT) / CHIP_SIZE + 5;  // Y方向ループ回数
	static const int LOOP_X = static_cast<int>(system::SCREEN_WIDTH) / CHIP_SIZE + 5;   // X方向ループ回数


	// 変数の定義（scrollの値によって値が更新される。constがついているので、値が変更できない）
	const int divX = static_cast<int>(scroll.x) / CHIP_SIZE; // division x
	const int divY = static_cast<int>(scroll.y) / CHIP_SIZE; // division y
	const int remX = static_cast<int>(scroll.x) % CHIP_SIZE; // remainder x
	const int remY = static_cast<int>(scroll.y) % CHIP_SIZE; // remainder y

	texture::begin(texNo);

	for (int y = 0; y < LOOP_Y; y++)
	{
		for (int x = 0; x < LOOP_X; x++)
		{
            //(x,y)それぞれマップ配列の 縦横の最大要素数 を超えないようにする
            if (divY + y >= CHIP_NUM_Y || divX + x >= CHIP_NUM_X) continue;

			char chip = map[divY + y][divX + x];    // マップチップのインデックス取得
			if (-1 == chip) continue;               // -1であれば空白

				  VECTOR2 pos(static_cast<float>(x * CHIP_SIZE - remX), static_cast<float>(y * CHIP_SIZE - remY));
			const VECTOR2 scale(1, 1);
			const VECTOR2 texPos(static_cast<float>(chip % CHIP_NUM_PER_LINE * CHIP_SIZE), static_cast<float>(chip / CHIP_NUM_PER_LINE * CHIP_SIZE));
			const VECTOR2 texSize(static_cast<float>(CHIP_SIZE), static_cast<float>(CHIP_SIZE));
			
			texture::draw(texNo, pos, scale, texPos, texSize);
		}
	}

	texture::end(texNo);
}

#define MINIMAP_MERGIN		(20)	// ミニマップの周りの余白
#define MINIMAP_UI_MERGIN	(60)	// ミニマップ下部のUI用の余白
//--------------------------------
//  ミニマップ描画
//--------------------------------
void BG::drawMini(int texNo, char **map)
{
	using namespace flagFunc;

	// マップ全体を描画する必要があるので
	// ループ回数はマップ幅数分

	// Y方向ループ回数
	int LOOP_Y = CHIP_NUM_Y;
	// X方向ループ回数
	int LOOP_X = CHIP_NUM_X;   

	// ミニマップの後ろに描画する半透明の黒いレクト
	primitive::rect((float)(MINI_MAP_POS_X - MINIMAP_MERGIN), (float)(MINI_MAP_POS_Y - MINIMAP_MERGIN),
		(float)(LOOP_X * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2),
		(float)(LOOP_Y * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2 + MINIMAP_UI_MERGIN),
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f);


	// ミニマップの描画開始
	texture::begin(texNo);

	for (int y = 0; y < LOOP_Y; y++)
	{
		for (int x = 0; x < LOOP_X; x++)
		{
			char chip = map[y][x];    // マップチップのインデックス取得
			if (-1 == chip) continue;               // -1であれば空白

			// そのマスを探索していなかったら描画しない
			if (VISIT_SCAN::VISIT_NONE == plVisit[y][x]) continue;

			VECTOR2 pos(static_cast<float>(x * CHIP_SIZE_MINI + MINI_MAP_POS_X), static_cast<float>(y * CHIP_SIZE_MINI + MINI_MAP_POS_Y ));
			const VECTOR2 scale(1, 1);
			const VECTOR2 texPos(static_cast<float>(chip % CHIP_NUM_PER_LINE * CHIP_SIZE_MINI), static_cast<float>(chip / CHIP_NUM_PER_LINE * CHIP_SIZE_MINI));
			const VECTOR2 texSize(static_cast<float>(CHIP_SIZE_MINI), static_cast<float>(CHIP_SIZE_MINI));

			texture::draw(texNo, pos, scale, texPos, texSize, VECTOR2(0, 0), 0,
				VECTOR4(1, 1, 1, 0.8f));
		}
	}
	
	texture::end(texNo);

	//=== <アイコン類の描画> ======================================================

	// プレイヤー位置の描画
	auto pl = pPlayerManager->getOBJ2D(0);
	spr_miniMapIcon00.draw((pl->position.x) / CHIP_MINI_SCALING + MINI_MAP_POS_X,
		(pl->position.y) / CHIP_MINI_SCALING + MINI_MAP_POS_Y);

	// URL位置の描画
	for (auto & pItem : *pItemManager)
	{
		if (!pItem.iWork[item::isURL]) continue;
		
		// URLがプレイヤーの訪れていない場所にあるなら描画しない
		if ( VISIT_SCAN::VISIT_NONE == plVisit[(int)pItem.position.y / CHIP_SIZE][(int)pItem.position.x / CHIP_SIZE] ) continue;
		
		spr_miniMapIcon01.draw((pItem.position.x) / CHIP_MINI_SCALING + MINI_MAP_POS_X,
			(pItem.position.y) / CHIP_MINI_SCALING + MINI_MAP_POS_Y, 0.5f, 0.5f);
	}

	// ブックマークしている位置の描画
	for (auto & pBookMark : *pBookMarkManager->getList())
	{
		if (!pBookMark.listAlg) continue;
			
		// 拡大率
		static float rRadius = 0;
		// 透明度
		static float w = 1;

		// 加算する拡大度
		static const float ADD_RADIUS = 0.5f;
		// 加算する透明度
		static const float ADD_ALPHA = 0.02f;
		// 拡大度調整用
		static const float RADIUS_FIX = 20.0f;

		// 選択しているブックマークオブジェクトだけ強調表示する
		if (Flag_Check(pBookMark.iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::okiniiriFlg))
		{
			rRadius += ADD_RADIUS;
			w -= ADD_ALPHA;

			if (0 >= w)
			{
				w = 1;
				rRadius = 0;
			}
			
			// 強調表示用の星
			spr_star00.draw((pBookMark.plScrollPos.objPos.x) / CHIP_MINI_SCALING + MINI_MAP_POS_X,
				(pBookMark.plScrollPos.objPos.y) / CHIP_MINI_SCALING + MINI_MAP_POS_Y, rRadius / RADIUS_FIX, rRadius / RADIUS_FIX, 0, 1, 1, 0, w);
		}

		// ブックマーク位置に表示する星
		spr_star00.draw((pBookMark.plScrollPos.objPos.x) / CHIP_MINI_SCALING + MINI_MAP_POS_X,
			(pBookMark.plScrollPos.objPos.y) / CHIP_MINI_SCALING + MINI_MAP_POS_Y, 0.2f, 0.2f, 0, 1, 1, 0, 1);
	}


	//=== <ミニマップアイコンの説明用のUI> =============================================
	static const float MINIMAP_UI_FIX_Y = 10.0f;
	static const float MINIMAP_UI_PL_ICON = 160.0f;
	static const float MINIMAP_UI_BM_STR = 230.0f;
	static const float MINIMAP_UI_BM_ICON = 415.0f;
	static const float MINIMAP_UI_URL_STR = 470.0f;
	static const float MINIMAP_UI_URL_ICON = 580.0f;

	pMessageWinManager->m_msgText.Render(L"プレイヤー：", VECTOR2((float)MINI_MAP_POS_X, (float)(MINI_MAP_POS_Y + LOOP_Y * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2 - MINIMAP_UI_FIX_Y)),
		VECTOR2(1.0f, 1.0f), VECTOR4(1.0f, 1.0f, 1.0f, 1.0f));
	spr_miniMapIcon00.draw(VECTOR2((float)(MINI_MAP_POS_X + MINIMAP_UI_PL_ICON), (float)(MINI_MAP_POS_Y + LOOP_Y * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2)), VECTOR2(1.0f, 1.0f));

	pMessageWinManager->m_msgText.Render(L"ブックマーク：", VECTOR2((float)(MINI_MAP_POS_X + MINIMAP_UI_BM_STR), (float)(MINI_MAP_POS_Y + LOOP_Y * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2 - MINIMAP_UI_FIX_Y)),
		VECTOR2(1.0f, 1.0f), VECTOR4(1.0f, 1.0f, 1.0f, 1.0f));
	spr_star00.draw(VECTOR2((float)(MINI_MAP_POS_X + MINIMAP_UI_BM_ICON), (float)(MINI_MAP_POS_Y + LOOP_Y * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2)), VECTOR2(0.4f, 0.4f), 0.0f,
		VECTOR4(1.0f, 1.0f, 0.0f, 1.0f));

	pMessageWinManager->m_msgText.Render(L"ＵＲＬ：", VECTOR2((float)(MINI_MAP_POS_X + MINIMAP_UI_URL_STR), (float)(MINI_MAP_POS_Y + LOOP_Y * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2 - MINIMAP_UI_FIX_Y)),
		VECTOR2(1.0f, 1.0f), VECTOR4(1.0f, 1.0f, 1.0f, 1.0f));
	spr_miniMapIcon01.draw(VECTOR2((float)(MINI_MAP_POS_X + MINIMAP_UI_URL_ICON), (float)(MINI_MAP_POS_Y + LOOP_Y * CHIP_SIZE_MINI + MINIMAP_MERGIN * 2)), VECTOR2(1.0f, 1.0f));
}

#undef MINIMAP_UI_MERGIN
#undef MINIMAP_MERGIN



//******************************************************************************
//
//      あたり判定
//
//******************************************************************************

//--------------------------------
//  マップ上の指定した座標の部分のマップチップのインデックスを返す
//--------------------------------
int BG::getData(char** map, float x, float y)
{
	int divX = static_cast<int>(x) >> 5;      // x方向のインデックス
	int divY = static_cast<int>(y) >> 5;      // y方向のインデックス

    if (divX < 0 || divX >= CHIP_NUM_X)return -1;
    if (divY < 0 || divY >= CHIP_NUM_Y)return -1;

	return map[divY][divX];
}

//--------------------------------
//  指定した地点の地形の属性を取得
//--------------------------------
BG::TR_ATTR BG::getTerrainAttr(float x, float y)
{
	// インデックス取得
	int index = getData(terr, x, y);

	// インデックスが-1であればTR_NONEを返す
	if (index < 0) return TR_NONE;

	// x方向のインデックス
	int remX = index % CHIP_NUM_PER_LINE;

	// y方向のインデックス
	int divY = index / CHIP_NUM_PER_LINE;

	// 添字の範囲チェック
	assert(remX >= 0 && remX < CHIP_NUM_PER_LINE);
	assert(divY >= 0 && divY < CHIP_LINE_NUM);

	// リターン
	return terrainAttr[divY][remX];

}
BG::TR_ATTR BG::getTerrainAttr2(float x, float y)
{
	// インデックス取得
	int index = getData(terr2, x, y);

	// インデックスが-1であればTR_NONEを返す
	if (index < 0) return TR_NONE;

	// x方向のインデックス
	int remX = index % CHIP_NUM_PER_LINE;

	// y方向のインデックス
	int divY = index / CHIP_NUM_PER_LINE;

	// 添字の範囲チェック
	assert(remX >= 0 && remX < CHIP_NUM_PER_LINE);
	assert(divY >= 0 && divY < CHIP_LINE_NUM);

	// リターン
	return terrainAttr[divY][remX];

}


//--------------------------------
//  指定されたy座標がマップチップの上側4分の１に含まれ無いか
//--------------------------------
bool BG::isLowerHalf(float y)
{
    return wrap(static_cast<int>(y), 0, CHIP_SIZE) > CHIP_SIZE >> 2;
}

//--------------------------------
//  指定されたx座標がマップチップの右側4分の1に含まれるか
//--------------------------------
bool BG::isLeftQuarter(float x)
{
    return wrap(static_cast<int>(x), 0, CHIP_SIZE) < CHIP_SIZE >> 2;
}

bool BG::isRightQuarter(float x)
{
    return wrap(static_cast<int>(x), 0, CHIP_SIZE) > ((CHIP_SIZE >> 2) * 3);
}

//--------------------------------

//--------------------------------
//  下方向のブロックに対するあたり
//--------------------------------
bool BG::isHitDown(float x, float y)
{
	switch (getTerrainAttr(x, y))                       // 地形の属性を取得する
	{
	case TR_ATTR::ALL_BLOCK: 
		
		return true;               // 全て壁の地形であった

		break;

	default:

		
		break;
	}

	return false;                                       // 地形ではなかった場合}
}

bool BG::isHitDown2(float x, float y)
{
    switch (getTerrainAttr(x, y))                       // 地形の属性を取得する
    {
    case TR_ATTR::HALF_BLOCK_LOWER:

        return isLowerHalf(y);               // 全て壁の地形であった

        break;

    default:


        break;
    }

    return false;                                       // 地形ではなかった場合}
}

//--------------------------------
//  全て壁であるブロックかどうか
//--------------------------------
bool BG::isHitAll(float x, float y)
{
	TR_ATTR attr = getTerrainAttr(x, y);    // (x, y)の地点の地形の属性を取得
	switch (attr) {
	case TR_ATTR::ALL_BLOCK:                // 全て壁の地形なら return true;
		
		return true;
		break;
	default:
	
	break;

	}

	
	return false;
}

//--------------------------------
//  下方向に壁にめり込んでいるかどうかを判定
//--------------------------------
bool BG::isFloor(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitDown(x - width, y)) return true;       // 左端から
		if (isHitDown(x + width, y)) return true;       // 右端から
	}
	return isHitDown(x, y);                             // 最後に真ん中で判定
}

bool BG::isFloor2(float x, float y, float width)
{
    for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
    {
        if (isHitDown2(x - width, y)) return true;       // 左端から
        if (isHitDown2(x + width, y)) return true;       // 右端から
    }
    return isHitDown2(x, y);                             // 最後に真ん中で判定
}

//--------------------------------
//  下方向にめり込んでいた場合、y座標を修正する
//--------------------------------
void BG::mapHoseiDown(OBJ2D* obj)
{
	float y = obj->position.y;                          // わかりやすく書くためいったんyに代入
	y -= wrap(y, 0.0f, static_cast<float>(CHIP_SIZE));  // 0.0fからCHIP_SIZEまでの間をラップアラウンドさせる
	obj->position.y = y - ADJUST_Y;                     // 少し浮かせる
	obj->speed.y = (std::min)(obj->speed.y, 0.0f);      // 地面にあたったので速度が止まる
}

void BG::mapHoseiDown2(OBJ2D* obj)
{
    float y = obj->position.y;                          // わかりやすく書くためいったんyに代入
    y -= wrap(y, 0.0f, static_cast<float>((CHIP_SIZE >> 2)));  // 0.0fからCHIP_SIZEまでの間をラップアラウンドさせる
    obj->position.y = y - ADJUST_Y;                     // 少し浮かせる
    obj->speed.y = (std::min)(obj->speed.y, 0.0f);      // 地面にあたったので速度が止まる
}

//--------------------------------
//  天井にあたっているか
//--------------------------------
bool BG::isCeiling(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitAll(x - width, y)) return true;        // 左端から
		if (isHitAll(x + width, y)) return true;        // 右端から
	}
	return isHitAll(x, y);                              // 最後に真ん中で判定
}

//--------------------------------
//  上方向補正処理
//--------------------------------
void BG::mapHoseiUp(OBJ2D* obj)
{
	float y = obj->position.y - obj->size.y;
	y -= wrap(y, static_cast<float>(-CHIP_SIZE), 0.0f);
	obj->position.y = y + obj->size.y;
	if (obj->speed.y < 0)
		obj->speed.y = 0.0f;                    // 天井にあたったので止まる
}

//--------------------------------
//  横方向に壁当たり判定
//--------------------------------
bool BG::isHitWall(float x, float y)
{
    TR_ATTR attr = getTerrainAttr(x, y);    // (x, y)の地点の地形の属性を取得
    switch (attr) {
    case TR_ATTR::QUARTER_BLOCK_LEFT:                // 全て壁の地形なら return true;

        return isLeftQuarter(x);

        break;

    case TR_ATTR::QUARTER_BLOCK_RIGHT:

        return isRightQuarter(x);

        break;
    default:

        break;

    }

    return false;
}


//--------------------------------
//  横方向に壁にあたっているかどうか
//--------------------------------
bool BG::isWall(float x, float y, float height)
{
	for (; height > 0; height -= CHIP_SIZE) {
		if (isHitAll(x, y - height))
			return true;
	}
	return isHitAll(x, y);
}

bool BG::isWall2(float x, float y, float height)
{
    for (; height > 0; height -= CHIP_SIZE) {
        if (isHitWall(x, y - height))
            return true;
    }
    return isHitWall(x, y);
}

//--------------------------------
//  右方向補正処理
//--------------------------------
void BG::mapHoseiRight(OBJ2D* obj)
{
	float x = obj->position.x + obj->size.x;
	x -= wrap(x, 0.0f, static_cast<float>(CHIP_SIZE));
	obj->position.x = x - obj->size.x - ADJUST_X;
	obj->speed.x = 0.0f;
}

void BG::mapHoseiRight2(OBJ2D* obj)
{
    float x = obj->position.x + obj->size.x;
    x -= wrap(x, 0.0f, static_cast<float>(CHIP_SIZE>>2));
    obj->position.x = x - obj->size.x - ADJUST_X;
    obj->speed.x = 0.0f;
}

//--------------------------------
//  左方向補正処理
//--------------------------------
void BG::mapHoseiLeft(OBJ2D* obj)
{
	float x = obj->position.x - obj->size.x;
	x -= wrap(x, static_cast<float>(-CHIP_SIZE), 0.0f);
	obj->position.x = x + obj->size.x + ADJUST_X;
	obj->speed.x = 0.0f;
}

void BG::mapHoseiLeft2(OBJ2D* obj)
{
    float x = obj->position.x - obj->size.x;
    x -= wrap(x, static_cast<float>(-CHIP_SIZE>>2), 0.0f);
    obj->position.x = x + obj->size.x + ADJUST_X;
    obj->speed.x = 0.0f;
}
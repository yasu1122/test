#ifndef INCLUDED_SELECT
#define	INCLUDED_SELECT

//******************************************************************************
//
//
//      タイトルシーン
//
//
//******************************************************************************

//==============================================================================
//
//      Selectクラス
//
//==============================================================================

class Select : public Scene, public Singleton<Select>
{
private:
    int mode;
public:
    void init();
    void update();
    void draw();
    void uninit();
};

static Select* const SCENE_SELECT = Select::getInstance();

//******************************************************************************

#endif // !INCLUDED_SELECT

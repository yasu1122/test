//******************************************************************************
//
//
//      プレイヤークラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>
#include<math.h>

using namespace GameLib;


void player_move(OBJ2D* obj);


//-----------------------
//  プロトタイプ宣言
//-----------------------
void player::PlMoveCancel2(OBJ2D* obj);

//******************************************************************************
//
//      プレイヤー移動処理
//
//******************************************************************************


//------< プレイヤーのアニメデータ >-----------------------------------------------
namespace
{ 

  //------< プレイヤーのアニメデータ >------------------------------------------
  //上方向
	AnimeData animePlayer[] = {
		
        { &spr_player00, 15 },
        { &spr_player01, 15 },
        { &spr_player02, 15 },
        { &spr_player03, 15 },

		{ nullptr, -1 },// 終了フラグ
	};
	
}

//--------------------------------
//  位置更新
//--------------------------------
bool pl_erase(OBJ2D* obj)
{
    using namespace flagFunc;

    if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plDead))
    {
        return true;
    }
    return false;
}

// フラグを変更
void mouseFlgChange(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace input;

    if (Flag_Check(pMenuManager->getOBJ2D(0)->iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg))return;


    if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0) {

		pEffectManager->searchSet(effect_move_wave,pMousePointa->getOBJ2D(0)->position);

        if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plDrag))return;
       
        player::PlMoveCancel(obj);                   // 移動を強制終了

        if (pPlayerManager->plJumpMoveFlgGet())return;

        if (pMenuManager->getSetFavoFlg())return;

        Flg_On(obj->iWork[player::iWork::actflg], player::ActFlg::plClickMove);
            
    }

}

// HACK:ドラッグ移動 移動制限関数
void PlMoveLimit(OBJ2D *obj)
{
    float leftLimit = pBGManager->getScrollPos().x + PL_LIMIT_LEFT;
    float rightLimit = pBGManager->getScrollPos().x + system::SCREEN_WIDTH - PL_LIMIT_RIGHT;
    float upLimit = pBGManager->getScrollPos().y + PL_LIMIT_UP;
    float downLimit = pBGManager->getScrollPos().y + system::SCREEN_HEIGHT- PL_LIMIT_DOWN;

    if (obj->position.x < leftLimit) obj->position.x = leftLimit;
    if (obj->position.x > rightLimit)obj->position.x = rightLimit;
    if (obj->position.y < upLimit)obj->position.y = upLimit;
    if (obj->position.y > downLimit)obj->position.y = downLimit;

}

// プレイヤー移動制限
void PlMoveLimit2(OBJ2D *obj)
{
    float leftLimit =  64.0f;
    float rightLimit = system::SCREEN_WIDTH - 64.0f;
    float upLimit = 96.0f;
    float downLimit = system::SCREEN_HEIGHT - 32.0f;

    if (obj->position.x < leftLimit) obj->position.x = leftLimit;
    if (obj->position.x > rightLimit)obj->position.x = rightLimit;
    if (obj->position.y < upLimit)obj->position.y = upLimit;
    if (obj->position.y > downLimit)obj->position.y = downLimit;

}

// 移動終了判定
bool PlMoveEnd(OBJ2D* obj, float endX, float endY)
{
    float x = obj->position.x - endX;
    float y = obj->position.y - endY;
    if (sqrtf(y * y + x * x) <= obj->radius + 16.0f) return true;
        
    return false;
}

//TODO: ドラッグ移動(あくまでデバッグ用！！使うな！！)
void PlDragMove(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace input;

    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);

    if (Flag_Check(obj->iWork[player::actflg], player::ActFlg::pldoorChange))
    {
        Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::plDragMove);
        return;
    }
    if (Flag_Check(obj->iWork[player::actflg], player::ActFlg::plDrag))
    {
        if ( STATE(0) & PAD_START)
        {
            Flg_On(obj->iWork[player::iWork::actflg], player::ActFlg::plDragMove);
            obj->position.x = (pMouse->position.x + pBGManager->getScrollPos().x);
            obj->position.y = (pMouse->position.y + pBGManager->getScrollPos().y);
            player::PlMoveCancel2(obj);
            PlMoveLimit(obj);
            return;
        }
        Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::plDragMove);
    }
}

// 移動関数
void PlClickMove(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace vecFunc;

    obj->angle = 0.0f;


    if (!Flag_Check(obj->iWork[player::actflg], player::plClickMove)&& !Flag_Check(obj->iWork[player::actflg], player::plNowMove)) return;
    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);


    VECTOR2 moveVec;        // プレイヤー移動時のベクトル

    // マウスポインタとプレイヤーの距離を取得
    float pointDistance = distanceCmp(obj->position, pMouse->position + pBGManager->getScrollPos());

    switch (obj->act_state)
    {
    case 0:
    {
        moveVec = UnitVec(obj->position, pMouse->position+ pBGManager->getScrollPos());
        obj->fWork[player::fWork::plMoveVec_x] = moveVec.x;
        obj->fWork[player::fWork::plMoveVec_y] = moveVec.y;
        obj->fWork[player::fWork::moveEndPos_x] = obj->position.x+(moveVec.x*pointDistance);
        obj->fWork[player::fWork::moveEndPos_y] = obj->position.y+(moveVec.y*pointDistance);
        obj->velocity = PLAYER_VELOCITY_MIN;
        obj->speed.x = moveVec.x*obj->velocity;
        obj->speed.y = moveVec.y*obj->velocity;
        Flg_Off(obj->iWork[player::actflg], player::ActFlg::plClickMove);
        Flg_On(obj->iWork[player::iWork::actflg], player::ActFlg::plNowMove);

        obj->act_state++;
    }
        // break;
    case 1:

        moveVec = VECTOR2(obj->fWork[player::fWork::plMoveVec_x], obj->fWork[player::fWork::plMoveVec_y]);

        obj->velocity += PLAYER_ACCEL;
        obj->velocity = clamp(obj->velocity, PLAYER_VELOCITY_MIN, PLAYER_VELOCITY_MAX);

        obj->speed.x = moveVec.x*obj->velocity;
        obj->speed.y = moveVec.y*obj->velocity;

        // プレイヤー移動終了フラグがオンなら各変数を初期化
        if (PlMoveEnd(obj, obj->fWork[player::fWork::moveEndPos_x], obj->fWork[player::fWork::moveEndPos_y]))
        {
            obj->act_state = 0;
            obj->speed.x = 0.0f;
            obj->speed.y = 0.0f;
            obj->fWork[player::fWork::moveEndPos_x] = 0.0f;
            obj->fWork[player::fWork::moveEndPos_y] = 0.0f;
            Flg_Off(obj->iWork[player::actflg], player::plNowMove);
        }
		
        // 移動方向へプレイヤーのアングルを設定
		obj->angle = ToRadian(180.0f)-atan2(obj->speed.x, obj->speed.y);

        break;
    }
    
}

// 移動を強制終了
void player::PlMoveCancel(OBJ2D* obj)
{
    using namespace flagFunc;

    if (Flag_Check(obj->iWork[player::iWork::actflg], player::plNowMove)) {
        obj->act_state = 0;

        obj->fWork[player::fWork::moveEndPos_x] = 0.0f;
        obj->fWork[player::fWork::moveEndPos_y] = 0.0f;
        obj->speed.x = 0.0f;
		obj->speed.y = 0.0f;

        Flg_Off(obj->iWork[player::actflg], player::plNowMove);
    }
}
void player::PlMoveCancel2(OBJ2D* obj)
{
	using namespace flagFunc;

	if (Flag_Check(obj->iWork[player::iWork::actflg], player::plNowMove)) {
		obj->act_state = 0;

		obj->fWork[player::fWork::moveEndPos_x] = 0.0f;
		obj->fWork[player::fWork::moveEndPos_y] = 0.0f;
		obj->speed.y = 0.0f;
		obj->speed.x = 0.0f;

		Flg_Off(obj->iWork[player::actflg], player::plNowMove);
	}
}

// ノックバック
void PlayerNockBack(OBJ2D* obj)
{
    using namespace flagFunc;

    if (!Flag_Check(obj->iWork[player::iWork::actflg],player::ActFlg::plNowNockBack))return;

    if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plClickMove) ||
        Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plNowMove))player::PlMoveCancel2(obj);

    switch (obj->act_state)
    {
    case 0:
        
        obj->iWork[player::iWork::Cnt] = PL_NOCK_BACK_CNT;
        obj->act_state++;
        //break;
    case 1:     
        obj->speed.x = obj->fWork[player::fWork::nockBackVecX] * PL_NOCK_BACK;
        obj->speed.y = obj->fWork[player::fWork::nockBackVecY] * PL_NOCK_BACK;
        obj->position += obj->speed;
        obj->color = (obj->cnt % PLAEYR_DAMAGE_COLOR_CNT) ? PLAYER_COLOR_DAMAGE : PLAYER_COLOR_NORMAL;

        if (obj->iWork[player::iWork::Cnt] <= 0) {
            obj->act_state = 0;
            obj->speed = VECTOR2(0.0f, 0.0f);
            obj->cnt = 0;
            obj->color = PLAYER_COLOR_NORMAL;
            Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::plNowNockBack);
        }

        obj->iWork[player::iWork::Cnt]--;
        break;
    }

}

// プレイヤーダメージ
void PlayerDamage(OBJ2D* obj)
{
    using namespace flagFunc;

    if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plIsDamage)&&obj->iWork[player::iWork::dCnt]<=0) {

        Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::plIsDamage);
        if (obj->iWork[player::iWork::dCnt]<=0) {
            obj->hp--;
			sound::play(SE::SE_DMG);
            if (obj->hp <= 0) {
                obj->iWork[player::iWork::plStock]--;
            }

            obj->iWork[player::iWork::dCnt] = 90;
        }
    }

    if (obj->iWork[player::iWork::dCnt] > 0) {
        obj->iWork[player::iWork::dCnt]--;
    }
}

// ウィルスと壁へ挟まれたらフラグをオン
void plPressFlgOn(OBJ2D* obj)
{
    using namespace flagFunc;

    if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plIsDamage))
    {
        Flg_On(obj->iWork[player::iWork::actflg], player::ActFlg::plVirusTerrPress);
    }
}

// ウィルスと壁へ挟まれたら即死
void plPressErase(OBJ2D* obj)
{
    using namespace flagFunc;

    if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plVirusTerrPress))
    {
        obj->hp = 0;
		sound::play(SE::SE_DMG);
        Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::plVirusTerrPress);
    }
}

// プレイヤー移動
void PlayerMovePos(OBJ2D* obj, float oldX, float oldY)
{
	using namespace input;
	using namespace player;
	using namespace flagFunc;


	if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::pldoorChange))return;

    PlayerNockBack(obj);

	PlClickMove(obj);

	obj->position.x += obj->speed.x;
	float deltaX = obj->position.x - oldX;

	// 地形あたり判定
	if (deltaX > 0)
	{
		if (pBGManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
		{
			PlMoveCancel(obj);              // 移動関数を強制終了
			pBGManager->mapHoseiRight(obj); // 右方向の補正処理
			obj->kabeFlag = true;           // 壁フラグをtrueにしておく
            plPressFlgOn(obj);
		}
		if (pGimmickTerrManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
		{
			PlMoveCancel(obj);              // 移動関数を強制終了
			pGimmickTerrManager->mapHoseiRight(obj); // 右方向の補正処理
			obj->kabeFlag = true;           // 壁フラグをtrueにしておく
            plPressFlgOn(obj);
        }
        if (pBGManager->isWall2(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
        {
            PlMoveCancel(obj);              // 移動関数を強制終了
            pBGManager->mapHoseiRight2(obj); // 右方向の補正処理
            obj->kabeFlag = true;           // 壁フラグをtrueにしておく
            plPressFlgOn(obj);
        }
		if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plisGimmickWall))
		{
			PlMoveCancel2(obj);              // 移動関数を強制終了
			obj->position.x = obj->target->position.x - obj->target->size.x - obj->size.x - GIMMICK_ADJUST_X;
			obj->kabeFlag = true;
            plPressFlgOn(obj);
        }


	}
	if (deltaX < 0)
	{
		if (pBGManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
		{

			PlMoveCancel(obj);              // 移動関数を強制終了
			pBGManager->mapHoseiLeft(obj);
			obj->kabeFlag = true;
            plPressFlgOn(obj);
        }
		if (pGimmickTerrManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
		{

			PlMoveCancel(obj);              // 移動関数を強制終了
			pGimmickTerrManager->mapHoseiLeft(obj);
			obj->kabeFlag = true;
            plPressFlgOn(obj);
        }
        if (pBGManager->isWall2(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
        {
            PlMoveCancel(obj);              // 移動関数を強制終了
            pBGManager->mapHoseiLeft2(obj);
            obj->kabeFlag = true;
            plPressFlgOn(obj);
        }
		if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plisGimmickWall))
		{

			PlMoveCancel2(obj);              // 移動関数を強制終了
			obj->position.x = obj->target->position.x + obj->target->size.x + obj->size.x + GIMMICK_ADJUST_X;
			obj->kabeFlag = true;
            plPressFlgOn(obj);
        }
	}
	if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plisGimmickCell))
	{
		obj->position.y -= WALL_PUSH_SPEED;//押される

		if (pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
		{
			obj->hp = 0;
			sound::play(SE::SE_DMG);
		}
	}
	

	obj->position.y += obj->speed.y;// 位置更新
	float deltaY = obj->position.y - oldY;  // 移動後の位置から移動前の位置を引く

	if (deltaY > 0)
	{   // 下方向チェック
		if (pBGManager->isFloor(obj->position.x, obj->position.y, obj->size.x))
		{
			// 床にあたっていたら
			pBGManager->mapHoseiDown(obj);  // 下方向の補正処理
			PlMoveCancel2(obj);
			obj->onGround = true;           // 地面フラグ
            plPressFlgOn(obj);
        }
		if (pGimmickTerrManager->isFloor(obj->position.x, obj->position.y, obj->size.x))
		{
			// 床にあたっていたら
			pGimmickTerrManager->mapHoseiDown(obj);  // 下方向の補正処理
			PlMoveCancel2(obj);
            plPressFlgOn(obj);
        }

        if (pBGManager->isFloor2(obj->position.x, obj->position.y, obj->size.x))
        {
            // 床にあたっていたら
            pBGManager->mapHoseiDown2(obj);  // 下方向の補正処理
            PlMoveCancel2(obj);
            obj->onGround = true;           // 地面フラグ
            plPressFlgOn(obj);
        }

		
	}


	
	if (deltaY < 0)
	{   // 上方向チェック
		if (pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
		{
			PlMoveCancel2(obj);              // 移動関数を強制終了
			pBGManager->mapHoseiUp(obj);    // 上方向の補正処理
			obj->jumpTimer = 0;             // 天井にぶつかったため、それ以上ジャンプできない
            plPressFlgOn(obj);
        }
		if (pGimmickTerrManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
		{
			PlMoveCancel2(obj);              // 移動関数を強制終了
			pGimmickTerrManager->mapHoseiUp(obj);    // 上方向の補正処理
            plPressFlgOn(obj);
        }
		


		
	}
	

}

// 通常ドア移動
void moveNextDoor(OBJ2D* obj)
{
    VECTOR2 hitDoorPos = VECTOR2(obj->fWork[player::fWork::hitDoorPosX], obj->fWork[player::fWork::hitDoorPosY]);
    if (pPlayerManager->plJumpMoveFlgGet())return;

    DoorURL pDoor = pDoorManager->doorURL[obj->iWork[player::iWork::linkDoor]];
	sound::play(SE::SE_WOAP);

    for (int i = 0; i < DOOR_LINK_MAX; ++i)
    {
        if (pDoor.linkPos[i] != hitDoorPos)
        {

            JumpData::posandscroll plJumpData;
            plJumpData.objPos = pDoor.linkPos[i] + pDoor.respawnPos[i];
            plJumpData.scrollPos = pDoor.scrollPos[i];
            pPlayerManager->plJumpMoveFlgSet(true, plJumpData, PL_JUMP_COUNT_MAX, player::BLACK_OUT_ON);
            pBlackoutManager->searchSet(blackout_move04, BLACK_OUT_INIT_POS);

            break;
        }
    }

}

// 広告(敵)による移動
void moveEmpJump(OBJ2D* obj)
{
    using namespace flagFunc;

    if (pPlayerManager->plJumpMoveFlgGet())return;

    DoorURL pEnemyLink = pEnemyManager->doorURL[obj->iWork[player::iWork::linkDoor]];
	sound::play(SE_WOAP);
    
    JumpData::posandscroll plJumpData;
    plJumpData.objPos = pEnemyLink.linkPos[0] + pEnemyLink.respawnPos[0];
    plJumpData.scrollPos = pEnemyLink.scrollPos[0];
    pPlayerManager->plJumpMoveFlgSet(true, plJumpData, PL_JUMP_COUNT_MAX, player::BLACK_OUT_ON);
    pBlackoutManager->searchSet(blackout_move04, BLACK_OUT_INIT_POS);


}

// 特定の場所へ移動するドアによる移動
void moveOnePlaceDoorJump(OBJ2D* obj)
{
    using namespace flagFunc;

    if (pPlayerManager->plJumpMoveFlgGet())return;

    DoorURL pOnePlaceDoor = pOnePlaceDoorManager->doorURL[obj->iWork[player::iWork::linkDoor]];
	sound::play(SE_WOAP);

    JumpData::posandscroll plJumpData;
    plJumpData.objPos = pOnePlaceDoor.linkPos[0] + pOnePlaceDoor.respawnPos[0];
    plJumpData.scrollPos = pOnePlaceDoor.scrollPos[0];
    pPlayerManager->plJumpMoveFlgSet(true, plJumpData, PL_JUMP_COUNT_MAX, player::BLACK_OUT_ON);
    pBlackoutManager->searchSet(blackout_move04, BLACK_OUT_INIT_POS);
}

//--------------------------------
//  扉に接触したら他の扉へ
//--------------------------------
void player::PlayerDoorMove(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace door;

    if (!Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::pldoorChange))return;
    if (Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plDrag))return;


    player::PlMoveCancel(obj);

    moveNextDoor(obj);

    obj->speed.y = 0.0f;

    Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::pldoorChange);
	STAGE->map_init_flg = true;

}

//--------------------------------
//  広告(敵)による移動
//--------------------------------
void PlayerEnemyMove(OBJ2D* obj)
{
	using namespace flagFunc;
	using namespace door;

	if (!Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plEnmJump))return;

    moveEmpJump(obj);


	player::PlMoveCancel(obj);

    Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::plEnmJump);
	obj->speed.y = 0.0f;

}
//--------------------------------------
//  特定の場所へ移動するドアによる移動
//--------------------------------------
void PlayerOnePlaceMove(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace door;

    if (!Flag_Check(obj->iWork[player::iWork::actflg], player::ActFlg::plOnePlaceJump))return;

    moveOnePlaceDoorJump(obj);


    player::PlMoveCancel(obj);

    Flg_Off(obj->iWork[player::iWork::actflg], player::ActFlg::plOnePlaceJump);
    obj->speed.y = 0.0f;

}

//--------------------------------
//  アイテム処理
//--------------------------------
// プレイヤーが取得しているアイテムに応じて処理を変化
// 
void PlayerPosseItem(OBJ2D* obj)
{
	using namespace input;

	// デバッグ用処理
	if (TRG(0) & PAD_TRG1) {
		obj->passiveItem = -1;
	}
	if (TRG(0) & PAD_TRG2) {
		obj->passiveItem = 0;
	}
	if (TRG(0) & PAD_TRG3) {
		obj->passiveItem = 1;
	}
	
}

//--------------------------------
//  プレイヤー更新関数
//--------------------------------
void player_move(OBJ2D* obj)
{
	using namespace input;  // 関数内で入力処理を行うときに記述する
    using namespace flagFunc;

	// 変数
	AnimeData* animeData = nullptr;
	
	switch (obj->state)
	{
	case 0:
		//////// 初期設定 ////////
		obj->actParam.rotateSpeed = ToRadian(1.f);
		obj->size = PLAYER_SIZE_NORMAL;
		obj->scale = PLAYER_SCALE;
		obj->velocity = PLAYER_VELOCITY_MIN;
		obj->moveFlg = false;
		obj->radius = PLAYER_RADIUS;
		obj->passiveItem = PLAYER_PASSIVEITEM;
		obj->throughFlg = false;
        obj->fWork[player::fWork::plMoveVec_y] = PLAYER_MOVE_VEC_INIT;
        obj->fWork[player::fWork::plMoveVec_x] = PLAYER_MOVE_VEC_INIT;
        obj->fWork[player::fWork::plFirstPosX] = obj->position.x;
        obj->fWork[player::fWork::plFirstPosY] = obj->position.y;
        obj->iWork[player::iWork::actflg] = player::ActFlg::plNatural;
        obj->iWork[player::iWork::clickCnt] = PLAYER_CLICK_CNT_INIT;
        obj->iWork[player::iWork::plStock] = PLAYER_STOCK_MAX;
        obj->act_state = PLAYER_ACTSTATE_INIT;
		obj->angle = ToRadian(PLAYER_ANGLE_INIT);
        obj->hp = PLAYER_HP_FIRST;
       
        obj->eraser = pl_erase;
		// アニメの初期設定
		animeData = animePlayer;   // 初期値として下向きのアニメを設定する

		obj->state++;
		//	break;このbreak;は意図的にコメントとして記述している

	case 1:
    {
        //////// 通常時 ////////
        animeData = animePlayer;

        obj->color = PLAYER_COLOR_INIT;

        float oldX = obj->position.x;
        float oldY = obj->position.y;

        PlayerMovePos(obj, oldX, oldY);

        PlayerEnemyMove(obj);

        PlayerOnePlaceMove(obj);

        PlayerPosseItem(obj);

        player::PlayerDoorMove(obj);

        mouseFlgChange(obj);

        plPressErase(obj);
		
        PlayerDamage(obj);

        pPlayerManager->plJumMoveSelect();
        
        obj->animeUpdate(animeData);

        //if (obj->iWork[player::iWork::Cnt] > 0) {
            obj->iWork[player::iWork::Cnt]--;
        //}

        if (obj->iWork[player::iWork::Cnt] <= 0) {
            obj->iWork[player::iWork::clickCnt] = 0;
        }

        if (obj->hp >= PLAYER_HP_MAX)obj->color = VECTOR4(0.0f, 1.0f, 0.0f, 1.0f);

        if (obj->hp <= PLAYER_HP_MIN) {
            obj->state = 3;
            obj->iWork[player::iWork::Cnt] = 100;
        }

        if (obj->iWork[player::iWork::plStock] < PLAYER_HP_MIN)
        {
            obj->hp = PLAYER_HP_MIN;
            obj->state = 2;
            obj->iWork[player::iWork::Cnt] = 100;
        }
		//ブックマーク可否チェック用に呼び出す
		pBGManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y);
		pBGManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y);
		pBGManager->isFloor(obj->position.x, obj->position.y, obj->size.x);
		pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x);

        break;
    }
    case 2:
        obj->color.w -= PLAYER_COLORW_SUB_VALUE;
        
        if (obj->iWork[player::iWork::Cnt] <= 0) {
            Flg_On(obj->iWork[player::iWork::actflg], player::ActFlg::plDead);
        }

        obj->iWork[player::iWork::Cnt]--;
        break;

    case 3:
        obj->color.w -= 0.02f;

        if (obj->iWork[player::iWork::Cnt] <= 0) {
            
            JumpData::posandscroll plJumpData;
            plJumpData.objPos = VECTOR2(obj->fWork[player::fWork::plFirstPosX], obj->fWork[player::fWork::plFirstPosY]);
            plJumpData.scrollPos=VECTOR2(obj->fWork[player::fWork::plFirstScrollPosX], obj->fWork[player::fWork::plFirstScrollPosY]);

            pPlayerManager->plJumpMoveFlgSet(true, plJumpData, PL_JUMP_COUNT_MAX, player::BLACK_OUT_ON);
            pBlackoutManager->searchSet(blackout_move04, BLACK_OUT_INIT_POS);

            obj->state = 4;
        }

        obj->iWork[player::iWork::Cnt]--;

        break;

    case 4:

        pPlayerManager->plJumpMove2();

        if (obj->hp > PLAYER_HP_MIN) {
            obj->state = 1;
        }

        break;
	}

}

// ゲーム開始演出用
void player_scene_start_move(OBJ2D* obj)
{
    using namespace input;  // 関数内で入力処理を行うときに記述する
    using namespace flagFunc;

    // 変数
    AnimeData* animeData = nullptr;

    switch (obj->state)
    {
    case 0:
        //////// 初期設定 ////////
        
        obj->size = PLAYER_SIZE_START;
        obj->scale = PLAYER_SCALE;
        obj->color = PLAYER_COLOR_NORMAL;
        obj->velocity = PLAYER_VELOCITY_MIN;
        obj->moveFlg = false;
        obj->radius = PLAYER_RADIUS;
        obj->passiveItem = PLAYER_PASSIVEITEM;
        obj->throughFlg = false;
        obj->fWork[player::fWork::plMoveVec_y] = PLAYER_MOVE_VEC_INIT;
        obj->fWork[player::fWork::plMoveVec_x] = PLAYER_MOVE_VEC_INIT;
        obj->fWork[player::fWork::plFirstPosX] = obj->position.x;
        obj->fWork[player::fWork::plFirstPosY] = obj->position.y;
        obj->iWork[player::iWork::actflg] = player::ActFlg::plNatural;
        obj->iWork[player::iWork::clickCnt] = PLAYER_CLICK_CNT_INIT;
        obj->iWork[player::iWork::plStock] = PLAYER_STOCK_MAX;
        obj->act_state = PLAYER_ACTSTATE_INIT;
        obj->angle = ToRadian(PLAYER_ANGLE_INIT);
        obj->hp = PLAYER_HP_FIRST;
        
        obj->eraser = pl_erase;
        // アニメの初期設定
        animeData = animePlayer;   // 初期値として下向きのアニメを設定する

        obj->state++;
        //	break;このbreak;は意図的にコメントとして記述している

    case 1:
        // 登場時のエフェクトをセット
        pEffectManager->searchSet(effect_move_wave, obj->position);
        obj->state++;
        //break;

    case 2:
        obj->color.w += PLAYER_COLORW_ADD_VALUE;

        if (obj->color.w >= PLAYER_COLORW_MAX)
        {
            obj->color.w = PLAYER_COLORW_MAX;
            obj->state++;
        }

        break;
    
    case 3:
    {
        //////// 通常時 ////////
        animeData = animePlayer;

        PlClickMove(obj);

        obj->position.x += obj->speed.x;
        obj->position.y += obj->speed.y;

        PlMoveLimit2(obj);

        pPlayerManager->plJumMoveSelect();

        obj->animeUpdate(animeData);

        if (obj->iWork[player::iWork::Cnt] > 0) {
            obj->iWork[player::iWork::Cnt]--;
        }

        if (obj->iWork[player::iWork::Cnt] <= 0) {
            obj->iWork[player::iWork::clickCnt] = 0;
        }

        //ブックマーク可否チェック用に呼び出す
        pBGManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y);
        pBGManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y);
        pBGManager->isFloor(obj->position.x, obj->position.y, obj->size.x);
        pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x);

        break;
    }
    
    }

}

//==============================================================================
//
//		PlayerManagerクラス
//
//==============================================================================

//--------------------------------
//  初期設定
//--------------------------------
void PlayerManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

    STAGE_SCRIPT*pScript = STAGE->getStageScript();

    movePosAndScroll = {};
    plJumpMoveCnt = 0;
    plJumpMoveFlg = false;

    //  プレイヤーデータの読み込み
    char** mapPlayer = new char*[pBGManager->CHIP_NUM_Y];
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapPlayer[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapPlayer[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }


    if (!pBGManager->loadMapData(pScript->fileNamePlayer, mapPlayer))
    {
        assert(!"プレイヤーデータ読み込み失敗");
    }

    // プレイヤー配置
    for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
        {
            const int playerIndex = mapPlayer[y][x];
            if (playerIndex < 0) continue;

            // 生成
            obj_w[0].mover = &player_move;
            obj_w[0].position = VECTOR2(static_cast<float>(x*BG::CHIP_SIZE + BG::CHIP_SIZE / 2), static_cast<float>(y*BG::CHIP_SIZE + BG::CHIP_SIZE));
            VECTOR2 pos = obj_w[0].position;
            pos = setScrollPos(pos);
            obj_w[0].fWork[player::fWork::plFirstScrollPosX] = pos.x;
            obj_w[0].fWork[player::fWork::plFirstScrollPosY] = pos.y;
            pBGManager->setScrollPos(pos.x, pos.y);
            break;
        }
        if (obj_w[0].mover)break;
    }

    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapPlayer[i];
    }
    delete[] mapPlayer;

}

// ゲーム開始演出用初期化
void PlayerManager::scene_start_init()
{
    OBJ2DManager::init();   // OBJ2DManagerの初期化

    movePosAndScroll = {};
    plJumpMoveCnt = 0;
    plJumpMoveFlg = false;

    // 生成
    obj_w[0].mover = &player_scene_start_move;
    obj_w[0].position = VECTOR2(system::SCREEN_WIDTH / 2.0f, system::SCREEN_HEIGHT / 2.0f);
}

//--------------------------------
//  更新
//--------------------------------
void PlayerManager::update()
{
	// 移動処理
	OBJ2DManager::update(); // OBJ2DManagerの更新
}

//--------------------------------
//  描画
//--------------------------------
void PlayerManager::draw()
{

	// 描画処理
	OBJ2DManager::draw();   // OBJ2DManagerの描画

    //debug::setString("player_hp:%d", obj_w[0].hp);
	/*for (auto &obj : *pPlayerManager) {
		primitive::rect(obj.position.x - obj.size.x - pBGManager->getScrollPos().x, obj.position.y - obj.size.y - pBGManager->getScrollPos().y, obj.size.x * 2, obj.size.y * 2, 0, 0, 0, 1, 0, 0, 0.3f);
	}*/
}

// ゲーム開始演出描画
void PlayerManager::scene_start_draw()
{
    
    // 描画処理
    OBJ2DManager::draw_not_scroll();

}


//--------------------------------
//  プレイヤーお気に入り移動
//--------------------------------

// 通常(暗転あり)
void PlayerManager::plJumpMove()
{
    using namespace flagFunc;

    OBJ2D* pPl = pPlayerManager->getOBJ2D(0);

    if (!plJumpMoveFlg)return;

    if (plJumpMoveCnt == PL_JUMP_TIME) {
        player::PlMoveCancel2(pPlayerManager->getOBJ2D(0));
        pPl->position = movePosAndScroll.objPos;
        pBGManager->setScrollPos(movePosAndScroll.scrollPos.x, movePosAndScroll.scrollPos.y);
        movePosAndScroll = {};
    }

    plJumpMoveCnt--;

    if (plJumpMoveCnt < 0)
    {

        plJumpMoveCnt = 0;
        plJumpMoveFlg = false;
        plJumpMoveSelectNum = 0;

    }
}

// リスポーン用
void PlayerManager::plJumpMove2()
{
    using namespace flagFunc;

    OBJ2D* pPlayer = pPlayerManager->getOBJ2D(0);

    if (!plJumpMoveFlg)return;

    if (plJumpMoveCnt == PL_JUMP_TIME) {
        player::PlMoveCancel2(pPlayerManager->getOBJ2D(0));
        pPlayer->position = movePosAndScroll.objPos;
        pBGManager->setScrollPos(movePosAndScroll.scrollPos.x, movePosAndScroll.scrollPos.y);
        movePosAndScroll = {};
        Flg_On(STAGE->iWork[stage::iWork::initflg], stage::InitFlg::map_init_pl_respawn);

    }

    plJumpMoveCnt--;

    if (plJumpMoveCnt < 0)
    {

        plJumpMoveCnt = 0;
        plJumpMoveFlg = false;
        plJumpMoveSelectNum = 0;
        pPlayer
            ->hp = PLAYER_HP_FIRST;
    }
}

// 通常(暗転無し)
void PlayerManager::plJumpMove3()
{
    using namespace flagFunc;

    OBJ2D* pPlayer = pPlayerManager->getOBJ2D(0);

    if (!plJumpMoveFlg)return;

    player::PlMoveCancel2(pPlayerManager->getOBJ2D(0));
    pPlayer->position = movePosAndScroll.objPos;
    pBGManager->setScrollPos(movePosAndScroll.scrollPos.x, movePosAndScroll.scrollPos.y);
    movePosAndScroll = {};
    plJumpMoveFlg = false;
    plJumpMoveSelectNum = 0;

}

// ジャンプ関数選択
void PlayerManager::plJumMoveSelect()
{
    switch (pPlayerManager->getPlJumpMoveSelect()) {

    case player::BLACK_OUT_OFF:
        // 暗転無し(プレイヤー硬直時間なし)
        pPlayerManager->plJumpMove3();
        break;

    case player::BLACK_OUT_ON:
        // 暗転あり(プレイヤー硬直時間あり)
        pPlayerManager->plJumpMove();
        break;

    }
}

void PlayerManager::plJumpMoveFlgSet(bool Flg, JumpData::posandscroll posAndScroll, int jumpMoveCnt, int jumpMoveState)
{
    plJumpMoveFlg = Flg;
    plJumpMoveCnt = jumpMoveCnt;
    movePosAndScroll = posAndScroll;
    plJumpMoveSelectNum = jumpMoveState;
}

//******************************************************************************
//
//
//      OBJ2Dクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;

//--------------------------------
//  コンストラクタ
//--------------------------------
OBJ2D::OBJ2D()
{
	clear();

	scale = VECTOR2(1, 1);          // 初期化忘れが無いように（スケールゼロなら何も表示されない）
	color = VECTOR4(1, 1, 1, 1);    // 初期化忘れが無いように（不透明度ゼロなら何も表示されない）
}

//-------------------------------
//  デストラクタ
//-------------------------------
OBJ2D::~OBJ2D()
{
    clear();
}

//--------------------------------
//  メンバ変数のクリア
//--------------------------------
void OBJ2D::clear()
{
	data = nullptr;
	SecureZeroMemory(&anime, sizeof(anime));
    SecureZeroMemory(&actParam, sizeof(actParam));

	position = {};
	init_position = {};
	clone_pos = {};

    linkPos = nullptr;

	scale = VECTOR2(1, 1);
	angle = 0.0f;
	color = VECTOR4(1, 1, 1, 1);
	size = {};
	flg = false;
    mover = nullptr;
	eraser = 0;
	velocity = 0.0f;
	speed = {};
	state = 0;
	timer = 0;
	param = 0;
	SecureZeroMemory(iWork, sizeof(iWork));
	SecureZeroMemory(fWork, sizeof(fWork));
    SecureZeroMemory(ipWork, sizeof(ipWork));
	cnt = 0;
	jumpTimer = 0;
	mutekiTimer = 0;
	damageTimer = 0;
	getURLcnt = 0;
	hp = 0;
	hitRect = {};
	judgeFlag = false;
	isDrawHitRect = false;
	invert_flg = false;
	inverted_flg = false;
	clone_flg = false;
	command = 0;
	delta = {};
	type = 99;
    onGround    = false;
    xFlip       = false;
    kabeFlag    = false;
    gakeFlag    = false;
    text        = nullptr;

	parent = nullptr;
}

//--------------------------------
//  移動
//--------------------------------
// 更新処理
void OBJ2D::update()
{
	if (mover) mover(this);

	// 消去チェック
	if (eraser) {
		if (eraser(this)) 
            clear();
	}

}

//--------------------------------
//  描画
//--------------------------------
void OBJ2D::draw()
{

	//if (SCENE_GAME->stop_fg) { this->GameSway(this); }


	if (data)// OBJ2Dのdataメンバにスプライトデータがあれば
	{
		data->draw(position- pBGManager->getScrollPos(), scale, angle, color);  // dataのdrawメソッドでスプライトを描画する
	}
}
//--------------------------------
//  描画
//--------------------------------
void OBJ2D::draw_not_scroll()
{
	//if (SCENE_GAME->stop_fg) { this->GameSway(this); }


	if (data)// OBJ2Dのdataメンバにスプライトデータがあれば
	{
		data->draw(position, scale, angle, color);  // dataのdrawメソッドでスプライトを描画する
	}
}

//--------------------------------
//  アニメーション更新
//--------------------------------
//  戻り値：true  アニメが先頭に戻る瞬間
//        :false それ以外
//--------------------------------
bool OBJ2D::animeUpdate(AnimeData* animeData)
{


	if (animeData == nullptr) assert(!"animeUpdate関数でanimeDataがnullptr");

	if (anime.pPrev != animeData)           // アニメデータが切り替わったとき
	{
		anime.pPrev = animeData;
		anime.patNum = 0;	                // 先頭から再生
		anime.frame = 0;
	}

	animeData += anime.patNum;
	data = animeData->data;                 // 現在のパターン番号に該当する画像を設定

	anime.frame++;
	if (anime.frame >= animeData->frame)    // 設定フレーム数表示したら
	{
		anime.frame = 0;
		anime.patNum++;                     // 次のパターンへ
		if ((animeData + 1)->frame < 0)     // 終了コードのとき
		{
			anime.patNum = 0;               // 先頭へ戻る
			return true;
		}
	}

	return false;
}

//******************************************************************************
//
//
//      OBJ2DManagerクラス
//
//
//******************************************************************************

//--------------------------------
//  初期化
//--------------------------------
// 初期設定
void OBJ2DManager::init()
{
	obj_w.resize(getSize());
	for (auto& item : *this) item.clear();
}

//--------------------------------
//  配列へ追加
//--------------------------------
OBJ2D* OBJ2DManager::searchSet(MOVER mover, VECTOR2 pos)
{
	for (auto& item : *this) {
		if (item.mover) continue;
		item.clear();
		item.mover = mover;
		item.position = pos;
		return &item;
	}
	return nullptr;
}


//--------------------------------
//  更新
//--------------------------------
// 更新処理
void OBJ2DManager::update()
{
	for (auto& item : *this) item.update();
}



//--------------------------------
//  描画
//--------------------------------
void OBJ2DManager::draw()
{
	auto it = end();
	while (it != begin()) {
		it--;
		it->draw();
	};
}
void OBJ2DManager::draw_not_scroll()
{
	auto it = end();
	while (it != begin()) {
		it--;
		it->draw_not_scroll();
	};
}
//画面揺れ
void OBJ2D::GameSway(OBJ2D*obj)
{
	sway *= -1;
	obj->position.x += sway;

}

void OBJ2D::PosInverce(OBJ2D*obj)
{
	for (auto & pGimmick : *pGimmickManager)
	{
		if (pGimmick.mover != gimmick_move_inverce)continue;
		if (obj->invert_flg&&pGimmick.invert_flg && !obj->inverted_flg)
		{
			obj->position.y += obj->invert_pos*2.f;
			obj->inverted_flg = true;
			obj->scale *= -1;
		}


		if (!pGimmick.invert_flg)obj->inverted_flg = false;
	}

	for (auto & pGimmick : *pGimmickManager)
	{
		if (pGimmick.mover != gimmick_move_inverce2)continue;

		if (obj->invert_flg2&&pGimmick.invert_flg2 && !obj->inverted_flg2)
		{
			obj->position.x += obj->invert_pos2*2.f;
			obj->inverted_flg2 = true;
			obj->scale.x *= -1;
		}

		if (!pGimmick.invert_flg2)obj->inverted_flg2 = false;
	}
}

#include "all.h"

#include <string>
#include<bitset>


using namespace GameLib;




//--------------------------------
//  ショット位置更新
//--------------------------------
// 
void PlShot_MovePos(OBJ2D* obj)
{
	VECTOR2 plPos = pPlayerManager->getOBJ2D(0)->position;
	float fromPlAngle = pPlayerManager->getOBJ2D(0)->fWork[player::fWork::fromPlShotAngle];
	float fromPlDistance = pPlayerManager->getOBJ2D(0)->fWork[player::fWork::fromPlShotDistance];

	obj->position.x = plPos.x + fromPlDistance * cosf(fromPlAngle);
	obj->position.y = plPos.y + fromPlDistance * sinf(fromPlAngle);
}

// 初期化
void PlShotManager::init()
{
	OBJ2DManager::init();
}

// 更新処理
void PlShotManager::update()
{
	OBJ2DManager::update();
}

// 描画処理
void PlShotManager::draw()
{
	OBJ2DManager::draw();

}
#include"all.h"

#include <string>
#include <bitset>

using namespace GameLib;


//-----------------------------< ウィルスアニメデータ >-------------------------------
AnimeData virusAnime[] = {
	{ &spr_virus00, 20 },
	{ &spr_virus01, 20 },
	{ nullptr,-1},
};


VECTOR2 getVec2Player(VECTOR2 src);
void enemy_virus_move06_child(OBJ2D*obj);
//--------------------------------------------
//
//  消去アルゴリズム
//
//--------------------------------------------

bool virus_erase(OBJ2D* obj)
{
	if (!obj->mover) {
		obj->clear();
		return true;
	}
	return false;
}

#define VIRUS_SPEED00			6
#define VIRUS_SPEED01			5
#define VIRUS_SPEED02			14
#define VIRUS_SPEED03			14
#define VIRUS_SPEED04			5
#define VIRUS_SPEED05			6
#define VIRUS_MOVE_CNT00		100
#define VIRUS_MOVE_CNT01		45
#define VIRUS_MOVE_CNT02		30
#define VIRUS_MOVE_CNT03		160
#define VIRUS_ROTATION_ANGLE	0.01f
#define VIRUS_ROTATION_RADIUS	250
#define VIRUS_MOVE_ANGLE		1.2f
#define VIRUS_SIZEX				20.0f
#define VIRUS_SIZEY				20.0f
#define VIRUS_SCALE				0.25f
#define BIG_VIRUS_SIZEX			60.0f
#define BIG_VIRUS_SIZEY			60.0f
#define BIG_VIRUS_SCALE			0.25f



//--------------------------------------------
//障害物縦1
void enemy_virus_move00(OBJ2D*obj)
{
	AnimeData* animeData = nullptr;

	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(VIRUS_SIZEX, VIRUS_SIZEY);
		obj->scale = VECTOR2(VIRUS_SCALE, VIRUS_SCALE);
		obj->data = &spr_virus00;
		obj->speed.y = VIRUS_SPEED00;
		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:

		obj->angle += VIRUS_ROTATION_ANGLE;

		if ((++obj->timer % VIRUS_MOVE_CNT00 == 0) || pBGManager->isWall2(obj->position.x, obj->position.y, obj->size.x) || pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x) || pBGManager->isFloor2(obj->position.x, obj->position.y, obj->size.x))
		{
			obj->speed.y *= -1;
			obj->timer = 0;
		}

		animeData = virusAnime;

		obj->position.y += obj->speed.y;

		obj->OBJ2D::PosInverce(obj);

		obj->animeUpdate(animeData);

	}

}

//障害物縦2
void enemy_virus_move01(OBJ2D*obj)
{
	AnimeData* animeData = nullptr;

	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(VIRUS_SIZEX, VIRUS_SIZEY);
		obj->scale = VECTOR2(VIRUS_SCALE, VIRUS_SCALE);
		obj->data = &spr_virus00;
		obj->speed.y = VIRUS_SPEED01;
		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:

		obj->angle += VIRUS_ROTATION_ANGLE;
		if ((++obj->timer % VIRUS_MOVE_CNT01 == 0) || pBGManager->isWall2(obj->position.x, obj->position.y, obj->size.x + 10) || pBGManager->isCeiling(obj->position.x, obj->position.y - (obj->size.y + 10), obj->size.x + 10) || pBGManager->isFloor2(obj->position.x, obj->position.y, obj->size.x + 10))
		{
			obj->speed.y *= -1;
			obj->timer = 0;
		}

		animeData = virusAnime;

		obj->position.y += obj->speed.y;

		obj->OBJ2D::PosInverce(obj);

		obj->animeUpdate(animeData);

	}

}

//障害物縦3
void enemy_virus_move02(OBJ2D*obj)
{
	AnimeData* animeData = nullptr;

	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(VIRUS_SIZEX, VIRUS_SIZEY);
		obj->scale = VECTOR2(VIRUS_SCALE, VIRUS_SCALE);
		obj->data = &spr_virus00;
		obj->speed.y = VIRUS_SPEED02;
		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:

		obj->angle += VIRUS_ROTATION_ANGLE;
		if ((++obj->timer % VIRUS_MOVE_CNT01 == 0) || pBGManager->isWall2(obj->position.x, obj->position.y, obj->size.x + 10) || pBGManager->isCeiling(obj->position.x, obj->position.y - (obj->size.y + 10), obj->size.x + 10) || pBGManager->isFloor2(obj->position.x, obj->position.y, obj->size.x + 10))
		{
			obj->speed.y *= -1;
			obj->timer = 0;
		}

		animeData = virusAnime;

		obj->position.y += obj->speed.y;

		obj->OBJ2D::PosInverce(obj);

		obj->animeUpdate(animeData);

	}

}

////障害物横1
void enemy_virus_move03(OBJ2D*obj)
{
	AnimeData* animeData = nullptr;

	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(VIRUS_SIZEX, VIRUS_SIZEY);
		obj->scale = VECTOR2(VIRUS_SCALE, VIRUS_SCALE);
		obj->data = &spr_virus00;
		obj->speed.x = VIRUS_SPEED03;
		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:

		obj->angle += VIRUS_ROTATION_ANGLE;
		if ((++obj->timer % VIRUS_MOVE_CNT02 == 0) ||
			pBGManager->isWall2(obj->position.x - (obj->size.x + 10), obj->position.y, (obj->size.y + 10)) ||
			pBGManager->isWall2(obj->position.x + (obj->size.x + 10), obj->position.y, (obj->size.y + 10)))
		{
			obj->speed.x *= -1;
			obj->timer = 0;
		}

		animeData = virusAnime;

		obj->position.x += obj->speed.x;

		obj->OBJ2D::PosInverce(obj);

		obj->animeUpdate(animeData);

	}

}


//障害物横2
void enemy_virus_move04(OBJ2D*obj)
{

	AnimeData* animeData = nullptr;

	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(VIRUS_SIZEX, VIRUS_SIZEY);
		obj->scale = VECTOR2(VIRUS_SCALE, VIRUS_SCALE);
		obj->data = &spr_virus00;
		obj->speed.x = VIRUS_SPEED04;
		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:
		obj->angle += VIRUS_ROTATION_ANGLE;


		if ((++obj->timer % VIRUS_MOVE_CNT01 == 0) ||
			pBGManager->isWall2(obj->position.x - (obj->size.x + 10), obj->position.y, (obj->size.y + 10)) ||
			pBGManager->isWall2(obj->position.x + (obj->size.x + 10), obj->position.y, (obj->size.y + 10)))
		{
			obj->speed.x *= -1;
			obj->timer = 0;
		}

		obj->position.x += obj->speed.x;

		animeData = virusAnime;

		obj->OBJ2D::PosInverce(obj);

		obj->animeUpdate(animeData);

	}



}

//障害物横3
void enemy_virus_move05(OBJ2D*obj)
{
	AnimeData* animeData = nullptr;

	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(VIRUS_SIZEX, VIRUS_SIZEY);
		obj->scale = VECTOR2(VIRUS_SCALE, VIRUS_SCALE);
		obj->data = &spr_virus00;
		obj->speed.x = VIRUS_SPEED04;
		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:
		obj->angle += VIRUS_ROTATION_ANGLE;

		if ((++obj->timer % VIRUS_MOVE_CNT03 == 0) ||
			pBGManager->isWall2(obj->position.x - (obj->size.x + 10), obj->position.y, (obj->size.y + 10)) ||
			pBGManager->isWall2(obj->position.x + (obj->size.x + 10), obj->position.y, (obj->size.y + 10)))
		{
			obj->speed.x *= -1;
			obj->timer = 0;
		}

		animeData = virusAnime;

		obj->position.x += obj->speed.x;


		obj->OBJ2D::PosInverce(obj);

		obj->animeUpdate(animeData);

	}



}



//回転の中心
void enemy_virus_move06(OBJ2D*obj)
{


	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(0.f, 0.f);
		obj->param = enemy::EnemyType::VIRUS;

		for (float i = 1.f; i <= 4.f; i++)
		{

			OBJ2D* child = pVirusManager->searchSet(enemy_virus_move06_child, VECTOR2(
				static_cast<float>(obj->position.x + sin(ToRadian(20.f * i)) * VIRUS_ROTATION_RADIUS),
				static_cast<float>(obj->position.y + cos(ToRadian(20.f * i)) * VIRUS_ROTATION_RADIUS)));
			if (child)
			{
				child->index = (int)i;
				child->parent = obj;
			}


		}


		{
			OBJ2D* child2 = pItemManager->searchSet(ItemMoveKey2, VECTOR2(
				static_cast<float>(obj->position.x + sin(ToRadian(20.f * 5)) * VIRUS_ROTATION_RADIUS),
				static_cast<float>(obj->position.y + cos(ToRadian(20.f * 5)) * VIRUS_ROTATION_RADIUS)));

			if (child2)
			{
				child2->index = (int)5;
				child2->parent = obj;
			}
		}
		obj->state++;
		//break;

	case 1:


		break;

	}

	obj->OBJ2D::PosInverce(obj);


}
//回転その2
void enemy_virus_move07(OBJ2D*obj)
{


	switch (obj->state) {

	case 0:

		
		obj->param = enemy::EnemyType::VIRUS;

		for (float i = 1.f; i <= 5.f; i++)
		{

			OBJ2D* child = pVirusManager->searchSet(enemy_virus_move06_child, VECTOR2(
				static_cast<float>(obj->position.x + sin(ToRadian(20.f * i)) * VIRUS_ROTATION_RADIUS),
				static_cast<float>(obj->position.y + cos(ToRadian(20.f * i)) * VIRUS_ROTATION_RADIUS)));
			if (child)
			{
				child->index = (int)i;
				child->parent = obj;
			}


		}



		obj->state++;
		//break;

	case 1:


		break;

	}

	obj->OBJ2D::PosInverce(obj);


}

//回転してるやつら
void enemy_virus_move06_child(OBJ2D*obj)
{
	AnimeData* animeData = nullptr;

	float & MoveAngle = obj->fWork[enemy::fWork::MoveAngle];


	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(VIRUS_SIZEX, VIRUS_SIZEY);
		obj->scale = VECTOR2(VIRUS_SCALE, VIRUS_SCALE);
		obj->data = &spr_virus00;
		MoveAngle = 0;
		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:
		obj->position.x = obj->parent->position.x + sin(ToRadian(75.f * obj->index + MoveAngle)) * VIRUS_ROTATION_RADIUS;
		obj->position.y = obj->parent->position.y + cos(ToRadian(75.f * obj->index + MoveAngle)) * VIRUS_ROTATION_RADIUS;

		MoveAngle += VIRUS_MOVE_ANGLE;

		animeData = virusAnime;

		obj->animeUpdate(animeData);

		break;
	}
	obj->OBJ2D::PosInverce(obj);

}

//壁に当たったら速度を反転
void enmMovePos(OBJ2D* obj, float oldX, float oldY)
{

	obj->position.x += obj->speed.x;
	float deltaX = obj->position.x - oldX;

	// 地形あたり判定
	if (deltaX > 0)
	{
		if (pBGManager->isWall(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
		{
			pBGManager->mapHoseiRight(obj); // 右方向の補正処理

			obj->speed.x = -VIRUS_SPEED00;

		}

		if (pBGManager->isWall2(obj->position.x + obj->size.x, obj->position.y, obj->size.y))
		{
			pBGManager->mapHoseiRight2(obj); // 右方向の補正処理
			obj->speed.x = -VIRUS_SPEED00;
		}

	}


	if (deltaX < 0)
	{
		if (pBGManager->isWall(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
		{
			pBGManager->mapHoseiLeft(obj);

			obj->speed.x = VIRUS_SPEED00;

		}
		if (pBGManager->isWall2(obj->position.x - obj->size.x, obj->position.y, obj->size.y))
		{

			pBGManager->mapHoseiLeft2(obj);
			obj->speed.x = VIRUS_SPEED00;
		}
	}


	obj->position.y += obj->speed.y;// 位置更新
	float deltaY = obj->position.y - oldY;  // 移動後の位置から移動前の位置を引く

	if (deltaY > 0)
	{   // 下方向チェック
		if (pBGManager->isFloor(obj->position.x, obj->position.y, obj->size.x))
		{

			obj->speed.y = -VIRUS_SPEED00;
			// 床にあたっていたら
			pBGManager->mapHoseiDown(obj);  // 下方向の補正処理

		}
		if (pBGManager->isFloor2(obj->position.x, obj->position.y, obj->size.x))
		{

			obj->speed.y = -VIRUS_SPEED00;
			// 床にあたっていたら
			pBGManager->mapHoseiDown2(obj);  // 下方向の補正処理

		}

	}


	if (deltaY < 0)
	{   // 上方向チェック
		if (pBGManager->isCeiling(obj->position.x, obj->position.y - obj->size.y, obj->size.x))
		{

			obj->speed.y = VIRUS_SPEED00;
			pBGManager->mapHoseiUp(obj);    // 上方向の補正処理

		}

	}



}
//反射敵
void enemy_virus_move08(OBJ2D*obj)
{

	float oldX = obj->position.x;
	float oldY = obj->position.y;


	switch (obj->state) {

	case 0:

		obj->size = VECTOR2(BIG_VIRUS_SIZEX, BIG_VIRUS_SIZEY);
		obj->data = &spr_virus00;
		obj->speed.x = VIRUS_SPEED00;
		obj->speed.y = VIRUS_SPEED00;

		obj->param = enemy::EnemyType::VIRUS;
		obj->state++;
		//break;

	case 1:


		enmMovePos(obj, oldX, oldY);


		break;

	}

}



MOVER VirusMoveArray[] = { enemy_virus_move00,enemy_virus_move01,enemy_virus_move02,enemy_virus_move03,enemy_virus_move04,enemy_virus_move05,enemy_virus_move06,enemy_virus_move07,enemy_virus_move08 };

void VirusManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化
	doorURL.resize(VirusManager::getEnmJumpSize());

	STAGE_SCRIPT*pScript = STAGE->getStageScript();

	// 敵データの読み込み
	char** mapEnemy = new char*[pBGManager->CHIP_NUM_Y];
	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapEnemy[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapEnemy[i], sizeof(char)*pBGManager->CHIP_NUM_X);
	}

	char** mapEnmJump = new char*[pBGManager->CHIP_NUM_Y];
	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapEnmJump[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapEnmJump[i], sizeof(char)*pBGManager->CHIP_NUM_X);

	}

	if (!pBGManager->loadMapData(pScript->fileNameVirus, mapEnemy))
	{
		assert(!"敵データ読み込み失敗");
	}


	// 敵配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int enemyIndex = mapEnemy[y][x];
			if (enemyIndex >= 0) {
				searchSet(VirusMoveArray[enemyIndex], VECTOR2(
					static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
					static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
				);

			}

		}
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapEnemy[i];
		delete[] mapEnmJump[i];
	}
	delete[] mapEnemy;
	delete[] mapEnmJump;
}

void VirusManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの更新
}

void VirusManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの描画
	
}


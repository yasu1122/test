#ifndef INCLUDED_GAME
#define	INCLUDED_GAME

//******************************************************************************
//
//
//      ゲームシーン
//
//
//******************************************************************************

//==============================================================================
//
//      Gameクラス
//
//==============================================================================

class Game : public Scene, public Singleton<Game>
{
public:

private:
    bool isPaused;

public:
    void init();
    void update();
    void draw();
    void uninit();			

};

static Game* const SCENE_GAME = Game::getInstance();

//******************************************************************************

#endif // !INCLUDED_GAME

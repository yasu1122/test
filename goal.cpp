//******************************************************************************
//
//
//      ゴールクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>


using namespace GameLib;

void goalMove(OBJ2D* obj)
{
	using namespace input;  // 関数内で入力処理を行うときに記述する



	switch (obj->state)
	{
	case 0:
		obj->size = VECTOR2(64.f, 64.f);
        obj->data = &spr_goal;
		
		obj->state++;
		// break;
	case 1:

		break;
	}


}




//--------------------------------
//      初期化
//--------------------------------
void GoalManager::init()
{

    OBJ2DManager::init();   // OBJ2DManagerの初期化
   
}


//--------------------------------
//      更新処理
//--------------------------------
void GoalManager::update()
{
    OBJ2DManager::update(); // OBJ2DManagerの更新
}


//--------------------------------
//      描画処理
//--------------------------------
void GoalManager::draw()
{
    OBJ2DManager::draw();   // OBJ2DManagerの描画
	
   /* for (auto& p:*pGoalManager)
    {
        primitive::rect(VECTOR2(p.position.x, p.position.y) - pBGManager->getScrollPos(), VECTOR2(p.size.x+p.size.x, p.size.y+p.size.y), VECTOR2(p.size.x, p.size.y), 0.0f, VECTOR4(1, 1, 0, 1));
    }*/
}

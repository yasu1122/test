#pragma once


class Text
{
private:
	static const int TEX_W = 512;
	int			texNo;
	wchar_t*	str;
	int			size;
	int			numOf1Line;
	int			roop_cnt;
	void _render(wchar_t c, VECTOR2 pos, VECTOR2 scale, VECTOR4 color);

	//wchar_t**	pStr;	// 文字列の先頭
	int			pStr;
	int			valuePerLine;		// 一行ごとの文字数
	int			valuePerRow;		// 一列ごとの文字数

public:
	bool		strEnd;

public:
	Text(int texNo, const char* textFilename);
	~Text() { if (str)delete[] str; }
	void Render(wchar_t* text, VECTOR2 pos, VECTOR2 scale, VECTOR4 color = VECTOR4(0, 0, 0, 0), int timer = 0, float maxWidth = FLT_MAX, float maxHeight = FLT_MAX);
	void CntReset() { roop_cnt = 0; }

};
#include "all.h"

#include <string>
#include <bitset>
#include <DirectXMath.h>

using namespace GameLib;

//-----------------< 定数 >--------------------------
#define UI_SCROLL_DRAW          (true)
#define UI_NOT_SCROLL_DRAW      (false)

#define UI_ROOM_TEXT_POSX       (system::SCREEN_WIDTH*0.5f)
#define UI_ROOM_TEXT_POSY       (41.0f)

#define UI_SPGAGE_SCALE         (VECTOR2(0.3f,0.3f))
#define UI_SPGAGE_COLOR         (VECTOR4(1.0f,1.0f,1.0f,1.0f))
#define UI_SPGAGE_POSX_ADJUST   (60.0f)
#define UI_SPGAGE_POSY_ADJUST   (35.0f)

#define UI_HPBAR_SCALE          (VECTOR2(1.0f,2.0f))
#define UI_HPBAR_COLOR          (VECTOR4(1.0f,1.0f,1.0f,1.0f))

#define UI_LIFE_SCALE           (VECTOR2(0.2f,0.2f))
#define UI_LIFE_COLOR           (VECTOR4(1.0f,1.0f,1.0f,1.0f))
#define UI_LIFE_POSX_ADJUST     (60.0f)
#define UI_LIFE_POSY_ADJUST     (70.0f)

#define UI_DRAW_ANGLE           (0.0f)

void UiSpGage(OBJ2D*);
void UiURLBar(OBJ2D*);
void UiLife(OBJ2D*);

UI_INIT_DATA UiInitArray[] =
{
	 { UiSpGage, 0.0f, 0.0f },
     { UiURLBar, 960.0f, 48.0f },
     { UiLife, 0.0f,0.0f},

	 { nullptr },
};

SpriteData* hpdata[]
{
	&spr_hp4,
	&spr_hp3,
	&spr_hp2,
	&spr_hp1,
	&spr_hp0,

};

// 部屋名表記のURL画像
SpriteData* sprURLTextData[] = {
    &spr_url_text_home,
    &spr_url_text_01,
    &spr_url_text_02,
    &spr_url_text_03,
    &spr_url_text_04,
    &spr_url_text_05,
    &spr_url_text_06,
    &spr_url_text_07,
    &spr_url_text_08,
    &spr_url_text_09,
    &spr_url_text_10,
    &spr_url_text_11,
};

//-------------------------------------------
//      残機表示
//-------------------------------------------
void UiSpGage(OBJ2D* obj)
{
    OBJ2D* pPl=pPlayerManager->getOBJ2D(0);

    int iHpData = pPl->iWork[player::iWork::plStock];

	switch (obj->state)
	{
	case 0:
        obj->scale = UI_SPGAGE_SCALE;
        obj->color = UI_SPGAGE_COLOR;
        obj->data = hpdata[iHpData];
		obj->state++;
        obj->flg = UI_SCROLL_DRAW;
		//break;
	case 1:
        obj->data = hpdata[iHpData];
        obj->position.x = pPl->position.x - UI_SPGAGE_POSX_ADJUST * obj->hp;
		obj->position.y = pPl->position.y+ UI_SPGAGE_POSY_ADJUST;
        obj->flg = true;

		break;
	}
}
//-------------------------------------------
//      URLバー
//-------------------------------------------
void UiURLBar(OBJ2D* obj)
{
    
    switch (obj->state)
    {
    case 0:
        obj->scale = UI_HPBAR_SCALE;
        obj->color = UI_HPBAR_COLOR;
        obj->data = &spr_url_bar;
        obj->flg = UI_NOT_SCROLL_DRAW;
        obj->state++;

        // break;

    case 1:

        break;
    }
}
//-------------------------------------------
//      Life
//-------------------------------------------
void UiLife(OBJ2D* obj)
{
    OBJ2D* pPl = pPlayerManager->getOBJ2D(0);

    switch (obj->state)
    {
    case 0:
        obj->scale = UI_LIFE_SCALE;
        obj->color = UI_LIFE_COLOR;
        obj->data = &spr_life;
        obj->flg = UI_SCROLL_DRAW;
        obj->state++;

        // break;

    case 1:
        obj->position.x = pPl->position.x - UI_LIFE_POSX_ADJUST*obj->hp;
        obj->position.y = pPl->position.y + UI_LIFE_POSY_ADJUST;

        break;
    }
}

void UiManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

    setRoomNum(home);
	
	for (int i = 0; ; ++i) {
		auto &p = UiInitArray[i];
		if (!p.mover) break;

		pUiManager->searchSet(p.mover, VECTOR2(p.posX, p.posY));
	}
}

void UiManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

void UiManager::draw()
{

    for (auto & pUi : *pUiManager) {
        if (!pUi.mover)continue;
        if (pUi.flg)
        {
            if(pUi.data) pUi.data->draw(pUi.position-pBGManager->getScrollPos(), pUi.scale, UI_DRAW_ANGLE,pUi.color);
        }
        else {
            if(pUi.data) pUi.data->draw(pUi.position, pUi.scale, UI_DRAW_ANGLE,pUi.color);
        }
    }

    sprURLTextData[getRoomNum()]->draw(VECTOR2(UI_ROOM_TEXT_POSX, UI_ROOM_TEXT_POSY));


	OBJ2D* pl = pPlayerManager->getOBJ2D(0);

	if (flagFunc::Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey))
		spr_gimmickterr03.draw(VECTOR2(pl->position.x-20.0f - pBGManager->getScrollX(), pl->position.y+100.0f - pBGManager->getScrollY()));

	if (flagFunc::Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey2))
		spr_gimmickterr04.draw(VECTOR2(pl->position.x+20.0f - pBGManager->getScrollX(), pl->position.y + 100.0f - pBGManager->getScrollY()));
	
}

#pragma once

// 定　数
#define PORTAL_MAX		    (20)                            // 実体最大数
#define URL_MAX             (12)                            // URL最大数
#define URL_COLORW_MAX      (1.0f)                          // 透明度最大数
#define URL_POS             (VECTOR2(586.0f,446.0f))        // URL登録座標
#define URL_POSX_ADJUST     (66.0f)                         // x座標調整用
#define URL_SCALE           (VECTOR2(1.0f,1.0f))            // URLスケール
#define URL_COLOR_NOTCLEAR  (VECTOR4(1.0f,1.0f,1.0f,1.0f))  // 通常時カラーコード
#define URL_COLOR_CLEAR     (VECTOR4(0.6f,0.6f,0.6f,1.0f))  // クリア時カラーコード
#define URL_ANGLE           (0.0f)                          // URLアングル

namespace portal {
	enum
	{
		getURLflg00 = (1 << 0),
		getURLflg01 = (1 << 1),
		getURLflg02 = (1 << 2),
		getURLflg03 = (1 << 3),
		getURLflg04 = (1 << 4),
		getURLflg05 = (1 << 5),
		getURLflg06 = (1 << 6),
		getURLflg07 = (1 << 7),
		getURLflg08 = (1 << 8),
		getURLflg09 = (1 << 9),
		getURLflg10 = (1 << 10),
		getURLflg11 = (1 << 11),

	};
	enum iWork
	{
		GET_URL_FLG

	};
}

// PORTAL_MAXManagerクラス
class PortalManager : public OBJ2DManager,  public Singleton<PortalManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:

	int getSize() { return PORTAL_MAX; }


};

#define pPortalManager	(PortalManager::getInstance())

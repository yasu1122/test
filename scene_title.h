#ifndef INCLUDED_TITLE
#define	INCLUDED_TITLE

//******************************************************************************
//
//
//      タイトルシーン
//
//
//******************************************************************************

//==============================================================================
//
//      Titleクラス
//
//==============================================================================

class Title : public Scene, public Singleton<Title>
{
private:
	int mode;
public:
	void init();
	void update();
	void draw();
	void uninit();
};

static Title* const SCENE_TITLE = Title::getInstance();

//******************************************************************************

#endif // !INCLUDED_TITLE

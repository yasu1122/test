#ifndef INCLUDED_OBJ2D
#define	INCLUDED_OBJ2D
//******************************************************************************
//
//
//      OBJ2Dクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "./GameLib/vector.h"
#include "./GameLib/obj2d_data.h"


// 前方宣言
class OBJ2D;

//==============================================================================

class OBJ2D;							// 前方宣言
typedef	void(*MOVER)(OBJ2D* obj);		// 関数ポインタ（更新処理）
typedef bool(*ERASER)(OBJ2D* obj);		// 関数ポインタ（消去チェック）


//==============================================================================

//==============================================================================
//
//      OBJ2Dクラス
//
//==============================================================================

class OBJ2D
{
public:
	// メンバ変数
	GameLib::SpriteData*    data;               // スプライトデータ
	GameLib::Anime          anime;              // アニメーションクラス

	ActorParam				actParam;			

	VECTOR2                 position;           // 位置
	VECTOR2                 init_position;           // 初期位置

	float                   invert_pos;           // 位置
	float                   invert_pos2;           // 位置

	float                   clone_pos;           // 位置

    VECTOR2*                linkPos;            // ペアリング用座標

	VECTOR2                 scale;              // スケール
	float                   angle;              // 角度
	VECTOR4                 color;              // 描画色
	VECTOR2                 size;               // あたり用サイズ（縦横）

												// 更新処理で使用
	MOVER					mover;				// 更新処理
	ERASER					eraser;				// 消去チェック
	int						type;				//種類
	float                   velocity;           // 速度
	VECTOR2                 speed;              // 瞬間の移動量ベクトル
	int                     state;              // ステート
	int						act_state;			// アクションステート
	int                     timer;              // タイマー
	int                     param;              // 汎用パラメータ
	int						sway = 10;
	int						index;				//インデックス
	int						cnt;				//汎用カウンタ
	int                     iWork[16];          // 汎用（使い方は自由）
	float                   fWork[20];          // 汎用（使い方は自由）

    int*                    ipWork[16];         // int型ポインタ

	int                     jumpTimer;          // 長押しジャンプタイマー
	int                     mutekiTimer;        // 無敵タイマー
	int                     damageTimer;        // ダメージタイマー
	int						hp;					// HP
	int						getURLcnt;			// URL保持数
	GameLib::fRECT          hitRect;            // あたり判定RECT

	bool                    judgeFlag;          // あたり判定の有無（true:有り / false:無し）
	bool                    isDrawHitRect;      // あたり判定描画フラグ
	bool                    flg;				// 汎用フラグ
	bool                    invert_flg;           // 
	bool                    invert_flg2;           // 

	bool                    inverted_flg;         // 
	bool                    inverted_flg2;         // 

	bool                    clone_flg;         // 


	int                     command;            // 他のオブジェクトからのコマンド
	VECTOR2                 delta;              // 移動量の差分

    bool                    onGround;           // 地面フラグ（地上にいる）
    bool                    xFlip;              // x方向反転フラグ
    bool                    kabeFlag;           // 壁フラグ（一歩先が壁）
    bool                    gakeFlag;           // 崖フラグ（一歩先が崖）

	// 追加
	bool					moveFlg;			// 移動中かのフラグ
	float					radius;
	int						passiveItem;		// アイテム所持枠	
	bool					throughFlg;			// オブジェクト貫通フラグ
	OBJ2D*					parent;
	OBJ2D*					target;
    wchar_t*                text;



public:

	OBJ2D();        // コンストラクタ
    ~OBJ2D();       // デストラクタ
	void clear();   // メンバ変数のクリア
	void update();
	void draw();    // 描画
	void draw_not_scroll();    // 描画

	void GameSway(OBJ2D*obj);//画面揺れ
	void PosInverce(OBJ2D*obj);//画面揺れ

	bool animeUpdate(GameLib::AnimeData* animeData);    // アニメーションのアップデート
};

//==============================================================================

// OBJ2DManagerクラス
class OBJ2DManager
{
protected:
	std::vector<OBJ2D>	obj_w;
public:
	virtual void init();				// 初期設定
	virtual	void update();				// 更新処理
	virtual void draw();				// 描画処理
	virtual void draw_not_scroll();				// 描画処理


	// ゲッターセッター諸々
	OBJ2D* getOBJ2D(int workNum) { return &obj_w[workNum]; }
	
	void setFWork(int workNum, int fWorkNum, float setFloat) { obj_w[workNum].fWork[fWorkNum] = setFloat; }
	
	//float getFWork(int workNum, int fWorkNum) { return obj_w[workNum].fWork[fWorkNum]; }
	//VECTOR2 getPos(int workNum) { return obj_w[workNum].position; }
	//bool getMoveFlg(int workNum) { return obj_w[workNum].moveFlg; }
	

	OBJ2D* searchSet(MOVER mover, VECTOR2 pos);	// OBJの追加
												//画面揺れ

	auto begin() { return obj_w.begin(); }
	auto end() { return obj_w.end(); }
private:
	virtual int getSize() = 0;
};


//******************************************************************************

#endif// ! INCLUDED_OBJ2D
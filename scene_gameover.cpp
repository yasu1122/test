//******************************************************************************
//
//
//      ゲームオーバー
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;



// 初期化
void GameOver::init()
{
    Scene::init();
}


//--------------------------------
//  更新処理
//--------------------------------
void GameOver::update()
{
    using namespace input;
    using namespace flagFunc;

    switch (state)
    {
    case 0:
        //////// 初期設定 ////////

        timer = 0;  // タイマーを初期化
        GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
        pMousePointa->init(0);
        pGameOverMenu->init();
		music::load(BGM_OVER, L"./Data/Musics/gameover.wav", 0.3f);
		music::play(BGM_OVER, true);

        texture::load(loadTexture);

        state++;    // 初期化処理の終了
                    //break;    // 意図的なコメントアウト

    case 1:
        //////// 通常時の処理 ////////
		if (TRG(0) & PAD_START)//クリック音
			sound::play(SE_CLICK);


        timer++;    // タイマーを足す


        pMousePointa->update();
        pGameOverMenu->update();

        pJudgeGameOverManager->update();

        if (TRG(0) & PAD_START&&pGameOverMenu->getTitleFlg()) {
            changeScene(SCENE_TITLE);  // タイトルへ戻る
        }
        if (TRG(0) & PAD_START&&pGameOverMenu->getSelectFlg()) {
            changeScene(SCENE_SELECT); // セレクト画面へ戻る
        }
        break;
    }

	
}

//--------------------------------
//  描画処理
//--------------------------------
void GameOver::draw()
{
    // 画面クリア
    GameLib::clear(VECTOR4(0.0f, 0.0f, 0.0f, 1));

    spr_game_over.draw(VECTOR2(system::SCREEN_WIDTH * 0.5f, (system::SCREEN_HEIGHT *0.5f)));


	pMousePointa->draw();
    pGameOverMenu->draw();
	

}
void GameOver::uninit()
{
	music::unload(BGM_OVER);
}
//******************************************************************************

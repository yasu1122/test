//******************************************************************************
//
//
//      ブックマークによるジャンプ移動不可の部屋を設定
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//------<変　数>----------------------------------------------------------------

// 更新処理
void monsterRoomMove(OBJ2D* obj)
{
    using namespace input;  // 関数内で入力処理を行うときに記述する

    switch (obj->state)
    {
    case 0:
        obj->size = VECTOR2((float)(system::SCREEN_WIDTH *0.5f), (float)(system::SCREEN_HEIGHT *0.5f));
        obj->data = &spr_monster_room;
        obj->state++;
        // break;
    case 1:
        // 意図的に何の処理もしていない

        break;
    }

}

// 初期化
void MonsterRoom::init() {
    OBJ2DManager::init();   // OBJ2DManagerの初期化

    STAGE_SCRIPT*pScript = STAGE->getStageScript();

    // 部屋のテキストデータの読み込み
    char** mapMonsterRoom = new char*[pBGManager->CHIP_NUM_Y];
    // データ読み込み領域を動的確保
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapMonsterRoom[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapMonsterRoom[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

    if (!pBGManager->loadMapData(pScript->fileNameMonsterRoom, mapMonsterRoom))
    {
        assert(!"部屋のテキストデータ読み込み失敗");
    }

    // 部屋のテキストデータの配置
    for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
        {
            const int doorIndex = mapMonsterRoom[y][x];
            if (doorIndex < 0)continue;

                // 生成
                searchSet(&monsterRoomMove, VECTOR2(static_cast<float>((x*BG::CHIP_SIZE + BG::CHIP_SIZE / 2)+16.0f), static_cast<float>(y*BG::CHIP_SIZE + BG::CHIP_SIZE)));

        }
    }

    // 確保した領域を解放
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapMonsterRoom[i];
    }
    delete[] mapMonsterRoom;
};

// 更新
void MonsterRoom::update()
{
    OBJ2DManager::update();
}

// 描　画
void MonsterRoom::draw()
{
    OBJ2DManager::draw();
}
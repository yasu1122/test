//******************************************************************************
//
//
//      タイトル
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;

void Title::init()
{
	Scene::init();	    // 基底クラスのinitを呼ぶ
}

//--------------------------------
//  更新処理
//--------------------------------
void Title::update()
{
    using namespace input;
    using namespace flagFunc;

    switch (state)
    {
    case 0:
        //////// 初期設定 ////////
		//GameLib::window::SetWindowPosTop();

        timer = 0;  // タイマーを初期化
		GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
		texture::load(loadTexture);    // テクスチャの読み込み
		music::load(BGM_TITLE, L"./Data/Musics/tuto.wav", 0.3f);
		music::play(BGM_TITLE,true);

		sound::load(L"./Data/Sounds/SE.xwb", 1.0f);

		pMousePointa->init(0);
        pTitleMenu->init();

        state++;    // 初期化処理の終了
        //break;    // 意図的なコメントアウト

    case 1:
        //////// 通常時の処理 ////////
		
		if (TRG(0) & PAD_START)//クリック音
			sound::play(SE_CLICK);


        timer++;    // タイマーを足す

		
        pMousePointa->update();
        pTitleMenu->update();

        pJudgeTitleManager->update();

        if (TRG(0) & PAD_START&&pTitleMenu->getStartFlg()) {
            changeScene(SCENE_SELECT);  // ゲームシーンに切り替え
        }
        if (TRG(0) & PAD_START&&pTitleMenu->getExitFlg()) {            
            GameEnd();                  // ゲームを終了
        }
        break;
    }

	
}

//--------------------------------
//  描画処理
//--------------------------------
void Title::draw()
{
    // 画面クリア
    GameLib::clear(VECTOR4(0.0f, 0.0f, 0.0f, 1));

    //font::textOut(4, "BOOKMAKER", VECTOR2(250, 100), VECTOR2(5.0f, 5.0f), VECTOR4(1, 1, 0, 1));

    spr_title_sprite.draw(VECTOR2(system::SCREEN_WIDTH * 0.5f, (system::SCREEN_HEIGHT *0.5f )));
	
    pTitleMenu->draw();

    pMousePointa->draw();

}

//******************************************************************************
void Title::uninit()
{

	// テクスチャの解放
	texture::releaseAll();
	music::stop(BGM_TITLE);
	music::unload(BGM_TITLE);

}

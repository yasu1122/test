#include"all.h"


using namespace GameLib;




void portal_move(OBJ2D* obj)
{
	obj->color.w = URL_COLORW_MAX;
	obj->data = &spr_portal;
	if (pPlayerManager->getOBJ2D(0)->getURLcnt >= URL_MAX)
		obj->color.w = URL_COLORW_MAX;
}

MOVER portal_move_array[] =
{
	portal_move,
};

void PortalManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = STAGE->getStageScript();

	// 敵データの読み込み
	char** mapPortal = new char*[pBGManager->CHIP_NUM_Y];
	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
		mapPortal[i] = new char[pBGManager->CHIP_NUM_X];
		SecureZeroMemory(mapPortal[i], sizeof(char)*pBGManager->CHIP_NUM_X);
	}

	if (!pBGManager->loadMapData(pScript->fileNamePortal, mapPortal))
	{
		assert(!"ポータルデータ読み込み失敗");
	}

	// 敵配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int portalIndex = mapPortal[y][x];
			if (portalIndex < 0) continue;
			searchSet(portal_move_array[portalIndex], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);

		}
	}

	for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
	{
		delete[] mapPortal[i];
	}
	delete[] mapPortal;

}

void PortalManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

SpriteData Portal_DATA[][URL_MAX] =
{
    {   spr_url00_00,
        spr_url00_01,
        spr_url00_02,
        spr_url00_03,
        spr_url00_04,
        spr_url00_05,
        spr_url00_06,
        spr_url00_07,
        spr_url00_08,
        spr_url00_09,
        spr_url00_10,
        spr_url00_11,
    },

    {   spr_url01_00,
        spr_url01_01,
        spr_url01_02,
        spr_url01_03,
        spr_url01_04,
        spr_url01_05,
        spr_url01_06,
        spr_url01_07,
        spr_url01_08,
        spr_url01_09,
        spr_url01_10,
        spr_url01_11,
    },

};

void PortalManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの初期化


	bool Portal_Array[] =
	{
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg00),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg01),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg02),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg03),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg04),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg05),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg06),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg07),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg08),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg09),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg10),
		flagFunc::Flag_Check(obj_w[0].iWork[portal::iWork::GET_URL_FLG], portal::getURLflg11),
	};

    VECTOR2 URLPos = URL_POS;

	for (int i = 0; i < URL_MAX; i++)
	{
		if (Portal_Array[i])
		{
			if (pPlayerManager->getOBJ2D(0)->getURLcnt >= URL_MAX)
				Portal_DATA[1][i].draw(VECTOR2(URLPos.x + URL_POSX_ADJUST * i, URLPos.y ), URL_SCALE, URL_ANGLE, URL_COLOR_NOTCLEAR);
			else
				Portal_DATA[0][i].draw(VECTOR2(URLPos.x + URL_POSX_ADJUST * i, URLPos.y ), URL_SCALE, URL_ANGLE, URL_COLOR_CLEAR);

		}

	}

}



//******************************************************************************
//
//
//      マウスポインタクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"
#include "GameLib\resource.h"

using namespace GameLib;

using namespace input;

//--------------------------------
//  マウスポインタ関数
//--------------------------------

// マウスポインタの位置情報取得
VECTOR2 getMousePos()
{
    return VECTOR2((float)getCursorPosX(), (float)getCursorPosY());
}


// ゲーム終了時に範囲制限解除しないといけない
// マウスポインタ範囲制限
void mouseMergin()
{
	////using namespace flagFunc;

	//if (!GetActiveWindow())
	//{
	//	//Flag_On(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg)
	//	ClipCursor(NULL);
	//	return;
	//}
	//RECT rc, rs;
	//rs = window::GetWindowRect();
	//rc = rs;
	//// 微修正
	//rc.top = (LONG)(rs.top + 32.f);
	//rc.left = (LONG)(rs.left + 8.f);
	//rc.bottom = (LONG)(rs.bottom - 8.f);
	//rc.right = (LONG)(rs.right - 8.f);

	//ClipCursor(&rc);
}

// マウスポインタ更新処理
void mouseMove(OBJ2D* obj)
{
//    AnimeData* animeData = nullptr;

    switch (obj->state)
    {
    case 0:
        //////// 初期設定 ////////
        obj->size = VECTOR2(8.0f, 8.0f);
        obj->position = VECTOR2(0.0f, 0.0f);
        obj->scale = VECTOR2(0.3f, 0.3f);
        obj->angle = ToRadian(0);
        obj->iWork[mouse_pointa::iWork::doorLink] = -1;
        obj->iWork[mouse_pointa::iWork::enmBlockName] = -1;
		obj->data = &spr_arrow;
        obj->radius = 0.0f;
        obj->state++;
	
        // break;
    case 1:
        obj->position = getMousePos();

        obj->data = (pMousePointa->getHitFlg()) ? &spr_hand00 : &spr_arrow;

        break;

    }
}

// 開始演出用更新処理
#define SCENE_START_MOVE_MAX (2)

bool MouseMoveEnd(OBJ2D* obj, float endX, float endY)
{
    float x = obj->position.x - endX;
    float y = obj->position.y - endY;
    return (sqrtf(y * y + x * x) <= obj->radius + 16.0f);

}

VECTOR2 scene_start_pos_data[SCENE_START_MOVE_MAX] =
{
    {-400.0f,0.0f},
    {200.0f,0.0f},
};

bool scene_start_mouse(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace vecFunc;

    OBJ2D* pPl = pPlayerManager->getOBJ2D(0);

    switch (obj->act_state)
    {
    case 0:
        pEffectManager->searchSet(effect_move_wave, obj->position);
        Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::plClickMove);
        sound::play(SE_CLICK);

        obj->act_state++;
        break;

    case 1:
        if (Flag_Check(obj->iWork[mouse_pointa::iWork::actFlg], mouse_pointa::clickLabel::plHitFlg))
        {
            obj->act_state++;
        }

        break;

    case 2:
    {
        VECTOR2 endPos;

        endPos.x = obj->fWork[mouse_pointa::fWork::moveEndPosX] = obj->position.x + scene_start_pos_data[obj->cnt].x;
        endPos.y = obj->fWork[mouse_pointa::fWork::moveEndPosY] = obj->position.y + scene_start_pos_data[obj->cnt].y;
        endPos = UnitVec(obj->position, endPos);

        obj->speed.x=obj->velocity*endPos.x;
        obj->speed.y=obj->velocity*endPos.y;
        
        if (obj->cnt >= SCENE_START_MOVE_MAX)
        {
            obj->act_state = 0;
            obj->cnt = 30;
            return true;
        }

        obj->cnt++;
        obj->act_state++;

        break;
    }
    case 3:
    {
        obj->position.x += obj->speed.x;
        obj->position.y += obj->speed.y;

        bool endFlg = MouseMoveEnd(obj, obj->fWork[mouse_pointa::fWork::moveEndPosX], obj->fWork[mouse_pointa::fWork::moveEndPosY]);

        if (endFlg) obj->act_state = 0;


        break;
    }
    case 4:


        break;
    }
 
    return false;
}

bool scene_start_menu(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace vecFunc;

    OBJ2D* pPl = pPlayerManager->getOBJ2D(0);

    switch (obj->act_state)
    {
    case 0:
        pEffectManager->searchSet(effect_move_wave, obj->position);
        Flg_On(pPl->iWork[player::iWork::actflg], player::ActFlg::plClickMove);

        obj->act_state++;
        break;

    case 1:
        if (Flag_Check(obj->iWork[mouse_pointa::iWork::actFlg], mouse_pointa::clickLabel::plHitFlg))
        {
            obj->act_state++;
        }

        break;

    case 2:
    {
        VECTOR2 endPos;

        endPos.x = obj->fWork[mouse_pointa::fWork::moveEndPosX] = obj->position.x + scene_start_pos_data[obj->cnt].x;
        endPos.y = obj->fWork[mouse_pointa::fWork::moveEndPosY] = obj->position.y + scene_start_pos_data[obj->cnt].y;
        endPos = UnitVec(obj->position, endPos);

        obj->speed.x = obj->velocity*endPos.x;
        obj->speed.y = obj->velocity*endPos.y;

        if (obj->cnt >= SCENE_START_MOVE_MAX)
        {
            obj->act_state = 4;
            obj->cnt = 30;
            return true;
        }

        obj->cnt++;
        obj->act_state++;

        break;
    }
    case 3:
    {
        obj->position.x += obj->speed.x;
        obj->position.y += obj->speed.y;

        bool endFlg = MouseMoveEnd(obj, obj->fWork[mouse_pointa::fWork::moveEndPosX], obj->fWork[mouse_pointa::fWork::moveEndPosY]);

        if (endFlg) obj->act_state = 0;


        break;
    }
    case 4:
        // 意図的に何もしていない
        break;
    }

    return false;
}

void mouseMove2(OBJ2D* obj)
{
    using namespace flagFunc;
    using namespace vecFunc;

    OBJ2D* pMenu = pMenuManager->getOBJ2D(0);

    //    AnimeData* animeData = nullptr;

    switch (obj->state)
    {
    case 0:
        //////// 初期設定 ////////
        obj->size = VECTOR2(30.0f, 30.0f);
        obj->position = VECTOR2((system::SCREEN_WIDTH/2.0f)+200.0f, system::SCREEN_HEIGHT/2.0f);
        obj->angle = ToRadian(0);
        obj->scale = VECTOR2(0.3f, 0.3f);
        obj->iWork[mouse_pointa::iWork::doorLink] = -1;
        obj->iWork[mouse_pointa::iWork::enmBlockName] = -1;
        obj->data = &spr_arrow;
        obj->velocity = 7.0f;
        obj->radius = 0.0f;
        obj->act_state = 0;
        obj->cnt = 60;
        obj->state++;

        // break;
    case 1:
        obj->cnt--;

        if (obj->cnt <= 0) {
            obj->cnt = 0;
            obj->state++;
        }
        
        break;

    case 2:
        if (scene_start_mouse(obj))obj->state++;

        break;

    case 3:
        obj->cnt--;

        if (obj->cnt <= 0)
        {
            Flg_On(pMenu->iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg);

            VECTOR2 endPos;
            endPos.x = obj->fWork[mouse_pointa::fWork::moveEndPosX] = pMenu->position.x;
            endPos.y = obj->fWork[mouse_pointa::fWork::moveEndPosY] = pMenu->position.y;
            endPos = UnitVec(obj->position, endPos);

            obj->speed.x = obj->velocity*endPos.x;
            obj->speed.y = obj->velocity*endPos.y;
            obj->state++;
        }
        break;

    case 4:
    {
        obj->position += obj->speed;

        bool endFlg = MouseMoveEnd(obj, obj->fWork[mouse_pointa::fWork::moveEndPosX], obj->fWork[mouse_pointa::fWork::moveEndPosY]);

        if (endFlg) {
            obj->state++;
            Flg_On(pMenu->iWork[menu::iWork::actFlg], menu::menuLabel::storageFlg);
            sound::play(SE_CLICK);
            obj->cnt = 30;
        }
        break;
    }
    case 5:
        obj->cnt--;

        if (obj->cnt <= 0) {
            obj->cnt = 30;
            pBlackoutManager->searchSet(blackout_move05, BLACK_OUT_INIT_POS);
            obj->state++;
        }

        break;

    case 6:
        obj->cnt--;

        if (obj->cnt <= 0) {
            Flg_On(obj->iWork[mouse_pointa::iWork::actFlg], mouse_pointa::clickLabel::sceneChangeFlg);
        }
        break;
    }

    //mouseMergin();

}

MOVER mousePointaMoveArray[] =
{
    mouseMove,
    mouseMove2,
    nullptr,
};

//--------------------------------
//  初期化
//--------------------------------
void MousePointa::init(int moveNum)
{
    OBJ2DManager::init();       // OBJ2DManagerの初期化

    hitFlg = false;

    // 生成
    begin()->mover = mousePointaMoveArray[moveNum];

}


//--------------------------------
//  更　新
//--------------------------------
void MousePointa::update()
{
    // 移動処理
    OBJ2DManager::update(); // OBJ2DManagerの更新
}

//--------------------------------
//  描　画
//--------------------------------
void MousePointa::draw()
{
    // 描画処理
    OBJ2DManager::draw_not_scroll();   // OBJ2DManagerの描画

}


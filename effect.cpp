#include "All.h"

using namespace GameLib;

AnimeData animeEffectWave[] = {
	{ &spr_wave_effect00, 2 },
	{ &spr_wave_effect01, 2 },
	{ &spr_wave_effect02, 2 },
	{ &spr_wave_effect03, 2 },
	{ &spr_wave_effect04, 2 },
	{ nullptr, -1 },// 終了フラグ
};

AnimeData animeEffectClick[] = {
    { &spr_effect_click00, 2 },
    { &spr_effect_click01, 2 },
    { &spr_effect_click02, 2 },
    { &spr_effect_click03, 2 },
    { &spr_effect_click04, 2 },
    { &spr_effect_click05, 2 },
    { &spr_effect_click05, 2 },
    { nullptr, -1 },    // 終了フラグ
};

AnimeData animeEffectClick01[] = {
    { &spr_effect_click00, 4 },
    { &spr_effect_click01, 4 },
    { &spr_effect_click02, 4 },
    { &spr_effect_click03, 4 },
    { &spr_effect_click04, 4 },
    { &spr_effect_click05, 4 },
    { &spr_effect_click05, 4 },
    { &spr_effect_click00, 4 },
    { &spr_effect_click01, 4 },
    { &spr_effect_click02, 4 },
    { &spr_effect_click03, 4 },
    { &spr_effect_click04, 4 },
    { &spr_effect_click05, 4 },
    { &spr_effect_click05, 4 },
    { nullptr, -1 },    // 終了フラグ
};


AnimeData animeEffectBook[] = {
	{&spr_effect_move00},
	{&spr_effect_move01},
	{&spr_effect_move02},
	{&spr_effect_move03},
	{&spr_effect_move04},
	{&spr_effect_move05},
	{&spr_effect_move06},
	{&spr_effect_move07},
	{&spr_effect_move08},
	{&spr_effect_move09},
	{&spr_effect_move10},
	{&spr_effect_move11},
	{&spr_effect_move12},
	{&spr_effect_move13},
	{&spr_effect_move14},
	{ nullptr, -1 },// 終了フラグ
};

AnimeData effectFavoButton[] = {
    {&spr_favo_button01, 64},
    { nullptr , -1},// 終了フラグ
};

void EffectManager::init()
{
	OBJ2DManager::init();
}


void EffectManager::update()
{
	OBJ2DManager::update();
}


void EffectManager::draw()
{
	for (auto & effect : *pEffectManager)
	{
		if(effect.mover == effect_move_wave||effect.mover==effect_move_wave01)
		OBJ2DManager::draw_not_scroll();
		else if(effect.mover == effect_move_mark)
		OBJ2DManager::draw();

	}
}

void effect_move_wave(OBJ2D*obj)
{
	AnimeData* animeData = NULL;
	animeData = animeEffectClick;
	obj->scale = VECTOR2(1.0f, 1.0f);
	if (obj->animeUpdate(animeData))
		obj->clear();

}

void effect_move_wave01(OBJ2D*obj)
{
    AnimeData* animeData = NULL;
    animeData = animeEffectClick01;
    obj->scale = VECTOR2(2.0f, 2.0f);
    if (obj->animeUpdate(animeData))
        obj->clear();

}

void effect_move_favo_button(OBJ2D* obj)
{
    AnimeData* animeData = NULL;

    switch (obj->state)
    {
    case 0:
        obj->scale = VECTOR2(1.4f, 1.4f);
        obj->state++;

        // break;
    case 1:
        animeData = effectFavoButton;

        obj->scale.x -= 0.015f;
        obj->scale.y -= 0.015f;
        obj->color.w -= 0.016f;

        if (obj->scale.x <= 1.0f)obj->scale = VECTOR2(1.4f, 1.4f);

        if (obj->animeUpdate(animeData)) obj->clear();

        break;
    }
}

void effect_move_mark(OBJ2D*obj)
{

	AnimeData* animeData = NULL;
	animeData = animeEffectBook;
	obj->scale = VECTOR2(0.4f, 0.4f);
	if (obj->animeUpdate(animeData))
		obj->clear();
}

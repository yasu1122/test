#ifndef  _ROOM_TEXT
#define  _ROOM_TEXT
//******************************************************************************
//
//
//      room_text.h
//
//
//******************************************************************************

//-------------------<定数>-----------------------------------------------------
#define ROOM_TEXT_MAX            (20)

enum iWork {
	roomNum,
};

// 部屋名指定用のラベル
enum roomTextRabel {
    home,
    room_01,
    room_02,
    room_03,
    room_04,
    room_05,
    room_06,
    room_07,
    room_08,
    room_09,
    room_10,
    room_11,
    room_12,
};

//===================================================
//
//      RoomTextクラス
//
//===================================================
class RoomText :public OBJ2DManager, public Singleton<RoomText>
{
public:
    void init();    // 初期化
    void update();  // 更新

private:
    int getSize() { return ROOM_TEXT_MAX; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pRoomText  (RoomText::getInstance())

//******************************************************************************
#endif  //_ROOM_TEXT

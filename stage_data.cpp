#include "all.h"

//******************************************************************************
//
//      チュートリアル
//
//******************************************************************************

//チュートリアルのデータ
STAGE_SCRIPT tutorial_script[] =
{
    "./Data/Maps/map/stage_tutorial_player.csv", 						       
    "./Data/Maps/map/stage_tutorial_door.csv",						       
    "./Data/Maps/map/stage_tutorial_back.csv",					       
    "./Data/Maps/map/stage_tutorial_terr.csv",
    "./Data/Maps/map/stage_tutorial_gimmick.csv",
	"./Data/Maps/map/stage_tutorial_gimmick_terr.csv",
	"./Data/Maps/map/stage_tutorial_gimmick_terr2.csv",
    "./Data/Maps/map/stage_tutorial_enemy.csv",
	"./Data/Maps/map/stage_tutorial_virusu.csv",
    "./Data/Maps/map/stage_tutorial_item.csv",
	"./Data/Maps/map/stage_tutorial_portal.csv",
    "./Data/Maps/map/stage_tutorial_enemy_jump.csv",
    "./Data/Maps/map/stage_tutorial_room_name.csv",
    "./Data/Maps/map/stage_tutorial_one_place_door.csv",
    "./Data/Maps/map/stage_tutorial_one_place.csv",
    "./Data/Maps/map/stage_tutorial_dismove.csv",

};


//******************************************************************************
//
//      stage01
//
//******************************************************************************

//ステージ１のデータ
STAGE_SCRIPT stage1_script[] = 
{
	"./Data/Maps/map/stage_easy_player.csv",
	"./Data/Maps/map/stage_easy_door.csv",
	"./Data/Maps/map/stage_easy_back.csv",
	"./Data/Maps/map/stage_easy_terr.csv",
	"./Data/Maps/map/stage_easy_gimmick.csv",
	"./Data/Maps/map/stage_easy_gimmick_terr.csv",
	"./Data/Maps/map/stage_easy_gimmick_terr2.csv",
	"./Data/Maps/map/stage_easy_enemy.csv",
	"./Data/Maps/map/stage_easy_virusu.csv",
	"./Data/Maps/map/stage_easy_item.csv",
	"./Data/Maps/map/stage_easy_portal.csv",
	"./Data/Maps/map/stage_easy_enemy_jump.csv",
	"./Data/Maps/map/stage_easy_room_name.csv",
	"./Data/Maps/map/stage_easy_one_place_door.csv",
	"./Data/Maps/map/stage_easy_one_place.csv",
	"./Data/Maps/map/stage_easy_dismove.csv",

};

//******************************************************************************
//
//      stage02
//
//******************************************************************************

//ステージ2のデータ
STAGE_SCRIPT stage2_script[] =
{
    "./Data/Maps/map/stage_hh_player.csv",
    "./Data/Maps/map/stage_hh_door.csv",
    "./Data/Maps/map/stage_hh_back.csv",
    "./Data/Maps/map/stage_hh_terr.csv",
    "./Data/Maps/map/stage_hh_gimmick.csv",
    "./Data/Maps/map/stage_hh_gimmick_terr.csv",
    "./Data/Maps/map/stage_hh_gimmick_terr2.csv",
    "./Data/Maps/map/stage_hh_enemy.csv",
    "./Data/Maps/map/stage_hh_virusu.csv",
    "./Data/Maps/map/stage_hh_item.csv",
    "./Data/Maps/map/stage_hh_portal.csv",
    "./Data/Maps/map/stage_hh_enemy_jump.csv",
    "./Data/Maps/map/stage_hh_room_name.csv",
    "./Data/Maps/map/stage_hh_one_place_door.csv",
    "./Data/Maps/map/stage_hh_one_place.csv",
    "./Data/Maps/map/stage_hh_dismove.csv",

};

//******************************************************************************
//
//      stage03
//
//******************************************************************************

//ステージ3のデータ
STAGE_SCRIPT stage3_script[] =
{ 
	"./Data/Maps/map/stage_normal_player.csv",
	"./Data/Maps/map/stage_normal_door.csv",
	"./Data/Maps/map/stage_normal_back.csv",
	"./Data/Maps/map/stage_normal_terr.csv",
	"./Data/Maps/map/stage_normal_gimmick.csv",
	"./Data/Maps/map/stage_normal_gimmick_terr.csv",
	"./Data/Maps/map/stage_normal_gimmick_terr2.csv",
	"./Data/Maps/map/stage_normal_enemy.csv",
	"./Data/Maps/map/stage_normal_virusu.csv",
	"./Data/Maps/map/stage_normal_item.csv",
	"./Data/Maps/map/stage_normal_portal.csv",
	"./Data/Maps/map/stage_normal_enemy_jump.csv",
	"./Data/Maps/map/stage_normal_room_name.csv",
	"./Data/Maps/map/stage_normal_one_place_door.csv",
	"./Data/Maps/map/stage_normal_one_place.csv",
	"./Data/Maps/map/stage_normal_dismove.csv",

};

//******************************************************************************
//
//      stage04
//
//******************************************************************************

//ステージ4のデータ
STAGE_SCRIPT stage4_script[] =
{
    "./Data/Maps/map/stage_advance_player.csv",
    "./Data/Maps/map/stage_advance_door.csv",
    "./Data/Maps/map/stage_advance_back.csv",
    "./Data/Maps/map/stage_advance_terr.csv",
    "./Data/Maps/map/stage_advance_gimmick.csv",
    "./Data/Maps/map/stage_advance_gimmick_terr.csv",
    "./Data/Maps/map/stage_advance_gimmick_terr2.csv",
    "./Data/Maps/map/stage_advance_enemy.csv",
    "./Data/Maps/map/stage_advance_virusu.csv",
    "./Data/Maps/map/stage_advance_item.csv",
    "./Data/Maps/map/stage_advance_portal.csv",
    "./Data/Maps/map/stage_advance_enemy_jump.csv",
    "./Data/Maps/map/stage_advance_room_name.csv",
    "./Data/Maps/map/stage_advance_one_place_door.csv",
    "./Data/Maps/map/stage_advance_one_place.csv",
    "./Data/Maps/map/stage_advance_dismove.csv",

};

//******************************************************************************
//
//      stage05
//
//******************************************************************************

//ステージ5のデータ
STAGE_SCRIPT stage5_script[] =
{
	"./Data/Maps/map/stage_hard_player.csv",
	"./Data/Maps/map/stage_hard_door.csv",
	"./Data/Maps/map/stage_hard_back.csv",
	"./Data/Maps/map/stage_hard_terr.csv",
	"./Data/Maps/map/stage_hard_gimmick.csv",
	"./Data/Maps/map/stage_hard_gimmick_terr.csv",
	"./Data/Maps/map/stage_hard_gimmick_terr2.csv",
	"./Data/Maps/map/stage_hard_enemy.csv",
	"./Data/Maps/map/stage_hard_virusu.csv",
	"./Data/Maps/map/stage_hard_item.csv",
	"./Data/Maps/map/stage_hard_portal.csv",
	"./Data/Maps/map/stage_hard_enemy_jump.csv",
	"./Data/Maps/map/stage_hard_room_name.csv",
	"./Data/Maps/map/stage_hard_one_place_door.csv",
	"./Data/Maps/map/stage_hard_one_place.csv",
	"./Data/Maps/map/stage_hard_dismove.csv",

};






// ステージデータのアドレス
STAGE_SCRIPT* stage_script[] = {
    tutorial_script,
	stage1_script,
	stage2_script,
    stage3_script,    
    stage4_script,
    stage5_script,
	nullptr,
};

//開始前文字列
char* stage_char[] = {

	"STAGE 1 START",
	"STAGE 2 START",

};







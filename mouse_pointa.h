#ifndef  _INCLUDE_MOUSE_POINTA
#define  _INCLUDE_MOUSE_POINTA
//******************************************************************************
//
//
//      mouse_pointa.h
//
//
//******************************************************************************

//-------------------<定数>-----------------------------------------------------
namespace mouse_pointa
{
    enum clickLabel {
        moveFlg         = (1<<0) ,
        storageFlg      = (1<<1) , 
        plHitFlg        = (1<<2) ,
        sceneChangeFlg  = (1<<3),
    };

    // iWorkのラベル
    enum iWork {
        actFlg,
        storage,
        doorLink,
        enmBlockName,
        
    };

    // fWorkのラベル
    enum fWork {
        Pos,
        Angle,
        moveEndPosX,
        moveEndPosY,
        moveVecX,
        moveVecY,
    };

}

#define MOUSE_POINT_MAX     (1)

//==============================================================================
//
//      MousePointaクラス
//
//==============================================================================
class MousePointa : public OBJ2DManager, public Singleton<MousePointa>
{
    
public:
    void init(int moveNun);    // 初期化
    void update();  // 更新
    void draw();    // 描画

    void setHitFlg(bool set) { hitFlg = set; }
    bool getHitFlg() { return hitFlg; }

    void setScrollPos(VECTOR2 plPos,VECTOR2 scrollPos) 
    { 
        scrollPlPos.objPos.x = plPos.x; scrollPlPos.objPos.y = plPos.y; 
        scrollPlPos.scrollPos.x = scrollPos.x; scrollPlPos.scrollPos.y = scrollPos.y;
    }
    JumpData::posandscroll getScrollPos() { return scrollPlPos; }

private:
    int getSize() { return MOUSE_POINT_MAX; }
    bool hitFlg;

    JumpData::posandscroll scrollPlPos;

};

//------< インスタンス取得 >-----------------------------------------------------
#define pMousePointa  (MousePointa::getInstance())

//******************************************************************************

#endif // ! _INCLUDE_MOUSE_POINTA

#ifndef INCLUDED_RESULT
#define	INCLUDED_RESULT

//******************************************************************************
//
//
//      タイトルシーン
//
//
//******************************************************************************

//==============================================================================
//
//      Titleクラス
//
//==============================================================================

class Result : public Scene, public Singleton<Result>
{
private:
	int mode;
public:
	void update();
	void draw();
    void uninit();
};

static Result* const SCENE_RESULT = Result::getInstance();

//******************************************************************************

#endif // !INCLUDED_RESULT

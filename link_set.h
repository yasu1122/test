#ifndef _INCLUDE_LINK_SET
#define _INCLUDE_LINK_SET
//******************************************************************************
//
//
//      link_set.h
//
//
//******************************************************************************

//-------------------<定数>-----------------------------------------------------

#define DOOR_LINK_MAX       (2)

//===============================================================================
//
//      DoorURL構造体
//
//===============================================================================
struct DoorURL {

    int link;
    int linkCnt;
    VECTOR2 linkPos[DOOR_LINK_MAX];
    VECTOR2 scrollPos[DOOR_LINK_MAX];
    VECTOR2 respawnPos[DOOR_LINK_MAX];

};

//==========================================================
//
//      linkSetクラス
//
//==========================================================
class LinkSet
{
public:
    std::vector<DoorURL> doorURL;                       // 扉のリンク格納

    void linkSet(int link, VECTOR2 linkPos);
    VECTOR2 setScrollPos(VECTOR2 pos);
    VECTOR2 setRespawnPos(VECTOR2 pos);
    void linkClear(DoorURL* url);

};


//******************************************************************************

#endif //!_INCLUDE_LINK_SET
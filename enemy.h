#pragma once

// ラベル定義
#define ENEMY_MAX					100
#define ENM_JUMP_POINT_MAX          (30)
#define DOOR_LINK_ENEMY_MAX          (30)

#define ENMEMY00_MOVE_DISTANCE		200
#define ENMEMY00_MOVE_SPEED			8

namespace enemy
{

	enum ActFlg {
        EraseFlg    =(1<<0),
	};

	// iWorkのラベル
	enum iWork {
		EnemyURL, 
		EnemyBlockName,
        ActFlg,
    };

	// fWorkのラベル
	enum fWork {
		weekPositionX,         // 位置
		weekPositionY,
		weekSizeX,         // 位置
		weekSizeY,
		MoveAngle
	};

	enum BlockName
	{
		http_xvideos_com,
		http_pornhub_com
	};

	enum EnemyType
	{
		AD,
		VIRUS,
	};

}

struct EnemyData
{
	MOVER mover;

	SpriteData* data;

    int         linkNo;
};

// EnemyManagerクラス
class EnemyManager : public OBJ2DManager, public LinkSet, public Singleton<EnemyManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
	void uninit();    // 初期化

private:
	int getSize() { return ENEMY_MAX; }
    int getEnmJumpSize() { return DOOR_DATA_MAX; }

public:
    void linkSearchSet(int link, VECTOR2 linkPos);

};

#define pEnemyManager	(EnemyManager::getInstance())


#include"all.h"

#include <string>
#include <bitset>

using namespace GameLib;
void block_move00_child(OBJ2D*obj);


//ブロックその1
void block_move00(OBJ2D*obj)
{

	switch (obj->state)
	{
	case 0:
		
		//break;
	case 1:
		
		break;
	}
}


//ぶつかって散らばったやつ
void block_move00_child(OBJ2D*obj)
{

	switch (obj->state)
	{
	case 0:
		

		//break;

	case 1:
		
		break;
	}
}



void BlockManager::init()
{
	OBJ2DManager::init();   // OBJ2DManagerの初期化

	STAGE_SCRIPT*pScript = STAGE->getStageScript();

							// 敵データの読み込み
    char** mapBlock = new char*[pBGManager->CHIP_NUM_Y];
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapBlock[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapBlock[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

	if (!pBGManager->loadMapData(pScript->fileNameBlock, mapBlock))
	{
		assert(!"敵データ読み込み失敗");
	}

	// 敵配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int blockIndex = mapBlock[y][x];
			if (blockIndex < 0) continue;
			searchSet(block_move00, VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);

		}
	}

    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapBlock[i];
    }
    delete[] mapBlock;

}

void BlockManager::update()
{
	OBJ2DManager::update();   // OBJ2DManagerの初期化
}

void BlockManager::draw()
{
	OBJ2DManager::draw();   // OBJ2DManagerの初期化
	primitive::circle(begin()->position-pBGManager->getScrollPos(), 5.f, VECTOR4(1, 0, 0, 1));

}

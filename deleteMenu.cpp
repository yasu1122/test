//******************************************************************************
//
//
//      削除メニュー表示
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//------<変　数>----------------------------------------------------------------

MOVER deleteMenuFuncArray[] =
{
    DeleteWindow,
    DelDecision,
    NotDel,
    DisEnableDel,
    nullptr,
};

//------<定　数>----------------------------------------------------------------
// デリート選択ウィンドウ
#define DEL_WINDOW_SIZE     (VECTOR2(128.0f,64.0f))
#define DEL_WINDOW_POS      (VECTOR2(system::SCREEN_WIDTH*0.5f,system::SCREEN_HEIGHT*0.5f))



// staticメンバの実態宣言
Text*  deleteMenu::deleteText;

//--------------------------------
//  deleteMenu関数
//--------------------------------

// デリート選択ウィンドウ
void DeleteWindow(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = DEL_WINDOW_SIZE;
        obj->position = DEL_WINDOW_POS;
        obj->text = L" ";
        obj->data = &spr_delete_menu;


        obj->iWork[menu::iWork::actFlg] = menu::menuLabel::naturalflg;
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);

        obj->state++;

        // break;
    case 1:

        break;
    }
}



// 削除メニュー決定
void DelDecision(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(32.0f, 8.0f);
        obj->position.x = system::SCREEN_WIDTH * 0.5f-64.0f;
        obj->position.y = system::SCREEN_HEIGHT * 0.5f + 32.0f;
        obj->text = L"は い";

        obj->iWork[menu::iWork::actFlg] = menu::menuLabel::naturalflg;

        obj->state++;

        //break;
    case 1:
        obj->color = VECTOR4(0.6f, 0.6f, 0.6f, 1.0f);
        pDeleteMenuManager->setDecisionFlg(Flag_Check(obj->iWork[delete_menu::actFlg], delete_menu::delMenuLabel::pushOn));

        if (!pDeleteMenuManager->getDeleteMenuViewFlg())return;

        if (pDeleteMenuManager->getDecisionFlg()) {
            obj->color = VECTOR4(0.4f, 0.4f, 0.4f, 1.0f);
            if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0)
            {
                VECTOR2 deletePos;  //削除するお気に入り登録の座標を格納する変数

                //--------------< 指定したお気に入り登録を削除 >----------------------------
                std::list<LIST>::iterator pBookMark = pBookMarkManager->getList()->begin();
                std::list<LIST>::iterator end = pBookMarkManager->getList()->end();

                while (pBookMark != end) {
                    if (Flag_Check(pBookMark->iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::okiniiriFlg)) {
                        
                        // 先頭のお気に入り登録は削除できないため削除不可能の表示フラグをオンにする
                        if (pBookMark == pBookMarkManager->getList()->begin()) {

                            Flg_Off(pBookMark->iWork[bookmark::iWork::actFlg], bookmark::bookMarkLabel::okiniiriFlg);
                            pDeleteMenuManager->setDisEnableDel(true);
                            
                            continue;
                        }
                        deletePos = pBookMark->plScrollPos.objPos;
                        pBookMark->clear();
                        pBookMark=pBookMarkManager->getList()->erase(pBookMark);
                        continue;
                    }
                    pBookMark++;
                }
                //----------------------------------------------------------------

                //--------< お気に入り登録位置の☆オブジェクトを削除 >------------

                pBookMarkObj->searchErase(deletePos);

                //----------------------------------------------------------------

                pDeleteMenuManager->setDeleteMenuViewFlg(false);

            }
        }

        break;
    }
}


// 削除しない
void NotDel(OBJ2D* obj)
{
    using namespace flagFunc;

//    AnimeData* animeData = nullptr;


    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(32.0f, 8.0f);
        obj->position.x = system::SCREEN_WIDTH * 0.5f + 64.0f;
        obj->position.y = system::SCREEN_HEIGHT * 0.5f + 32.0f;

        obj->text = L"いいえ";
        obj->iWork[delete_menu::iWork::actFlg] = delete_menu::delMenuLabel::naturalflg;

        obj->state++;

        //break;
    case 1:
        obj->color = VECTOR4(0.6f, 0.6f, 0.6f, 1.0f);
        pDeleteMenuManager->setNotDeleteFlg(Flag_Check(obj->iWork[delete_menu::actFlg], delete_menu::delMenuLabel::pushOn));

        if (pDeleteMenuManager->getNotDeleteFlg()) {
            obj->color= VECTOR4(0.4f, 0.4f, 0.4f, 1.0f);
            if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0)
            {
                pDeleteMenuManager->setEraseList(nullptr);
                pDeleteMenuManager->setDeleteMenuViewFlg(false);

            }
        }

        break;
    }
}

// 削除不可能表示
void DisEnableDel(OBJ2D *obj) {
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(128.0f, 64.0f);
        obj->position.x = system::SCREEN_WIDTH * 0.5f;
        obj->position.y = system::SCREEN_HEIGHT * 0.5f;

        obj->text = L"";
        obj->data=&spr_sakujohuka;
        obj->iWork[delete_menu::iWork::actFlg] = delete_menu::delMenuLabel::naturalflg;
        obj->color = VECTOR4(0.9f, 0.9f, 0.9f, 1.0f);

        obj->state++;

        //break;
    case 1:
        //--------<通常処理>-----------

        if (pDeleteMenuManager->getDisEnableDel())
        {
            obj->state++;
        }
        break;
    case 2:
        if (pDeleteMenuManager->getDisEnableDel()) {
            if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0) {
                pDeleteMenuManager->setDisEnableDel(false);
                obj->state = 1;
            }
        }

        break;
    }
}

// デストラクタ
deleteMenu::~deleteMenu() {
    if (deleteText) {
        delete deleteText;
    }
}

// 初期化
void deleteMenu::init()
{
    OBJ2DManager::init();       // OBJ2DManagerの初期化

    deleteText= new Text(TEXT2, "./Data/Text/font_2_ch.txt");   // テキストクラス実体を初期化

    setEraseList(nullptr);      // 削除対象のリスト設定
    setDisEnableDel(false);     // 削除不可能ウィンドウ表示フラグ
    setDeleteMenuViewFlg(false);// デリート選択ウィンドウ表示フラグ
    setDecisionFlg(false);      // 削除実行可能フラグ
    setNotDeleteFlg(false);     // デリート選択ウィンドウ終了フラグ

    // デリートメニューを生成
    for (int i = 0;; ++i)
    {
        if (!deleteMenuFuncArray[i])break;

        searchSet(deleteMenuFuncArray[i], VECTOR2(0.0f, 0.0f)/*初期設定のため座標を(x , y)(0.0f,0.0f)へ設定*/);

    }
}

// 更新処理
void deleteMenu::update()
{
    using namespace flagFunc;

    OBJ2DManager::update();   // OBJ2DManagerの更新

}

// 描画処理
void deleteMenu::draw()
{
    using namespace flagFunc;

    for (auto & p : *pDeleteMenuManager)
    {
        if (!p.mover)continue;
        if (p.mover != DisEnableDel) {
            if (getDeleteMenuViewFlg()) {
                primitive::rect(VECTOR2(p.position.x, p.position.y), VECTOR2(p.size.x + p.size.x, p.size.y + p.size.y), VECTOR2(p.size.x, p.size.y), 0.0f, p.color);
                if (p.data)p.data->draw(p.position, p.scale, 0.0f, p.color);
            }
        }
    }

    for (auto & p : *pDeleteMenuManager)
    {
        if (!p.mover)continue;
        if (getDeleteMenuViewFlg()) {
            if (p.mover == DisEnableDel)continue;
            if (p.mover!= DeleteWindow)
                deleteText->Render(p.text, VECTOR2(p.position.x-(p.size.x/1.5f) , p.position.y - p.size.y), VECTOR2(0.59f, 0.59f), VECTOR4(0.0f, 0.0f, 0.0f, 1.0f));
        }
        if (getDisEnableDel()&&p.mover==DisEnableDel) {
            if (p.data)p.data->draw(p.position, p.scale, 0.0f, p.color);
        }

    }
}
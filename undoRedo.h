#ifndef     _UNDOREDO_H
#define     _UNDOREDO_H
//******************************************************************************
//
//
//      UndoRedoManagerクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include <list>
#include "./GameLib/vector.h"
#include "./GameLib/obj2d_data.h"

//==============================================================================
//
//      UndoRedoクラス
//
//==============================================================================

struct Terr_Data {
	char** terr;
	char** terr2;
};

class UndoRedo
{
public:

    // ブックマーク
    static std::list<std::list<LIST>>  undoList;            // 履歴(過去のリストを保存する)
    static std::list<std::list<LIST>>  redoList;            // 履歴(Undo前のリストを保存する)

    // プレイヤー座標
    static std::list<JumpData::posandscroll>   undoPlPos;                  // 履歴(過去のプレイヤーの座標を保存する)
    static std::list<JumpData::posandscroll>   redoPlPos;                  // 履歴(Undo前のプレイヤーの座標を保存する)

    // エネミー
    static std::list<std::vector<OBJ2D>> undoEnemyList;     // 履歴(過去のエネミーの情報を保存する)
    static std::list<std::vector<OBJ2D>> redoEnemyList;     // 履歴(Undo前のエネミーの情報を保存する)

    // ウイルス
    static std::list<std::vector<OBJ2D>> undoVirusList;     // 履歴(過去のエネミーの情報を保存する)
    static std::list<std::vector<OBJ2D>> redoVirusList;     // 履歴(Undo前のエネミーの情報を保存する)

    // ギミック
    static std::list<std::vector<OBJ2D>> undoGimmickList;   // 履歴(過去のエネミーの情報を保存する)
    static std::list<std::vector<OBJ2D>> redoGimmickList;   // 履歴(Undo前のエネミーの情報を保存する)

    // ポータル
    static std::list<std::vector<OBJ2D>> undoPortalList;    // 履歴(過去のポータルの情報を保存する)
    static std::list<std::vector<OBJ2D>> redoPortalList;    // 履歴(Undo前のポータルの情報を保存する) 

    // 扉
    static std::list<std::vector<OBJ2D>> undoDoorList;      // 履歴(過去の扉の情報を保存する)
    static std::list<std::vector<OBJ2D>> redoDoorList;      // 履歴(Undo前の扉の情報を保存する)

    // アイテム
    static std::list<std::vector<OBJ2D>> undoItemList;      // 履歴(過去のアイテムの情報を保存する)
    static std::list<std::vector<OBJ2D>> redoItemList;      // 履歴(Undo前のアイテムの情報を保存する)

    // ☆
    static std::list<std::vector<OBJ2D>> undoBkObjList;        // 履歴(過去の☆の情報を保存する)
    static std::list<std::vector<OBJ2D>> redoBkObjList;        // 履歴(Undo前の☆の情報を保存する)

public:
    UndoRedo() {};
    ~UndoRedo() {};

    virtual void undoRedoInit()=0;                    // 初期化

    // bookMarkに新しく追加
    //LIST* add(LISTAlg* bookAlg, int url, int blockUrl, VECTOR2  pos, wchar_t* urlText);

public:

    // 純粋仮想関数
    virtual void undoAdd() = 0;
    virtual void undoEndErase() = 0;
    virtual void getUndoEnd() = 0;

    virtual void redoAdd() = 0;
    virtual void redoEndErase() = 0;
    virtual void getRedoEnd() = 0;

    virtual void undoErase() = 0;
    virtual void redoErase() = 0;

    virtual void runUndo() = 0;
    virtual void runRedo() = 0;

    //virtual void run();

};

//******************************************************************************
#endif  //!_UNDOREDO_H
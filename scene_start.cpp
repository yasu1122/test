//******************************************************************************
//
//
//      開始演出
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;


//--------------------------------
//  初期化処理
//--------------------------------
void SceneStart::init()
{
    Scene::init();	    // 基底クラスのinitを呼ぶ

    state = 0;
    isPaused = false;   // ポーズフラグの初期化

}

//--------------------------------
//  更新処理
//--------------------------------
void SceneStart::update()
{
    using namespace input;

    // デバッグ文字列表示
    switch (state)
    {
    case 0:
        //////// 初期設定 ////////

        timer = 0;
        GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
        
        // テクスチャの読み込み
        texture::load(loadTexture);

        pPlayerManager->scene_start_init();

        pMousePointa->init(1);

        pMenuManager->sceneStartInit();

        pBlackoutManager->init();

        pEffectManager->init();

        // HACK:ゲームループ時に発生するバグに対する応急処置
        pBGManager->setScrollPos(0.0f, 0.0f);

        state++;    // 初期化処理の終了
        
         // break;      // 意図的なコメントアウト

    case 1:
        //////// 通常時の処理 ////////    
		if (TRG(0) & PAD_START)//クリック音
			sound::play(SE_CLICK);


        pMousePointa->update();

        pPlayerManager->update();

        pMenuManager->update2();

        pEffectManager->update();

        pBlackoutManager->update();

        pJudgeSceneStartManager->update();


        if (flagFunc::Flag_Check(pMousePointa->getOBJ2D(0)->iWork[mouse_pointa::iWork::actFlg],mouse_pointa::clickLabel::sceneChangeFlg))
        {
            switch (STAGE->getStageNo())
            {
            case 0:     SCENE_START->changeScene(SCENE_TUTO); break;
            default:    SCENE_START->changeScene(SCENE_GAME); break;
            }
        }

        break;

    }

	
}

//--------------------------------
//  描画処理
//--------------------------------
void SceneStart::draw()
{
    // 画面クリア
    GameLib::clear(VECTOR4(0, 0, 0, 1));

    pPlayerManager->scene_start_draw();

    pMenuManager->draw();

    pMousePointa->draw();

    pEffectManager->draw_not_scroll();

    pBlackoutManager->draw();       // ブラックアウト

}
//--------------------------------
//  終了処理
//--------------------------------
void SceneStart::uninit()
{

    // テクスチャの解放
    texture::releaseAll();
	
}

//******************************************************************************

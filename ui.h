#pragma once

struct UI_INIT_DATA 
{
	MOVER mover;
	float posX;
	float posY;
};


class UiManager : public OBJ2DManager, public Singleton<UiManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画

private:
    int roomNum;

public:
    void setRoomNum(int num) { roomNum = num; }
    int getRoomNum() { return roomNum; }

private:
	int getSize() { return 8; }

};

#define pUiManager	(UiManager::getInstance())
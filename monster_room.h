#ifndef  _MONSTER_ROOM
#define  _MONSTER_ROOM
//******************************************************************************
//
//
//      monster_room.h
//
//
//******************************************************************************

//-------------------<定数>-----------------------------------------------------
#define MONSTER_ROOM_MAX            (15)

//===================================================
//
//      RoomTextクラス
//
//===================================================
class MonsterRoom :public OBJ2DManager, public Singleton<MonsterRoom>
{
public:
    void init();    // 初期化
    void update();  // 更新
    void draw();    // 描画

private:
    int getSize() { return MONSTER_ROOM_MAX; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pMonsterRoom  (MonsterRoom::getInstance())

//******************************************************************************
#endif  //_MONSTER_ROOM

#pragma once

#define STAGE_MAX 2



namespace stage {
	// ステージ遷移フラグ
	enum InitFlg {
		map_init            = (1<<0),
        map_init_pl_respawn = (1<<1),
        scene_pause         = (1<<2),
	};

	// iWorkのラベル
	enum iWork {
		initflg,
        generalflg,
	};

	// fWorkのラベル
	enum fWork {

	};
}




class StageManager :public Singleton<StageManager>
{

public:

	int   iWork[16];
	float fWork[16];

	int	stageNo;

private:

	
	int				state;
	int				timer;
	char*			str_start;
	STAGE_SCRIPT*	pScript;
	URL_DATA*		pURL;



public:
	
	void init();        // 初期化
	void update();      // 更新
	void draw();        // 描画

    void pauseFlgOn();  // ポーズフラグオン
    void respawnInit(); // リスポーン時の再初期化
	int URLmax=0;
	STAGE_SCRIPT* getStageScript(){ return pScript; }
	URL_DATA*	  getURL() { return pURL; }

    int getStageNo() { return stageNo; }

	int getSize() { return STAGE_MAX; }

	int init_start_cnt;
	bool map_init_flg;

private:
    bool return_title_flg;

public:
    void setReturnTitleFlg(bool flg) { return_title_flg = flg; }
    bool getReturnTitleFlg() { return return_title_flg; }

};

static StageManager* const STAGE = StageManager::getInstance();


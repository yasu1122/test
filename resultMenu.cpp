//******************************************************************************
//
//
//      メニュー表示
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//------<変　数>----------------------------------------------------------------

//--------------------------------
//  menu関数
//--------------------------------

// タイトルに戻る
void gameReStart(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        obj->size = VECTOR2(128.0f, 70.0f);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
		obj->scale = VECTOR2(0.5f, 0.5f);
		obj->data = &spr_game_over_ui00;
        // break;

    case 1:
        pResultMenu->setStartFlg(Flag_Check(obj->iWork[result_menu::iWork::actFlg], result_menu::menuLabel::pushOn));

        if (Flag_Check(obj->iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.5f, 0.5f, 0.5f, 1.0f);
            obj->scale = VECTOR2(0.8f, 0.8f);
        }
        else
        {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(0.7f, 0.7f);
        }

        break;
    }
}

//ステージセレクトに戻る
void Select(OBJ2D* obj)
{
	using namespace flagFunc;

	//AnimeData* animeData = nullptr;

	switch (obj->state)
	{
	case 0:
		//--------<初期設定>-----------
        obj->size = VECTOR2(128.0f, 70.0f);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
       // obj->position = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 1.3);
        obj->scale = VECTOR2(0.5f, 0.5f);
        obj->data = &spr_game_over_ui01;
		obj->state++;
		// break;

	case 1:
		pResultMenu->setExitFlg(Flag_Check(obj->iWork[result_menu::iWork::actFlg], result_menu::menuLabel::pushOn));

        if (Flag_Check(obj->iWork[title_menu::iWork::actFlg], title_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.5f, 0.5f, 0.5f, 1.0f);
            obj->scale = VECTOR2(0.8f, 0.8f);
        }
        else
        {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(0.7f, 0.7f);
        }
        break;
	}
}



// 初期化
void ResultMenu::init()
{
    OBJ2DManager::init();       // OBJ2DManagerの初期化

	searchSet(gameReStart, VECTOR2(1600.0f, 256.0f));
	searchSet(Select, VECTOR2(1600.0f, 412.0f));


}

// 更新処理
void ResultMenu::update()
{
    using namespace flagFunc;


    OBJ2DManager::update();   // OBJ2DManagerの更新


}

// 描画処理
void ResultMenu::draw()
{
    using namespace flagFunc;

    OBJ2DManager::draw_not_scroll();   // OBJ2DManagerの描画

}


//******************************************************************************


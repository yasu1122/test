//******************************************************************************
//
//
//      セレクトメニュー表示
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//------<構造体>----------------------------------------------------------------
struct MenuFuncData {
    MOVER menu;     // 関数のアドレス
    VECTOR2 pos;    // 生成する座標
};

//------<定　数>----------------------------------------------------------------
#define ICON_POS_TUTORIAL    (VECTOR2 (system::SCREEN_WIDTH*0.25f, system::SCREEN_HEIGHT*0.5f-140.0f))
#define ICON_POS_STAGE1      (VECTOR2 (system::SCREEN_WIDTH*0.5, system::SCREEN_HEIGHT*0.5f-140.0f))
#define ICON_POS_STAGE2      (VECTOR2 (system::SCREEN_WIDTH*0.75, system::SCREEN_HEIGHT*0.5f-140.0f))
#define ICON_POS_STAGE3      (VECTOR2 (system::SCREEN_WIDTH*0.25f, system::SCREEN_HEIGHT*0.5f+140.0f))
#define ICON_POS_STAGE4      (VECTOR2 (system::SCREEN_WIDTH*0.5f, system::SCREEN_HEIGHT*0.5f+140.0f))
#define ICON_POS_STAGE5      (VECTOR2 (system::SCREEN_WIDTH*0.75f, system::SCREEN_HEIGHT*0.5f+140.0f))

#define ICON_SIZE_X          (128.0f)
#define ICON_SIZE_Y          (128.0f)
#define ICON_SCALE_X         (0.5f)
#define ICON_SCALE_Y         (0.5f)
#define ICON_SCALE_X2        (0.6f)
#define ICON_SCALE_Y2        (0.6f)

#define RETURN_ICON_POS      (VECTOR2 (1700.f,960.0f))
#define RETURN_ICON_SIZE_X   (89.0f)
#define RETURN_ICON_SIZE_Y   (44.0f)
#define RETURN_ICON_SCALE_X  (0.35f)
#define RETURN_ICON_SCALE_Y  (0.35f)
#define RETURN_ICON_SCALE_X2 (0.45f)
#define RETURN_ICON_SCALE_Y2 (0.45f)

//------<関数プロトタイプ宣言>--------------------------------------------------
void stage1(OBJ2D* obj);
void stage2(OBJ2D* obj);
void stage3(OBJ2D* obj);
void stage4(OBJ2D* obj);
void stage5(OBJ2D* obj);
void tutorial(OBJ2D* obj);
void returnTitle(OBJ2D* obj);

//------<変　数>----------------------------------------------------------------
MenuFuncData menuFuncDataArray[] = {
    { &tutorial     , ICON_POS_TUTORIAL },
    { &stage1       , ICON_POS_STAGE1 },
    { &stage2       , ICON_POS_STAGE2 },
    { &stage3       , ICON_POS_STAGE3 },
    { &stage4       , ICON_POS_STAGE4 },
    { &stage5       , ICON_POS_STAGE5 },
    { &returnTitle  , RETURN_ICON_POS },
    { nullptr       , VECTOR2(0.0f,0.0f)},

};


//--------------------------------
//  menu関数
//--------------------------------

// stage1
void stage1(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(ICON_SIZE_X, ICON_SIZE_Y);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->data = &spr_select_icon_stage01;
        obj->state++;
        // break;

    case 1:
        pSelectMenu->setStage1Flg(Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn));


        if (Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X2, ICON_SCALE_Y2);
            STAGE->stageNo = 1;
        }
        else {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X, ICON_SCALE_Y);
        }

        break;
    }
}

// stage2
void stage2(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(ICON_SIZE_X, ICON_SIZE_Y);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->data = &spr_select_icon_stage02;
        obj->state++;
        // break;

    case 1:
        pSelectMenu->setStage2Flg(Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn));


        if (Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X2, ICON_SCALE_Y2);
            STAGE->stageNo = 2;
        }
        else {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X, ICON_SCALE_Y);
        }

        break;
    }
}


// stage3
void stage3(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(ICON_SIZE_X, ICON_SIZE_Y);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->data = &spr_select_icon_stage03;
        obj->state++;
        // break;

    case 1:
        pSelectMenu->setStage3Flg(Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn));

        if (Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X2, ICON_SCALE_Y2);
            STAGE->stageNo = 3;
        }
        else {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X, ICON_SCALE_Y);
        }

        break;
    }
}

// stage4
void stage4(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(ICON_SIZE_X, ICON_SIZE_Y);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->data = &spr_select_icon_stage04;
        obj->state++;
        // break;

    case 1:
        pSelectMenu->setStage4Flg(Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn));


        if (Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X2, ICON_SCALE_Y2);
            STAGE->stageNo = 4;
        }
        else {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X, ICON_SCALE_Y);
        }

        break;
    }
}

// stage5
void stage5(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(ICON_SIZE_X, ICON_SIZE_Y);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->data = &spr_select_icon_stage05;
        obj->state++;
        // break;

    case 1:
        pSelectMenu->setStage5Flg(Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn));

        if (Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X2, ICON_SCALE_Y2);
            STAGE->stageNo = 5;
        }
        else {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X, ICON_SCALE_Y);
        }

        break;
    }
}

void tutorial(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(ICON_SIZE_X, ICON_SIZE_Y);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->data = &spr_select_icon_tutorial;
        obj->state++;
        // break;

    case 1:
        pSelectMenu->setTutorialFlg(Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn));

        if (Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X2, ICON_SCALE_Y2);
            STAGE->stageNo = 0;
        }
        else {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(ICON_SCALE_X, ICON_SCALE_Y);
        }

        break;
    }
}

void returnTitle(OBJ2D* obj)
{
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(RETURN_ICON_SIZE_X, RETURN_ICON_SIZE_Y);
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->data = &spr_select_icon_title;
        obj->state++;
        // break;

    case 1:
        pSelectMenu->setTitleFlg(Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn));

        if (Flag_Check(obj->iWork[select_menu::iWork::actFlg], select_menu::menuLabel::pushOn))
        {
            obj->color = VECTOR4(0.7f, 0.7f, 0.7f, 1.0f);
            obj->scale = VECTOR2(RETURN_ICON_SCALE_X2, RETURN_ICON_SCALE_Y2);
        }
        else {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(RETURN_ICON_SCALE_X, RETURN_ICON_SCALE_Y);
        }

        break;
    }
}

// 初期化
void SelectMenu::init()
{
    OBJ2DManager::init();       // OBJ2DManagerの初期化

    setStage1Flg(false);
    setStage2Flg(false);
    setStage3Flg(false);
    setStage4Flg(false);
    setStage5Flg(false);
    setTutorialFlg(false);
    setTitleFlg(false);

    for (int menuFunc_i = 0;; ++menuFunc_i)
    {
        if (!menuFuncDataArray[menuFunc_i].menu) break;
        MOVER menuFunction = menuFuncDataArray[menuFunc_i].menu;
        VECTOR2 menuPos = menuFuncDataArray[menuFunc_i].pos;

        searchSet(menuFunction, menuPos);
    }

}

// 更新処理
void SelectMenu::update()
{
    using namespace flagFunc;


    OBJ2DManager::update();   // OBJ2DManagerの更新


}

// 描画処理
void SelectMenu::draw()
{
    using namespace flagFunc;


    OBJ2DManager::draw_not_scroll();   // OBJ2DManagerの描画

}


//******************************************************************************



//******************************************************************************
//
//
//      部屋の設定
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"
#include <string.h>

using namespace GameLib;
using namespace input;

//------<変　数>----------------------------------------------------------------
wchar_t* firstStageRoom[20]{
    L"ホーム",
    L"１の部屋",
    L"２の部屋",
    L"３の部屋",
    L"４の部屋",
    nullptr,
};

wchar_t** stageRoomNameData[]{
    firstStageRoom,
};

//====================================================
//
//      mover関数
//
//====================================================
void roomMove(OBJ2D *obj)
{
    switch (obj->state)
    {
    case 0:
        obj->size = VECTOR2(system::SCREEN_WIDTH / 2, system::SCREEN_HEIGHT / 2);
        obj->state++;
        break;

    case 1:

        break;
    }
}

//====================================================
//
//      通常更新
//
//====================================================
void RoomPointManager::init()
{
    OBJ2DManager::init();

    STAGE_SCRIPT*pScript = STAGE->getStageScript();

    // 扉データの読み込み
    char** mapRoom = new char*[pBGManager->CHIP_NUM_Y];
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapRoom[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapRoom[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

    if (!pBGManager->loadMapData(pScript->fileNameRoom, mapRoom))
    {
        assert(!"部屋データ読み込み失敗");
    }

    // 部屋ポイント(どの部屋であるかの目印)配置
    for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
        {
            const int roomIndex = mapRoom[y][x];
            if (roomIndex < 0) continue;

            // 生成
            OBJ2D* pRoom = searchSet(&roomMove, VECTOR2(static_cast<float>(x*BG::CHIP_SIZE + BG::CHIP_SIZE / 2), static_cast<float>(y*BG::CHIP_SIZE + BG::CHIP_SIZE)));
            
           // 文字列のコピー
            pRoom->text = stageRoomNameData[STAGE->getStageNo()][roomIndex];

            pRoom->iWork[door::iWork::doorLink] = mapRoom[y][x];
            pRoomPointManager->searchSet(roomMove, pRoom->position);

        }
    }


    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapRoom[i];
    }
    delete[] mapRoom;


}

// 更新処理
void RoomPointManager::update()
{
    OBJ2DManager::update();
}


//******************************************************************************


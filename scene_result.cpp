//******************************************************************************
//
//
//      リザルト
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;

//--------------------------------
//  更新処理
//--------------------------------
void Result::update()
{
    using namespace input;
    using namespace flagFunc;

    switch (state)
    {
    case 0:
        //////// 初期設定 ////////

        timer = 0;  // タイマーを初期化
        GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
        pMousePointa->init(0);
        pResultMenu->init();
		texture::load(loadTexture);
		music::load(BGM_CLEAR, L"./Data/Musics/win_book.wav", 0.3f);
		music::play(BGM_CLEAR, true);

        state++;    // 初期化処理の終了
        //break;    // 意図的なコメントアウト

    case 1:
        //////// 通常時の処理 ////////

		if (TRG(0) & PAD_START)//クリック音
			sound::play(SE_CLICK);


        timer++;    // タイマーを足す

		
        pMousePointa->update();

		pResultMenu->update();

        pJudgeTitleManager->update();

        if (TRG(0) & PAD_START&&pResultMenu->getStartFlg()) {
            changeScene(SCENE_TITLE);  // ゲームシーンに切り替え
        }
        if (TRG(0) & PAD_START&&pResultMenu->getExitFlg()) {
            changeScene(SCENE_SELECT);  // ゲームシーンに切り替え
        }
        break;
    }

}

//--------------------------------
//  描画処理
//--------------------------------
void Result::draw()
{
    // 画面クリア
    GameLib::clear(VECTOR4(0.0f, 0.0f, 0.0f, 1));


    spr_game_clear.draw(VECTOR2(system::SCREEN_WIDTH * 0.5f, (system::SCREEN_HEIGHT *0.5f)));

	pResultMenu->draw();
	pMousePointa->draw();


}

void Result::uninit()
{

    // テクスチャの解放
    texture::releaseAll();
	music::unload(BGM_CLEAR);

}


//******************************************************************************

#pragma once

// 使用するウィンドウフレームのデータ
struct WINDOW_SPR_DATA
{
	GameLib::SpriteData* winSpr;		// スプライト
	VECTOR2 texMergin;					// ウィンドウごとの端の余白部分
	float speakerMergin;				// ウィンドウ上部の名前を入れるスペース分の余白
};

namespace messageWindow 
{
	enum iWork
	{
		messageCnt,						// 何番目のメッセージか
		messageNum,						// 部屋番号と対応している
		messageOldNum,					// 移動前の部屋番号を一時保存する用
	};

	enum fWork
	{
		txtMerginX,						// 文字列を描画できる可能幅
		txtMerginY,
		txtStartPosX,					// ウィンドウフレームの周りの余白
		txtStartPosY,
	};

}

class MessageWinManager : public OBJ2DManager, public Singleton<MessageWinManager>
{
private:
	// 定数
	static const int MESSAGE_WINDOW_MAX = 1;

	// 関数
	int getSize() { return MESSAGE_WINDOW_MAX; };

public:
	// 関数
	void init(
		//int winTexNum		// ウィンドウのスプライトの種類
		//,int messageNum		// メッセージの管理番号（message_tbl.cpp参照）
		//,int fontNum		// フォントの番号
		);
	void update();
	void draw_not_scroll();
	MessageWinManager() { };
	~MessageWinManager() { };

	// メッセージ用のフォントデータ
	Text m_msgText = Text(TEXNO::TEXT3, "./Data/Text/Tutorial_ch.txt");
	// メッセージ主用のフォントデータ
	Text m_nameText = Text(TEXNO::TEXT3, "./Data/Text/Tutorial_ch.txt");
};


#define pMessageWinManager  (MessageWinManager::getInstance())

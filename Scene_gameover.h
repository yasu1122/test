#ifndef INCLUDED_GAMEOVER
#define	INCLUDED_GAMEOVER

//******************************************************************************
//
//
//      ゲームオーバーシーン
//
//
//******************************************************************************

//==============================================================================
//
//      GameOverクラス
//
//==============================================================================

class GameOver : public Scene, public Singleton<GameOver>
{
private:
    int mode;

public:
    void init();
	void uninit();

    void update();
    void draw();
};

static GameOver* const SCENE_GAMEOVER = GameOver::getInstance();

//******************************************************************************

#endif // !INCLUDED_GAMEOVER

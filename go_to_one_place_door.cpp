//******************************************************************************
//
//
//      扉クラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>


using namespace GameLib;

//******************************************************************************
//
//      扉による処理関連
//
//******************************************************************************

//-------< 扉URLクラス >--------------------------------------------------------

//-------< 扉クラス >-----------------------------------------------------------

// 扉の更新処理
void onePlaceDoorMove(OBJ2D* obj)
{
    using namespace input;  // 関数内で入力処理を行うときに記述する

                            // 変数
                            //    AnimeData* animeData = nullptr;

    switch (obj->state)
    {
    case 0:
        obj->size = VECTOR2(32.f, 64.f);
        obj->data = &spr_door00;

        obj->state++;
        // break;
    case 1:


        break;
    }


}


//==============================================================================
//
//		DoorManagerクラス
//
//==============================================================================

//--------------------------------
//      初期化
//--------------------------------
void OnePlaceDoorManager::init()
{

    OBJ2DManager::init();   // OBJ2DManagerの初期化
    doorURL.resize(OnePlaceDoorManager::getDoorURLSize());

    for (auto & pDoorURL : doorURL)
    {
        linkClear(&pDoorURL);
    }

    STAGE_SCRIPT*pScript = STAGE->getStageScript();

    // 特定の場所へ飛ぶ扉データの読み込み
    char** mapOnePlaceDoor = new char*[pBGManager->CHIP_NUM_Y];
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapOnePlaceDoor[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapOnePlaceDoor[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

    if (!pBGManager->loadMapData(pScript->fileNameOnePlaceDoor, mapOnePlaceDoor))
    {
        assert(!"特定の場所へ飛ぶ扉データ読み込み失敗");
    }

    // 特定の場所データの読み込み
    char** mapOnePlaceDoorLink = new char*[pBGManager->CHIP_NUM_Y];
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapOnePlaceDoorLink[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapOnePlaceDoorLink[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

    if (!pBGManager->loadMapData(pScript->fileNameOnePlaceDoorLink, mapOnePlaceDoorLink))
    {
        assert(!"特定の場所データの読み込み失敗");
    }

    // 特定の場所へ飛ぶ扉配置
    for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
        {
            const int doorIndex = mapOnePlaceDoor[y][x];
            if (doorIndex >= 0) {

                // 生成
                OBJ2D* pOnePlaceDoor = searchSet(&onePlaceDoorMove, VECTOR2(static_cast<float>(x*BG::CHIP_SIZE + BG::CHIP_SIZE / 2), static_cast<float>(y*BG::CHIP_SIZE + BG::CHIP_SIZE)));
                pOnePlaceDoor->iWork[one_place_door::iWork::doorLink] = mapOnePlaceDoor[y][x];

            }

            const int doorLinkIndex = mapOnePlaceDoorLink[y][x];
            if (doorLinkIndex < 0)continue;
            pOnePlaceDoorManager->linkSearchSet(doorLinkIndex,VECTOR2(
                static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
                static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
            );
        }
    }

    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapOnePlaceDoor[i];
        delete[] mapOnePlaceDoorLink[i];
    }
    delete[] mapOnePlaceDoor;
    delete[] mapOnePlaceDoorLink;

}


//--------------------------------
//      更新処理
//--------------------------------
void OnePlaceDoorManager::update()
{
    OBJ2DManager::update(); // OBJ2DManagerの更新
}


//--------------------------------
//      描画処理
//--------------------------------
void OnePlaceDoorManager::draw()
{
    OBJ2DManager::draw();   // OBJ2DManagerの描画

    /*for (auto p = pOnePlaceDoorManager->begin(); p < pOnePlaceDoorManager->end(); ++p)
    {
        if (!p->mover)continue;
        primitive::rect(VECTOR2(p->position.x, p->position.y) - pBGManager->getScrollPos(), VECTOR2(p->size.x + p->size.x, p->size.y + p->size.y), VECTOR2(p->size.x, p->size.y), 0.0f, VECTOR4(1, 0, 0, 1));
    }*/
}

// ドアのリンクセット
void OnePlaceDoorManager::linkSearchSet(int link, VECTOR2 linkPos)
{

    if (doorURL[link].linkCnt >= ONE_PLACE_DOOR_LINK_MAX)return;

    int linkCnt = doorURL[link].linkCnt;

    doorURL[link].linkPos[linkCnt] = linkPos;
    doorURL[link].scrollPos[linkCnt] = setScrollPos(linkPos);
    doorURL[link].respawnPos[linkCnt] = setRespawnPos(linkPos);

    doorURL[link].linkCnt++;

};


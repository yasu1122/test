
//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>

using namespace GameLib;

//--------------------------------
//  コンストラクタ
//--------------------------------
GimmickTerrManager::GimmickTerrManager() 
{
    mapDelete();
}

//--------------------------------
//  デストラクタ
//--------------------------------

GimmickTerrManager::~GimmickTerrManager()
{
	mapDelete();
}

using namespace flagFunc;

//進行不可
void gimmickterr_move00(OBJ2D* obj)
{
	obj->data = &spr_gimmickterr00;
}

//赤
void gimmickterr_move01(OBJ2D* obj)
{
	obj->data = &spr_gimmickterr01;


	if (!pGimmickTerrManager->change_flg)
	{
		obj->color.w = 0.3f;
	}

	else
		obj->color.w = 1.0f;
}
//青
void gimmickterr_move02(OBJ2D* obj)
{
	obj->data = &spr_gimmickterr02;
	

		if (pGimmickTerrManager->change_flg)
		{
			obj->color.w = 0.3f;
		}

		else
			obj->color.w = 1.0f;

	
}
//鍵１
void gimmickterr_move03(OBJ2D* obj)
{
	obj->data = &spr_gimmickterr03;
	if (pGimmickTerrManager->erase_flg_key)
		obj->clear();

}
//鍵2
void gimmickterr_move04(OBJ2D* obj)
{
	obj->data = &spr_gimmickterr04;
	if (pGimmickTerrManager->erase_flg_key2)
		obj->clear();
}

void gimmickterr_move05(OBJ2D* obj)
{
	obj->data = &spr_gimmickterr05;
	OBJ2D *pl = pPlayerManager->getOBJ2D(0);

	if (pl->getURLcnt > STAGE->URLmax)
		pGimmickTerrManager->erase_flg = true;
	else
		pGimmickTerrManager->erase_flg = false;


	if (pGimmickTerrManager->erase_flg)
		obj->clear();
}

void gimmickterr_move06(OBJ2D* obj)
{

	obj->data = &spr_gimmickterr06;
	if (pGimmickTerrManager->success_flg)//正解フラグを立てる
		obj->color.w = 0;//正解してたら透明に
	else
		obj->color.w = 1;//それ以外は元の透明度に

}


MOVER GimmickTerrMoveArray[]=
{
	gimmickterr_move00,
	gimmickterr_move01,
	gimmickterr_move02,
	gimmickterr_move03,
	gimmickterr_move04,
	gimmickterr_move05,
	gimmickterr_move06,	
};

//******************************************************************************
//
//
//      BGクラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>

using namespace GameLib;



//--------------------------------
//  初期設定
//--------------------------------
void GimmickTerrManager::init()
{
    mapDelete();

	
	OBJ2DManager::init();
	clear();
	STAGE_SCRIPT*pScript = STAGE->getStageScript();

	// マップ全体のサイズを取得
	if (!allMapCount(pScript->fileNameBack))
	{
		assert(!"マップ全体のサイズのロードに失敗");
	}

	terr = new char*[CHIP_NUM_Y];
	terr2 = new char*[CHIP_NUM_Y];

	for (int i = 0; i < CHIP_NUM_Y; ++i)
	{

		terr[i] = new char[CHIP_NUM_X];
		terr2[i] = new char[CHIP_NUM_X];

		SecureZeroMemory(terr[i], sizeof(char)*pBGManager->CHIP_NUM_X);
		SecureZeroMemory(terr2[i], sizeof(char)*pBGManager->CHIP_NUM_X);

	}

	if (!loadMapData(pScript->fileNameGTerr, terr))
	{
		assert(!"地形データのロードに失敗");
	}
	if (!loadMapData(pScript->fileNameGTerr2, terr2))
	{
		assert(!"地形データ2のロードに失敗");
	}
	// 配置
	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int gterrIndex = terr[y][x];

			if (gterrIndex < 0) continue;
			searchSet(GimmickTerrMoveArray[gterrIndex], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);
			
		}
	}

	for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
	{
		for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
		{
			const int gterr2Index = terr2[y][x];

			if (gterr2Index < 0) continue;
			searchSet(GimmickTerrMoveArray[gterr2Index], VECTOR2(
				static_cast<float>(x * BG::CHIP_SIZE + BG::CHIP_SIZE / 2),
				static_cast<float>(y * BG::CHIP_SIZE + BG::CHIP_SIZE))
			);
		}
	}
	


}

//--------------------------------
//  メンバ変数のクリア
//--------------------------------
void GimmickTerrManager::clear()
{
	change_flg = false;
	erase_flg_key = false;
	erase_flg_key2 = false;
	erase_flg = false;
	erase_flg_pass = false;
	change_flg = false;
	success_flg = false;
	spawn_window_flg = false;
}

//--------------------------------
//  地形データ、背景データ削除
//--------------------------------
void GimmickTerrManager::mapDelete()
{
	if (terr) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] terr[i];
		delete[] terr;
	}
	if (terr2) {
		for (int i = 0; i < CHIP_NUM_Y; ++i)
			delete[] terr2[i];
		delete[] terr2;
	}
	
	terr = nullptr;
	terr2 = nullptr;

}

bool GimmickTerrManager::allMapCount(const char* file_name)
{
	// マップデータ読み込み
	std::ifstream inputFile_terr(file_name);
	if (inputFile_terr.fail()) return false;


	// ラムダ式関数定義
	auto split = [](const char* src, const char delim, int *x)
	{
		std::istringstream stream{ src };
		std::string output;
		int cnt = 0;
		while (std::getline(stream, output, delim))
		{
			cnt++;
		}
		*x = cnt;
	};


	const int bufSize = 1024;
	char buf[bufSize];
	int y = 0;
	while (inputFile_terr.getline(buf, bufSize - 1))
	{
		split(buf, ',', &CHIP_NUM_X);
		y++;
	}

	CHIP_NUM_Y = y;

	WIDTH = (float)((CHIP_SIZE)*(CHIP_NUM_X));
	HEIGHT = (float)((CHIP_SIZE)*(CHIP_NUM_Y));

	return true;

}


//--------------------------------
//  マップデータのロード
//--------------------------------
bool GimmickTerrManager::loadMapData(const char* fileName, char** map)
{
	std::ifstream inputFile(fileName);
	if (inputFile.fail()) return false;

	auto split = [](const char* src, const char delim, const int Y, char** out)
	{
		std::istringstream stream{ src };
		std::string output;
		int cnt = 0;
		while (std::getline(stream, output, delim))
		{
			out[Y][cnt] = static_cast<char>(std::stoi(output));
			cnt++;
		}
	};

	const int bufSize = 4096;

	char* buf = new char[bufSize];

	int y = 0;
	while (inputFile.getline(&buf[0], bufSize - 1))
	{
		split(buf, ',', y, map);//&map[y++][0]とmap[y++]は同じ意味
		y++;
	}

	delete[] buf;

	return true;
}

//--------------------------------
//  更新処理
//--------------------------------
void GimmickTerrManager::update()
{

	OBJ2DManager::update();
}



//--------------------------------
//  地形描画
//--------------------------------
void GimmickTerrManager::draw()
{
	OBJ2DManager::draw();

}




//******************************************************************************
//
//      あたり判定
//
//******************************************************************************

//--------------------------------
//  マップ上の指定した座標の部分のマップチップのインデックスを返す
//--------------------------------
int GimmickTerrManager::getData(char** map, float x, float y)
{
	int divX = static_cast<int>(x) >> 5;      // x方向のインデックス
	int divY = static_cast<int>(y) >> 5;      // y方向のインデックス

	if (divX < 0 || divX >= CHIP_NUM_X)return -1;
	if (divY < 0 || divY >= CHIP_NUM_Y)return -1;

	return map[divY][divX];
}

//--------------------------------
//  指定した地点の地形の属性を取得
//--------------------------------
GimmickTerrManager::TR_ATTR GimmickTerrManager::getTerrainAttr(float x, float y)
{
	// インデックス取得
	int index = getData(terr, x, y);

	// インデックスが-1であればTR_NONEを返す
	if (index < 0) return TR_NONE;

	// x方向のインデックス
	int remX = index % CHIP_NUM_PER_LINE;

	// y方向のインデックス
	int divY = index / CHIP_NUM_PER_LINE;

	// 添字の範囲チェック
	assert(remX >= 0 && remX < CHIP_NUM_PER_LINE);
	assert(divY >= 0 && divY < CHIP_LINE_NUM);

	// リターン
	return terrainAttr[divY][remX];

}
//--------------------------------
//  指定した地点の地形の属性を取得
//--------------------------------
GimmickTerrManager::TR_ATTR GimmickTerrManager::getTerrainAttr2(float x, float y)
{
	// インデックス取得
	int index = getData(terr2, x, y);

	// インデックスが-1であればTR_NONEを返す
	if (index < 0) return TR_NONE;

	// x方向のインデックス
	int remX = index % CHIP_NUM_PER_LINE;

	// y方向のインデックス
	int divY = index / CHIP_NUM_PER_LINE;

	// 添字の範囲チェック
	assert(remX >= 0 && remX < CHIP_NUM_PER_LINE);
	assert(divY >= 0 && divY < CHIP_LINE_NUM);

	// リターン
	return terrainAttr[divY][remX];

}

//--------------------------------
//  指定されたy座標がマップチップの上側４分の１に含まれるか
//--------------------------------
bool GimmickTerrManager::isUpperQuater(float y)
{
	return wrap(static_cast<int>(y), 0, CHIP_SIZE) < CHIP_SIZE >> 2;
}


//--------------------------------
//  下方向のブロックに対するあたり
//--------------------------------
bool GimmickTerrManager::isHitDown(float x, float y)
{
	switch (getTerrainAttr(x, y))                       // 地形の属性を取得する
	{
	case TR_ATTR::ALL_BLOCK:	// 全て壁の地形であった
		return true;               

		break;

	case TR_ATTR::ALL_BLOCK_RED:	// 全て壁の地形であった
		if (pGimmickTerrManager->change_flg)
		{
			return true;
		}

		else
			return false;

		break;

	case TR_ATTR::ALL_BLOCK_BLUE:	 // 全て壁の地形であった

		if (pGimmickTerrManager->change_flg)
		{
			return false;
		}

		else
			return true;

		break;
	case TR_ATTR::ALL_BLOCK_KEY:	// 全て壁の地形であった

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey))
			pGimmickTerrManager->erase_flg_key = true;
	}
	if (!pGimmickTerrManager->erase_flg_key)

		return true;               

		break;
	case TR_ATTR::ALL_BLOCK_KEY2:	 // 全て壁の地形であった


	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey2))
			pGimmickTerrManager->erase_flg_key2 = true;
	}

	if(!pGimmickTerrManager->erase_flg_key2)
		return true;              

		break;
	case TR_ATTR::ALL_BLOCK_ELASE:	 // 全て壁の地形であった
		
	if(!pGimmickTerrManager->erase_flg)
			return true;              

		break;

	case TR_ATTR::ALL_BLOCK_PASS:	// 全て壁の地形であった
		pGimmickTerrManager->spawn_window_flg = true;

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);
		if (!pGimmickTerrManager->success_flg)//正解フラグチェック
		{
			return true;
		}

	}

		break;


	
	default:


		break;
	}

	switch (getTerrainAttr2(x, y))                       // 地形の属性を取得する
	{
	case TR_ATTR::ALL_BLOCK:	// 全て壁の地形であった
		return true;

		break;

	case TR_ATTR::ALL_BLOCK_RED:	// 全て壁の地形であった
		if (pGimmickTerrManager->change_flg)
		{
			return true;
		}

		else
			return false;

		break;

	case TR_ATTR::ALL_BLOCK_BLUE:	 // 全て壁の地形であった

		if (pGimmickTerrManager->change_flg)
		{
			return false;
		}

		else
			return true;

		break;
	case TR_ATTR::ALL_BLOCK_KEY:	// 全て壁の地形であった

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey))
			pGimmickTerrManager->erase_flg_key = true;
	}
	if (!pGimmickTerrManager->erase_flg_key)

		return true;

	break;
	case TR_ATTR::ALL_BLOCK_KEY2:	 // 全て壁の地形であった


	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey2))
			pGimmickTerrManager->erase_flg_key2 = true;
	}

	if (!pGimmickTerrManager->erase_flg_key2)
		return true;

	break;
	case TR_ATTR::ALL_BLOCK_ELASE:	 // 全て壁の地形であった

		if (!pGimmickTerrManager->erase_flg)
			return true;

		break;

	case TR_ATTR::ALL_BLOCK_PASS:	// 全て壁の地形であった
		pGimmickTerrManager->spawn_window_flg = true;

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);
		if (!pGimmickTerrManager->success_flg)//正解フラグをチェック
		{
			return true;
		}

	}

	break;



	default:


		break;
	}

	return false;                                       // 地形ではなかった場合}
}
//--------------------------------
//  全て壁であるブロックかどうか
//--------------------------------
bool GimmickTerrManager::isHitAll(float x, float y)
{
	switch (getTerrainAttr(x, y))                       // 地形の属性を取得する
	{
	case TR_ATTR::ALL_BLOCK:	// 全て壁の地形であった
		return true;

		break;

	case TR_ATTR::ALL_BLOCK_RED:	// 全て壁の地形であった
		if (pGimmickTerrManager->change_flg)
		{
			return true;
		}

		else
			return false;

		break;

	case TR_ATTR::ALL_BLOCK_BLUE:	 // 全て壁の地形であった

		if (pGimmickTerrManager->change_flg)
		{
			return false;
		}

		else
			return true;

		break;
	case TR_ATTR::ALL_BLOCK_KEY:	// 全て壁の地形であった

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey))
			pGimmickTerrManager->erase_flg_key = true;
	}
	if (!pGimmickTerrManager->erase_flg_key)

		return true;

	break;
	case TR_ATTR::ALL_BLOCK_KEY2:	 // 全て壁の地形であった


	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey2))
			pGimmickTerrManager->erase_flg_key2 = true;
	}

	if (!pGimmickTerrManager->erase_flg_key2)
		return true;

	break;
	case TR_ATTR::ALL_BLOCK_ELASE:	 // 全て壁の地形であった

		if (!pGimmickTerrManager->erase_flg)
			return true;

		break;

	case TR_ATTR::ALL_BLOCK_PASS:	// 全て壁の地形であった

		pGimmickTerrManager->spawn_window_flg = true;


	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);
		if (!pGimmickTerrManager->success_flg)//正解フラグをチェック
		{
			return true;
		}

	}

	break;



	default:


		break;
	}

	switch (getTerrainAttr2(x, y))                       // 地形の属性を取得する
	{
	case TR_ATTR::ALL_BLOCK:	// 全て壁の地形であった
		return true;

		break;

	case TR_ATTR::ALL_BLOCK_RED:	// 全て壁の地形であった
		if (pGimmickTerrManager->change_flg)
		{
			return true;
		}

		else
			return false;

		break;

	case TR_ATTR::ALL_BLOCK_BLUE:	 // 全て壁の地形であった

		if (pGimmickTerrManager->change_flg)
		{
			return false;
		}

		else
			return true;

		break;
	case TR_ATTR::ALL_BLOCK_KEY:	// 全て壁の地形であった

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey))
			pGimmickTerrManager->erase_flg_key = true;
	}
	if (!pGimmickTerrManager->erase_flg_key)

		return true;

	break;
	case TR_ATTR::ALL_BLOCK_KEY2:	 // 全て壁の地形であった


	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);

		if (Flag_Check(pl->iWork[player::iWork::actflg], player::ActFlg::plIsKey2))
			pGimmickTerrManager->erase_flg_key2 = true;
	}

	if (!pGimmickTerrManager->erase_flg_key2)
		return true;

	break;
	case TR_ATTR::ALL_BLOCK_ELASE:	 // 全て壁の地形であった

		if (!pGimmickTerrManager->erase_flg)
			return true;

		break;

	case TR_ATTR::ALL_BLOCK_PASS:	// 全て壁の地形であった
		pGimmickTerrManager->spawn_window_flg = true;

	{
		OBJ2D* pl = pPlayerManager->getOBJ2D(0);
		if (!pGimmickTerrManager->success_flg)//正解フラグを立てる
		{
			return true;
		}

	}

	break;



	default:


		break;
	}

	return false;                                       // 地形ではなかった場合}

}

//--------------------------------
//  下方向に壁にめり込んでいるかどうかを判定
//--------------------------------
bool GimmickTerrManager::isFloor(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitDown(x - width, y)) return true;       // 左端から
		if (isHitDown(x + width, y)) return true;       // 右端から
	}
	return isHitDown(x, y);                             // 最後に真ん中で判定
}

//--------------------------------
//  下方向にめり込んでいた場合、y座標を修正する
//--------------------------------
void GimmickTerrManager::mapHoseiDown(OBJ2D* obj)
{
	float y = obj->position.y;                          // わかりやすく書くためいったんyに代入
	y -= wrap(y, 0.0f, static_cast<float>(CHIP_SIZE));  // 0.0fからCHIP_SIZEまでの間をラップアラウンドさせる
	obj->position.y = y - ADJUST_Y;                     // 少し浮かせる
	obj->speed.y = (std::min)(obj->speed.y, 0.0f);      // 地面にあたったので速度が止まる
}


//--------------------------------
//  天井にあたっているか
//--------------------------------
bool GimmickTerrManager::isCeiling(float x, float y, float width)
{
	for (; width > 0; width -= CHIP_SIZE)               // widthをCHIP_SIZE分減らしていく
	{
		if (isHitAll(x - width, y)) return true;        // 左端から
		if (isHitAll(x + width, y)) return true;        // 右端から
	}
	return isHitAll(x, y);                              // 最後に真ん中で判定
}

//--------------------------------
//  上方向補正処理
//--------------------------------
void GimmickTerrManager::mapHoseiUp(OBJ2D* obj)
{
	float y = obj->position.y - obj->size.y;
	y -= wrap(y, static_cast<float>(-CHIP_SIZE), 0.0f);
	obj->position.y = y + obj->size.y;
	if (obj->speed.y < 0)
		obj->speed.y = 0.0f;                    // 天井にあたったので止まる
}

//--------------------------------
//  横方向に壁にあたっているかどうか
//--------------------------------
bool GimmickTerrManager::isWall(float x, float y, float height)
{
	for (; height > 0; height -= CHIP_SIZE) {
		if (isHitAll(x, y - height))
			return true;
	}
	return isHitAll(x, y);
}

//--------------------------------
//  右方向補正処理
//--------------------------------
void GimmickTerrManager::mapHoseiRight(OBJ2D* obj)
{
	float x = obj->position.x + obj->size.x;
	x -= wrap(x, 0.0f, static_cast<float>(CHIP_SIZE));
	obj->position.x = x - obj->size.x - ADJUST_X;
	obj->speed.x = 0.0f;
}

//--------------------------------
//  左方向補正処理
//--------------------------------
void GimmickTerrManager::mapHoseiLeft(OBJ2D* obj)
{
	float x = obj->position.x - obj->size.x;
	x -= wrap(x, static_cast<float>(-CHIP_SIZE), 0.0f);
	obj->position.x = x + obj->size.x + ADJUST_X;
	obj->speed.x = 0.0f;
}

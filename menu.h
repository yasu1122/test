#ifndef  _INCLUDE_MENU_H
#define  _INCLUDE_MENU_H
//******************************************************************************
//
//
//      menu.h
//
//
//******************************************************************************

using namespace GameLib;

//-------------------<定数>-----------------------------------------------------
namespace menu
{
    enum menuLabel {
        naturalflg  =(0<<0),
        viewFlg     =(1<<0),
        storageFlg  =(1<<1),
        okiniiriFlg =(1<<2),
        disEnableBk =(1<<3),
        setFavoflg  =(1<<4),
        pushOn      =(1<<5),
    };

    // iWorkのラベル
    enum iWork {
        actFlg,
        storage,
        doorLink,
        cnt,
    };

    // fWorkのラベル
    enum fWork {
        Pos,
        Angle,
    };

    // ipWorkのラベル
    enum ipWork {
        actFlgPointa,
    };

}


#define MENU_MAX     (20)

//==============================================================================
//
//      Menu管理関数
//
//==============================================================================
void favoButton(OBJ2D* obj);
void okiniiriListTab(OBJ2D* obj);
void returnTitleScene(OBJ2D* obj);
void disenable(OBJ2D* obj);

//==============================================================================
//
//      Menuクラス
//
//==============================================================================

class Menu : public OBJ2DManager, public Singleton<Menu>
{
public:
    static Text*  menuText;        // 部屋名テキスト

public:
    ~Menu();

    void init();    // 初期化
    void sceneStartInit();

    void update();  // 更新
    void update2(); // ゲーム開始演出用

    void draw();    // 描画

    bool getPause() { return menuPause; }
    void setPause(bool flg) { menuPause = flg; }
    bool getReturnTitleFlg() { return returnTitleFlg; }
    void setReturnTitleFlg(bool flg) { returnTitleFlg = flg; }
    bool getSetFavoFlg() { return setFavoFlg; }
    void setSetFavoFlg(bool flg) { setFavoFlg = flg; }
    bool getSetFavoMenu() { return setFavoMenu; }
    void setSetFavoMenu(bool flg) { setFavoMenu = flg; }

private:
    int getSize() { return MENU_MAX; }
    bool menuPause;
    bool returnTitleFlg;
    bool setFavoFlg;
    bool setFavoMenu;

};

//------< インスタンス取得 >----------------------------------------------------

#define pMenuManager  (Menu::getInstance())

//******************************************************************************


#endif  //_INCLUDE_MENU_H
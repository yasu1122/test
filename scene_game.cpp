//******************************************************************************
//
//
//      ゲーム
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"
#define HIT_STOP_FRAME 8
using namespace GameLib;


//--------------------------------
//  初期化処理
//--------------------------------
void Game::init()
{
    Scene::init();	    // 基底クラスのinitを呼ぶ
	//speeach_num = 0;
	state = 0;
    isPaused = false;   // ポーズフラグの初期化

}

//--------------------------------
//  更新処理
//--------------------------------
void Game::update()
{
    using namespace input;

    // ソフトリセット
    if ((STATE(0) & PAD_START) &&  // 0コンのセレクトボタンが押されている状態で
        STAGE->getReturnTitleFlg())       // 0コンのスタートボタンが押された瞬間
    {
        changeScene(SCENE_TITLE);   // タイトルシーンに切り替える
        return;
    }

    // デバッグ文字列表示
    switch (state)
    {
    case 0:
        //////// 初期設定 ////////
		music::load(BGM_GAME, L"./Data/Musics/main.wav", 0.3f);
		music::play(BGM_GAME,true);
        timer = 0;
        GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
        // テクスチャの読み込み
        texture::load(loadTexture);
		STAGE->init();
		
		pMessageWinManager->init();

        pBlackoutManager->init();       // ブラックアウト

		// updateのstate0を通さないとdrawが上手く動作しないため
		//STAGE->update();


        state++;    // 初期化処理の終了
    // break;      // 意図的なコメントアウト

    case 1:
        //////// 通常時の処理 ////////
		if (TRG(0) & PAD_START)//クリック音
			sound::play(SE_CLICK);


        timer++;
		
		pMessageWinManager->update();

        pBlackoutManager->update();         // ブラックアウト

		if (isPaused)
			break;

		STAGE->update();

		if (flagFunc::Flag_Check(pGoalManager->getOBJ2D(0)->iWork[goal::iWork::goal], goal::Flg::isGoal))
		{
			SCENE_GAME->changeScene(SCENE_RESULT);
		}

		if (!pPlayerManager->getOBJ2D(0)->mover) 
			SCENE_GAME->changeScene(SCENE_GAMEOVER);


        //if (TRG(0) & PAD_START&&pMenuManager->getReturnTitleFlg()) {
        //    SCENE_GAME->changeScene(SCENE_TITLE);
        //}

        break;

    }

	
}

//--------------------------------
//  描画処理
//--------------------------------
void Game::draw()
{
	// 画面クリア
	GameLib::clear(VECTOR4(0, 0, 0, 1));


	STAGE->draw();
	pMessageWinManager->draw_not_scroll();

    pBlackoutManager->draw();       // ブラックアウト


	//font::textOut(4, "GAME", VECTOR2(60, 60), VECTOR2(2.0f, 2.0f), VECTOR4(1, 1, 0, 1));
}
//--------------------------------
//  終了処理
//--------------------------------
void Game::uninit()
{
    // テクスチャの解放
    texture::releaseAll();
	music::stop(BGM_GAME);
	music::unload(BGM_GAME);
}

//******************************************************************************

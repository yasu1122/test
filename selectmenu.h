#ifndef  _INCLUDE_SELECT_MENU_H
#define  _INCLUDE_SELECT_MENU_H
//******************************************************************************
//
//
//      selectMenu.h
//
//
//******************************************************************************

using namespace GameLib;

//-------------------<定数>-----------------------------------------------------

namespace select_menu
{
    // flgnumber
    enum menuLabel {
        naturalflg = (0 << 0),
        pushOn = (1 << 0),
    };

    // iWorkのラベル
    enum iWork {
        actFlg,
    };
};

#define SELECT_MENU_MAX     (7)

//==============================================================================
//
//      SelectMenuクラス
//
//==============================================================================

class SelectMenu : public OBJ2DManager, public Singleton<SelectMenu>
{
public:

    void init();    // 初期化
    void update();  // 更　新
    void draw();    // 描　画

    void setStage1Flg(bool Flg) { stage1Flg = Flg; }
    bool getStage1Flg() { return stage1Flg; }
    void setStage2Flg(bool Flg) { stage2Flg = Flg; }
    bool getStage2Flg() { return stage2Flg; }
    void setStage3Flg(bool Flg) { stage3Flg = Flg; }
    bool getStage3Flg() { return stage3Flg; }
    void setStage4Flg(bool Flg) { stage4Flg = Flg; }
    bool getStage4Flg() { return stage4Flg; }
    void setStage5Flg(bool Flg) { stage5Flg = Flg; }
    bool getStage5Flg() { return stage5Flg; }
    void setTutorialFlg(bool Flg) { tutorialFlg = Flg; }
    bool getTutorialFlg() { return tutorialFlg; }
    void setTitleFlg(bool Flg) { titleFlg = Flg; }
    bool getTitleFlg() { return titleFlg; }

private:

    bool stage1Flg;
    bool stage2Flg;
    bool stage3Flg;
    bool stage4Flg;
    bool stage5Flg;
    bool tutorialFlg;
    bool titleFlg;
    int  getSize() { return SELECT_MENU_MAX; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pSelectMenu  (SelectMenu::getInstance())

//******************************************************************************
#endif //! _INCLUDE_SELECT_MENU_H

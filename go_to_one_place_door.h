#ifndef  _INCLUDE_ONE_PLACE_DOOR
#define  _INCLUDE_ONE_PLACE_DOOR
//******************************************************************************
//
//
//      go_to_one_place_door.h
//
//
//******************************************************************************

//-------------------<定数>-----------------------------------------------------
#define ONE_PLACE_DOOR_MAX            (10)
#define ONE_PLACE_DOOR_DATA_MAX       (10)
#define ONE_PLACE_DOOR_LINK_MAX       (6)


namespace one_place_door
{
    enum changeFlg {
        doorChange = (1 << 0),
    };

    enum iWork {
        doorCnt,
        doorChangeflg,
        doorLink,
        doorURL,
    };

};


//==============================================================================
//
//		DoorManagerクラス
//
//==============================================================================
class OnePlaceDoorManager : public OBJ2DManager, public LinkSet, public Singleton<OnePlaceDoorManager>
{
public:
    void init();    // 初期化
    void update();  // 更新
    void draw();    // 描画

    void linkSearchSet(int link, VECTOR2 linkPos);
    
    int doorStageNo;                                    // ステージナンバー

    //DoorURL* getDoorURL(int num) { return &doorURL[num]; }

private:

    //void doorURLClear(DoorURL* doorURL);
    int getSize() { return ONE_PLACE_DOOR_MAX; }
    int getDoorURLSize() { return ONE_PLACE_DOOR_DATA_MAX; }


};
void doorMove(OBJ2D* obj);


//------< インスタンス取得 >-----------------------------------------------------
#define pOnePlaceDoorManager  (OnePlaceDoorManager::getInstance())

//==============================================================================

#endif //!_INCLUDE_ONE_PLACE_DOOR
#ifndef  _INCLUDE_RESULT_MENU_H
#define  _INCLUDE_RESULT_MENU_H
//******************************************************************************
//
//
//      resultMenu.h
//
//
//******************************************************************************

using namespace GameLib;

//-------------------<定数>-----------------------------------------------------

namespace result_menu
{
    // flgnumber
    enum menuLabel {
        naturalflg  =(0<<0),
        pushOn      =(1<<0),
    };

    // iWorkのラベル
    enum iWork {
        actFlg ,
    };
};

#define RESULT_MENU_MAX     (100)

//==============================================================================
//
//      ResultMenuクラス
//
//==============================================================================

class ResultMenu : public OBJ2DManager, public Singleton<ResultMenu>
{
public:

    void init();    // 初期化
    void update();  // 更　新
    void draw();    // 描　画

    void setStartFlg(bool Flg) { StartFlg = Flg; }
    bool getStartFlg() { return StartFlg; }
    void setExitFlg(bool Flg) { ExitFlg = Flg; }
    bool getExitFlg() { return ExitFlg; }
	
private:

    bool StartFlg;
    bool ExitFlg;
    int getSize() { return RESULT_MENU_MAX; }

};

//------< インスタンス取得 >-----------------------------------------------------
#define pResultMenu  (ResultMenu::getInstance())


//******************************************************************************
#endif //! _INCLUDE_Result_MENU_H


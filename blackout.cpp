//******************************************************************************
//
//
//      ブラックアウトクラス
//
//
//******************************************************************************

//------< インクルード >--------------------------------------------------------
#include"all.h"


using namespace GameLib::system;
//------------------------------------------
//  消去アルゴリズム
//------------------------------------------
bool blackout_erase(OBJ2D* obj)
{
    using namespace flagFunc;

    if (obj->flg)
    {
        return true;
    }
    return false;
}

//左から
void blackout_move00(OBJ2D*obj)
{

	switch (obj->state)
	{
	case 0:
		obj->flg = false;
		obj->scale.x = 0;
		obj->scale.y = SCREEN_HEIGHT;
		obj->data = &color_black;
		obj->state++;
		//break;
	case 1:
		
		obj->scale.x+=10;
		//終了条件
		if (obj->scale.x >= (SCREEN_WIDTH + 100)) obj->state++;

		break;

	case 2:

		obj->flg = true;

		break;

	}
}


//右から
void blackout_move01(OBJ2D*obj)
{


	switch (obj->state)
	{
	case 0:
		obj->flg = false;
		obj->scale.x = 0;
		obj->scale.y = SCREEN_HEIGHT;
		obj->data = &color_black;
		obj->state++;
		//break;
	case 1:

		obj->scale.x -= 10;
		//終了条件
		if (obj->scale.x <= -SCREEN_WIDTH)
			obj->state++;

		break;

	case 2:

		obj->flg = true;
		break;

	}
}

//上から
void blackout_move02(OBJ2D*obj)
{

	switch (obj->state)
	{
	case 0:
		obj->flg = false;
		obj->scale.x = SCREEN_WIDTH;
		obj->scale.y = 0;
		obj->data = &color_black;
        obj->eraser = blackout_erase;
		obj->state++;
		//break;
	case 1:
		obj->scale.y += 10;
		//終了条件
		if (obj->scale.y >= SCREEN_HEIGHT)
        {
            obj->cnt = 30;
            obj->state++;
        }
        break;

    case 2:
        if (obj->cnt <= 0) {
            obj->cnt = 0;
            obj->scale.y = -SCREEN_HEIGHT;
            obj->position.y = SCREEN_HEIGHT;
            obj->state++;
        }

        obj->cnt--;

        break;

    case 3:
        obj->scale.y += 10.0f;

        if (obj->scale.y >= 0)
        {
            obj->flg = true;
        }

        break;

    }


}


//下から
void blackout_move03(OBJ2D*obj)
{

	switch (obj->state)
	{
	case 0:
		obj->flg = false;
		obj->scale.x = SCREEN_WIDTH;
		obj->scale.y = 0.0f;
		obj->data = &color_black;
		obj->state++;
		//break;
	case 1:

		obj->scale.y -= 10.0f;
		//終了条件
        if (obj->scale.y <= -SCREEN_HEIGHT)
        {
            obj->cnt = 10;
            obj->state++;
        }
		break;

	case 2:
        if (obj->cnt <= 0) {
            obj->cnt = 0;
            obj->state++;
        }

        obj->cnt--;

		break;

    case 3:
        obj->scale.y += 10.0f;
        obj->position.y += 10.0f;

        if (obj->scale.y >= 0)
        {
            obj->flg = true;
        }

        break;

	}


}

// 徐々に暗く
void blackout_move04(OBJ2D*obj)
{
    switch (obj->state)
    {
    case 0:
        obj->flg = false;
        obj->scale.x = SCREEN_WIDTH;
        obj->scale.y = SCREEN_HEIGHT;
        obj->data = &color_black;
        obj->eraser = blackout_erase;
        obj->color.w = 0.0f;
        obj->state++;

        //break;
    case 1:
        obj->color.w += 0.1f;
        //終了条件
        if (obj->color.w >= 1.0f)
        {
            obj->cnt = 10;
            obj->state++;
        }
        break;

    case 2:
        if (obj->cnt <= 0) {
            obj->cnt = 0;
            obj->state++;
        }

        obj->cnt--;

        break;

    case 3:
        obj->color.w -= 0.1f;

        if (obj->color.w <= 0.0f)
        {
            obj->color.w = 0.0f;
            obj->flg = true;

			
        }

        break;

    }
}

// 徐々に暗く
void blackout_move05(OBJ2D*obj)
{
    switch (obj->state)
    {
    case 0:
        obj->flg = false;
        obj->scale.x = SCREEN_WIDTH;
        obj->scale.y = SCREEN_HEIGHT;
        obj->data = &color_black;
        obj->eraser = blackout_erase;
        obj->color.w = 0.0f;
        obj->state++;
        //break;
    case 1:
        obj->color.w += 0.1f;
        //終了条件
        if (obj->color.w >= 1.0f)
        {
            obj->cnt = 10;
            obj->state++;
        }
        break;

    case 2:
        // 意図的に何もしていない

        break;

    }
}

// 初期化
void BlackoutManager::init()
{
	OBJ2DManager::init();
}

// 更新処理
void BlackoutManager::update()
{
	OBJ2DManager::update();
}

// 描画処理
void BlackoutManager::draw()
{

	OBJ2DManager::draw_not_scroll();

}

//******************************************************************************
//
//
//      各部屋のテキスト
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//------<変　数>----------------------------------------------------------------

namespace TextData {
    // テキストデータ
    wchar_t* textData[] = {
        L"ポータル",
        L"１の部屋",
        L"２の部屋",
        L"３の部屋",
        L"４の部屋",
        L"５の部屋",
        L"６の部屋",
        L"７の部屋",
        L"８の部屋",
        nullptr,
    };

    // データ配列
    wchar_t** textDataArray[]
    {
        textData,
        textData,
        textData,
        textData,
        textData,
        textData,
    };
};

// 更新処理
void roomTextMove(OBJ2D* obj)
{
    using namespace input;  // 関数内で入力処理を行うときに記述する

    switch (obj->state)
    {
    case 0:
        obj->size = VECTOR2((float)(system::SCREEN_WIDTH/2),(float)(system::SCREEN_HEIGHT/2));

        obj->state++;
        // break;
    case 1:
        // 意図的に何の処理もしていない

        break;
    }

}

// 初期化
void RoomText::init() {
    OBJ2DManager::init();   // OBJ2DManagerの初期化

    STAGE_SCRIPT*pScript = STAGE->getStageScript();

    // 部屋のテキストデータの読み込み
    char** mapRoomText = new char*[pBGManager->CHIP_NUM_Y];
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapRoomText[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapRoomText[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

    if (!pBGManager->loadMapData(pScript->fileNameRoomName, mapRoomText))
    {
        assert(!"部屋のテキストデータ読み込み失敗");
    }

    // 部屋のテキストデータの配置
    for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
        {
            const int doorIndex = mapRoomText[y][x];
            if (doorIndex < 0) continue;

            // 生成
            OBJ2D* pText = searchSet(&roomTextMove, VECTOR2(static_cast<float>((x*BG::CHIP_SIZE + BG::CHIP_SIZE / 2)+32.0f), static_cast<float>(y*BG::CHIP_SIZE + BG::CHIP_SIZE)));
            pText->text = TextData::textDataArray[STAGE->getStageNo()][mapRoomText[y][x]];
			pText->iWork[roomNum] = doorIndex;

        }
    }


    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapRoomText[i];
    }
    delete[] mapRoomText;
};

// 更新
void RoomText::update()
{
    OBJ2DManager::update();
}

//******************************************************************************
//
//
//      扉クラス
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

#include <string>
#include <bitset>


using namespace GameLib;

//******************************************************************************
//
//      扉による処理関連
//
//******************************************************************************

//-------< 扉URLクラス >--------------------------------------------------------

//-------< 扉クラス >-----------------------------------------------------------

// 扉の更新処理
void doorMove(OBJ2D* obj)
{
	using namespace input;  // 関数内で入力処理を行うときに記述する

	// 変数
//    AnimeData* animeData = nullptr;

	switch (obj->state)
	{
	case 0:
		obj->size = VECTOR2(64.0f, 64.0f);
        obj->data = &spr_door00;

		obj->state++;
		// break;
	case 1:


		break;
	}


}


//==============================================================================
//
//		DoorManagerクラス
//
//==============================================================================

//--------------------------------
//      初期化
//--------------------------------
void DoorManager::init()
{

    OBJ2DManager::init();   // OBJ2DManagerの初期化
    doorURL.resize(DoorManager::getDoorURLSize());

    for (auto & pDoorURL : doorURL)
    {
        linkClear(&pDoorURL);
    }

	STAGE_SCRIPT*pScript = STAGE->getStageScript();

    // 扉データの読み込み
    char** mapDoor = new char*[pBGManager->CHIP_NUM_Y];
    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i) {
        mapDoor[i] = new char[pBGManager->CHIP_NUM_X];
        SecureZeroMemory(mapDoor[i], sizeof(char)*pBGManager->CHIP_NUM_X);
    }

    if (!pBGManager->loadMapData(pScript->fileNameDoor, mapDoor))
    {
        assert(!"扉データ読み込み失敗");
    }

    // 扉配置
    for (int y = 0; y < pBGManager->CHIP_NUM_Y; y++)
    {
        for (int x = 0; x < pBGManager->CHIP_NUM_X; x++)
        {
            const int doorIndex = mapDoor[y][x];
            if (doorIndex < 0) continue;

            // 生成
            OBJ2D* pDoor = searchSet(&doorMove, VECTOR2(static_cast<float>(x*BG::CHIP_SIZE + BG::CHIP_SIZE / 2), static_cast<float>(y*BG::CHIP_SIZE + BG::CHIP_SIZE)));
            pDoor->iWork[door::iWork::doorLink] = mapDoor[y][x];
            pDoorManager->linkSet(mapDoor[y][x], pDoor->position);

        }
    }


    for (int i = 0; i < pBGManager->CHIP_NUM_Y; ++i)
    {
        delete[] mapDoor[i];
    }
    delete[] mapDoor;

}


//--------------------------------
//      更新処理
//--------------------------------
void DoorManager::update()
{
    OBJ2DManager::update(); // OBJ2DManagerの更新
}


//--------------------------------
//      描画処理
//--------------------------------
void DoorManager::draw()
{
    OBJ2DManager::draw();   // OBJ2DManagerの描画

    /*for (auto p = pDoorManager->begin(); p < pDoorManager->end(); ++p)
    {
        primitive::rect(VECTOR2(p->position.x, p->position.y) - pBGManager->getScrollPos(), VECTOR2(p->size.x+p->size.x, p->size.y+p->size.y), VECTOR2(p->size.x, p->size.y), 0.0f, VECTOR4(1, 0, 0, 1));
    }*/
}


//******************************************************************************
//
//
//      セレクト画面
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;

void Select::init()
{
    Scene::init();	    // 基底クラスのinitを呼ぶ
}

//--------------------------------
//  更新処理
//--------------------------------
void Select::update()
{
    using namespace input;
    using namespace flagFunc;

    switch (state)
    {
    case 0:
        //////// 初期設定 ////////
        //GameLib::window::SetWindowPosTop();

        timer = 0;  // タイマーを初期化
        GameLib::setBlendMode(Blender::BS_ALPHA);   // 通常のアルファ処理
        texture::load(loadTexture);    // テクスチャの読み込み
		music::load(BGM_SELECT, L"./Data/Musics/portal.wav", 0.3f);
		music::play(BGM_SELECT,true);

        pMousePointa->init(0);
        pSelectMenu->init();

        state++;    // 初期化処理の終了
                    //break;    // 意図的なコメントアウト

    case 1:
        //////// 通常時の処理 ////////
		if (TRG(0) & PAD_START)//クリック音
			sound::play(SE_CLICK);


        timer++;    // タイマーを足す


        pMousePointa->update();
        pSelectMenu->update();

        pJudgeSceneSelectManager->update();

        if (TRG(0) & PAD_START&&pSelectMenu->getStage1Flg()) {
            
            changeScene(SCENE_START);  // ゲームシーンに切り替え
            break;
        }
        if (TRG(0) & PAD_START&&pSelectMenu->getStage2Flg()) {

            changeScene(SCENE_START);  // ゲームシーンに切り替え
            break;
        }
        if (TRG(0) & PAD_START&&pSelectMenu->getStage3Flg()) {

            changeScene(SCENE_START);  // ゲームシーンに切り替え
            break;
        }
        if (TRG(0) & PAD_START&&pSelectMenu->getStage4Flg()) {

            changeScene(SCENE_START);  // ゲームシーンに切り替え
            break;
        }
        if (TRG(0) & PAD_START&&pSelectMenu->getStage5Flg()) {

            changeScene(SCENE_START);  // ゲームシーンに切り替え
            break;
        }
        if (TRG(0) & PAD_START&&pSelectMenu->getTutorialFlg()) {
            changeScene(SCENE_START);  // チュートリアルに切り替え
            break;
        }
        if (TRG(0) & PAD_START&&pSelectMenu->getTitleFlg()) {
            changeScene(SCENE_TITLE);  // タイトルへ戻る
            break;
        }
        

        break;
    }
}

//--------------------------------
//  描画処理
//--------------------------------
void Select::draw()
{
    // 画面クリア
    GameLib::clear(VECTOR4(0.0f, 0.0f, 0.0f, 1));

    spr_select.draw(VECTOR2(system::SCREEN_WIDTH *0.5f, system::SCREEN_HEIGHT * 0.5f));

    pSelectMenu->draw();

    pMousePointa->draw();

}

//******************************************************************************
void Select::uninit()
{

    // テクスチャの解放
    texture::releaseAll();
  
	music::stop(BGM_SELECT);
	music::unload(BGM_SELECT);

}

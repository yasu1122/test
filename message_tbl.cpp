#include "all.h"


// 部屋ごとに難易度ごとのメッセージを用意する

//===== <チュートリアルメッセージ> ======================================================================//
namespace {
	wchar_t* msgTutoRoom0[] =
	{
		L"右上の★ボタンを押すと現在の位置を”ブックマーク”できる",
		L"右クリックでメニューを開けば、選択したブックマーク位置に飛ぶことができる",
		NULL,
	};
	wchar_t* msgTutoRoom1[] =
	{
		L"緑のＵＲＬを取得すればウイルスの攻撃を一度だけしのぐことができる",
		NULL,
	};
	wchar_t* msgTutoRoom2[] =
	{
		L"シャッターはスイッチを押している間しか開かない",
		NULL,
	};
	wchar_t* msgTutoRoom3[] =
	{
		L"ウイルスに触れるとダメージを受けてしまうえにホームに戻されてしまう",
		NULL,
	};
	wchar_t* msgTutoRoom4[] =
	{
		NULL,
	};
	wchar_t* msgTutoRoom5[] =
	{
		NULL,
	};
	wchar_t* msgTutoRoom6[] =
	{
		NULL,
	};
	wchar_t* msgTutoRoom7[] =
	{
		NULL,
	};
	wchar_t* msgTutoRoom8[] =
	{
		NULL,
	};
	wchar_t* msgTutoRoom9[] =
	{
		NULL,
	};
	wchar_t* msgTutoRoom10[] =
	{
		NULL,
	};
	wchar_t* msgTutoRoom11[] =
	{
		NULL,
	};
}

// 第二引数と第三引数はキャラデータなどからconst wchar_t*型のデータを持ってくる想定
MSG_TBL2 msgTuto_tbl[] =
{
	{ MSG_ROOM0, msgTutoRoom0, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 100) },
	{ MSG_ROOM1, msgTutoRoom1, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM2, msgTutoRoom2, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 170) },
	{ MSG_ROOM3, msgTutoRoom3, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM4, msgTutoRoom4, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM5, msgTutoRoom5, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM6, msgTutoRoom6, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM7, msgTutoRoom7, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 - 100) },
	{ MSG_ROOM8, msgTutoRoom8, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM9, msgTutoRoom9, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM10, msgTutoRoom10, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM11, msgTutoRoom11, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },

	{ MSG_NONE, NULL },
};
//===== <チュートリアルメッセージ> ======================================================================//


//===== <イージーメッセージ> ============================================================================//
namespace
{
	wchar_t* msgEasyRoom0[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom1[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom2[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom3[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom4[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom5[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom6[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom7[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom8[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom9[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom10[] =
	{
		NULL,
	};
	wchar_t* msgEasyRoom11[] =
	{
		NULL,
	};
}

MSG_TBL2 msgEasy_tbl[] =
{
	{ MSG_ROOM0,  msgEasyRoom0, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 100) },
	{ MSG_ROOM1,  msgEasyRoom1, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM2,  msgEasyRoom2, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 170) },
	{ MSG_ROOM3,  msgEasyRoom3, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM4,  msgEasyRoom4, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM5,  msgEasyRoom5, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM6,  msgEasyRoom6, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM7,  msgEasyRoom7, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 - 100) },
	{ MSG_ROOM8,  msgEasyRoom8, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM9,  msgEasyRoom9, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM10, msgEasyRoom10, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM11, msgEasyRoom11, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },

	{ MSG_NONE, NULL },
};
//===== <イージーメッセージ> ============================================================================//



//===== <ノーマルメッセージ> ============================================================================//
namespace
{
	wchar_t* msgNormRoom0[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom1[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom2[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom3[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom4[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom5[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom6[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom7[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom8[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom9[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom10[] =
	{
		NULL,
	};
	wchar_t* msgNormRoom11[] =
	{
		NULL,
	};
}

MSG_TBL2 msgNorm_tbl[] =
{
	{ MSG_ROOM0,  msgNormRoom0, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 100) },
	{ MSG_ROOM1,  msgNormRoom1, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM2,  msgNormRoom2, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 170) },
	{ MSG_ROOM3,  msgNormRoom3, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM4,  msgNormRoom4, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM5,  msgNormRoom5, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM6,  msgNormRoom6, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM7,  msgNormRoom7, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 - 100) },
	{ MSG_ROOM8,  msgNormRoom8, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM9,  msgNormRoom9, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM10, msgNormRoom10, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM11, msgNormRoom11, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },


	{ MSG_NONE, NULL },
};
//===== <ノーマルメッセージ> ============================================================================//



//===== <ハードメッセージ> ==============================================================================//
namespace {
	wchar_t* msgHardRoom0[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom1[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom2[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom3[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom4[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom5[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom6[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom7[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom8[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom9[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom10[] =
	{
		NULL,
	};
	wchar_t* msgHardRoom11[] =
	{
		NULL,
	};



}

MSG_TBL2 msgHard_tbl[] =
{
	{ MSG_ROOM0, msgHardRoom0, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 100) },
	{ MSG_ROOM1, msgHardRoom1, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM2, msgHardRoom2, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 170) },
	{ MSG_ROOM3, msgHardRoom3, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM4, msgHardRoom4, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM5, msgHardRoom5, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM6, msgHardRoom6, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM7, msgHardRoom7, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 - 100) },
	{ MSG_ROOM8, msgHardRoom8, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM9, msgHardRoom9, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM10, msgHardRoom10, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM11, msgHardRoom11, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },


	{ MSG_NONE, NULL },
};
//===== <ハードメッセージ> ==============================================================================//



//===== <メッセージ> ==============================================================================//
namespace
{
	wchar_t* msgex1Room0[] =
	{
		NULL,
	};
	wchar_t* msgex1Room1[] =
	{
		NULL,
	};
	wchar_t* msgex1Room2[] =
	{
		NULL,
	};
	wchar_t* msgex1Room3[] =
	{
		NULL,
	};
	wchar_t* msgex1Room4[] =
	{
		NULL,
	};
	wchar_t* msgex1Room5[] =
	{
		NULL,
	};
	wchar_t* msgex1Room6[] =
	{
		NULL,
	};
	wchar_t* msgex1Room7[] =
	{
		NULL,
	};
	wchar_t* msgex1Room8[] =
	{
		NULL,
	};
	wchar_t* msgex1Room9[] =
	{
		NULL,
	};
	wchar_t* msgex1Room10[] =
	{
		NULL,
	};
	wchar_t* msgex1Room11[] =
	{
		NULL,
	};
}

MSG_TBL2 msgex1_tbl[] =
{
	{ MSG_ROOM0, msgex1Room0, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 100) },
	{ MSG_ROOM1, msgex1Room1, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM2, msgex1Room2, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 170) },
	{ MSG_ROOM3, msgex1Room3, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM4, msgex1Room4, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM5, msgex1Room5, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM6, msgex1Room6, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM7, msgex1Room7, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 - 100) },
	{ MSG_ROOM8, msgex1Room8, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM9, msgex1Room9, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM10, msgex1Room10, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM11, msgex1Room11, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },


	{ MSG_NONE, NULL },
};
//===== <メッセージ> ==============================================================================//

//===== <メッセージ> ==============================================================================//
namespace
{
	wchar_t* msgex2Room0[] =
	{
		NULL,
	};
	wchar_t* msgex2Room1[] =
	{
		L"このページではブックマークすると自身の分身を生み出すことができる",
		NULL,
	};
	wchar_t* msgex2Room2[] =
	{
		NULL,
	};
	wchar_t* msgex2Room3[] =
	{
		NULL,
	};
	wchar_t* msgex2Room4[] =
	{
		NULL,
	};
	wchar_t* msgex2Room5[] =
	{
		NULL,
	};
	wchar_t* msgex2Room6[] =
	{
		NULL,
	};
	wchar_t* msgex2Room7[] =
	{
		NULL,
	};
	wchar_t* msgex2Room8[] =
	{
		NULL,
	};
	wchar_t* msgex2Room9[] =
	{
		NULL,
	};
	wchar_t* msgex2Room10[] =
	{
		NULL,
	};
	wchar_t* msgex2Room11[] =
	{
		NULL,
	};
}

MSG_TBL2 msgex2_tbl[] =
{
	{ MSG_ROOM0, msgex2Room0, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 100) },
	{ MSG_ROOM1, msgex2Room1, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM2, msgex2Room2, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 170) },
	{ MSG_ROOM3, msgex2Room3, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM4, msgex2Room4, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM5, msgex2Room5, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM6, msgex2Room6, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2) },
	{ MSG_ROOM7, msgex2Room7, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 - 100) },
	{ MSG_ROOM8, msgex2Room8, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM9, msgex2Room9, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM10, msgex2Room10, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },
	{ MSG_ROOM11, msgex2Room11, VECTOR2(system::SCREEN_WIDTH / 2 + 400, system::SCREEN_HEIGHT / 2 + 150) },


	{ MSG_NONE, NULL },
};
//===== <メッセージ> ==============================================================================//


MSG_TBL2* msgStage_tbl[] =
{
	msgTuto_tbl,
	msgEasy_tbl,
	msgex1_tbl,   
	msgNorm_tbl,
	msgHard_tbl,
	msgex2_tbl,   
};

//// 発言者の名前
//const wchar_t* speeaker0[] =
//{
//	L"チュートリアル",
//	NULL,
//};




#ifndef INCLUDED_SCENE
#define INCLUDED_SCENE

//******************************************************************************
//
//
//      シーン
//
//
//******************************************************************************

//==============================================================================
//
//      Sceneクラス
//
//==============================================================================
class Scene
{
protected:
	int stop_cnt;
    int state;          // 状態
    int timer;          // タイマー
    bool end_flg;

    Scene* nextScene;   // 次のシーン

public:
	bool stop_fg;

    Scene* execute();   // 実行処理

    virtual void init()
    { // 初期化処理
        state = 0;
        timer = 0;
        nextScene = nullptr;
    };
    virtual void uninit() {};   // 終了処理
    virtual void update() {};   // 更新処理
    virtual void draw()   {};   // 描画処理

    void changeScene(Scene *scene) { nextScene = scene; }   // シーン変更処理
    Scene *getScene() const { return nextScene; }           // nextSceneのゲッター
	void stop_fg_on() { stop_fg = true; return; }
    void GameEnd() { end_flg = true; }

};

//******************************************************************************
//
//
//      シーン管理
//
//
//******************************************************************************

//==============================================================================
//
//      SceneManagerクラス
//
//==============================================================================
class SceneManager
{
public:
    void execute(Scene *);  // 実行処理
};



//******************************************************************************

#endif // !INCLUDED_SCENE

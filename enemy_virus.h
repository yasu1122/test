#pragma once

// ラベル定義
#define VIRUS_MAX					1000


namespace virus
{

	enum ActFlg {
		EraseFlg = (1 << 0),
	};

	// iWorkのラベル
	enum iWork {
		EnemyURL,
		EnemyBlockName,
		ActFlg,
	};

	// fWorkのラベル
	enum fWork {
		weekPositionX,     // 位置
		weekPositionY,
		weekSizeX,         // 位置
		weekSizeY
	};

	enum BlockName
	{
		http_xvideos_com,
		http_pornhub_com
	};

	enum EnemyType
	{
		AD,
		VIRUS,
	};

}



// VirusManagerクラス
class VirusManager : public OBJ2DManager, public LinkSet, public Singleton<VirusManager>
{
public:
	void init();    // 初期化
	void update();  // 更新
	void draw();    // 描画
private:
	int getSize() { return VIRUS_MAX; }
	int getEnmJumpSize() { return DOOR_DATA_MAX; }

};

#define pVirusManager	(VirusManager::getInstance())


#pragma once


enum MESSAGE_HAVED
{
	MSG_NONE = -1,

	MSG_ROOM0 = 0,
	MSG_ROOM1,
	MSG_ROOM2,
	MSG_ROOM3,
	MSG_ROOM4,
	MSG_ROOM5,
	MSG_ROOM6,
	MSG_ROOM7,
	MSG_ROOM8,
	MSG_ROOM9,
	MSG_ROOM10,
	MSG_ROOM11,
};


struct MSG_TBL2
{
	MESSAGE_HAVED messageNum;		// メッセージ管理番号（MESSAGE_HAVED）
	wchar_t** pMessage;				// メッセージ本文（全角文字使用可）
	//const wchar_t** pSpeaker;		// キャラの名前（〃）
	VECTOR2 pos;
	bool read;
};

extern MSG_TBL2 msgTuto_tbl[];
extern MSG_TBL2 msgEasy_tbl[];
extern MSG_TBL2 msgNorm_tbl[];
extern MSG_TBL2 msgHard_tbl[];
extern MSG_TBL2* msgStage_tbl[];
//******************************************************************************
//
//
//      メニュー表示
//
//
//******************************************************************************

//------< インクルード >---------------------------------------------------------
#include "all.h"

using namespace GameLib;
using namespace input;

//------<変　数>----------------------------------------------------------------
Text*  Menu::menuText;

MOVER mouseFuncArray[] =
{
    favoButton,
    okiniiriListTab,
    returnTitleScene,
    disenable,
    nullptr,
};

//------<定　数>----------------------------------------------------------------
#define MENU_POS_X      (1200.0f)
#define MENU_POS_Y      (256.0f)

//--------------------------------
//  menu関数
//--------------------------------
// 表示フラグをオンにする
void adress_storage_on(OBJ2D* obj)
{
    using namespace flagFunc;
    OBJ2D* pl = pPlayerManager->getOBJ2D(0);

    if (Flag_Check(obj->iWork[mouse_pointa::iWork::storage], mouse_pointa::clickLabel::storageFlg)) {

        if (TRG(0)&PAD_SELECT && (STATE(0)&PAD_SELECT) != 0) {
            if (pBookMarkManager->getHitFlg())return;

            for (auto & pMenu : *pMenuManager)
            {
                Flg_On(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg);
            }
        }
    }

    if (!Flag_Check(obj->iWork[mouse_pointa::iWork::storage], mouse_pointa::clickLabel::storageFlg)) {

        if (TRG(0)&PAD_SELECT && (STATE(0)&PAD_SELECT) != 0) {
            for (auto & pMenu : *pMenuManager)
            {
                if (pBookMarkManager->getHitFlg())return;

                Flg_On(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg);

                // プレイヤーの位置情報とスクロール値をマウスポインタに取得
                pMousePointa->setScrollPos(pl->position, pBGManager->getScrollPos());
            }
        }

    }

}

// マウスポインタが当たってなければメニューフラグオフ
void menuFlgOff()
{
    using namespace flagFunc;

    // 各種フラグが立っていたら早期にリターン
    if (pMenuManager->getPause())return;
    if (pDeleteMenuManager->getDeleteMenuViewFlg())return;
    if (pMenuManager->getSetFavoFlg())return;
    if (pMenuManager->getSetFavoMenu())return;

    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);

    

    if (!pMousePointa->getHitFlg()) {
        if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0) {

            for (auto & pMenu : *pMenuManager) {

                All_Flag_Off(pMenu.iWork[menu::iWork::actFlg]);
                Flg_Off(pMouse->iWork[mouse_pointa::iWork::storage], mouse_pointa::clickLabel::storageFlg);
                pMouse->iWork[mouse_pointa::iWork::doorLink] = -1;
            }

            for (auto & pDMenu : *pDeleteMenuManager) {

                All_Flag_Off(pDMenu.iWork[delete_menu::iWork::actFlg]);
                Flg_Off(pMouse->iWork[mouse_pointa::iWork::storage], mouse_pointa::clickLabel::storageFlg);
                pMouse->iWork[mouse_pointa::iWork::doorLink] = -1;
            }
            //pDeleteMenuManager->setDeleteMenuViewFlg(false);
        }
    }
}


// favoボタン
void favoButton(OBJ2D* obj)
{
    using namespace flagFunc;

    if (pMenuManager->getPause())return;
    OBJ2D * pPlayer = pPlayerManager->getOBJ2D(0);

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(64.0f, 64.0f);
        obj->position = VECTOR2(1840.0f, 45.0f);
        obj->iWork[menu::iWork::actFlg] = menu::menuLabel::naturalflg;
        obj->color = VECTOR4(0.8f, 0.8f, 0.8f, 1.0f);
        obj->state++;

        pEffectManager->searchSet(effect_move_wave01, VECTOR2(obj->position.x,obj->position.y));
        pEffectManager->searchSet(effect_move_favo_button, VECTOR2(obj->position.x, obj->position.y));

        // break;

    case 1:
        //--------<通常処理>-----------
        pMenuManager->setSetFavoFlg(Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::pushOn));

        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->scale = VECTOR2(1.0f, 1.0f);
        obj->data = &spr_favo_button00;

        if (pMenuManager->getSetFavoFlg()) {
            obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
            obj->scale = VECTOR2(1.4f, 1.4f);
            obj->data = &spr_favo_button01;

            // プレイヤーの位置情報とスクロール値をマウスポインタに取得
            pMousePointa->setScrollPos(pPlayer->position, pBGManager->getScrollPos());
        }

        break;
    }
}

// favoボタン(メニューの方)
void okiniiriListTab(OBJ2D* obj)
{
    using namespace flagFunc;

    if (pMenuManager->getPause())return;

    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);
    OBJ2D * pPlayer = pPlayerManager->getOBJ2D(0);

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(128.0f, 25.6f);
        obj->position = VECTOR2(MENU_POS_X, MENU_POS_Y);
        obj->data = &spr_favo_buttom;
        obj->iWork[menu::iWork::actFlg] = menu::menuLabel::naturalflg;
        obj->state++;
        // break;

    case 1:
        //--------<通常処理>-----------
        pMenuManager->setSetFavoMenu(Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::pushOn));
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);

        //********************************************************************************
        // 表示フラグが立ってなければ画面外へ座標を移す
        //********************************************************************************
        // HACK:表示フラグがオフの際も当たり判定をとってしまうのでその応急処置
        //********************************************************************************

        if (!Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg)) {
            obj->position.x = pMouse->position.x + 1200.0f;
            obj->position.y = pMouse->position.y;
        }


        if (TRG(0) & PAD_SELECT && (STATE(0) & PAD_SELECT) != 0) {
            if (pBookMarkManager->getHitFlg())return;

            OBJ2D* pDelMenu = pDeleteMenuManager->getOBJ2D(0);
            if (Flag_Check(pDelMenu->iWork[delete_menu::iWork::actFlg], delete_menu::delMenuLabel::viewFlg))
                return;

            obj->position.x = system::SCREEN_WIDTH*0.65f;
            obj->position.y = system::SCREEN_HEIGHT*0.25f;

        }

        //********************************************************************************

        if (pMenuManager->getSetFavoMenu()) {
            obj->color = VECTOR4(0.5f, 0.5f, 0.5f, 1.0f);
            // プレイヤーの位置情報とスクロール値をマウスポインタに取得
            pMousePointa->setScrollPos(pPlayer->position, pBGManager->getScrollPos());
        }

        break;
    }
}

// タイトルへ戻る
void returnTitleScene(OBJ2D* obj)
{
    using namespace flagFunc;

    if (pMenuManager->getPause())return;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------

        obj->size = VECTOR2(128.0f, 70.0f);
        obj->color = VECTOR4(0.5f, 1.0f, 1.0f, 1.0f);
        obj->position = VECTOR2(300.0f, 900.0f);
        obj->scale = VECTOR2(0.6f, 0.6f);
        obj->data = &spr_select_icon_title;
        // break;

    case 1:
        //--------<通常処理>-----------

        // マウスポインタと接触していたらフラグオン
        pMenuManager->setReturnTitleFlg(Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::pushOn));

        STAGE->setReturnTitleFlg(false);                // SCENESTAGE内タイトルへ戻るのフラグをオフ
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
        obj->scale = VECTOR2(0.7f, 0.7f);

        // 
        if (Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg)) {

            if (Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::pushOn))
            {
                obj->color = VECTOR4(0.5f, 0.5f, 0.5f, 1.0f);
                obj->scale = VECTOR2(0.8f, 0.8f);
                STAGE->setReturnTitleFlg(true);             // タイトルへ戻るフラグをオン
            }

        }

        break;
    }

}

// ゲーム開始演出用
void sceneStartMenu(OBJ2D* obj)
{
    using namespace flagFunc;

    if (pMenuManager->getPause())return;

    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        obj->size = VECTOR2(64.0f, 12.8f);
        obj->data = &spr_room_name_home;

        obj->position.x = (pMouse->position.x >= 1152.0f) ? (pMouse->position.x - 96.0f) : (pMouse->position.x + 96.0f);
        obj->position.y = (pMouse->position.y >= 411.8f) ? (pMouse->position.y - 128.0f) : (pMouse->position.y + 128.0f);
        obj->text = L" ";

        obj->iWork[menu::iWork::actFlg] = menu::menuLabel::naturalflg;

        obj->state++;
        // break;

    case 1:
        //--------<通常処理>-----------

        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);

        if (!Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg)) {
            obj->position.x = (pMouse->position.x >= 1152.0f) ? (pMouse->position.x - 96.0f) : (pMouse->position.x + 96.0f);
            obj->position.y = pMouse->position.y;

        }

        if (Flag_Check(obj->iWork[menu::iWork::actFlg], menu::menuLabel::storageFlg)) {
            obj->color = VECTOR4(0.5f, 0.5f, 0.5f, 1.0f);
        }

        break;
    }
}

// 登録不可表示
void disenable(OBJ2D* obj) {
    using namespace flagFunc;

    switch (obj->state)
    {
    case 0:
        //--------<初期設定>-----------
        //obj->size = VECTOR2(128.0f, 64.0f);
        obj->position.x = system::SCREEN_WIDTH / 2.0f;
        obj->position.y = system::SCREEN_HEIGHT / 2.0f;
        obj->text = L" ";

        obj->data = &spr_tourokuhuka00;

        obj->iWork[menu::iWork::actFlg] = menu::menuLabel::naturalflg;
        obj->color = VECTOR4(1.0f, 1.0f, 1.0f, 1.0f);

        obj->state++;

        // break;
    case 1:
        //--------<通常処理>-----------

        if (!pMenuManager->getPause())return;

        if (TRG(0) & PAD_START && (STATE(0) & PAD_START) != 0) {
            pMenuManager->setPause(false);
            Flg_Off(obj->iWork[menu::iWork::actFlg], menu::menuLabel::disEnableBk);
        }

        break;
    }
}

//　デストラクタ
Menu::~Menu()
{
    if (menuText) delete menuText;
}

// 通常初期化
void Menu::init()
{
    if (menuText) delete menuText;

    OBJ2DManager::init();       // OBJ2DManagerの初期化

    menuText = new Text(TEXT2, "./Data/Text/font_2_ch.txt");
    setPause(false);

    for (int i = 0;; ++i)
    {
        if (!mouseFuncArray[i])break;
        searchSet(mouseFuncArray[i], VECTOR2(0.0f, 0.0f));
    }

}

// ゲーム開始演出用
void Menu::sceneStartInit()
{
    if (menuText) delete menuText;

    OBJ2DManager::init();       // OBJ2DManagerの初期化

    menuText = new Text(TEXT2, "./Data/Text/font_2_ch.txt");
    setPause(false);

    obj_w[0].mover = sceneStartMenu;
    obj_w[0].position = VECTOR2(0.0f, 0.0f);

}


// 通常更新処理
void Menu::update()
{
    using namespace flagFunc;

    OBJ2D* pMouse = pMousePointa->getOBJ2D(0);

    adress_storage_on(pMouse);
    menuFlgOff();

    OBJ2DManager::update();   // OBJ2DManagerの更新

    
    
}

// ゲーム開始演出用更新処理
void Menu::update2()
{
    using namespace flagFunc;

    OBJ2DManager::update();   // OBJ2DManagerの更新

}

// 描画処理
void Menu::draw()
{
    using namespace flagFunc;

    for (auto & pMenu : *pMenuManager) {
        if (!pMenu.mover)continue;

        if (pMenu.mover != favoButton&&pMenu.mover!=disenable) {
            if (Flag_Check(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::viewFlg)) {
                if (pMenu.data) {
                    pMenu.data->draw(pMenu.position, pMenu.scale, pMenu.angle, pMenu.color);
                    continue;
                }
            }
        }
        
        if (pMenu.mover == disenable) {
            if (Flag_Check(pMenu.iWork[menu::iWork::actFlg], menu::menuLabel::disEnableBk)) {
                pMenu.data->draw(pMenu.position, pMenu.scale, 0.0f, pMenu.color);
            }
        }
        if (pMenu.mover == favoButton) {
            pMenu.data->draw(pMenu.position, pMenu.scale, 0.0f, pMenu.color);
        }
    }

}
